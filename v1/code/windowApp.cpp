
// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

//  debug only
#include <iostream>

#include "WindowApp.h"

bool32 WindowApp::GetKeyState(int button)//, const unsigned char *buttons = new const unsigned char[1])
{
	//GLFW_PRESS   1
	//The key or mouse button was pressed.
	//GLFW_RELEASE   0
	//The key or mouse button was released.
	//GLFW_REPEAT   2
	//The key was held down until it repeated.
	return glfwGetKey((GLFWwindow*)window, button) == GLFW_PRESS ? true : false;
}



// pass in a list of player's bound input 
// it's fine for now to be static
void WindowApp::InitKeyboardGameKeys(game_buttons *gameKeys)
{
	gameKeys->start = GLFW_KEY_ESCAPE;
	gameKeys->select = GLFW_KEY_F;

	// just an easy switch for testing
#if 0
	gameKeys->up = GLFW_KEY_UP;
	gameKeys->down = GLFW_KEY_DOWN;
	gameKeys->left = GLFW_KEY_LEFT;
	gameKeys->right = GLFW_KEY_RIGHT;
#else 
	gameKeys->up = GLFW_KEY_W;
	gameKeys->down = GLFW_KEY_S;
	gameKeys->left = GLFW_KEY_A;
	gameKeys->right = GLFW_KEY_D;
#endif

	gameKeys->action1 = GLFW_KEY_Q;
	gameKeys->action2 = GLFW_KEY_E;
	gameKeys->action3 = GLFW_KEY_LEFT_SHIFT;
	gameKeys->action4 = GLFW_KEY_P;
}

void WindowApp::InitJoypadGameKeys(game_buttons *gameKeys)
{

	gameKeys->start = 7;
	gameKeys->select = 6;


	gameKeys->up = 10;
	gameKeys->down = 12;
	gameKeys->left = 13;
	gameKeys->right = 11;


	gameKeys->action1 = 0;
	gameKeys->action2 = 1;
	gameKeys->action3 = 2;
	gameKeys->action4 = 3;

}

bool32 WindowApp::IsJoystickPresent()
{
	return glfwJoystickPresent(GLFW_JOYSTICK_1) == GLFW_TRUE ? true : false;
}


void WindowApp::ProcessInput(game_controller *controller, const game_buttons *buttons)
{

	const unsigned char *analogButtons;
	if (controller->isAnalog)
	{
		//int axesCount;
		// all axes range from -1 to 1 
		//const float* axes = glfwGetJoystickAxes(GLFW_JOYSTICK_1, &axesCount);

		/*printf("count: %d\n", count);
		printf("Lstick x axis: %a\n", axes[0]);
		printf("Lstick y axis: %a\n", axes[1]);
		printf("Rstick x axis: %a\n", axes[2]);
		printf("Rstick y axis: %a\n", axes[3]);
		printf("L2: %a\n", axes[4]);
		printf("R2: %a\n", axes[5]);*/

		//int buttonCount;
		//const unsigned char *buttons = glfwGetJoystickButtons(GLFW_JOYSTICK_1, &buttonCount);
		//const char *name = glfwGetJoystickName(GLFW_JOYSTICK_1);

		/*//printf("%d\n", buttonCount);
		printf("%s\n", name);
		if (buttons[0] == GLFW_PRESS) { printf("x      button: pressed\n"); }
		if (buttons[1] == GLFW_PRESS) { printf("o      button: pressed\n"); }
		if (buttons[2] == GLFW_PRESS) { printf("[]     button: pressed\n"); }
		if (buttons[3] == GLFW_PRESS) { printf("tri    button: pressed\n"); }
		if (buttons[4] == GLFW_PRESS) { printf("L1     button: pressed\n"); }
		if (buttons[5] == GLFW_PRESS) { printf("R1     button: pressed\n"); }
		if (buttons[6] == GLFW_PRESS) { printf("select button: pressed\n"); }
		if (buttons[7] == GLFW_PRESS) { printf("start  button: pressed\n"); }
		if (buttons[8] == GLFW_PRESS) { printf("Lclick button: pressed\n"); }
		if (buttons[9] == GLFW_PRESS) { printf("Rclick button: pressed\n"); }
		if (buttons[10] == GLFW_PRESS) { printf("dpad Up   button: pressed\n"); }
		if (buttons[11] == GLFW_PRESS) { printf("dpad Right button: pressed\n"); }
		if (buttons[12] == GLFW_PRESS) { printf("dpad down  button: pressed\n"); }
		if (buttons[13] == GLFW_PRESS) { printf("dpad Left  button: pressed\n"); }*/

		int buttonCount;
		analogButtons = glfwGetJoystickButtons(GLFW_JOYSTICK_1, &buttonCount);
	}



	// menu 
	controller->start.pressed = controller->isAnalog ? analogButtons[buttons->start] : GetKeyState(buttons->start);
	controller->select.pressed = controller->isAnalog ? analogButtons[buttons->select] : GetKeyState(buttons->select);

	// up, down, left, right
	controller->up.pressed = controller->isAnalog ? analogButtons[buttons->up] : GetKeyState(buttons->up);
	controller->down.pressed = controller->isAnalog ? analogButtons[buttons->down] : GetKeyState(buttons->down);
	controller->left.pressed = controller->isAnalog ? analogButtons[buttons->left] : GetKeyState(buttons->left);
	controller->right.pressed = controller->isAnalog ? analogButtons[buttons->right] : GetKeyState(buttons->right);

	// face / action buttons
	controller->action1.pressed = controller->isAnalog ? analogButtons[buttons->action1] : GetKeyState(buttons->action1);
	controller->action2.pressed = controller->isAnalog ? analogButtons[buttons->action2] : GetKeyState(buttons->action2);
	controller->action3.pressed = controller->isAnalog ? analogButtons[buttons->action3] : GetKeyState(buttons->action3);
	controller->action4.pressed = controller->isAnalog ? analogButtons[buttons->action4] : GetKeyState(buttons->action4);
}

bool WindowApp::KeyDown(game_button_state oldController, game_button_state newController)
{
	return ((oldController.pressed && newController.pressed) || (!oldController.pressed && newController.pressed)) ? true : false;
}

bool WindowApp::KeyUp(game_button_state oldController, game_button_state newController)
{
	return (oldController.pressed && !newController.pressed) ? true : false;
}

bool WindowApp::KeyPress(game_button_state oldController, game_button_state newController)
{
	return (!oldController.pressed && newController.pressed) ? true : false;
}


void WindowApp::SetCursorPos(double x, double y)
{
	glfwSetCursorPos((GLFWwindow*)window, x, y);
}

void WindowApp::GetCursorPos(double *x, double *y)
{
	glfwGetCursorPos((GLFWwindow*)window, x, y);
}

void WindowApp::PostWindowCloseMessage(bool32 state)
{
	glfwSetWindowShouldClose((GLFWwindow*)window, state);
}

bool32 WindowApp::WindowShouldClose()
{
	return glfwWindowShouldClose((GLFWwindow*)window);
}

void WindowApp::SetTime(real32 time)
{
	glfwSetTime((real64)time);
}

real32 WindowApp::GetTime()
{
	return (real32)glfwGetTime();
}

void WindowApp::Terminate()
{
	glfwTerminate();
}




// check if the provided window handle is valid and create its OpenGL context


int WindowApp::InitGLEW()
{
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (err != GLEW_OK) {
		std::cout << "Glew Error: -> " << glewGetErrorString(err) << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}
	return 0;
}


WindowApp::WindowApp(int _windowWidth, int _windowHeight)
{
	InitGLFW(3, 3);

	windowWidth = _windowWidth;
	windowHeight = _windowHeight;

	char* windowName = "window";	
	//monitors = glfwGetMonitors(&monitorCount);

	// for fullscreen put this in first null -> glfwGetPrimaryMonitor()
	window = (size_t)glfwCreateWindow(windowWidth, windowHeight, windowName, NULL, NULL);

	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window.\n");
		getchar();
		glfwTerminate();
		exit(1);
	}
	glfwMakeContextCurrent((GLFWwindow*)window);


	// setup glew
	if (InitGLEW() == -1)
	{
		exit(1);
	}

	// Ensure we can capture the input
	glfwSetInputMode((GLFWwindow*)window, GLFW_STICKY_KEYS, GL_TRUE);
	// Hide the mouse and enable unlimited movement
	glfwSetInputMode((GLFWwindow*)window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);


}

void WindowApp::GetScreenCentre(float * halfX, float *halfY)
{
	*halfX = windowWidth * 0.5f;
	*halfY = windowHeight * 0.5f;
}

void WindowApp::SetDoubleBuffering(bool32 state)
{
	glfwWindowHint(GLFW_DOUBLEBUFFER, state);
	doubleBuffered = state;
	
	if (doubleBuffered == false)
	{
		glDrawBuffer(GL_FRONT);
		glReadBuffer(GL_FRONT);
	}
	else
	{
		glDrawBuffer(GL_BACK);
		glReadBuffer(GL_BACK);
	}

}

void WindowApp::SetAASampleRate(int samples)
{
	// can't anti alias without double buffering on.
	glfwWindowHint(GLFW_SAMPLES, samples);
	aaSampleRate = samples;
}


int WindowApp::InitGLFW(int OGLversionMajor, int OGLversionMinor)
{
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		getchar();
		return -1;
	}


	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, OGLversionMajor);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, OGLversionMinor);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_FALSE);

	SetDoubleBuffering(true);
	SetAASampleRate(4);

	glfwWindowHint(GLFW_DEPTH_BITS, 16);
	glfwWindowHint(GLFW_RED_BITS, 8);
	glfwWindowHint(GLFW_GREEN_BITS, 8);
	glfwWindowHint(GLFW_BLUE_BITS, 8);
	glfwWindowHint(GLFW_ALPHA_BITS, 8);

	glfwSwapInterval(1);
	glfwSetTime(0.0);

	return 0;
}

void WindowApp::SwapBuffers()
{
	if (doubleBuffered)
	{
		glfwSwapBuffers((GLFWwindow*)window);
	}
	glfwPollEvents();
}
