#ifndef RENDERER_H
#define RENDERER_H


#include "common.h"

#include <vector>

#define GLFW_OPENGL_PROFILE 1

// Include GLEW
#include <GL/glew.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// these are all assimp files
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>       // Output data structure
#include <assimp/postprocess.h> // Post processing flags

// map data structure
#include <map>



//using namespace glm;
//using the namespace will conflict their int32 with my int32
// better off specifying anyways

// test vars, remove later
const int spawnWidth = 20;
const int spawnHeight = 20;
const int numberOfVertices = 12;

struct Instance
{
	glm::vec3 position;
	glm::mat4 transformMatrix;
	glm::mat4 normalTransformMatrix;
	int modelIndexNumber;
};

struct Model
{
	GLshort vertexCount;
	void *indexOffset;
	GLuint textureObject;
	const char *name;
};


#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))
#define ROUND_DOWN_2_DEC(a) (floorf(a * 100) / 100)
#define ZERO_MEM(a) memset(a, 0, sizeof(a))
#define NUM_BONES_PER_VEREX 8
struct VertexBoneData
{
	unsigned int IDs[NUM_BONES_PER_VEREX];
	float Weights[NUM_BONES_PER_VEREX];

	VertexBoneData()
	{
		Reset();
	};

	void Reset()
	{
		//ZERO_MEM(IDs);
		//ZERO_MEM(Weights);
		for (unsigned int i = 0; i < ARRAY_COUNT(IDs); i++)
		{
			IDs[i] = 0;
			assert(IDs[i] == 0);
		}
		for (unsigned int i = 0; i < ARRAY_COUNT(Weights); i++)
		{
			Weights[i] = 0.0;
			assert(Weights[i] == 0.0);
		}
	}

	void AddBoneData(unsigned int BoneID, float Weight)
	{
		for (unsigned int i = 0; i < ARRAY_SIZE_IN_ELEMENTS(IDs); i++) 
		{
			if (Weights[i] == 0.0)
			{
				IDs[i] = BoneID;
				Weights[i] = Weight;
				return;
			}
		}
		// should never get here - more bones than we have space for
		assert(0);
	}

};


struct BoneInfo
{
	aiMatrix4x4 BoneOffset;
	aiMatrix4x4 FinalTransformation;

	/*BoneInfo()
	{
		BoneOffset.SetZero();
		FinalTransformation.SetZero();
	}*/
};


class Renderer
{
public:
	Renderer();
	void BindVertexData();

	void StartRender();
	void EndRender(bool32 doubleBuffered);

	void RenderInstanced(GLsizei vertexCount, void * index, GLsizei renderCount);
	void RenderIndexed(Instance *instanceList);

	void SetClearColour(float r, float g, float b, float a);

	//void SetupTranslationMatrixBuffer(GLuint size, glm::mat4 offsetData[]);
	//void UpdateTranslationMatrixBuffer(GLuint size, glm::mat4 offsetData[]);

	void SetLightPosition(glm::vec3 position);

	void SetLightIntensity(glm::vec4 intensity);
	void SetAmbientLightIntensity(glm::vec4 intensity);





	int AssimpLoad(const char * Filename, const char * texturepath);

	void MoveCamera(glm::vec3 move);
	void SetCameraPosition(glm::vec3 newPosition);
	void SetCameraDirection(glm::vec3 direction);
	void SetCameraUp(glm::vec3 up);

	void SetPerspectiveFov(real32 fov) { cameraFov = fov; };
	void SetPerspectiveZNear(real32 znear) { cameraZNear = znear; };
	void SetPerspectiveZFar(real32 zfar) { cameraZFar = zfar; };
	void SetPerspectiveAspectRatio(real32 ratio) { displayRatio = ratio; };

	void SetProjectionMatrix(glm::mat4 matrix);

	glm::vec3 GetCameraPosition() { return cameraPosition; };

	int GetModel(const char * imagepath, const char * texturepath);



	void CalcInterpolatedRotation(aiQuaternion & Out, float AnimationTime, const aiNodeAnim * pNodeAnim);
	void CalcInterpolatedScaling(aiVector3D & Out, float AnimationTime, const aiNodeAnim * pNodeAnim);
	void CalcInterpolatedPosition(aiVector3D & Out, float AnimationTime, const aiNodeAnim * pNodeAnim);

	unsigned int FindRotation(float AnimationTime, const aiNodeAnim * pNodeAnim);
	unsigned int FindPosition(float AnimationTime, const aiNodeAnim * pNodeAnim);
	unsigned int FindScaling(float AnimationTime, const aiNodeAnim * pNodeAnim);

	void LoadBones(unsigned int MeshIndex, const aiMesh* pMesh, int extraOffset);

	aiMatrix4x4 BoneTransform(float timeInSeconds, std::vector<aiMatrix4x4>& transforms);
	void SetBoneTransform(unsigned int i, const aiMatrix4x4 & Transform);

	void ReadNodeHeirarchy(float AnimationTime, const aiNode* pNode, const aiMatrix4x4& ParentTransform);

	const aiNodeAnim * FindNodeAnim(const aiAnimation * pAnimation, const std::string NodeName);


private:
	void SetViewMatrix(glm::vec3 at, glm::vec3 eye, glm::vec3 up);
	GLuint loadBMP_custom(const char * imagepath);

private:
	GLuint program;
	GLuint shaderprojectionViewMatrixID;
	GLuint shaderTransformMatrixID;
	GLuint shaderNormalTransformMatrixID;




	// bone stuff. ogldev tutorial38.
	std::vector<VertexBoneData> Bones;
	GLuint BONE_ID_LOCATION;
	GLuint BONE_WEIGHT_LOCATION;
	std::map<std::string, uint32> m_BoneMapping; // maps a bone name to its index based on name 
	uint32 m_NumBones = 0;
	std::vector<BoneInfo> m_BoneInfo;

	Assimp::Importer Importer2;
	//const aiScene* pScene = Importer2.ReadFile("../models/henrietta.dae",
	const aiScene* pScene = Importer2.ReadFile("../models/boblampclean.md5mesh",
		aiProcess_Triangulate |
		aiProcess_GenSmoothNormals |
		aiProcess_FlipUVs);

	aiMatrix4x4 m_GlobalInverseTransform;
	static const unsigned int MAX_BONES = 100;
	GLuint m_boneLocation[MAX_BONES];

	// vertex data 
	GLuint vertexBufferObject;
	// colour data 
	GLuint uvBufferObject;
	// vertex normal data 
	GLuint vertexNormalObject;
	// indexed data 
	GLuint indexBufferObject;
	// translation data 
	GLuint translationBufferObject;
	// vertex array object 
	GLuint indexedVAO;
	

	std::vector <glm::vec3> total_vertices;
	std::vector <glm::vec2> total_uvs;
	std::vector <glm::vec3> total_normals;
	std::vector <GLshort> total_Indices;


	std::vector <Model> modelList;



	GLuint shaderViewMatrixID;
	GLuint shaderModelMatrixID;

	//GLuint shaderLightDirectionID;
	GLuint shaderLightPosID;
	GLuint shaderLightIntensityID;
	GLuint shaderAmbientLightIntensityID;
	//glm::vec4 lightPosition = glm::vec4(0.866f, 0.5f, 0.0f, 0.0f);




	// vars for movement and positioning 
	glm::mat4 viewMatrix;
	glm::vec3 cameraPosition;
	glm::vec3 cameraDirection;
	glm::vec3 cameraUp;

	// Projection matrix : 45? Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	glm::mat4 projectionMatrix;
	real32 cameraFov;
	real32 cameraZNear;
	real32 cameraZFar;
	real32 displayRatio;
};

#endif