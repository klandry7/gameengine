// NOTE: this is labelled as win32 to be safe, but if all goes well it should end up fairly generic. Time shall tell




#include "WindowApp.h"
#include "renderer.h"

// Include GLM
// using namespace glm;
// using the namespace will conflict their int32 with my int32
// better off specifying anyways
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>


#include "common.h"

// Include standard io, libraries, int
#include <stdio.h>
#include <stdlib.h>


// these are both debug only
#include <iostream>
#include <iomanip>// for std::setprecision


// IDEA 
// maybe have a vao for primitives and one for models ?
// pushing and popping transformation matrices?

// note:
// when thinking about opengl and rendering, remember to keep it generic
// the goal is to eventually move opengl stuff to it's own file entirely
// game just passes down what needs to be rendered.
// game doesn't know how it happens, just says "here's a model, here's it's coordinates, and that's all I got"


// maybe setup input to use 
//GLFW_PRESS   1
//The key or mouse button was pressed.
//GLFW_RELEASE   0
//The key or mouse button was released.
//GLFW_REPEAT   2
//The key was held down until it repeated.


// program entry point
int main(int argc, char *argv[])
{
	// 1st param is always w
	/*for (int i = 0; i < argc; i++)
	{
		std::cout << i << ": " << *argv[i] << std::endl;
	}*/

	WindowApp *application = new WindowApp(1024, 768);
	
	application->SetTime(0);

	Renderer *renderer = new Renderer();

	//int cubeModelIndex = renderer->AssimpLoad("../models/cube.obj", "../models/uvMap.bmp");
	//int monkeyModelIndex = renderer->AssimpLoad("../models/testMonkey.obj", "../models/colourMap.bmp");
	//int sphereModelIndex = renderer->AssimpLoad("../models/smoothSphere.obj", "../models/colourMap.bmp");
	//int planeModelIndex = renderer->AssimpLoad("../models/plane-better.obj", "../models/uvMap.bmp");
	//int pleinairModelIndex = renderer->GetModel("../models/pleinair_textured.obj", "../models/Pleinair_texture.bmp");
	//int batModelIndex = renderer->GetModel("../models/bat.obj", "../models/colourMap.bmp");

	
	//int assimpModelIndex = renderer->AssimpLoad("../models/phoenix_ugv.md2", "../models/uvMap.bmp");
	int assimpModelIndex = renderer->AssimpLoad("../models/boblampclean.md5mesh", "../models/uvMap.bmp");
	//int assimpModelIndex = renderer->AssimpLoad("../models/henrietta.blend", "../models/uvMap.bmp");
	
	//assert(assimpModelIndex > 1);

	//printf("%i\n", assimpModelIndex);
	 
	renderer->BindVertexData();


	int instanceCount = 1;// spawnWidth * spawnHeight;

	Instance *instanceList = new Instance[instanceCount];

	glm::mat4 *offsetData = new glm::mat4[instanceCount];


	/*for (int y = 0; y < spawnHeight; y++)
	{
		for (int x = 0; x < spawnWidth; x++)
		{
			int i = spawnWidth * y + x;
			instanceList[i] = {};
			instanceList[i].position = glm::vec3(x + x*2.0f, 0.0f, y + y*2.0f);
			instanceList[i].transformMatrix = glm::translate(glm::mat4(1.0f), instanceList[i].position);
			instanceList[i].normalTransformMatrix = glm::translate(glm::mat4(1.0f), instanceList[i].position);
			if (i == 0)
			{
				//instanceList[i].modelIndexNumber = cubeModelIndex;
				//instanceList[i].modelIndexNumber = i % 2 == 0 ? sphereModelIndex : monkeyModelIndex;
				instanceList[i].modelIndexNumber = planeModelIndex;
			}
			else if (i == 1)
			{
				instanceList[i].modelIndexNumber = assimpModelIndex;
			}
			else 
			{
				instanceList[i].modelIndexNumber = i % 2 == 0 ? sphereModelIndex : monkeyModelIndex;
			}
			
			//offsetData[i] = glm::translate(glm::mat4(1.0f), instanceList[i].position);
		}
	}*/

	instanceList[0] = {};
	instanceList[0].position = glm::vec3(2.0f, 0.0f, 2.0f);
	instanceList[0].transformMatrix = glm::translate(glm::mat4(1.0f), instanceList[0].position);
	instanceList[0].normalTransformMatrix = glm::translate(glm::mat4(1.0f), instanceList[0].position);
	instanceList[0].modelIndexNumber = assimpModelIndex;

	

	//renderer->SetupTranslationMatrixBuffer(instanceCount, offsetData);

	// need to watch out for if screen size changes
	// just in general, actually. I don't cover that at all yet
	float screenHalfWidth;
	float screenHalfHeight;
	application->GetScreenCentre(&screenHalfWidth, &screenHalfHeight);


	real32 originalMoveSpeed = 10.0f;
	real32 moveSpeed = originalMoveSpeed;
	real32 mouseSpeed = 0.8f;
	// initial horizontal angle: towards -z
	float horizontalAngle = 0;
	// initialvertical angle: none
	float verticalAngle = 0.0f;
	real32 sineValue = 0;



	game_buttons keyboardGameKeys = {};
	application->InitKeyboardGameKeys(&keyboardGameKeys);

	game_buttons gamepadGameKeys = {};
	application->InitJoypadGameKeys(&gamepadGameKeys);

	// have an old and new so you can tell if it's 
	// being held or it's the first press or it's released
	game_controller oldKeyboardController = {};
	game_controller newKeyboardController = {};

	game_controller oldJoypadController = {};
	game_controller newJoypadController = {};


	float animationTime = 0;


	real32 lastTime = application->GetTime();
	real32 currentTime = lastTime;
	real32 dt = 0;

	bool32 globalRunning = true;
	bool32 pause = true;
	while (globalRunning)
	{

		// if things are screwy (counting 8 axes), might need to mess with motioninjoy a bit.
		// this is all static just for now. obviously later would want to do a setup per controller 
		if (application->IsJoystickPresent())
		{
			newJoypadController = {};
			newJoypadController.isAnalog = true;
			application->ProcessInput(&newJoypadController, &gamepadGameKeys);
		}

		newKeyboardController = {};
		newKeyboardController.isAnalog = false;
		application->ProcessInput(&newKeyboardController, &keyboardGameKeys);



		// very dirty mvp and movement 
		// get mouse position
		double xpos, ypos;
		//glfwGetCursorPos(renderer->GetWindow(), &xpos, &ypos);
		application->GetCursorPos(&xpos, &ypos);
		// have to make sure the cursor stays at the centre for the screen, reset mouse position for next frame
		application->SetCursorPos(screenHalfWidth, screenHalfHeight);

		// compute new orientation
		// move in relation to how far away the mouse is from the centre of the screen
		horizontalAngle += mouseSpeed * dt * float(screenHalfWidth - xpos);
		verticalAngle += mouseSpeed * dt * float(screenHalfHeight - ypos);

		//direction: spherical coordinates to cartesian coordinates conversion
		glm::vec3 direction(
			cos(verticalAngle) * sin(horizontalAngle),
			sin(verticalAngle),
			cos(verticalAngle) * cos(horizontalAngle)
		);

		// right vector
		glm::vec3 right = glm::vec3(
			sin(horizontalAngle - 3.14f / 2.0f),
			0,
			cos(horizontalAngle - 3.14f / 2.0f)
		);

		// up vector. the perpendicular to direction and right (cross product between right and direction)
		glm::vec3 up = glm::cross(right, direction);




		// instead of checking explicitly the keyboard or joypad, should stick the currently active input into a temp struct
		// and check against that so there's only 1 loop 
		// actually, in reality, this is passed up to the game layer just as "input"
		// so this entire chunk is completely irrelavant (as is the above argument). Don't worry about it
		if (application->KeyUp(oldJoypadController.start, newJoypadController.start) || application->KeyUp(oldKeyboardController.start, newKeyboardController.start))
		{
			// post a close window message 
			application->PostWindowCloseMessage(true);
		}

		if (application->KeyDown(oldKeyboardController.action3, newKeyboardController.action3) || application->KeyDown(oldJoypadController.action3, newJoypadController.action3))
		{
			moveSpeed = 30.0f;
		}
		else
		{
			moveSpeed = originalMoveSpeed;
		}

		if (application->KeyDown(oldKeyboardController.up, newKeyboardController.up) || application->KeyDown(oldJoypadController.up, newJoypadController.up))
		{
			renderer->MoveCamera(glm::vec3(direction.x * dt * moveSpeed, 0, direction.z * dt * moveSpeed));
		}
		if (application->KeyDown(oldKeyboardController.down, newKeyboardController.down) || application->KeyDown(oldJoypadController.down, newJoypadController.down))
		{
			renderer->MoveCamera(-glm::vec3(direction.x * dt * moveSpeed, 0, direction.z * dt * moveSpeed));
		}

		if (application->KeyDown(oldKeyboardController.left, newKeyboardController.left) || application->KeyDown(oldJoypadController.left, newJoypadController.left))
		{
			renderer->MoveCamera(-right * dt * moveSpeed);
		}
		if (application->KeyDown(oldKeyboardController.right, newKeyboardController.right) || application->KeyDown(oldJoypadController.right, newJoypadController.right))
		{
			renderer->MoveCamera(right * dt * moveSpeed);
		}

		if (application->KeyDown(oldKeyboardController.action1, newKeyboardController.action1) || application->KeyDown(oldJoypadController.action1, newJoypadController.action1))
		{
			renderer->MoveCamera(-glm::vec3(0, moveSpeed * dt, 0));
		}
		if (application->KeyDown(oldKeyboardController.action2, newKeyboardController.action2) || application->KeyDown(oldJoypadController.action2, newJoypadController.action2))
		{
			renderer->MoveCamera(glm::vec3(0, moveSpeed * dt, 0));
		}

		if (application->KeyPress(oldKeyboardController.action4, newKeyboardController.action4))
		{
			//std::cout << application->GetDoubleBuffering() << std::endl;
			application->SetDoubleBuffering(!application->GetDoubleBuffering());
		}

		renderer->SetCameraDirection(renderer->GetCameraPosition() + direction);
		renderer->SetCameraUp(up);

		if (application->KeyUp(oldKeyboardController.select, newKeyboardController.select))
		{
			pause = !pause;
		}

		oldKeyboardController = newKeyboardController;
		oldJoypadController = newJoypadController;

		glm::vec3 lightPosition = glm::vec3((spawnWidth ) + glm::sin(sineValue), glm::cos(sineValue) * 10.0f, (spawnHeight ) + glm::cos(sineValue));

		

		if (!pause)
		{
			sineValue += dt * 2;
			/*for (int i = 0; i < instanceCount; i++)
			{
				if (i == 0)
				{
					instanceList[i].position.x = spawnWidth*1.5;// spawnWidth * 2;
					instanceList[i].position.y = -10;// spawnHeight * 2;
					instanceList[i].position.z = spawnHeight*1.5;
					glm::mat4 translation = glm::translate(glm::mat4(1.0f), instanceList[i].position);
					glm::mat4 scale = glm::scale(glm::mat4(1.0f), glm::vec3(spawnWidth*1.5, spawnWidth*10.5f, spawnHeight*1.5));
					instanceList[i].transformMatrix = translation *scale;
					//instanceList[i].normalTransformMatrix= translation *scale;
					// read modern 3D Tut09 Normal Transformation
					// when you scale something non uniformly, vertex normals need to be changed this way
					instanceList[i].normalTransformMatrix = glm::transpose(glm::inverse(scale));
				}
				else if (i == 1)
				{
					instanceList[i].position = lightPosition;
					glm::mat4 translation = glm::translate(glm::mat4(1.0f), instanceList[i].position);
					instanceList[i].transformMatrix = translation;// *scale;
					instanceList[i].normalTransformMatrix = translation;
				}
				else
				{
					instanceList[i].position.y = glm::sin(sineValue + i);
					glm::mat4 translation = glm::translate(glm::mat4(1.0f), instanceList[i].position);
					glm::mat4 rotation = glm::rotate(translation, sineValue, glm::vec3(0.0f, 0.0f, 1.0f));
					glm::mat4 scale = glm::scale(glm::mat4(1.0f), glm::vec3(glm::abs(glm::sin(sineValue)), glm::abs(glm::sin(sineValue)), glm::abs(glm::sin(sineValue))));
					// if I want to start using scaling, read this: 
					instanceList[i].transformMatrix = rotation;// *scale;
					instanceList[i].normalTransformMatrix = rotation;
				}
				
			}*/
		}

		// should be able to set animations to single or repeating
		animationTime += dt;
		// a vector in which to store the transforms
		std::vector<aiMatrix4x4> Transforms;
		// get the transforms 
		renderer->BoneTransform(animationTime, Transforms);
		

		if (pause)
		{
			animationTime = 0.0f;
			for (unsigned int i = 0; i < Transforms.size(); i++)
			{
				Transforms.at(i) = aiMatrix4x4();
			}
		}


		renderer->StartRender();

		//set the transform uniform in the shader
		for (unsigned int i = 0; i < Transforms.size(); i++)
		{
			/*printf("%i\n", i);
			printf("%f %f %f %f\n", Transforms[i].a1, Transforms[i].a2, Transforms[i].a3, Transforms[i].a4);
			printf("%f %f %f %f\n", Transforms[i].b1, Transforms[i].b2, Transforms[i].b3, Transforms[i].b4);
			printf("%f %f %f %f\n", Transforms[i].c1, Transforms[i].c2, Transforms[i].c3, Transforms[i].c4);
			printf("%f %f %f %f\n", Transforms[i].d1, Transforms[i].d2, Transforms[i].d3, Transforms[i].d4);*/
			renderer->SetBoneTransform(i, Transforms[i]);
		}

		renderer->SetLightPosition(lightPosition);
		renderer->SetLightIntensity(glm::vec4(0.8f, 0.8f, 0.8f, 1.0f));
		renderer->SetAmbientLightIntensity(glm::vec4(0.2f, 0.2f, 0.2f, 1.0f));

		renderer->RenderIndexed(instanceList);
		//printf("ok\n");
		renderer->EndRender(application->GetDoubleBuffering());

		// calculate dt 
		// need to account for a target dt and locking framerates 
		currentTime = application->GetTime();
		dt = currentTime - lastTime;
		lastTime = currentTime;
		if (dt > 0.1f)
		{
			dt = 0.1f;
		}
		//std::cout << std::fixed << std::setprecision(20) << dt << '\n';
		if (argc > 1 && *argv[1] == 't'){ std::cout << dt << '\n'; }	
		//std::cout << "FPS: " << 1/dt << std::endl;

		application->SwapBuffers();
		if (application->WindowShouldClose())
		{
			globalRunning = false;
		}
	}

	// Close all created windows and terminates GLFW
	application->Terminate();

	return 0;
}

