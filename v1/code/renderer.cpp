
#include "renderer.h"

// create the renderer, do basic initialization 
#include <iostream>

// for reading files.
// would also like to switch from strings to char array
#include <fstream>
#include <sstream>
#include <string>

#include <vector>

#include <glm/gtc/type_ptr.hpp>



#define TINYOBJLOADER_IMPLEMENTATION // define this in only *one* .cc
#include "tiny_obj_loader.h"

#define FOURxTHREE 4.0f / 3.0f
#define SIXTEENxNINE 16.0f / 9.0f

std::string ReadFile(const char *filePath)
{
	std::string shaderCode;
	std::ifstream shaderStream(filePath, std::ios::in);
	if (shaderStream.is_open())
	{
		std::string line = "";
		while (getline(shaderStream, line))
		{
			shaderCode += "\n" + line;
		}
		shaderStream.close();
		return shaderCode;
	}
	else
	{
		printf("failed to open file [%s]", filePath);
		getchar();
		return NULL;
	}
}

// based on this: http://www.opengl-tutorial.org/beginners-tutorials/tutorial-2-the-first-triangle/
GLuint LoadShader(GLenum shaderType, const char *filePath)
{
	// https://www.opengl.org/wiki/Shader_Compilation
	// create the shader
	GLuint shaderID = glCreateShader(shaderType);

	// read the shader code from the file 
	// should move this to it's own function 
	std::string shaderCode = ReadFile(filePath);


	// compile the shader 
	printf("compiling shader: %s\n", filePath);
	char const *sourcePointer = shaderCode.c_str();
	// shader handle, number of elements, source code, specifies and array of string lengths 
	glShaderSource(shaderID, 1, &sourcePointer, NULL);
	// compile the shader created above 
	glCompileShader(shaderID);

	// Check Shader status
	GLint result = GL_FALSE;
	int infoLogLength;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0 || result == GL_FALSE)
	{
		std::vector<char> ShaderErrorMessage(infoLogLength + 1);
		glGetShaderInfoLog(shaderID, infoLogLength, NULL, &ShaderErrorMessage[0]);
		printf("%s\n", &ShaderErrorMessage[0]);
	}
	else
	{
		printf("success!\n");
	}

	return shaderID;
}

// write something so that if shaders fail, load super simple default shaders 
// based on this: http://www.opengl-tutorial.org/beginners-tutorials/tutorial-2-the-first-triangle/
GLuint GetProgram(GLuint *shaderList, int shaderListSize)
{
	// create the program
	GLuint programID = glCreateProgram();

	// link the shaders to the program 
	for (int i = 0; i < shaderListSize; i++)
	{
		printf("shaders[%d] attached\n", i);
		glAttachShader(programID, shaderList[i]);
	}

	// link the program 
	glLinkProgram(programID);
	GLint isLinked = 0;
	glGetProgramiv(programID, GL_LINK_STATUS, (int *)&isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &maxLength);

		//The maxLength includes the NULL character
		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(programID, maxLength, &maxLength, &infoLog[0]);
		printf("%s\n", &infoLog[0]);
		//programID = GL_FALSE;
	}
	else
	{
		printf("program linked\n");
	}

	for (int i = 0; i < shaderListSize; i++)
	{
		glDeleteShader(shaderList[i]);
		printf("shaders[%d] deleted\n", i);
	}

	return programID;
}



// for testing 
// http://www.opengl-tutorial.org/beginners-tutorials/tutorial-5-a-textured-cube/
GLuint Renderer::loadBMP_custom(const char * imagepath) 
{

	printf("Reading image %s\n", imagepath);

	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file;
	fopen_s(&file, imagepath, "rb");
	if (!file) { printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", imagepath); getchar(); return 0; }

	// Read the header, i.e. the 54 first bytes

	// If less than 54 bytes are read, problem
	if (fread(header, 1, 54, file) != 54) {
		printf("Not a correct BMP file\n");
		return 0;
	}
	// A BMP files always begins with "BM"
	if (header[0] != 'B' || header[1] != 'M') {
		printf("Not a correct BMP file\n");
		return 0;
	}
	// Make sure this is a 24bpp file
	if (*(int*)&(header[0x1E]) != 0) { printf("Not a correct BMP file\n");    return 0; }
	if (*(int*)&(header[0x1C]) != 24) { printf("Not a correct BMP file\n");    return 0; }

	// Read the information about the image
	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);

	// Some BMP files are misformatted, guess missing information
	if (imageSize == 0)    imageSize = width*height * 3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos == 0)      dataPos = 54; // The BMP header is done that way

										 // Create a buffer
	data = new unsigned char[imageSize];

	// Read the actual data from the file into the buffer
	fread(data, 1, imageSize, file);

	// Everything is in memory now, the file wan be closed
	fclose(file);

	// Create one OpenGL texture
	GLuint textureID;
	glGenTextures(1, &textureID);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	// OpenGL has now copied the data. Free our own version
	delete[] data;

	// Poor filtering, or ...
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 

	// ... nice trilinear filtering.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);

	printf("Reading image %s successful\n", imagepath);

	// Return the ID of the texture we just created
	return textureID;
}




void PrintAiMat4(aiMatrix4x4 mat)
{
	printf("%f %f %f %f\n", mat.a1, mat.a2, mat.a3, mat.a4);
	printf("%f %f %f %f\n", mat.b1, mat.b2, mat.b3, mat.b4);
	printf("%f %f %f %f\n", mat.c1, mat.c2, mat.c3, mat.c4);
	printf("%f %f %f %f\n", mat.d1, mat.d2, mat.d3, mat.d4);
}



void Renderer::LoadBones(unsigned int MeshIndex, const aiMesh* pMesh, int extraOffset)
{
	// for every bone 
	for (unsigned int i = 0; i < pMesh->mNumBones; i++) 
	{
		unsigned int BoneIndex = 0;
		std::string BoneName(pMesh->mBones[i]->mName.data);
		//printf("%s\n", pMesh->mBones[i]->mName.data);

		if (m_BoneMapping.find(BoneName) == m_BoneMapping.end())
		{
			BoneIndex = m_NumBones;
			m_NumBones++;
			BoneInfo bi;
			m_BoneInfo.push_back(bi);
			m_BoneMapping[BoneName] = BoneIndex;
			m_BoneInfo[BoneIndex].BoneOffset = pMesh->mBones[i]->mOffsetMatrix;
		}
		else 
		{
			BoneIndex = m_BoneMapping[BoneName];
		}

		//for every weight
		for (unsigned int j = 0; j < pMesh->mBones[i]->mNumWeights; j++) 
		{
			//unsigned int VertexID = m_Entries[MeshIndex].BaseVertex + pMesh->mBones[i]->mWeights[j].mVertexId;
			// get the weight and id 
			// the id is kind of shitty right now.
			// so basically it's stored as an offset from 0, but it's supposed to line up with the model index offset (Model.indexOffset)
			// you'll need to call into it as the current offset from your position in total_Indices
			// just need to remember you can't straight up call into Bones using your current position in total_indices
			// if it comes down to it, just stop loading all other models, get animation working and jump over to v2
			
			unsigned int VertexID = extraOffset + pMesh->mBones[i]->mWeights[j].mVertexId;
			float Weight = pMesh->mBones[i]->mWeights[j].mWeight;
			Bones[VertexID].AddBoneData(BoneIndex, Weight);
		}
	}
}

// calculate bone transformations that go to the shader every frame 
// should be passing in an animation index instead of always 0.
// check using unsigned int aiScene::mNumAnimations and bool aiScene::HasAnimations()
aiMatrix4x4 Renderer::BoneTransform(float timeInSeconds, std::vector<aiMatrix4x4>& transforms)
{
	aiMatrix4x4 identity;

	
	float fat = (float)pScene->mAnimations[0]->mTicksPerSecond;

	float TicksPerSecond = (float)(pScene->mAnimations[0]->mTicksPerSecond != 0 ? pScene->mAnimations[0]->mTicksPerSecond : 25.0f);
	float TimeInTicks = timeInSeconds * TicksPerSecond;
	float AnimationTime = (float)(fmod(TimeInTicks, pScene->mAnimations[0]->mDuration));

	
	//printf("%s\n", pScene->mRootNode->mName.data);

	//printf("%f %f %f %f\n", pScene->mRootNode->mTransformation.a1, pScene->mRootNode->mTransformation.a2, pScene->mRootNode->mTransformation.a3, pScene->mRootNode->mTransformation.a4);
	//printf("%f %f %f %f\n", pScene->mRootNode->mTransformation.b1, pScene->mRootNode->mTransformation.b2, pScene->mRootNode->mTransformation.b3, pScene->mRootNode->mTransformation.b4);
	
	//printf("%f\n", pScene->mRootNode->mTransformation.c1);
	

	//printf("%f %f %f %f\n", pScene->mRootNode->mTransformation.c1, pScene->mRootNode->mTransformation.c2, pScene->mRootNode->mTransformation.c3, pScene->mRootNode->mTransformation.c4);
	//printf("%f %f %f %f\n", pScene->mRootNode->mTransformation.d1, pScene->mRootNode->mTransformation.d2, pScene->mRootNode->mTransformation.d3, pScene->mRootNode->mTransformation.d4);


	//PrintAiMat4(pScene->mRootNode->mTransformation);
	//printf("je;llkljasdf\n");
	//assert(pScene->mRootNode->mTransformation != NULL);

	ReadNodeHeirarchy(AnimationTime, pScene->mRootNode, identity);

	

	transforms.resize(m_NumBones);

	for (unsigned int i = 0; i < m_NumBones; i++)
	{
		transforms[i] = m_BoneInfo[i].FinalTransformation;
	}


	return identity;
}

void Renderer::SetBoneTransform(unsigned int Index, const aiMatrix4x4& AssimpMatrix)
{
	assert(Index < MAX_BONES);


	//glUniformMatrix4fv(m_boneLocation[Index], 1, GL_TRUE, (const GLfloat*)Transform);



	/*float newMat4[16];
	
	newMat4[0] = Transform.a1;
	newMat4[1] = Transform.a2;
	newMat4[2] = Transform.a3;
	newMat4[3] = Transform.a4;

	newMat4[4] = Transform.b1;
	newMat4[5] = Transform.b2;
	newMat4[6] = Transform.b3;
	newMat4[7] = Transform.b4;

	newMat4[8] = Transform.c1;
	newMat4[9] = Transform.c2;
	newMat4[10] = Transform.c3;
	newMat4[11] = Transform.c4;

	newMat4[12] = Transform.d1;
	newMat4[13] = Transform.d2;
	newMat4[14] = Transform.d3;
	newMat4[15] = Transform.d4;

	glm::mat4 outMat4 = glm::make_mat4(newMat4);*/

	//glUniformMatrix4fv(m_boneLocation[Index], 1, GL_FALSE, &outMat4[0][0]);

	float m[4][4];

	m[0][0] = AssimpMatrix.a1; m[0][1] = AssimpMatrix.a2; m[0][2] = AssimpMatrix.a3; m[0][3] = AssimpMatrix.a4;
	m[1][0] = AssimpMatrix.b1; m[1][1] = AssimpMatrix.b2; m[1][2] = AssimpMatrix.b3; m[1][3] = AssimpMatrix.b4;
	m[2][0] = AssimpMatrix.c1; m[2][1] = AssimpMatrix.c2; m[2][2] = AssimpMatrix.c3; m[2][3] = AssimpMatrix.c4;
	m[3][0] = AssimpMatrix.d1; m[3][1] = AssimpMatrix.d2; m[3][2] = AssimpMatrix.d3; m[3][3] = AssimpMatrix.d4;


	glUniformMatrix4fv(m_boneLocation[Index], 1, GL_TRUE, (const GLfloat*)m);

	/*
	glm::mat4 temp = glm::mat4(1.0);
	glUniformMatrix4fv(m_boneLocation[Index], 1, GL_FALSE, &temp[0][0]);
	*/
}


void Renderer::ReadNodeHeirarchy(float AnimationTime, const aiNode* pNode, const aiMatrix4x4 & ParentTransform)
{
	std::string NodeName(pNode->mName.data);
	
	const aiAnimation* pAnimation = pScene->mAnimations[0];
	
	

	aiMatrix4x4 NodeTransformation(pNode->mTransformation);
	//printf("gogurt\n");
	const aiNodeAnim* pNodeAnim = FindNodeAnim(pAnimation, NodeName);

	

	if (pNodeAnim) 
	{
		// Interpolate scaling and generate scaling transformation matrix
		aiVector3D Scaling;
		CalcInterpolatedScaling(Scaling, AnimationTime, pNodeAnim);
		aiMatrix4x4 ScalingM;
		//ScalingM.InitScaleTransform(Scaling.x, Scaling.y, Scaling.z);
		ScalingM.a1 = Scaling.x;
		ScalingM.b2 = Scaling.y;
		ScalingM.c3 = Scaling.z;

		// Interpolate rotation and generate rotation transformation matrix
		aiQuaternion RotationQ;
		CalcInterpolatedRotation(RotationQ, AnimationTime, pNodeAnim);
		aiMatrix4x4 RotationM = aiMatrix4x4(RotationQ.GetMatrix());

		// Interpolate translation and generate translation transformation matrix
		aiVector3D Translation;
		CalcInterpolatedPosition(Translation, AnimationTime, pNodeAnim);
		aiMatrix4x4 TranslationM;
		//TranslationM.InitTranslationTransform(Translation.x, Translation.y, Translation.z);
		TranslationM.a4 = Translation.x;
		TranslationM.b4 = Translation.y;
		TranslationM.c4 = Translation.z;


		// Combine the above transformations
		NodeTransformation = TranslationM * RotationM * ScalingM;
	}

	aiMatrix4x4 GlobalTransformation = ParentTransform * NodeTransformation;

	if (m_BoneMapping.find(NodeName) != m_BoneMapping.end()) 
	{
		unsigned int BoneIndex = m_BoneMapping[NodeName];
		m_BoneInfo[BoneIndex].FinalTransformation = m_GlobalInverseTransform * GlobalTransformation * m_BoneInfo[BoneIndex].BoneOffset;
	}

	for (unsigned int i = 0; i < pNode->mNumChildren; i++) 
	{
		ReadNodeHeirarchy(AnimationTime, pNode->mChildren[i], GlobalTransformation);
	}
}


const aiNodeAnim* Renderer::FindNodeAnim(const aiAnimation* pAnimation, const std::string NodeName)
{
	//printf("asdf\n");
	for (unsigned int i = 0; i < pAnimation->mNumChannels; i++) 
	{
		//printf("glass houses %i\n", i);
		const aiNodeAnim* pNodeAnim = pAnimation->mChannels[i];
		
		if (std::string(pNodeAnim->mNodeName.data) == NodeName) 
		{
			return pNodeAnim;
		}
	}

	return NULL;
}



void Renderer::CalcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumScalingKeys == 1) 
	{
		Out = pNodeAnim->mScalingKeys[0].mValue;
		return;
	}

	unsigned int ScalingIndex = FindScaling(AnimationTime, pNodeAnim);
	unsigned int NextScalingIndex = (ScalingIndex + 1);
	assert(NextScalingIndex < pNodeAnim->mNumScalingKeys);
	float DeltaTime = (float)(pNodeAnim->mScalingKeys[NextScalingIndex].mTime - pNodeAnim->mScalingKeys[ScalingIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mScalingKeys[ScalingIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& Start = pNodeAnim->mScalingKeys[ScalingIndex].mValue;
	const aiVector3D& End = pNodeAnim->mScalingKeys[NextScalingIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}

void Renderer::CalcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumPositionKeys == 1) 
	{
		Out = pNodeAnim->mPositionKeys[0].mValue;
		return;
	}

	unsigned int PositionIndex = FindPosition(AnimationTime, pNodeAnim);
	unsigned int NextPositionIndex = (PositionIndex + 1);
	assert(NextPositionIndex < pNodeAnim->mNumPositionKeys);
	float DeltaTime = (float)(pNodeAnim->mPositionKeys[NextPositionIndex].mTime - pNodeAnim->mPositionKeys[PositionIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mPositionKeys[PositionIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& Start = pNodeAnim->mPositionKeys[PositionIndex].mValue;
	const aiVector3D& End = pNodeAnim->mPositionKeys[NextPositionIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}


void Renderer::CalcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	// we need at least two values to interpolate...
	if (pNodeAnim->mNumRotationKeys == 1) 
	{
		Out = pNodeAnim->mRotationKeys[0].mValue;
		return;
	}

	unsigned int RotationIndex = FindRotation(AnimationTime, pNodeAnim);
	unsigned int NextRotationIndex = (RotationIndex + 1);
	assert(NextRotationIndex < pNodeAnim->mNumRotationKeys);
	float DeltaTime = (float)(pNodeAnim->mRotationKeys[NextRotationIndex].mTime - pNodeAnim->mRotationKeys[RotationIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mRotationKeys[RotationIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiQuaternion& StartRotationQ = pNodeAnim->mRotationKeys[RotationIndex].mValue;
	const aiQuaternion& EndRotationQ = pNodeAnim->mRotationKeys[NextRotationIndex].mValue;
	aiQuaternion::Interpolate(Out, StartRotationQ, EndRotationQ, Factor);
	Out = Out.Normalize();
}

unsigned int Renderer::FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumRotationKeys > 0);

	for (unsigned int i = 0; i < pNodeAnim->mNumRotationKeys - 1; i++)
	{
		if (AnimationTime < (float)pNodeAnim->mRotationKeys[i + 1].mTime) 
		{
			return i;
		}
	}

	assert(0);

	return 0;
}

unsigned int Renderer::FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	for (unsigned int i = 0; i < pNodeAnim->mNumPositionKeys - 1; i++) 
	{
		if (AnimationTime < (float)pNodeAnim->mPositionKeys[i + 1].mTime) 
		{
			return i;
		}
	}

	assert(0);

	return 0;
}

unsigned int Renderer::FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumScalingKeys > 0);

	for (unsigned int i = 0; i < pNodeAnim->mNumScalingKeys - 1; i++)
	{
		if (AnimationTime < (float)pNodeAnim->mScalingKeys[i + 1].mTime) 
		{
			return i;
		}
	}

	assert(0);

	return 0;
}


int Renderer::AssimpLoad(const char * Filename, const char * texturepath)
{
	const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

	

	Assimp::Importer Importer;
	/*pScene = Importer.ReadFile(Filename,
		aiProcess_Triangulate |
		aiProcess_GenSmoothNormals |
		aiProcess_FlipUVs);*/

	// if you successfully extracted a scene 
	if (pScene)
	{
		printf("Has animations: %i\n", pScene->HasAnimations());
		printf("Number of animations: %i\n", pScene->mNumAnimations);

		printf("\nAssimp Loading Model %s ...\n", Filename);
		

		m_GlobalInverseTransform = pScene->mRootNode->mTransformation;
		m_GlobalInverseTransform.Inverse();

		int sizeCount = 0;
		GLshort firstOffset = -1;
		std::vector<int> extraOffsets;

		// for every mesh in the scene 
		for (unsigned int i = 0; i < pScene->mNumMeshes; i++)
		{

			const aiMesh* paiMesh = pScene->mMeshes[i];
			unsigned int materialIndex = paiMesh->mMaterialIndex;
			
			std::vector <glm::vec3> aiVertices;
			std::vector <glm::vec3> aiNormals;
			std::vector <glm::vec2> aiTexCoords;
			std::vector <GLshort> Indices;

			// get vertex information
			for (unsigned int n = 0; n < paiMesh->mNumVertices; n++)
			{
				// get the data
				const aiVector3D* pPos = &(paiMesh->mVertices[n]);
				const aiVector3D* pNormal = &(paiMesh->mNormals[n]);// : &Zero3D;
				const aiVector3D* pTexCoord = paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][n]) : &Zero3D;
				// push it back
				aiVertices.push_back(glm::vec3((float)pPos->x, (float)pPos->y, (float)pPos->z));
				aiNormals.push_back(glm::vec3((float)pNormal->x, (float)pNormal->y, (float)pNormal->z));
				aiTexCoords.push_back(glm::vec2((float)pTexCoord->x, (float)pTexCoord->y));
			}
			//printf("Vertex Information Extracted\n");

			// get index information
			for (unsigned int n = 0; n < paiMesh->mNumFaces; n++) 
			{
				const aiFace& Face = paiMesh->mFaces[n];
				assert(Face.mNumIndices == 3);
				Indices.push_back((GLshort)Face.mIndices[0]);
				Indices.push_back((GLshort)Face.mIndices[1]);
				Indices.push_back((GLshort)Face.mIndices[2]);
			}
			//printf("Face Indices Extracted\n");


			// can add a thing to extract textures?


			// add the model to the scene 
			total_vertices.insert(total_vertices.end(), aiVertices.begin(), aiVertices.end());
			total_uvs.insert(total_uvs.end(), aiTexCoords.begin(), aiTexCoords.end());
			total_normals.insert(total_normals.end(), aiNormals.begin(), aiNormals.end());

			GLshort offset = (GLshort)total_Indices.size();

			if (firstOffset == -1)
			{
				firstOffset = offset;
			}

			for (size_t n = 0; n < Indices.size(); n++)
			{
				total_Indices.push_back(offset + Indices[n]);
			}

			extraOffsets.push_back(sizeCount);
			sizeCount += aiVertices.size();


			
		}

		GLuint textureObject = loadBMP_custom(texturepath);

		Model model = {};
		model.vertexCount = (GLshort)sizeCount;// textureObject.size();
		model.indexOffset = (void *)(firstOffset * sizeof(GLshort));
		model.textureObject = textureObject;
		model.name = texturepath;


		int index = modelList.size();
		modelList.push_back(model);

		printf("model created successfully\n");


		// resize bones to be the total number of vertices in the model
		//printf("%i", );

		//if (pScene->mNumMeshes > 1)
		//{
		Bones.resize(sizeCount);
		for (unsigned int i = 0; i < Bones.size(); i++)
		{
			Bones.at(i).Reset();
		}


		for (unsigned int i = 0; i < pScene->mNumMeshes; i++)
		{
			LoadBones(index, pScene->mMeshes[i], extraOffsets.at(i));
		}
		/*}
		else
		{
			printf("trash\n");
		}*/


		//PrintAiMat4(pScene->mRootNode->mTransformation);

		return index;



		//return true;
	}

	return -1;
	//return false;
}

Renderer::Renderer()
{
	GLuint vertexShader = LoadShader(GL_VERTEX_SHADER, "../shaders/animation.vert");
	GLuint fragmentShader = LoadShader(GL_FRAGMENT_SHADER, "../shaders/animation.frag");
	int shaderListSize = 2;
	GLuint *shaderList = new GLuint[shaderListSize];
	shaderList[0] = vertexShader;
	shaderList[1] = fragmentShader;

	// setup shaders and get the program
	program = GetProgram(shaderList, shaderListSize);

	if (program == GL_FALSE)
	{
		printf("program creation failed");
		//return -1;
		exit(1);
	}

	// setup the camera
	SetCameraPosition(glm::vec3(1, 3, 1));
	SetCameraDirection(glm::vec3(1.0f, 0.0f, 0.0f));
	SetCameraUp(glm::vec3(0.0f, 1.0f, 0.0f));

	SetPerspectiveFov(45.0f);
	SetPerspectiveZFar(300.0f);
	SetPerspectiveZNear(0.1f);
	SetPerspectiveAspectRatio(FOURxTHREE);
	SetProjectionMatrix(glm::perspective(cameraFov, displayRatio, cameraZNear, cameraZFar));

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);
	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);
	// cull back faces 
	glCullFace(GL_BACK);
	// front faces are clockwise
	glFrontFace(GL_CCW);

	// Get a handle for our uniform in the vertex shader 
	shaderprojectionViewMatrixID = glGetUniformLocation(program, "projectionViewMatrix");
	shaderTransformMatrixID = glGetUniformLocation(program, "transformationMatrix");
	shaderNormalTransformMatrixID = glGetUniformLocation(program, "normalTransformMatrix");

	shaderViewMatrixID = glGetUniformLocation(program, "viewMatrix");

	shaderLightPosID = glGetUniformLocation(program, "lightPos");
	shaderLightIntensityID = glGetUniformLocation(program, "lightIntensity");
	shaderAmbientLightIntensityID = glGetUniformLocation(program, "ambientLightIntensity");

	// setup uniform gBones[]
	for (unsigned int i = 0; i < ARRAY_SIZE_IN_ELEMENTS(m_boneLocation); i++) 
	{
		char Name[128];
		memset(Name, 0, sizeof(Name));
		// this is c++ 11. write a string to a buffer
		// pointer to buffer, bytes to be used, the string, additional args(just like printf)
		snprintf(Name, sizeof(Name), "gBones[%d]", i);
		printf("%s\n", Name);
		m_boneLocation[i] = glGetUniformLocation(program, Name);
	}


	SetClearColour(0.0f, 0.0f, 0.4f, 1.0f);


	// generate the vao/vbos
	glGenVertexArrays(1, &indexedVAO);
	glBindVertexArray(indexedVAO);

	glGenBuffers(1, &vertexBufferObject);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &uvBufferObject);
	glEnableVertexAttribArray(1);

	glGenBuffers(1, &vertexNormalObject);
	glEnableVertexAttribArray(2);

	glGenBuffers(1, &indexBufferObject);
	glEnableVertexAttribArray(3);

	glGenBuffers(1, &BONE_ID_LOCATION);
	glEnableVertexAttribArray(4);

	glGenBuffers(1, &BONE_WEIGHT_LOCATION);
	glEnableVertexAttribArray(5);


	/*for (int i = 0; i < 4; i++) { glEnableVertexAttribArray(4 + i); }*/

	glBindVertexArray(0);
}

void Renderer::BindVertexData()
{
	
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, total_vertices.size() * sizeof(glm::vec3), &total_vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	
	glBindBuffer(GL_ARRAY_BUFFER, uvBufferObject);
	glBufferData(GL_ARRAY_BUFFER, total_uvs.size() * sizeof(glm::vec2), &total_uvs[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	
	glBindBuffer(GL_ARRAY_BUFFER, vertexNormalObject);
	glBufferData(GL_ARRAY_BUFFER, total_normals.size() * sizeof(glm::vec3), &total_normals[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, total_Indices.size() * sizeof(GLshort), &total_Indices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	glBindBuffer(GL_ARRAY_BUFFER, BONE_ID_LOCATION);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Bones[0]) * Bones.size(), &Bones[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, BONE_WEIGHT_LOCATION);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Bones[0]) * Bones.size(), &Bones[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(indexedVAO);


	// 1st attribute buffer : vertices
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glVertexAttribPointer(
		0,  // The attribute we want to configure
		3,                            // size
		GL_FLOAT,                     // type
		GL_FALSE,                     // normalized?
		0,                            // stride
		(void*)0                      // array buffer offset
	);

	// 2nd attribute buffer : UVs
	glBindBuffer(GL_ARRAY_BUFFER, uvBufferObject);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size : U+V => 2
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	// 3rd attribute buffer : vertex normals
	glBindBuffer(GL_ARRAY_BUFFER, vertexNormalObject);
	glVertexAttribPointer(
		2,  // The attribute we want to configure
		3,                            // size
		GL_FLOAT,                     // type
		GL_FALSE,                     // normalized?
		0,                            // stride
		(void*)0                      // array buffer offset
	);

	// 4th attribute buffer: index data 
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
	glVertexAttribPointer(
		3,  // The attribute we want to configure
		3,                            // size
		GL_FLOAT,                     // type
		GL_FALSE,                     // normalized?
		0,                            // stride
		(void*)0                      // array buffer offset
	);

	// notice the I. this is because it's int, not float
	// 5th attribute. bone id's
	glBindBuffer(GL_ARRAY_BUFFER, BONE_ID_LOCATION);
	glVertexAttribIPointer(4, 4, GL_INT, sizeof(VertexBoneData), (const GLvoid*)0);

	// 6th attribute. bone weights
	glBindBuffer(GL_ARRAY_BUFFER, BONE_WEIGHT_LOCATION);
	glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(VertexBoneData), (const GLvoid*)(NUM_BONES_PER_VEREX * 4));



	glBindVertexArray(0);
}


/*void Renderer::SetupTranslationMatrixBuffer(GLuint size, glm::mat4 offsetData[])
{
	// Vertex Buffer Object
	glBindVertexArray(indexedVAO);
	glGenBuffers(1, &translationBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, translationBufferObject);
	glBufferData(GL_ARRAY_BUFFER, size * sizeof(glm::mat4), offsetData, GL_STATIC_DRAW);

	// to send a mat4 it needs to be split up into 4 parts since you can only have a max size of 4
	for (int i = 0; i < 4; i++)
	{
		glVertexAttribPointer(
			4 + i,  // The attribute we want to configure
			4,					// size
			GL_FLOAT,           // type
			GL_FALSE,			// normalized?
			sizeof(glm::mat4),	// stride
			(const GLvoid*)(sizeof(glm::vec4) * i) // array buffer offset
		);
		glVertexAttribDivisor(4 + i, 1);
	}
	glBindVertexArray(0);
}

// https://www.opengl.org/wiki/Buffer_Object_Streaming
// apparently map buffer is better than subData
// http://stackoverflow.com/questions/13913887/opengl-buffer-update
void Renderer::UpdateTranslationMatrixBuffer(GLuint size, glm::mat4 offsetData[])
{
	glBindVertexArray(indexedVAO);
	glBindBuffer(GL_ARRAY_BUFFER, translationBufferObject);
	//glClearBufferData(GL_ARRAY_BUFFER, GL_RGBA32F, GL_RED,GL_FLOAT,0);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(glm::mat4) * size, offsetData);
	//glBufferData(GL_ARRAY_BUFFER, size * sizeof(glm::mat4), offsetData, GL_STATIC_DRAW);

	// to send a mat4 it needs to be split up into 4 parts since you can only have a max size of 4
	for (int i = 0; i < 4; i++)
	{
		glVertexAttribPointer(
			4 + i,  // The attribute we want to configure
			4,					// size
			GL_FLOAT,           // type
			GL_FALSE,			// normalized?
			sizeof(glm::mat4),	// stride
			(const GLvoid*)(sizeof(glm::vec4) * i) // array buffer offset
		);
		glVertexAttribDivisor(4 + i, 1);
	}
	glBindVertexArray(0);
}*/

float sinev = 0.0f;
void Renderer::StartRender()
{
	// clear the buffers  
	// stencil buffer isn't being used right now, clearing so I don't forget later
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);// | GL_STENCIL_BUFFER_BIT);

	glUseProgram(program);
	glBindVertexArray(indexedVAO);

	SetViewMatrix(cameraPosition, cameraDirection, cameraUp);
	glm::mat4 projectionView = projectionMatrix * viewMatrix;
	glUniformMatrix4fv(shaderprojectionViewMatrixID, 1, GL_FALSE, &projectionView[0][0]);

	glUniformMatrix4fv(shaderViewMatrixID, 1, GL_FALSE, &viewMatrix[0][0]);

	
	
}



void Renderer::EndRender(bool32 doubleBuffered)
{
	glBindVertexArray(0);
	glUseProgram(0);

	// Swap buffers
	if (!doubleBuffered)
	{
		glFlush();
		glFinish();
	}
}

// could break render into 
// start render
// render objects 
// complete render
// for more flexibility
void Renderer::RenderInstanced(GLsizei vertexCount, void * index, GLsizei renderCount)
{
	/*glBindTexture(
	, textureObject);
	glDrawElementsInstanced(GL_TRIANGLES, 12*3, GL_UNSIGNED_SHORT, 0, renderCount);
	glBindTexture(GL_TEXTURE_2D, 0);*/
}



void Renderer::RenderIndexed(Instance *instanceList)
{
	//for (int i = 0; i < spawnWidth*spawnHeight; i++)
	for (int i = 0; i < 1; i++)
	{
		int index = instanceList[i].modelIndexNumber;
		glBindTexture(GL_TEXTURE_2D, modelList[index].textureObject);
		glUniformMatrix4fv(shaderTransformMatrixID, 1, GL_FALSE, &instanceList[i].transformMatrix[0][0]);
		glUniformMatrix4fv(shaderNormalTransformMatrixID, 1, GL_FALSE, &instanceList[i].normalTransformMatrix[0][0]);
		glDrawElements(GL_TRIANGLES, modelList[index].vertexCount, GL_UNSIGNED_SHORT, modelList[index].indexOffset);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
}



void Renderer::SetClearColour(float r, float g, float b, float a)
{
	glClearColor(r, g, b, a);
}

void Renderer::SetProjectionMatrix(glm::mat4 matrix)
{
	projectionMatrix = matrix;
}

int Renderer::GetModel(const char * imagepath, const char * texturepath)
{
	// let it send a nothing to texture so it loads a default texture

	//GLuint textureObject = loadBMP_custom(texturepath);
	//int index = modelList.size();
	//modelList.push_back(NewModel(imagepath, textureObject));
	//return index;
	return -1;
}

void Renderer::SetViewMatrix(glm::vec3 at, glm::vec3 eye, glm::vec3 up)
{
	//std::cout << up.x << " " << up.y << " " << up.z << std::endl;
	viewMatrix = glm::lookAt(
		at,		// camera is here
		eye,	// and looks here.
		up		// head is up 
	);

}

void Renderer::SetLightPosition(glm::vec3 position)
{
	//lightPosition = position;
	// transform the light to camera space 
	//glm::vec4 lightPosCameraSpace = viewMatrix * lightPosition;
	glUniform3fv(shaderLightPosID, 1, glm::value_ptr(position));
}

void Renderer::SetLightIntensity(glm::vec4 intensity)
{
	glUniform4fv(shaderLightIntensityID, 1, glm::value_ptr(intensity));
}

void Renderer::SetAmbientLightIntensity(glm::vec4 intensity)
{
	glUniform4fv(shaderAmbientLightIntensityID, 1, glm::value_ptr(intensity));
}

void Renderer::MoveCamera(glm::vec3 move)
{
	cameraPosition += move;
}

void Renderer::SetCameraPosition(glm::vec3 newPosition)
{
	cameraPosition = newPosition;
}

void Renderer::SetCameraDirection(glm::vec3 direction)
{
	cameraDirection = direction;
}

void Renderer::SetCameraUp(glm::vec3 up)
{
	cameraUp = up;
}
