#ifndef WindowApp_H
#define WindowApp_H



#include "common.h"

// this struct is the set of buttons needed in game 
// it will be defined once for keyboard and once for joypad
// keyboard one will have GLFW key codes 
// joypad one will have the GLFW button codes 
// 
struct game_buttons
{
	int start;
	int select;

	int up;
	int down;
	int left;
	int right;

	int action1;
	int action2;
	int action3;
	int action4;

	// L/R triggers

};

struct game_button_state
{
	bool32 pressed;
};

// if gamepad (analog) need to handle joystick
struct game_controller
{
	bool32 isAnalog;
	// union so I can easily loop through the buttons but also access them by name
	union
	{
		game_button_state Buttons[5];
		struct
		{
			game_button_state start;
			game_button_state select;
			game_button_state up;
			game_button_state down;
			game_button_state left;
			game_button_state right;
			game_button_state action1;
			game_button_state action2;
			game_button_state action3;
			game_button_state action4;
		};
	};
};

// move everything into here 
// it'll loop through the joypads 
// 
struct game_input
{
	// it should be safe to just always assume there's a keyboard because even if there isn't 
	// it'll just return false for checking any presses so it basically handles itself
	game_controller keyboard;
	game_controller joypads[4];
};


class WindowApp
{
public:
	WindowApp(int windowWidth, int windowHeight);
	
	void GetScreenCentre(float *halfX, float *halfY);

	void SetDoubleBuffering(bool32 state);
	bool32 GetDoubleBuffering() { return doubleBuffered; };
	void SetAASampleRate(int samples);

	void InitKeyboardGameKeys(game_buttons *gameKeys);
	void InitJoypadGameKeys(game_buttons * gameKeys);

	bool32 IsJoystickPresent();

	bool32 GetKeyState(int button);
	void ProcessInput(game_controller *controller, const game_buttons *buttons);

	bool KeyDown(game_button_state oldController, game_button_state newController);
	bool KeyUp(game_button_state oldController, game_button_state newController);
	bool KeyPress(game_button_state oldController, game_button_state newController);

	void SetCursorPos(double x, double y);
	void GetCursorPos(double *x, double *y);

	void PostWindowCloseMessage(bool32 state);
	bool32 WindowShouldClose();

	void SetTime(real32 time);
	real32 GetTime();

	void SwapBuffers();
	void Terminate();

private:
	int InitGLFW(int OGLversionMajor, int OGLversionMinor);
	int InitGLEW();
	
private:
	// generic to keep the glfw header in the cpp.
	// needs to be cast as (GLFWwindow*)
	size_t window;

	bool32 doubleBuffered;
	int aaSampleRate;
	
	int windowWidth;
	int windowHeight;



	//int monitorCount;
	//GLFWmonitor** monitors;
};

#endif