@ECHO off 
Setlocal EnableDelayedExpansion
rem need "Setlocal EnableDelayedExpansion" for ! 
rem "instructing the shell to expand the environment variable dynamically during execution."
rem http://superuser.com/questions/288255/windows-command-line-how-to-append-a-variable-in-a-loop

set entryFile=win32_main

rem 4189, 4700, 4701, 4701, 4100 are uninitialized/unused warning. may want it in the future 
rem WX -> treat warnings as errors
rem ^ can be used to break commands into multiple lines

rem warning flags I've removed before:  /wd4710  /wd4514 /wd4820 /wd4668 /wd4577 /wd4049 
set WarningFlags= /W4 /WX /wd4201 /wd4189 /wd4700 /wd4100 /wd4701 /wd4703
set LinkerWarningFlags= /ignore:4099 /NODEFAULTLIB:LIBCMT

rem path to the libraries
set libs= ..\..\libs\

set CommonLinkerFlags= -opt:ref -incremental:no user32.lib gdi32.lib shell32.lib opengl32.lib ^
%libs%glfw-3.2.1.bin.WIN32\lib-vc2015\glfw3.lib ^
%libs%glew-2.0.0\lib\Release\Win32\glew32s.lib ^
%libs%assimp--3.0.1270-sdk\lib\assimp_release-dll_win32\assimp.lib

set CommonCompilerFlags= -Zi -fp:fast -Gm- -GR- -Oi -nologo /EHsc /MD -DGLEW_STATIC=1 -DDEBUG_BUILD=1 -Dlassimpd

set IncludePaths= /I ..\ ^
/I ..\..\ ^
/I %libs% ^
/I %libs%glm-0.9.7.1\ ^
/I %libs%glew-2.0.0\include ^
/I %libs%glfw-3.2.1.bin.WIN32\include ^
/I %libs%assimp--3.0.1270-sdk\include

echo checking for assimp dll ...
IF NOT EXIST ..\build\Assimp32.dll (
	echo assimp dll not found in build directory, now retrieving
	xcopy %libs%assimp--3.0.1270-sdk\bin\assimp_release-dll_win32\Assimp32.dll ..\build\
) ELSE (
	echo assimp dll found in build directory
)

IF NOT EXIST ..\build mkdir ..\build
pushd ..\build

rem build the list of all cpp's needed for linking
set "cppnames="
for /R ..\code %%f in (*.cpp) do (
	if not "%%~nxf"=="%entryFile%.cpp" (
		set "cppnames=!cppnames!..\code\%%~nxf "
	)	
)
cl "..\code\%entryFile%.cpp" %cppnames% %IncludePaths% %WarningFlags% %CommonCompilerFlags% /link %LinkerWarningFlags% %CommonLinkerFlags% 

if "%~1"=="" (
    echo No params, build only
    GOTO DONE
) else (
    echo passed _%1_
    if "%1"=="run" (GOTO RUN) else (GOTO DONE)
)
:RUN
set PARAMS=
if not "%~2"=="" (
    :LOOP
    set PARAMS=%PARAMS% %2
    shift

    IF NOT "%~2"=="" (GOTO LOOP)
    echo window params: %PARAMS%
) else (echo no window params)
echo ...
"%entryFile%.exe" %PARAMS%
:DONE
echo batch script done
popd