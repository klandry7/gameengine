

#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormal_modelspace;
// 3 is index data  
layout(location = 4) in ivec4 BoneIDs;
layout(location = 5) in vec4 Weights;


//bone vars 
const int MAX_BONES = 100;
uniform mat4 gBones[MAX_BONES];


out vec2 UV;
out vec3 Position_worldspace;
out vec3 Normal_cameraspace;
out vec3 EyeDirection_cameraspace;
out vec3 LightDirection_cameraspace;

out mat4 MV;
out vec3 LightPosition_worldspace;


out vec3 ambientColor;
out vec3 specularColor;

uniform mat4 projectionViewMatrix;
uniform mat4 transformationMatrix; // aka the model matrix.
//uniform mat4 normalTransformMatrix;
// for lighting 
uniform mat4 viewMatrix; 


//uniform vec3 lightDirection; // for directional lights 
uniform vec3 lightPos;  
uniform vec4 lightIntensity;
uniform vec4 ambientLightIntensity;

void main()
{	
	mat4 BoneTransform = gBones[BoneIDs[0]] * Weights[0];
    BoneTransform += gBones[BoneIDs[1]] * Weights[1];
    BoneTransform += gBones[BoneIDs[2]] * Weights[2];
    BoneTransform += gBones[BoneIDs[3]] * Weights[3];
	//BoneTransform += gBones[BoneIDs[4]];// * Weights[4];
	//BoneTransform += gBones[BoneIDs[5]] * Weights[5];

	vec4 PosL = BoneTransform * vec4(vertexPosition_modelspace, 1.0);
	vec4 NormalL = BoneTransform * vec4(vertexNormal_modelspace, 0.0);
	vec3 vpm = PosL.xyz;
	vec3 npm = NormalL.xyz;
	//vec3 vpm = vertexPosition_modelspace;
	//vec3 npm = vertexNormal_modelspace;

	// the output position of the vertex in clip space. 
	gl_Position  = projectionViewMatrix * transformationMatrix * vec4(vpm,1.0);

	// the position of the vertex in world space
	Position_worldspace = (transformationMatrix * vec4(vpm,1.0)).xyz;

	// the vector that goes from the vertex to the camera in camera space 
	// in camera space, the camera is at the origin. the camera moves the world
	vec3 vertexPosition_cameraspace = (viewMatrix * transformationMatrix * vec4(vpm,1.0)).xyz;
	EyeDirection_cameraspace = vec3(0,0,0) - vertexPosition_cameraspace;

	// vector that goes from the vertex to the light in cam space. 
	vec3 LightPosition_cameraspace = (viewMatrix * vec4(lightPos,1.0)).xyz;
	LightDirection_cameraspace = LightPosition_cameraspace + EyeDirection_cameraspace;

	// normal of the vertex in camera space 
	// only correct if the model matrix doesn't sccale the model. need to use inverse transpose
	Normal_cameraspace = (viewMatrix * transformationMatrix * vec4(npm,0)).xyz;

	UV = vertexUV;
	
	LightPosition_worldspace = lightPos;
	MV = viewMatrix * transformationMatrix;


	ambientColor = ambientLightIntensity.xyz;
	specularColor = lightIntensity.xyz;
}

