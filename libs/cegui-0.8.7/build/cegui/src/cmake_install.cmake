# Install script for directory: P:/libs/cegui-0.8.7/cegui/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/cegui")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "P:/libs/cegui-0.8.7/build/lib/CEGUIBase-0_d.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "P:/libs/cegui-0.8.7/build/lib/CEGUIBase-0.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "P:/libs/cegui-0.8.7/build/lib/CEGUIBase-0.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "P:/libs/cegui-0.8.7/build/lib/CEGUIBase-0.lib")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "P:/libs/cegui-0.8.7/build/bin/CEGUIBase-0_d.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "P:/libs/cegui-0.8.7/build/bin/CEGUIBase-0.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "P:/libs/cegui-0.8.7/build/bin/CEGUIBase-0.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "P:/libs/cegui-0.8.7/build/bin/CEGUIBase-0.dll")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/cegui-0/CEGUI" TYPE FILE FILES
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Affector.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Animation.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/AnimationInstance.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/AnimationManager.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Animation_xmlHandler.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Base.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/BasicImage.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/BasicRenderedStringParser.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/BidiVisualMapping.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/BoundSlot.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/CEGUI.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/CentredRenderedString.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/ChainedXMLHandler.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Clipboard.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Colour.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/ColourRect.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/CompositeResourceProvider.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Config_xmlHandler.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/CoordConverter.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/DataContainer.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/DefaultLogger.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/DefaultRenderedStringParser.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/DefaultResourceProvider.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/DynamicModule.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Element.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Event.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/EventArgs.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/EventSet.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Exceptions.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/FactoryModule.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/FactoryRegisterer.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Font.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/FontGlyph.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/FontManager.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Font_xmlHandler.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/FormattedRenderedString.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/ForwardRefs.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/FreeFunctionSlot.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/FreeTypeFont.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/FribidiVisualMapping.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/FunctorCopySlot.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/FunctorPointerSlot.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/FunctorReferenceBinder.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/FunctorReferenceSlot.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/GUIContext.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/GUILayout_xmlHandler.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/GeometryBuffer.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/GlobalEventSet.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/IconvStringTranscoder.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Image.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/ImageCodec.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/ImageFactory.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/ImageManager.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/InjectedInputReceiver.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/InputEvent.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Interpolator.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/IteratorBase.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/JustifiedRenderedString.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/KeyFrame.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/LeftAlignedRenderedString.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/LinkedEvent.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Logger.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/MemberFunctionSlot.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/MemoryAllocatedObject.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/MemoryAllocation.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/MemoryOgreAllocator.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/MemorySTLWrapper.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/MemoryStdAllocator.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/MinibidiVisualMapping.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/MinizipResourceProvider.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/MouseCursor.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/NamedElement.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/NamedXMLResourceManager.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/PCRERegexMatcher.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/PixmapFont.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Property.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/PropertyHelper.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/PropertySet.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Quaternion.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Rect.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RefCounted.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RegexMatcher.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RenderEffect.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RenderEffectFactory.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RenderEffectManager.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RenderQueue.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RenderTarget.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RenderedString.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RenderedStringComponent.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RenderedStringImageComponent.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RenderedStringParser.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RenderedStringTextComponent.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RenderedStringWidgetComponent.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RenderedStringWordWrapper.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Renderer.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RenderingContext.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RenderingSurface.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RenderingWindow.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/ResourceProvider.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/RightAlignedRenderedString.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Scheme.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/SchemeManager.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Scheme_xmlHandler.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/ScriptModule.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/SimpleTimer.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Singleton.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Size.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/SlotFunctorBase.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/String.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/StringTranscoder.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/SubscriberSlot.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/System.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/SystemKeys.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/TextUtils.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Texture.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/TextureTarget.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/TplInterpolators.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/TplProperty.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/TplWRFactoryRegisterer.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/TplWindowFactory.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/TplWindowFactoryRegisterer.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/TplWindowProperty.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/TplWindowRendererFactory.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/TplWindowRendererProperty.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/TypedProperty.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/UDim.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Vector.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Vertex.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Win32StringTranscoder.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/Window.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/WindowFactory.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/WindowFactoryManager.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/WindowManager.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/WindowRenderer.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/WindowRendererManager.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/XMLAttributes.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/XMLHandler.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/XMLParser.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/XMLSerializer.h"
    "P:/libs/cegui-0.8.7/build/cegui/src/../include/CEGUI/Config.h"
    "P:/libs/cegui-0.8.7/build/cegui/src/../include/CEGUI/ModuleConfig.h"
    "P:/libs/cegui-0.8.7/build/cegui/src/../include/CEGUI/Version.h"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/cegui-0/CEGUI/widgets" TYPE FILE FILES
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/All.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/ButtonBase.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/ClippedContainer.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/ComboDropList.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/Combobox.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/DefaultWindow.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/DragContainer.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/Editbox.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/FrameWindow.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/GridLayoutContainer.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/GroupBox.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/HorizontalLayoutContainer.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/ItemEntry.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/ItemListBase.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/ItemListbox.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/LayoutCell.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/LayoutContainer.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/ListHeader.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/ListHeaderSegment.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/Listbox.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/ListboxItem.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/ListboxTextItem.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/MenuBase.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/MenuItem.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/Menubar.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/MultiColumnList.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/MultiLineEditbox.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/PopupMenu.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/ProgressBar.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/PushButton.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/RadioButton.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/ScrollablePane.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/Scrollbar.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/ScrolledContainer.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/ScrolledItemListBase.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/SequentialLayoutContainer.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/Slider.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/Spinner.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/TabButton.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/TabControl.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/Thumb.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/Titlebar.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/ToggleButton.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/Tooltip.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/Tree.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/TreeItem.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/widgets/VerticalLayoutContainer.h"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/cegui-0/CEGUI/falagard" TYPE FILE FILES
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/ComponentBase.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/Dimensions.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/Enums.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/EventAction.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/EventLinkDefinition.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/FalagardPropertyBase.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/FormattingSetting.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/FrameComponent.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/ImageryComponent.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/ImagerySection.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/LayerSpecification.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/NamedArea.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/NamedDefinitionCollator.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/PropertyDefinition.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/PropertyDefinitionBase.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/PropertyInitialiser.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/PropertyLinkDefinition.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/SectionSpecification.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/StateImagery.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/TextComponent.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/WidgetComponent.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/WidgetLookFeel.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/WidgetLookManager.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/XMLEnumHelper.h"
    "P:/libs/cegui-0.8.7/cegui/src/../include/CEGUI/falagard/XMLHandler.h"
    )
endif()

