#version 330

layout(location = 0) in vec3 VertexPosition;
layout(location = 1) in vec2 VertexTexCoord;

uniform mat4 transform;

out vec2 texCoord0;

void main()
{
    gl_Position = transform * vec4(VertexPosition, 1);
    texCoord0 = VertexTexCoord;
}