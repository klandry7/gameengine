#version 330

in vec2 texCoord0;

out vec4 OutFragColor;

uniform sampler2D sampler;
uniform vec3 ambientLight;
uniform vec3 baseColor;

void main()
{
	vec4 totalLight = ambientLight;
	vec4 color = vec4(baseColor, 1);
	OutFragColor = color * totalLight;
   //vec4(0.0, 0.0, 1.0, 1.0);// OutFragColor = texture2D(sampler, texCoord0.xy);//texture(InTexture, TexCoord0);// * InColor;
}