#include "Texture.h"
#include <stdio.h>

Texture::Texture(GLuint _id)
{
	id = _id;
	printf("%i\n", id);
}

GLuint Texture::GetId()
{
	return id;
}

void Texture::BindTexture()
{
	//glActiveTexture(GL_TEXTURE0 );
	glBindTexture(GL_TEXTURE_2D, id);
}

Texture::~Texture()
{
}
