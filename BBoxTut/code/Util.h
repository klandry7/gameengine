#ifndef UTIL_H
#define UTIL_H

#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Vertex.h"

class Util
{
public:
	Util();
	~Util();

	static std::vector<glm::vec3> CreateFlippedBuffer(Vertex *vertices, int length);


private:

};


#endif // !UTIL_H
