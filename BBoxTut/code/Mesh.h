#ifndef MESH_H
#define MESH_H

//#define GLFW_OPENGL_PROFILE 1

// Include GLEW
#include <GL/glew.h>

#include "Vertex.h"

#include <vector>

class Mesh
{
public:
	Mesh(); 
	Mesh(std::vector<glm::vec3> data, std::vector<glm::vec2> texCoords, std::vector<glm::vec3> normals, std::vector<GLshort> indices);
	void Init();
	~Mesh();

	void AddVertices(std::vector<glm::vec3> data, std::vector<glm::vec2> texCoords, std::vector<glm::vec3> normals, std::vector<GLshort> indices);

	void Draw();

private:
	// vertex buffer object
	GLuint vertexBufferObject;
	// texture coordinate buffer object
	GLuint texCoordBufferObject;
	// vertex normal buffer object
	GLuint vertexNormalBufferObject;
	// index buffer object
	GLuint indexBufferObject;
	// vertex array object
	GLuint vertexArrayObject;
	GLshort vertexCount;


	//GLuint textureID;
};



#endif