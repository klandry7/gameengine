#include "Util.h"
#include "common.h"

std::vector<glm::vec3> Util::CreateFlippedBuffer(Vertex *vertices, int length)
{
	std::vector <glm::vec3> buffer;

	for (int i = 0; i < length; i++)
	{
		buffer.push_back(vertices[i].GetPos());
	}


	return buffer;
}