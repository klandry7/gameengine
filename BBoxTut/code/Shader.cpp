#include "Shader.h"
#include <stdio.h>
#include <vector>

#include <glm/gtc/type_ptr.hpp>

// for file reading
#include <fstream>
#include <sstream>
#include <string>

Shader::Shader(const char * nom)
{
	shaderName = nom;
	program = glCreateProgram();

	if (program == 0)
	{
		printf("shader creation failed");
	}
}

Shader::~Shader()
{
}

void Shader::AddVertexShader(const char * text)
{
	AddProgram(text, GL_VERTEX_SHADER);
}

void Shader::AddFragmentShader(const char * text)
{
	AddProgram(text, GL_FRAGMENT_SHADER);
}

void Shader::AddGeometryShader(const char * text)
{
	AddProgram(text, GL_GEOMETRY_SHADER);
}

void Shader::CompileShader()
{
	glLinkProgram(program);

	GLint isLinked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		//The maxLength includes the NULL character
		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);
		printf("%s\n", &infoLog[0]);
	}
	else
	{
		printf("program for shader [%s] linked\n", shaderName);
	}

	// gether all the uniforms so you don't have to add them manually
	int total = -1;
	glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &total);
	for (int i = 0; i < total; i++)
	{
		int nameLen = -1, num = -1;
		GLenum type = GL_ZERO;
		char uniformName[100];
		glGetActiveUniform(program, GLuint(i), sizeof(uniformName) - 1, &nameLen, &num, &type, uniformName);
		uniformName[nameLen] = 0;
		GLuint location = glGetUniformLocation(program, uniformName);
		uniforms.insert(std::make_pair(uniformName, location));
		printf("uniform [%s] detected and added to list for shader [%s] \n", uniformName, shaderName);
	}
}

void Shader::Bind()
{
	glUseProgram(program);
}

// pass in a list of string / value pairs ? 
// update all by iterating over uniforms
/*
void Shader::UpdateUniforms()
{

}
*/

void Shader::SetUniformi(const char * uniformName, int value)
{
	glUniform1i(uniforms[uniformName], value);
}

void Shader::SetUniformf(const char * uniformName, float value)
{
	glUniform1f(uniforms[uniformName], value);
}

void Shader::SetUniform3fv(const char * uniformName, glm::vec3 value)
{
	glUniform3fv(uniforms[uniformName], 1, glm::value_ptr(value));
}

void Shader::SetUniform4fv(const char * uniformName, glm::mat4 value)
{
	glUniformMatrix4fv(uniforms[uniformName], 1, GL_FALSE, &value[0][0]);
}

void Shader::AddProgram(const char * fp, GLuint type)
{
	std::string shaderCode;
	std::ifstream shaderStream(fp, std::ios::in);
	if (shaderStream.is_open())
	{
		std::string line = "";
		while (getline(shaderStream, line))
		{
			shaderCode += line + "\n";
		}
		shaderStream.close();		
	}
	else
	{
		printf("failed to open file");
		getchar();
		return;
	}
	const char * text = shaderCode.c_str();
	

	GLuint shader = glCreateShader(type);

	if (shader == 0)
	{
		printf("shader creation failed\n");
	}

	//printf("[%s]\n", text);

	glShaderSource(shader, 1, &text, NULL);
	glCompileShader(shader);

	// Check Shader status
	GLint result = GL_FALSE;
	int infoLogLength;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0 || result == GL_FALSE)
	{
		std::vector<char> ShaderErrorMessage(infoLogLength + 1);
		glGetShaderInfoLog(shader, infoLogLength, NULL, &ShaderErrorMessage[0]);
		printf("shader [%s] error\n %s\n", shaderName, &ShaderErrorMessage[0]);
	}
	else
	{
		//printf("shader added to program success\n");
	}

	glAttachShader(program, shader);
}	