#include "Mesh.h"
#include "common.h"
#include "Util.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <stdio.h>



// I should have this store all the vertex information or something so you can request a model
// and it will pass you the needed info instead of loading the same stuff multiple times

Mesh::Mesh()
{
	Init();
}

Mesh::Mesh(std::vector<glm::vec3> data, std::vector<glm::vec2> texCoords, std::vector<glm::vec3> normals, std::vector<GLshort> indices)
{
	Init();
	AddVertices(data, texCoords, normals, indices);
}

void Mesh::Init()
{
	glGenVertexArrays(1, &vertexArrayObject);
	glBindVertexArray(vertexArrayObject);


	// vertex buffer
	glGenBuffers(1, &vertexBufferObject);
	glEnableVertexAttribArray(0);
	// texcoord buffer
	glGenBuffers(1, &texCoordBufferObject);
	glEnableVertexAttribArray(1);
	// vertex normals buffer
	glGenBuffers(1, &vertexNormalBufferObject);
	glEnableVertexAttribArray(2);
	// index buffer
	glGenBuffers(1, &indexBufferObject);
	glEnableVertexAttribArray(3);

	glBindVertexArray(0);

	vertexCount = 0;
}

Mesh::~Mesh()
{

}

void Mesh::AddVertices(std::vector<glm::vec3> vertices, std::vector<glm::vec2> texCoords, std::vector<glm::vec3> normals, std::vector<GLshort> indices)
{
	vertexCount = (GLshort)indices.size();

	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, texCoordBufferObject);
	glBufferData(GL_ARRAY_BUFFER, texCoords.size() * sizeof(glm::vec2), &texCoords[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, vertexNormalBufferObject);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLshort), &indices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	
	glBindVertexArray(vertexArrayObject);

	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0);

	glBindBuffer(GL_ARRAY_BUFFER, texCoordBufferObject);
	glVertexAttribPointer(
		1,
		2,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0);

	
	glBindBuffer(GL_ARRAY_BUFFER, vertexNormalBufferObject);
	glVertexAttribPointer(
		2,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
	glVertexAttribPointer(
		3,
		3,                         
		GL_FLOAT,
		GL_FALSE,                    
		0,                          
		(void*)0                  
	);

	glBindVertexArray(0);

	//printf("%i", length);
	/*for (int i = 0; i < length; i++)
	{
		glm::vec3 tempVert = temp[i];
		printf("%f %f %f\n", tempVert.x, tempVert.y, tempVert.z);
	}*/
	

	/*

	const char * filename = "../models/colourMap.bmp";

	printf("Reading image %s\n", filename);

	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file;
	fopen_s(&file, filename, "rb");
	if (!file) { printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", filename);}

	// Read the header, i.e. the 54 first bytes

	// If less than 54 bytes are read, problem
	if (fread(header, 1, 54, file) != 54) {
		printf("Not a correct BMP file\n");
	}
	// A BMP files always begins with "BM"
	if (header[0] != 'B' || header[1] != 'M') {
		printf("Not a correct BMP file\n");
	}
	// Make sure this is a 24bpp file
	if (*(int*)&(header[0x1E]) != 0) { printf("Not a correct BMP file\n"); }
	if (*(int*)&(header[0x1C]) != 24) { printf("Not a correct BMP file\n"); }

	// Read the information about the image
	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);

	// Some BMP files are misformatted, guess missing information
	if (imageSize == 0)    imageSize = width*height * 3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos == 0)      dataPos = 54; // The BMP header is done that way

										 // Create a buffer
	data = new unsigned char[imageSize];

	// Read the actual data from the file into the buffer
	fread(data, 1, imageSize, file);

	// Everything is in memory now, the file wan be closed
	fclose(file);


	// Create one OpenGL texture

	glGenTextures(1, &textureID);

	GLuint boundTexture = 0;
	glGetIntegerv(GL_TEXTURE_BINDING_2D, (GLint*)&boundTexture);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	// OpenGL has now copied the data. Free our own version
	delete[] data;

	// Poor filtering, or ...
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 

	// ... nice trilinear filtering.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, boundTexture);
	glBindTexture(GL_TEXTURE_2D, 0);

	printf("Reading image %s successful\n", filename);

	// Return the ID of the texture we just created
	//return ;

	*/


}

void Mesh::Draw()
{

	//glEnableVertexAttribArray(0);

	glBindVertexArray(vertexArrayObject);

	//glBindTexture(GL_TEXTURE_2D, textureID);

	glDrawElements(GL_TRIANGLES, vertexCount, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	//glBindTexture(GL_TEXTURE_2D, 0);
	//glDisableVertexAttribArray(0);
}

