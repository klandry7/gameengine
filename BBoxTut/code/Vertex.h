#ifndef VERTEX_H
#define VERTEX_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Vertex
{
public:
	Vertex(glm::vec3 pos);
	~Vertex();

	glm::vec3 GetPos();
	void SetPos(glm::vec3 pos);

public:
	const static int SIZE = 3;

private:
	glm::vec3 pos;
};



#endif