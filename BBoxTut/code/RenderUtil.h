#ifndef RENDERUTIL_H
#define RENDERUTIL_H

class RenderUtil
{
public:
	RenderUtil();
	~RenderUtil();

	void ClearScreen();
	void InitGraphics();
	void EnableTextures(bool state);

	char * GetOpenGLVersion();

private:

};


#endif