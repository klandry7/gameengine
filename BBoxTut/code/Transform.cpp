#include "Transform.h"

#include <glm/gtc/matrix_transform.hpp>

#include <stdio.h>

Transform::Transform()
{
	translation = glm::vec3(0.0f, 0.0f, 0.0f);
	rotation = glm::vec3(0.0f, 0.0f, 0.0f);
	scale = glm::vec3(1.0f, 1.0f, 1.0f);
}

Transform::~Transform()
{
}

glm::vec3 Transform::GetTranslation()
{
	return translation;
}
void Transform::SetTranslation(glm::vec3 t)
{
	translation = t;
}
void Transform::SetTranslation(float x, float y, float z)
{
	translation = glm::vec3(x, y, z);
}

glm::vec3 Transform::GetRotation()
{
	return rotation;
}
void Transform::SetRotation(glm::vec3 r)
{
	rotation = r;
}
void Transform::SetRotation(float x, float y, float z)
{
	rotation = glm::vec3(x, y, z);
}

glm::vec3 Transform::GetScale()
{
	return scale;
}
void Transform::SetScale(glm::vec3 r)
{
	scale = r;
}
void Transform::SetScale(float x, float y, float z)
{
	scale = glm::vec3(x, y, z);
}

Camera * Transform::GetCamera()
{
	return camera;
}
void Transform::SetCamera(Camera * cam)
{
	//camera = new Camera(cam->GetPos(), cam->GetForward(), cam->GetUp());
	camera->SetForward(cam->GetForward());
	camera->SetUp(cam->GetUp());
	camera->SetPos(cam->GetPos());
}

glm::mat4 Transform::GetTransformation()
{
	glm::mat4 translationMatrix = glm::translate(glm::mat4(1.0f), GetTranslation());
	glm::mat4 rotationMatrix = glm::rotate(translationMatrix, GetRotation().z, glm::vec3(0.0f, 0.0f, 1.0f));
	rotationMatrix = glm::rotate(rotationMatrix, GetRotation().x, glm::vec3(1.0f, 0.0f, 0.0f));
	rotationMatrix = glm::rotate(rotationMatrix, GetRotation().y, glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0f), GetScale());

	return rotationMatrix * scaleMatrix;
}

glm::mat4 Transform::GetProjectedTransformation()
{
	glm::mat4 projMat = glm::perspective(fov, width / height, zNear, zFar);
	glm::mat4 viewMat = glm::lookAt(
		GetCamera()->GetPos(), 
		GetCamera()->GetPos() + GetCamera()->GetForward(),
		GetCamera()->GetUp());

	/*printf("pos <%f, %f, %f>\n forward <%f, %f, %f>\n up <%f, %f, %f>\n\n",
		GetCamera()->GetPos().x, GetCamera()->GetPos().y, GetCamera()->GetPos().z,
		GetCamera()->GetForward().x, GetCamera()->GetForward().y, GetCamera()->GetForward().z,
		GetCamera()->GetUp().x, GetCamera()->GetUp().y, GetCamera()->GetUp().z);*/

	return projMat * viewMat * GetTransformation();
}

void Transform::SetProjection(float _fov, float _width, float _height, float _zNear, float _zFar)
{
	fov = _fov;
	width = _width;
	height = _height;
	zNear = _zNear;
	zFar = _zFar;
}

float Transform::zNear = 0.1f;
float Transform::zFar = 100;
float Transform::width = 4;
float Transform::height = 3;
float Transform::fov = 45;

Camera * Transform::camera = new Camera();