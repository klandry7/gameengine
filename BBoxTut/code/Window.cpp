#include "Window.h"


#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>


//  debug only
#include <iostream>


void Window::PostWindowCloseMessage(bool state)
{
	glfwSetWindowShouldClose((GLFWwindow*)window, state);
}

bool32 Window::WindowShouldClose()
{
	return glfwWindowShouldClose((GLFWwindow*)window);
}

void Window::SetTime(real32 time)
{
	glfwSetTime((real64)time);
}

real32 Window::GetTime()
{
	return (real32)glfwGetTime();
}

void Window::Terminate()
{
	glfwTerminate();
}

void Window::SetMouseState(bool32 show, bool32 restrictToWindow)
{
	if (show)
	{
		glfwSetInputMode((GLFWwindow*)window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
	else if(!restrictToWindow)
	{
		glfwSetInputMode((GLFWwindow*)window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	}
	else if (restrictToWindow)
	{
		glfwSetInputMode((GLFWwindow*)window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	}
	

}


// check if the provided window handle is valid and create its OpenGL context


int Window::InitGLEW()
{
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (err != GLEW_OK) {
		std::cout << "Glew Error: -> " << glewGetErrorString(err) << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}
	return 0;
}


Window::Window(int _windowWidth, int _windowHeight, const char * windowName)
{
	if (InitGLFW(3, 3) != 0)
	{
		fprintf(stderr, "Failed to init GLFW.\n");
		getchar();
		exit(0);
	}

	windowWidth = _windowWidth;
	windowHeight = _windowHeight;

	//monitors = glfwGetMonitors(&monitorCount);

	// for fullscreen put this in first null -> glfwGetPrimaryMonitor()
	window = (size_t)glfwCreateWindow(windowWidth, windowHeight, windowName, NULL, NULL);

	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window.\n");
		getchar();
		glfwTerminate();
		exit(1);
	}
	glfwMakeContextCurrent((GLFWwindow*)window);


	// setup glew
	if (InitGLEW() == -1)
	{
		exit(1);
	}

	// Ensure we can capture the input
	glfwSetInputMode((GLFWwindow*)window, GLFW_STICKY_KEYS, GL_TRUE);


}

void Window::GetScreenCentre(float * halfX, float *halfY)
{
	*halfX = windowWidth * 0.5f;
	*halfY = windowHeight * 0.5f;
}

void Window::SetDoubleBuffering(bool state)
{
	glfwWindowHint(GLFW_DOUBLEBUFFER, state);
	doubleBuffered = state;

	if (doubleBuffered == false)
	{
		glDrawBuffer(GL_FRONT);
		glReadBuffer(GL_FRONT);
	}
	else
	{
		glDrawBuffer(GL_BACK);
		glReadBuffer(GL_BACK);
	}
}

void Window::SetAASampleRate(int samples)
{
	// can't anti alias without double buffering on.
	glfwWindowHint(GLFW_SAMPLES, samples);
	aaSampleRate = samples;
}


int Window::InitGLFW(int OGLversionMajor, int OGLversionMinor)
{
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		getchar();
		return -1;
	}


	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, OGLversionMajor);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, OGLversionMinor);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_FALSE);

	SetDoubleBuffering(true);
	SetAASampleRate(4);

	glfwWindowHint(GLFW_DEPTH_BITS, 16);
	glfwWindowHint(GLFW_RED_BITS, 8);
	glfwWindowHint(GLFW_GREEN_BITS, 8);
	glfwWindowHint(GLFW_BLUE_BITS, 8);
	glfwWindowHint(GLFW_ALPHA_BITS, 8);

	glfwSwapInterval(1);
	glfwSetTime(0.0);

	return 0;
}



// this signifies the end of a frame. 
void Window::SwapBuffers()
{
	if (doubleBuffered)
	{
		glfwSwapBuffers((GLFWwindow*)window);
	}
	else
	{
		glFlush();
		glFinish();
	}
}

void Window::PollEvents()
{
	glfwPollEvents();
}
