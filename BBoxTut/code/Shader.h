#ifndef SHADER_H
#define SHADER_H

//#define GLFW_OPENGL_PROFILE 1

#include <GL/glew.h>

// all this just for the "map" type. 
// should replace it with my own dictionary implementation or something
#include <cstdlib>
#include <cctype>
#include <map> 
#include <list>


#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


class Shader
{
public:
	Shader(const char *nom);
	~Shader();

	void AddVertexShader(const char * text);
	void AddFragmentShader(const char * text);
	void AddGeometryShader(const char * text);

	void CompileShader();
	void Bind();

	//void UpdateUniforms();

	void SetUniformi(const char * uniformName, int value);
	void SetUniformf(const char * uniformName, float value);
	void SetUniform3fv(const char * uniformName, glm::vec3 value);
	void SetUniform4fv(const char * uniformName, glm::mat4 value);

private:
	void AddProgram(const char * fp, GLuint type);

private:
	GLuint program;
	const char *shaderName;
	std::map<const char *, GLint > uniforms;

};

#endif // !SHADER_H
