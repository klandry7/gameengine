#include "Vertex.h"


Vertex::Vertex(glm::vec3 _pos)
{
	pos = _pos;
}

Vertex::~Vertex()
{
}

glm::vec3 Vertex::GetPos()
{
	return pos;
}

void Vertex::SetPos(glm::vec3 _pos)
{
	pos = _pos;
}
