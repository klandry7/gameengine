#ifndef MATERIAL_H
#define MATERIAL_H

#include <glm/glm.hpp>
#include "Texture.h"

// bbox part 22

class Material
{
public:
	Material();
	~Material();

private:
	glm::vec3 color;
	Texture texture;
};

Material::Material()
{
}

Material::~Material()
{
}

#endif // !MATERIAL_H
