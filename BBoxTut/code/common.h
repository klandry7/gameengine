
// debug
#include <assert.h>

#include <stdint.h> 

typedef int8_t int8;		//  8-bit int. equivalent to unsigned char
typedef int16_t int16;		//  16-bit int. equivalent to unsigned char
typedef int32_t int32;		//  32-bit int. equivalent to unsigned char
typedef int64_t int64;		//  64-bit int. equivalent to unsigned char

typedef uint8_t uint8;		// unsigned 8-bit int. equivalent to unsigned char
typedef uint16_t uint16;	// unsigned 16-bit int. equivalent to unsigned char
typedef uint32_t uint32;	// unsigned 32-bit int. equivalent to unsigned char
typedef uint64_t uint64;	// unsigned 64-bit int. equivalent to unsigned char

typedef int32_t bool32; // lets us check 0/1 as well as true/false without having to suppress warning C4800

typedef float real32;	// 32 bit wide
typedef double real64;	// 64 bit wide

#define ARRAY_COUNT( array ) (sizeof( array ) / (sizeof( array[0] ) * (sizeof( array ) != sizeof(void*) || sizeof( array[0] ) <= sizeof(void*))))
//#define ARRAY_COUNT(x) ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))