#ifndef RESOURCELOADER_H
#define RESOURCELOADER_H

#include "Mesh.h"
#include <string>
#include <glm/glm.hpp>
#include "Texture.h"

class ResourceLoader
{
public:
	ResourceLoader();
	~ResourceLoader();

	//char * LoadShader(const char * fileName);



	std::string ReadFile(const char * filePath);

	Texture * LoadTexture(const char * filename);

	Mesh * LoadMesh(const char * filename);

private:

};



#endif 


