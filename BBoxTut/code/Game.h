#ifndef GAME_H
#define GAME_H

#include "Mesh.h"
#include "Shader.h"
#include "Transform.h"
#include "Window.h"
#include "ResourceLoader.h"
#include "Camera.h"
#include "Texture.h"

class Game
{
public:
	Game();
	~Game();
	 
	void Input(Window *window);
	void Update(float dt);
	void Render();

private:
	Mesh * mesh;
	Texture * texture;
	Shader * shader;
	Shader * phongShader;
	Transform * transform;

	ResourceLoader * resourceLoader;

	Camera * camera;

	float sinValue=0;

};


#endif
