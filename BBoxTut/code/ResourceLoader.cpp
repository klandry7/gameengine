#include "ResourceLoader.h"
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>

ResourceLoader::ResourceLoader()
{
}

ResourceLoader::~ResourceLoader()
{
}

/*const char * ResourceLoader::LoadShader(const char * filePath)
{
	std::string shaderCode;
	std::ifstream shaderStream(filePath, std::ios::in); 
	if (shaderStream.is_open())
	{
		std::string line = "";
		while (getline(shaderStream, line))
		{
			shaderCode += line + "\n";
		}
		shaderStream.close();

		const char * source = shaderCode.c_str();
		return source;
	}
	else
	{
		printf("failed to open file");
		getchar();
		return NULL;
	}
}*/

std::string ResourceLoader::ReadFile(const char *filePath)
{
	std::string shaderCode;
	std::ifstream shaderStream(filePath, std::ios::in);
	if (shaderStream.is_open())
	{
		std::string line = "";
		while (getline(shaderStream, line))
		{
			shaderCode += "\n" + line;
		}
		shaderStream.close();
		return shaderCode;
	}
	else
	{
		printf("failed to open file");
		getchar();
		return NULL;
	}
}

Texture * ResourceLoader::LoadTexture(const char * filename)
{
	printf("Reading image %s\n", filename);

	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file;
	fopen_s(&file, filename, "rb");
	if (!file) { printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", filename); getchar(); return 0; }

	// Read the header, i.e. the 54 first bytes

	// If less than 54 bytes are read, problem
	if (fread(header, 1, 54, file) != 54) {
		printf("Not a correct BMP file\n");
		return 0;
	}
	// A BMP files always begins with "BM"
	if (header[0] != 'B' || header[1] != 'M') {
		printf("Not a correct BMP file\n");
		return 0;
	}
	// Make sure this is a 24bpp file
	if (*(int*)&(header[0x1E]) != 0) { printf("Not a correct BMP file\n");    return 0; }
	if (*(int*)&(header[0x1C]) != 24) { printf("Not a correct BMP file\n");    return 0; }

	// Read the information about the image
	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);

	// Some BMP files are misformatted, guess missing information
	if (imageSize == 0)    imageSize = width*height * 3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos == 0)      dataPos = 54; // The BMP header is done that way

											// Create a buffer
	data = new unsigned char[imageSize];

	// Read the actual data from the file into the buffer
	fread(data, 1, imageSize, file);

	// Everything is in memory now, the file wan be closed
	fclose(file);

	

	/*

	// Create one OpenGL texture
	GLuint textureID;
	glGenTextures(1, &textureID);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	// OpenGL has now copied the data. Free our own version
	delete[] data;

	// Poor filtering, or ...
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 

	// ... nice trilinear filtering.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);

	printf("Reading image %s successful\n", filename);

	// Return the ID of the texture we just created
	//return ;
	
	*/


	GLuint texture;
	glGenTextures(1, &texture);

	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glGenerateMipmap(GL_TEXTURE_2D);
	//glBindTexture(GL_TEXTURE_2D, 0);

	return new Texture(texture);
}

/*typedef struct
{
	glm::vec3 vertex;
	glm::vec2 texCoord;
	glm::vec3 normal;
}FullVertex;*/

Mesh * ResourceLoader::LoadMesh(const char * filename)
{
	// should be just sending file name and appending it to the resource folder
	// should also have a virtual file system where you can mount directories

	// raw data gotten from parsing the obj file
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> texCoords;
	std::vector<glm::vec3> normals;
	std::vector<GLshort> vertexIndices;
	std::vector<GLshort> texCoordIndices;
	std::vector<GLshort> normalIndices;

	// the data formatted properly to be sent to opengl
	std::vector<glm::vec3> outVertices;
	std::vector<glm::vec2> outTexCoords;
	std::vector<glm::vec3> outNormals;
	std::vector<GLshort> outIndices;

	FILE * file;
	fopen_s(&file, filename, "r");
	if (file == NULL)
	{
		printf("cannot open file [%s]\n", filename);
		getchar();
		return new Mesh();
	}


	while (1)
	{
		char lineHeader[128];
		// read the first word of the line
		int res = fscanf_s(file, "%s", lineHeader, _countof(lineHeader));
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

		if (strcmp(lineHeader, "v") == 0)
		{
			glm::vec3 vertex;
			fscanf_s(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader, "vt") == 0)
		{
			glm::vec2 uv;
			fscanf_s(file, "%f %f\n", &uv.x, &uv.y);
			//uv.y = -uv.y; // Invert V coordinate if using DDS texture, which are inverted. Remove if you want to use TGA or BMP loaders.
			texCoords.push_back(uv);
		}
		else if (strcmp(lineHeader, "vn") == 0)
		{
			glm::vec3 normal;
			fscanf_s(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0)
		{
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf_s(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9)
			{
				printf("File face format is incorrect for file [%s]\n", filename);
				return new Mesh();
			}

			vertexIndices.push_back((GLshort)(vertexIndex[0] ));
			vertexIndices.push_back((GLshort)(vertexIndex[1] ));
			vertexIndices.push_back((GLshort)(vertexIndex[2] ));

			texCoordIndices.push_back((GLshort)(uvIndex[0] ));
			texCoordIndices.push_back((GLshort)(uvIndex[1] ));
			texCoordIndices.push_back((GLshort)(uvIndex[2] ));

			normalIndices.push_back((GLshort)(normalIndex[0] ));
			normalIndices.push_back((GLshort)(normalIndex[1] ));
			normalIndices.push_back((GLshort)(normalIndex[2] ));
		}
	}

	unsigned int counter = 0;

	// For each vertex of each triangle
	for (unsigned int i = 0; i<vertexIndices.size(); i++) 
	{
		// Get the indices of its attributes
		unsigned int vertexIndex = vertexIndices[i];
		unsigned int uvIndex = texCoordIndices[i];
		unsigned int normalIndex = normalIndices[i];

		// Get the attributes thanks to the index
		glm::vec3 vertex = vertices[vertexIndex-1];
		glm::vec2 uv = texCoords[uvIndex-1];
		glm::vec3 normal = normals[normalIndex-1];

		// try to find if the attribute has already been added
		// find is immensely slow, faster to just shove all the information.
		// but that's also bad on memory. oh what to do.
		//unsigned int hv = std::find(outVertices.begin(), outVertices.end(), vertex) - outVertices.begin();
		//unsigned int ht = std::find(outTexCoords.begin(), outTexCoords.end(), uv) - outTexCoords.begin();
		//unsigned int hn = std::find(outNormals.begin(), outNormals.end(), normal) - outNormals.begin();
		
		// if the vertex is a duplicate
		/*if (hv < outVertices.size() && hv < outTexCoords.size() && hv < outNormals.size() && hv == ht && hn == ht)
		{
			printf("dup %i\n", i);
			outIndices.push_back((GLshort)hv);
		}
		else
		{*/
			// Put the attributes in buffers
			outVertices.push_back(vertex);
			outTexCoords.push_back(uv);
			outNormals.push_back(normal);

			//outIndices.push_back((GLshort)(outVertices.size() -1 ));
			//outIndices.push_back((GLshort)(counter++));
			outIndices.push_back((GLshort)(i));
		//}
	}

	/*for (unsigned int i = 0; i < outTexCoords.size(); i++)
	{
		printf("%f, %f\n", outTexCoords[i].x, outTexCoords[i].y);
	}*/


	return new Mesh(outVertices, outTexCoords, outNormals, outIndices);
}

