#ifndef MAINCOMPONENT_H
#define MAINCOMPONENT_H

#include "Window.h"
#include "Game.h"
#include "RenderUtil.h"

class MainComponent
{
public:
	MainComponent();

	void Start();
	void Stop();

private:
	void Run();
	void Render();
	void Cleanup();


private:
	bool32 running;
	Window * window;
	Game * game;
	RenderUtil * renderutil;

	real32 lastTime;
	real32 currentTime;
	real32 dt;

	// go back to part 2 to actually implement this stuff
	real32 unprocessedTime;
	real32 startTime;
};


#endif