#ifndef INPUT_H
#define INPUT_H

#include "common.h"




enum GAME_BUTTONS
{
	start,
	select,

	up,
	down,
	left,
	right,

	action1,
	action2,
	action3,
	action4
};

enum INPUTS
{
	keyboard,
	joypad1,
	joypad2,
	joypad3,
	joypad4
};

enum BUTTON_STATE
{
	pressed,
	released,
	heldDown
};

// this struct is the set of buttons needed in game 
// it will be defined once for keyboard and once for joypad
// keyboard one will have GLFW key codes 
// joypad one will have the GLFW button codes 
struct game_buttons
{
	int start;
	int select;

	int up;
	int down;
	int left;
	int right;

	int action1;
	int action2;
	int action3;
	int action4;

	// L/R triggers

};

class Input
{
	// all the structs needed for the input class
private:

	struct game_button_state
	{
		bool32 pressed;
	};

	// if gamepad (analog) need to handle joystick
	struct game_controller
	{
		bool32 isAnalog;
		// union so I can easily loop through the buttons but also access them by name
		union
		{
			game_button_state Buttons[5];
			struct
			{
				game_button_state start;
				game_button_state select;
				game_button_state up;
				game_button_state down;
				game_button_state left;
				game_button_state right;
				game_button_state action1;
				game_button_state action2;
				game_button_state action3;
				game_button_state action4;
			};
		};
	};

	struct game_input
	{
		// it should be safe to just always assume there's a keyboard because even if there isn't 
		// it'll just return false for checking any presses so it basically handles itself
		game_controller keyboard;
		game_controller joypads[4];
	};

public:
	Input();
	~Input();


	void UpdateInput(size_t window);
	void SwapInput();

	
	void InitKeyboardGameKeys(game_buttons * gameKeys);
	void InitJoypadGameKeys(game_buttons * gameKeys);

	bool32 IsJoystickPresent();

	
	bool GetButtonState(GAME_BUTTONS button, INPUTS controller, BUTTON_STATE state);
	
private:
	bool32 GetKeyState(int button, size_t window);

	
	void ProcessInput(game_controller * controller, const game_buttons * buttons, size_t window);

	bool ButtonStateSelector(game_button_state oldController, game_button_state newController, BUTTON_STATE state);
	bool ButtonHeldDown(game_button_state oldController, game_button_state newController);
	bool ButtonReleased(game_button_state oldController, game_button_state newController);
	bool ButtonPressed(game_button_state oldController, game_button_state newController);


public:
	game_buttons keyboardKeys;
	game_buttons joypadButtons[4];

private:
	game_controller currentKeyboardController;
	game_controller previousKeyboardController;

	game_controller currentJoypadControllers[4];
	game_controller previousJoypadControllers[4];
};

namespace inputNamespace
{
	extern Input * input;
};


#endif