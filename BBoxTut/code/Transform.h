#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <glm/glm.hpp>
#include "Camera.h"

class Transform
{
public:
	Transform();
	~Transform();

	glm::vec3 GetTranslation();
	void SetTranslation(glm::vec3 t);
	void SetTranslation(float x, float y, float z);

	glm::vec3 GetRotation();
	void SetRotation(glm::vec3 r);
	void SetRotation(float x, float y, float z);

	glm::vec3 GetScale();
	void SetScale(glm::vec3 r);
	void SetScale(float x, float y, float z);

	Camera * GetCamera();
	void SetCamera(Camera * cam);

	glm::mat4 GetTransformation();
	glm::mat4 GetProjectedTransformation();

	static void SetProjection(float _fov, float _width, float _height, float _zNear, float _zFar);


private:
	glm::vec3 translation;
	glm::vec3 rotation;
	glm::vec3 scale;
	
	// this is gross
	static float zNear;
	static float zFar;
	static float width;
	static float height;
	static float fov;

	// ... no
	static Camera * camera;

};

#endif // !TRANSFORM_H
