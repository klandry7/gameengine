#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>

class Camera
{
public:
	Camera();
	Camera(glm::vec3 _pos, glm::vec3 _forward, glm::vec3 _up);
	~Camera();

	void Move(glm::vec3 dir, float amount);
	void RotateX(float angle);
	void RotateY(float angle);


	glm::vec3 GetLeft();
	glm::vec3 GetRight();

	void SetPos(glm::vec3 p);
	glm::vec3 GetPos();

	void SetForward(glm::vec3 f);
	glm::vec3 GetForward();

	void SetUp(glm::vec3 u);
	glm::vec3 GetUp();

public:
	const glm::vec3 yAxis = glm::vec3(0, 1, 0);
private:
	glm::vec3 pos;
	glm::vec3 forward;
	glm::vec3 up;

};



#endif // !CAMERA_H
