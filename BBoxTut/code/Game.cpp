#include "Game.h"
#include "Vertex.h"

#include <stdio.h>
#include "common.h"

#include <vector>

#include "Input.h"

Game::Game()
{
	//mesh = new Mesh();
	shader = new Shader("passthrough");
	phongShader = new Shader("phong");
	transform = new Transform();
	transform->SetProjection(45, 800, 600, 0.1, 100);
	camera = new Camera(glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0));
	transform->SetCamera(camera);
	resourceLoader = new ResourceLoader();

	mesh = resourceLoader->LoadMesh("../models/testMonkey.obj");

	texture = resourceLoader->LoadTexture("../models/colourMap.bmp");
	//printf("%i\n", texture->GetId());

	/*
	std::vector<glm::vec3> data;
	data.push_back(glm::vec3(0.3f, 0.3f, 0.2f));
	data.push_back(glm::vec3(0.75f, -0.75f, 0.0f));
	data.push_back(glm::vec3(-0.75f, -0.75f, 0.0f));

	std::vector<GLshort> indices;
	indices.push_back(0);
	indices.push_back(1);
	indices.push_back(2);

	mesh->AddVertices(data, indices);
	*/

	shader->AddVertexShader("../shaders/basicVert.vert");
	shader->AddFragmentShader("../shaders/basicFrag.frag");
	shader->CompileShader();

	phongShader->AddVertexShader("../shaders/phongVert.vert");
	phongShader->AddFragmentShader("../shaders/phongFrag.frag");
	phongShader->CompileShader();
}

Game::~Game()
{
}

void Game::Input(Window *window)
{
	/*if (inputNamespace::input->GetButtonState(GAME_BUTTONS::right, INPUTS::keyboard, BUTTON_STATE::pressed))
	{
		printf("right press\n");
	}
	else if (inputNamespace::input->GetButtonState(GAME_BUTTONS::right, INPUTS::keyboard, BUTTON_STATE::released))
	{
		printf("right release\n");
	}
	else if (inputNamespace::input->GetButtonState(GAME_BUTTONS::right, INPUTS::keyboard, BUTTON_STATE::heldDown))
	{
		printf("right held\n");
	}*/

	if (inputNamespace::input->GetButtonState(GAME_BUTTONS::right, INPUTS::keyboard, BUTTON_STATE::heldDown))
	{
		camera->Move(camera->GetRight(), 1);
		//printf("right <%f, %f, %f>\n", camera->GetRight().x, camera->GetRight().y, camera->GetRight().z);
	}
	if (inputNamespace::input->GetButtonState(GAME_BUTTONS::left , INPUTS::keyboard, BUTTON_STATE::heldDown))
	{
		camera->Move(camera->GetLeft(), 1);
	}
	if (inputNamespace::input->GetButtonState(GAME_BUTTONS::up, INPUTS::keyboard, BUTTON_STATE::heldDown))
	{
		camera->Move(camera->GetForward(), 1);
		//printf("right <%f, %f, %f>\n", camera->GetRight().x, camera->GetRight().y, camera->GetRight().z);
	}
	if (inputNamespace::input->GetButtonState(GAME_BUTTONS::down, INPUTS::keyboard, BUTTON_STATE::heldDown))
	{
		camera->Move(-camera->GetForward(), 1);
	}

	if (inputNamespace::input->GetButtonState(GAME_BUTTONS::action1, INPUTS::keyboard, BUTTON_STATE::heldDown))
	{
		camera->RotateX(0.1);
	}
	if (inputNamespace::input->GetButtonState(GAME_BUTTONS::action2, INPUTS::keyboard, BUTTON_STATE::heldDown))
	{
		camera->RotateX(-0.1);
	}

	transform->SetCamera(camera);


	if (inputNamespace::input->GetButtonState(GAME_BUTTONS::start, INPUTS::keyboard, BUTTON_STATE::released))
	{
		printf("quit message posted \n");
		window->PostWindowCloseMessage(true);
	}
}

void Game::Update(float dt)
{
	sinValue += dt;
	//printf("%f\n", dt);
	transform->SetTranslation(glm::vec3((float)sin(sinValue), (float)cos(sinValue), -6.0f));
	transform->SetRotation(glm::vec3(0, 0, (float)sin(sinValue)));
	//transform->SetScale(glm::vec3(0.1f, 0.1f, 0.1f));
}

void Game::Render()
{
	shader->Bind();
	shader->SetUniform4fv("transform", transform->GetProjectedTransformation());
	shader->SetUniform3fv("InColor", glm::vec3(0.7, 0.7, 0.0));
	texture->BindTexture();
	mesh->Draw();
}
