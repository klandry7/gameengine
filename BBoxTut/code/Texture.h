#ifndef TEXTURE_H
#define TEXTURE_H

//#define GLFW_OPENGL_PROFILE 1

#include <GL/glew.h>

class Texture
{
public:
	Texture(GLuint _id);
	~Texture();

	GLuint GetId();
	void BindTexture();

private:
	GLuint id;
};


#endif // !TEXTURE_H
