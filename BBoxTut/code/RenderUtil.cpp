#include "RenderUtil.h"

//#define GLFW_OPENGL_PROFILE 1

#include <GL/glew.h>


RenderUtil::RenderUtil()
{

}

RenderUtil::~RenderUtil()
{

}

void RenderUtil::ClearScreen()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void RenderUtil::InitGraphics()
{
	glClearColor(0.0f, 0.0f, 0.1f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);
	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);
	// cull back faces 
	glCullFace(GL_BACK);
	// front faces are clockwise
	glFrontFace(GL_CCW);

	glEnable(GL_TEXTURE_2D);

	// frag shader output is in linear rgb colour space. Gamma correction.
	glEnable(GL_FRAMEBUFFER_SRGB);
}

void RenderUtil::EnableTextures(bool state)
{
	/*if (state)
	{
		glEnable(GL_TEXTURE_2D);
	}
	else
	{
		glDisable(GL_TEXTURE_2D);
	}*/
}

char * RenderUtil::GetOpenGLVersion()
{
	return (char *)glGetString(GL_VERSION);
}