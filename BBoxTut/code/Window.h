#ifndef WINDOW_H
#define WINDOW_H


#include "common.h"



class Window
{
public:
	Window(int _windowWidth, int _windowHeight, const char * title);

	//bool KeyDown();

	//CreateWindow(int width, int height, const char * title);
	void PostWindowCloseMessage(bool state);
	bool32 WindowShouldClose();
	void SetTime(real32 time);
	float GetTime();
	void Terminate();
	void SetMouseState(bool32 show, bool32 restrictToWindow);
	void GetScreenCentre(float * halfX, float * halfY);
	void SetDoubleBuffering(bool state);
	void SetAASampleRate(int samples);
	void SwapBuffers();
	void PollEvents();

	size_t GetWindow()
	{
		return window;
	}



private:
	int InitGLEW();
	int InitGLFW(int OGLversionMajor, int OGLversionMinor);



private:
	size_t window;

	bool doubleBuffered;
	int aaSampleRate;

	int windowWidth;
	int windowHeight;



	

};
#endif