#include "Camera.h"
#include <glm/gtx/rotate_vector.hpp>
#include <stdio.h>

Camera::Camera()
{
	pos = glm::vec3(0, 0, 0);
	up = glm::vec3(0, 1, 0);
	forward = glm::vec3(0, 0, 1);
	//Camera(glm::vec3(0, 0, 10), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
}

Camera::Camera(glm::vec3 _pos, glm::vec3 _forward, glm::vec3 _up)
{
	pos = _pos;
	up = glm::normalize(_up);
	forward = glm::normalize(_forward);
}

Camera::~Camera()
{
}

void Camera::Move(glm::vec3 dir, float amount)
{
	SetPos(GetPos() + (glm::normalize(dir) * amount));
}

void Camera::RotateX(float angle)
{
	glm::vec3 horizontalAxis = glm::cross(yAxis, GetForward());
	horizontalAxis = glm::normalize(horizontalAxis);
	

	glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), angle, horizontalAxis);
	glm::vec4 rotated = rotation * glm::vec4(GetForward().x, GetForward().y, GetForward().z, 1);
	SetForward(glm::vec3(rotated.x, rotated.y, rotated.z));

	SetUp(glm::cross(GetForward(), horizontalAxis));
}

void Camera::RotateY(float angle)
{
	glm::vec3 horizontalAxis = glm::cross(yAxis, GetForward());
	horizontalAxis = glm::normalize(horizontalAxis);
	
	SetForward(glm::rotateY(yAxis, angle));
	SetUp(glm::cross(GetRight(), GetForward()));
}

glm::vec3 Camera::GetLeft()
{
	glm::vec3 left = glm::cross(GetUp(), GetForward());

	left = glm::normalize(left);
	return left;
}

glm::vec3 Camera::GetRight()
{
	//printf("forward <%f, %f, %f>\n", GetForward().x, GetForward().y, GetForward().z);
	//printf("up <%f, %f, %f>\n", GetUp().x, GetUp().y, GetUp().z);

	return glm::normalize(glm::cross(GetForward(), GetUp()));
}

void Camera::SetPos(glm::vec3 p)
{
	pos = p;
}
glm::vec3 Camera::GetPos()
{
	return pos;
}

void Camera::SetForward(glm::vec3 f)
{
	forward = glm::normalize(f);
}
glm::vec3 Camera::GetForward()
{
	return forward;
}

void Camera::SetUp(glm::vec3 u)
{
	up = glm::normalize(u);
}
glm::vec3 Camera::GetUp()
{
	return up;
}
