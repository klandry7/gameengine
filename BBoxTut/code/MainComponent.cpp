#include "MainComponent.h"

#include <stdio.h>

#include "Input.h"

#include "ResourceLoader.h"

#define TARGET_SECONDS_PER_FRAME 1.0f / 60.0f
#define SECONDARY_SECONDS_PER_FRAME	1.0f / 30.0f

MainComponent::MainComponent()
{
	running = false;

	window = new Window(800, 600, "engine");
	window->SetMouseState(true, false);
	//window->SetDoubleBuffering(false);

	//window->InitJoypadGameKeys(&window->joypadButtons[0]);
	inputNamespace::input->InitKeyboardGameKeys(&inputNamespace::input->keyboardKeys);

	renderutil = new RenderUtil();
	renderutil->InitGraphics();

	game = new Game();

	//renderutil->EnableTextures(true);
	//printf("%s\n", renderutil->GetOpenGLVersion());


	//printf("%s\n", ResourceLoader::LoadShader("../shaders/InstancedVert.vert"));
}

void MainComponent::Start()
{
	if (running)
		return;

	Run();
}

void MainComponent::Stop()
{
	if (!running)
		return;

	running = false;
}

void MainComponent::Run()
{
	if (window)
		running = true;

	window->SetTime(0);
	currentTime = window->GetTime();
	lastTime = 0;
	dt = currentTime;


	unprocessedTime = 0;

	bool render;

	while (running)
	{
		
		render = false;

		currentTime = window->GetTime();
		dt = currentTime - lastTime;
		lastTime = currentTime;

		unprocessedTime += dt;

		/*
		window->PollEvents();
		inputNamespace::input->UpdateInput(window->GetWindow());
		game->Input(window);
		game->Update(dt);
		inputNamespace::input->SwapInput();

		if (window->WindowShouldClose())
		{
			running = false;
		}

		Render();*/

		while (unprocessedTime > TARGET_SECONDS_PER_FRAME)
		{
			render = true;

			unprocessedTime -= TARGET_SECONDS_PER_FRAME;
			dt = TARGET_SECONDS_PER_FRAME;

			window->PollEvents();
			inputNamespace::input->UpdateInput(window->GetWindow());
			game->Input(window);
			game->Update(dt);

			inputNamespace::input->SwapInput();

			if (window->WindowShouldClose())
			{
				running = false;
			}			
		}
		if (render)
		{
			Render();
		}
		else
		{
		}

	}
	Cleanup();
}

void MainComponent::Render()
{
	renderutil->ClearScreen();
	game->Render();
	window->SwapBuffers();
}


void MainComponent::Cleanup()
{
	window->Terminate();
}

int main()
{
	MainComponent * mc = new MainComponent();

	mc->Start();

	return 0;
}