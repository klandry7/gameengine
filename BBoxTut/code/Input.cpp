#include "Input.h"
#include <GLFW/glfw3.h>

Input * inputNamespace::input = new Input();

#define JOYPAD_1 GLFW_JOYSTICK_1
#define JOYPAD_2 GLFW_JOYSTICK_2
#define JOYPAD_3 GLFW_JOYSTICK_3
#define JOYPAD_4 GLFW_JOYSTICK_4

Input::Input()
{
	currentKeyboardController = {};
	previousKeyboardController = {};
	
	for (int i = 0; i < ARRAY_COUNT(currentJoypadControllers); i++)
	{
		currentJoypadControllers[i] = {};
	}

	for (int i = 0; i < ARRAY_COUNT(previousJoypadControllers); i++)
	{
		previousJoypadControllers[i] = {};
	}

}


Input::~Input()
{
}






void Input::UpdateInput(size_t window)
{
	if (IsJoystickPresent())
	{
		ProcessInput(&currentJoypadControllers[0], &joypadButtons[0], window);
	}

	ProcessInput(&currentKeyboardController, &keyboardKeys, window);


}

bool32 Input::GetKeyState(int button, size_t window)//, const unsigned char *buttons = new const unsigned char[1])
{
	//GLFW_PRESS   1
	//The key or mouse button was pressed.
	//GLFW_RELEASE   0
	//The key or mouse button was released.
	//GLFW_REPEAT   2
	//The key was held down until it repeated.
	return glfwGetKey((GLFWwindow*)window, button) == GLFW_PRESS ? true : false;
}



// pass in a list of player's bound input 
// it's fine for now to be static
// instead of passing in a reference
// should just pass in a set game_buttons 
// and then set keyboardkeys to that
void Input::InitKeyboardGameKeys(game_buttons *gameKeys)
{

	gameKeys->start = GLFW_KEY_ESCAPE;
	gameKeys->select = GLFW_KEY_F;

	// just an easy switch for testing
#if 0
	gameKeys->up = GLFW_KEY_UP;
	gameKeys->down = GLFW_KEY_DOWN;
	gameKeys->left = GLFW_KEY_LEFT;
	gameKeys->right = GLFW_KEY_RIGHT;
#else 
	gameKeys->up = GLFW_KEY_W;
	gameKeys->down = GLFW_KEY_S;
	gameKeys->left = GLFW_KEY_A;
	gameKeys->right = GLFW_KEY_D;
#endif

	gameKeys->action1 = GLFW_KEY_Q;
	gameKeys->action2 = GLFW_KEY_E;
	gameKeys->action3 = GLFW_KEY_LEFT_SHIFT;
	gameKeys->action4 = GLFW_KEY_P;
}

void Input::InitJoypadGameKeys(game_buttons *gameKeys)
{

	gameKeys->start = 7;
	gameKeys->select = 6;

	gameKeys->up = 10;
	gameKeys->down = 12;
	gameKeys->left = 13;
	gameKeys->right = 11;

	gameKeys->action1 = 0;
	gameKeys->action2 = 1;
	gameKeys->action3 = 2;
	gameKeys->action4 = 3;
}

bool32 Input::IsJoystickPresent()
{
	return glfwJoystickPresent(JOYPAD_1) == GLFW_TRUE ? true : false;
}


void Input::ProcessInput(game_controller *controller, const game_buttons *buttons, size_t window)
{
	const unsigned char *analogButtons;
	if (controller->isAnalog)
	{
		int buttonCount;
		analogButtons = glfwGetJoystickButtons(JOYPAD_1, &buttonCount);
	}

	// menu 
	controller->start.pressed = controller->isAnalog ? analogButtons[buttons->start] : GetKeyState(buttons->start, window);
	controller->select.pressed = controller->isAnalog ? analogButtons[buttons->select] : GetKeyState(buttons->select, window);

	// up, down, left, right
	controller->up.pressed = controller->isAnalog ? analogButtons[buttons->up] : GetKeyState(buttons->up, window);
	controller->down.pressed = controller->isAnalog ? analogButtons[buttons->down] : GetKeyState(buttons->down, window);
	controller->left.pressed = controller->isAnalog ? analogButtons[buttons->left] : GetKeyState(buttons->left, window);
	controller->right.pressed = controller->isAnalog ? analogButtons[buttons->right] : GetKeyState(buttons->right, window);

	// face / action buttons
	controller->action1.pressed = controller->isAnalog ? analogButtons[buttons->action1] : GetKeyState(buttons->action1, window);
	controller->action2.pressed = controller->isAnalog ? analogButtons[buttons->action2] : GetKeyState(buttons->action2, window);
	controller->action3.pressed = controller->isAnalog ? analogButtons[buttons->action3] : GetKeyState(buttons->action3, window);
	controller->action4.pressed = controller->isAnalog ? analogButtons[buttons->action4] : GetKeyState(buttons->action4, window);
}

bool Input::GetButtonState(GAME_BUTTONS button, INPUTS controller, BUTTON_STATE state)
{
	if (controller == INPUTS::keyboard)
	{
		// up, down, left, right
		if (button == GAME_BUTTONS::up)
			return ButtonStateSelector(previousKeyboardController.up, currentKeyboardController.up, state);
		if (button == GAME_BUTTONS::down)
			return ButtonStateSelector(previousKeyboardController.down, currentKeyboardController.down, state);
		if (button == GAME_BUTTONS::left)
			return ButtonStateSelector(previousKeyboardController.left, currentKeyboardController.left, state);
		if (button == GAME_BUTTONS::right)
			return ButtonStateSelector(previousKeyboardController.right, currentKeyboardController.right, state);

		if (button == GAME_BUTTONS::action1)
			return ButtonStateSelector(previousKeyboardController.action1, currentKeyboardController.action1, state);
		if (button == GAME_BUTTONS::action2)
			return ButtonStateSelector(previousKeyboardController.action2, currentKeyboardController.action2, state);
		if (button == GAME_BUTTONS::action3)
			return ButtonStateSelector(previousKeyboardController.action3, currentKeyboardController.action3, state);
		if (button == GAME_BUTTONS::action4)
			return ButtonStateSelector(previousKeyboardController.action4, currentKeyboardController.action4, state);



		// start, select
		if(button == GAME_BUTTONS::start)
			return ButtonStateSelector(previousKeyboardController.start, currentKeyboardController.start, state);

		// face / action buttons

	}
	return false;
}

bool Input::ButtonStateSelector(game_button_state oldController, game_button_state newController, BUTTON_STATE state)
{
	if (state == BUTTON_STATE::pressed)
		return ButtonPressed(oldController, newController);
	else if (state == BUTTON_STATE::released)
		return ButtonReleased(oldController, newController);
	else if (state == BUTTON_STATE::heldDown)
		return ButtonHeldDown(oldController, newController);
	else
		return false;
}

bool Input::ButtonHeldDown(game_button_state oldController, game_button_state newController)
{
	return ((oldController.pressed && newController.pressed) || (!oldController.pressed && newController.pressed)) ? true : false;
}

bool Input::ButtonReleased(game_button_state oldController, game_button_state newController)
{
	return (oldController.pressed && !newController.pressed) ? true : false;
}

bool Input::ButtonPressed(game_button_state oldController, game_button_state newController)
{
	return (!oldController.pressed && newController.pressed) ? true : false;
}


void Input::SwapInput()
{
	previousKeyboardController = currentKeyboardController;
	currentKeyboardController = {};

	for (int i = 0; i < ARRAY_COUNT(currentJoypadControllers); i++)
	{
		previousJoypadControllers[i] = currentJoypadControllers[i];
		currentJoypadControllers[i] = {};
	}
}