#version 330 core

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_texCoord;
layout(location = 2) in vec3 in_normal;
// 3 is index data
layout(location = 4) in uvec4 BoneIDs;
layout(location = 5) in vec4 Weights;
layout(location = 6) in mat4 instancedTRS;
//... 7,8,9 taken up since it's mat4

//bone vars 
const int gBones_size = 100;
uniform mat4 gBones[gBones_size];

uniform bool animated;
// need to make another shader for instanced, will do later
uniform bool instanced;

uniform mat4 projectionViewMatrix;
uniform mat4 transformMatrix;

out vec2 texCoord;

void main()
{	
	if(animated)
	{
		mat4 BoneTransform = gBones[BoneIDs[0]] * Weights.x; // equivalent to Weights[0]
		BoneTransform += gBones[BoneIDs[1]] * Weights.y;
		BoneTransform += gBones[BoneIDs[2]] * Weights.z;
		BoneTransform += gBones[BoneIDs[3]] * Weights.w;

		vec4 PosL = BoneTransform * vec4(in_position, 1.0);
		//vec4 NormalL = BoneTransform * vec4(in_normal, 0.0);
		//vec3 vpm = PosL.xyz;
		//vec3 npm = NormalL.xyz;

		gl_Position = projectionViewMatrix * transformMatrix * PosL;	
	}
	else if(instanced)
	{
		gl_Position = projectionViewMatrix * instancedTRS * vec4(in_position, 1.0);
	}
	else
	{
		gl_Position = projectionViewMatrix * transformMatrix * vec4(in_position, 1.0);	
	}
	texCoord = in_texCoord;
}