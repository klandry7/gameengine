#version 330 core

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_texCoord;
layout(location = 2) in vec3 in_normal;
// 3 is index data
layout(location = 6) in mat4 instancedTRS;
//... 7,8,9 taken up since it's mat4


// need to make another shader for instanced, will do later
uniform bool instanced;

uniform mat4 projectionViewMatrix;
uniform mat4 transformMatrix;

//out vec2 texCoord;
out vec4 col;


void main()
{	
	if(instanced)
	{
		gl_Position = projectionViewMatrix * instancedTRS * vec4(in_position, 1.0);
	}
	else
	{
		gl_Position = projectionViewMatrix * transformMatrix * vec4(in_position, 1.0);	
	}
	//texCoord = in_texCoord;
	col = vec4(in_position, 1.0);
}