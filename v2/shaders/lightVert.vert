#version 330 core

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_texCoord;
layout(location = 2) in vec3 in_normal;
// 3 is index data
layout(location = 4) in uvec4 BoneIDs;
layout(location = 5) in vec4 Weights;
layout(location = 6) in mat4 instancedTRS;
//... 7,8,9 taken up since it's mat4

//bone vars 
const int gBones_size = 100;
uniform mat4 gBones[gBones_size];

uniform bool animated;
// need to make another shader for instanced, will do later
uniform bool instanced;

uniform mat4 projectionViewMatrix;
uniform mat4 transformMatrix;
uniform mat4 viewMatrix;
out vec2 texCoord;



// light vars 
out vec3 Position_worldspace;
out vec3 Normal_cameraspace;
out vec3 EyeDirection_cameraspace;
out vec3 LightDirection_cameraspace;

out mat4 MV;
out vec3 LightPosition_worldspace;

out vec3 ambientColor;
out vec3 specularColor;

//uniform vec3 lightDirection; // for directional lights 
uniform vec3 lightPos;  
uniform vec4 lightIntensity;
uniform vec4 ambientLightIntensity;

void main()
{	
	if(animated)
	{
		mat4 BoneTransform = gBones[BoneIDs[0]] * Weights.x; // equivalent to Weights[0]
		BoneTransform += gBones[BoneIDs[1]] * Weights.y;
		BoneTransform += gBones[BoneIDs[2]] * Weights.z;
		BoneTransform += gBones[BoneIDs[3]] * Weights.w;

		vec4 PosL = BoneTransform * vec4(in_position, 1.0);
		vec4 NormalL = BoneTransform * vec4(in_normal, 0.0);
		//vec3 vpm = PosL.xyz;
		//vec3 npm = NormalL.xyz;

		gl_Position = projectionViewMatrix * transformMatrix * PosL;	
		
		// the position of the vertex in world space
		Position_worldspace = (transformMatrix * PosL).xyz;

		// the vector that goes from the vertex to the camera in camera space 
		// in camera space, the camera is at the origin. the camera moves the world
		vec3 vertexPosition_cameraspace = (viewMatrix * transformMatrix * PosL).xyz;
		EyeDirection_cameraspace = vec3(0,0,0) - vertexPosition_cameraspace;

		// vector that goes from the vertex to the light in cam space. 
		vec3 LightPosition_cameraspace = (viewMatrix * vec4(lightPos,1.0)).xyz;
		LightDirection_cameraspace = LightPosition_cameraspace + EyeDirection_cameraspace;

		// normal of the vertex in camera space 
		// only correct if the model matrix doesn't sccale the model. need to use inverse transpose
		Normal_cameraspace = (viewMatrix * transformMatrix * NormalL).xyz;


	}
	else if(instanced)
	{
		gl_Position = projectionViewMatrix * instancedTRS * vec4(in_position, 1.0);
	}
	else
	{
		vec4 PosL = vec4(in_position, 1.0);
		vec4 NormalL = vec4(in_normal, 0.0);

		gl_Position = projectionViewMatrix * transformMatrix * PosL;

		
		// the position of the vertex in world space
		Position_worldspace = (transformMatrix * PosL).xyz;

		// the vector that goes from the vertex to the camera in camera space 
		// in camera space, the camera is at the origin. the camera moves the world
		vec3 vertexPosition_cameraspace = (viewMatrix * transformMatrix * PosL).xyz;
		EyeDirection_cameraspace = vec3(0,0,0) - vertexPosition_cameraspace;

		// vector that goes from the vertex to the light in cam space. 
		vec3 LightPosition_cameraspace = (viewMatrix * vec4(lightPos,1.0)).xyz;
		LightDirection_cameraspace = LightPosition_cameraspace + EyeDirection_cameraspace;

		// normal of the vertex in camera space 
		// only correct if the model matrix doesn't sccale the model. need to use inverse transpose
		Normal_cameraspace = (viewMatrix * transformMatrix * NormalL).xyz;
	
	}
	texCoord = in_texCoord;

	LightPosition_worldspace = lightPos;
	MV = viewMatrix * transformMatrix;

	ambientColor = ambientLightIntensity.xyz;
	specularColor = lightIntensity.xyz;
}