#version 330 core

//in vec2 texCoord;
in vec4 col;
//uniform sampler2D myTextureSampler;
out vec4 color;

uniform float elapsedTime;

void main()
{
	//vec3 MaterialDiffuseColor = texture( myTextureSampler, texCoord ).rgb;
	color = vec4(col.x, col.y + sin(elapsedTime), col.z, 1.0);///vec4(MaterialDiffuseColor,1.0); 
}