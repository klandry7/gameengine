#version 330 core

in vec2 texCoord;
uniform sampler2D myTextureSampler;
out vec4 color;

void main()
{
	vec3 MaterialDiffuseColor = texture( myTextureSampler, texCoord ).rgb;
	color = vec4(MaterialDiffuseColor,1.0);
}