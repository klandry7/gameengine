Date: 11.02.2015
Created by:
Vinrax
vinrax@gmail.com

Cartoon Zombie model, already rigged and animated. 

Model have 2 animations:
Walk(40 frames)
Idle(30 frames)

Tris: 716

Feel free to use this in your projects, whether it be open source or commercial, as long as proper credit is given(name, email/website).

CC-BY 3.0 License
http://creativecommons.org/licenses/by/3.0/