#ifndef PHYSICSCOMPONENT_H
#define PHYSICSCOMPONENT_H

namespace RDE
{
	namespace MAPGEN
	{
		struct Map;
	}
}

namespace RDE
{
	class GameObject;

	class PhysicsComponent
	{
	public:
		// virtual init
		virtual void Init(RDE::GameObject & self) = 0;
		// virtual update
		virtual void Update(RDE::GameObject & self, float dt, const RDE::MAPGEN::Map *map) = 0;
		// virtual dtor
		virtual ~PhysicsComponent() {};
	};
}

#endif // !PHYSICSCOMPONENT_H