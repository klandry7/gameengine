var indexSectionsWithContent =
{
  0: "abcdefghijklmnoprstuvwxyz~",
  1: "abcdfghimprstuvw",
  2: "cr",
  3: "abcdfghimprstuw",
  4: "abcdefghilmnoprstuvw~",
  5: "abcdefghijklmnoprstuvwxyz",
  6: "bimrtu",
  7: "bdgilst",
  8: "abcdfhjklmnoprsuw",
  9: "abcdfgjlmnprst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Macros"
};

