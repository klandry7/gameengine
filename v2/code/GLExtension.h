#ifndef GLEXTENSION_H
#define GLEXTENSION_H

#define GLEW_ACTIVE 1

//
#if GLEW_ACTIVE
	#include <GL/glew.h>
#elif defined(NANOGUI_GLAD)
	#if defined(NANOGUI_SHARED) && !defined(GLAD_GLAPI_EXPORT)
		#define GLAD_GLAPI_EXPORT
	#endif
#include <nanogui\ext\glad\include\glad\glad.h>
#else
	#if defined(__APPLE__)
		#define GLFW_INCLUDE_GLCOREARB
	#else
		#define GL_GLEXT_PROTOTYPES
	#endif
#endif

#endif // !GLEXTENSION_H
