#ifndef HIERARCHYTRAVERSAL_H
#define HIERARCHYTRAVERSAL_H

namespace RDE
{
	class GameObject;
	class Camera;
}

namespace RDE
{
	namespace HIERARCHY
	{
		void UpdateHierarchyTraversal(RDE::GameObject * rootNode, float dt);
		void RenderHierarchyTraversal(RDE::GameObject * rootNode, RDE::Camera * camera, float interpolation);
	}
}
#endif // !HIERARCHYTRAVERSAL_H
