#include "defaultRenderComponent.h"
#include "renderer.h"
#include "gameObject.h"
#include "transform.h"
#include "shader.h"
#include "animManager.h"
#include <glm/gtc/matrix_transform.hpp>

DefaultRenderComponent::DefaultRenderComponent(const char * _modelName, const char * _textureName) 
{
	modelName = _modelName;
	textureName = _textureName;

	if (RDE::RENDERER::instance->ModelAnimCount(modelName) > 0)
	{
		animated = true;
	}
	else
	{
		animated = false;
	}

	//animated = false;
}

void DefaultRenderComponent::Init(RDE::GameObject & gameObject)
{
	if (animated)
	{
		gameObject.animManager->Init(modelName, RDE::RENDERER::instance->ModelAnimCount(modelName));
	}
}

DefaultRenderComponent::~DefaultRenderComponent()
{
	//delete modelName;
	//delete textureName;
	//delete defaultShaderReference;
}

void DefaultRenderComponent::Update(RDE::GameObject & gameObject, glm::mat4 parentTransform, float interpolation)
{
	if (gameObject.IsRoot())
		return;

	
	/*if (strcmp(modelName, "henrietta") == 0)
	{
		printf("%s ", modelName);
		printf("%i\n", gameObject.animManager->GetAnimCount());
	}*/


	if (animated)
	{
		// the anim system is very not perfect, but it works for now.
		std::vector<glm::mat4> boneTransform = RDE::RENDERER::instance->GetBoneTransforms(modelName, gameObject.animManager->CurrentActiveAnimTime(), gameObject.animManager->CurrentActiveAnim());
		if (boneTransform.size() > 0)
		{
			gameObject.defaultShaderReference->SetUniform1i("animated", true);
			for (unsigned int i = 0; i < boneTransform.size(); i++)
			{
				char Name[128];
				memset(Name, 0, sizeof(Name));
				// this is c++ 11. write a string to a buffer
				// pointer to buffer, bytes to be used, the string, additional args(just like printf)
				snprintf(Name, sizeof(Name), "gBones[%d]", i);
				gameObject.defaultShaderReference->SetUniformMatrix4fv(Name, true, boneTransform[i]);
			}
		}
	}
	else
	{
		gameObject.defaultShaderReference->SetUniform1i("animated", false);
	}

	//glm::vec3 previousState = gameObject.transform->translation;
	//gameObject.transform->translation = ((gameObject.transform->translation + gameObject.velocity) * interpolation) + (previousState * (1.0f - interpolation));
	//gameObject.transform->translation = ((gameObject.transform->translation) * interpolation) + (gameObject.prevPos * (1.0f - interpolation));
	
	// not efficient. works for now.
	RDE::Transform interpolatedTransform = *gameObject.transform;
	interpolatedTransform.translation = ((interpolatedTransform.translation) * interpolation) + (gameObject.prevPos * (1.0f - interpolation));

	gameObject.defaultShaderReference->SetUniformMatrix4fv("transformMatrix", false, (parentTransform * interpolatedTransform.GetMatrix()));
	RDE::RENDERER::instance->DrawModelIndexed(modelName, textureName);

	// drawing a bounding box
	/*
	gameObject.defaultShaderReference->SetUniform1i("animated", false);
	std::vector<RDE::bounding_box_struct> bboxList = gameObject.GetBoundingBox();
	if (bboxList.size() > 0)
	{
		RDE::bounding_box_struct bbox = bboxList.at(0);

		glm::mat4 parentTransformTotal = (parentTransform * interpolatedTransform.GetMatrix());
		
		for (int i = 0; i < 8; i++)
		{
			glm::mat4 drawPos = glm::translate(glm::mat4(1.0f), bbox.vertices[i]);
			gameObject.defaultShaderReference->SetUniformMatrix4fv("transformMatrix", false, parentTransformTotal * drawPos);
			RDE::RENDERER::instance->DrawModelIndexed("sphere", "henrietta");
		}
	}
	*/
}

std::vector<RDE::bounding_box_struct> DefaultRenderComponent::GetBbox()
{
	return RDE::RENDERER::instance->GetBoundingBoxList(modelName);
}
