#include "playerRenderComponent.h"

#include "renderer.h"
#include "gameObject.h"
#include "transform.h"
#include "shader.h"
#include "animManager.h"

PlayerRenderComponent::PlayerRenderComponent(const char * _modelName, const char * _textureName)
{
	modelName = _modelName;
	textureName = _textureName;

	if (RDE::RENDERER::instance->ModelAnimCount(modelName) > 0)
	{
		animated = true;
	}
	else
	{
		animated = false;
	}

	//animated = false;
}

void PlayerRenderComponent::Init(RDE::GameObject & gameObject)
{
	if (animated)
	{
		gameObject.animManager->Init(modelName, RDE::RENDERER::instance->ModelAnimCount(modelName));
	}
}

PlayerRenderComponent::~PlayerRenderComponent()
{
	//delete modelName;
	//delete textureName;
	//delete defaultShaderReference;
}

void PlayerRenderComponent::Update(RDE::GameObject & gameObject, glm::mat4 parentTransform, float interpolation)
{
	if (gameObject.IsRoot())
		return;

	//printf("%s ", modelName);


	if (animated)
	{
		// the anim system is very not perfect, but it works for now.
		std::vector<glm::mat4> boneTransform = RDE::RENDERER::instance->GetBoneTransforms(modelName, gameObject.animManager->CurrentActiveAnimTime(), gameObject.animManager->CurrentActiveAnim());
		if (boneTransform.size() > 0)
		{

			//if (std::string(modelName) == "henrietta")
			//	printf("%i\n", boneTransform.size());

			gameObject.defaultShaderReference->SetUniform1i("animated", true);
			for (unsigned int i = 0; i < boneTransform.size(); i++)
			{
				char Name[128];
				memset(Name, 0, sizeof(Name));
				// this is c++ 11. write a string to a buffer
				// pointer to buffer, bytes to be used, the string, additional args(just like printf)
				snprintf(Name, sizeof(Name), "gBones[%d]", i);
				gameObject.defaultShaderReference->SetUniformMatrix4fv(Name, true, boneTransform[i]);
			}
		}
	}
	else
	{
		gameObject.defaultShaderReference->SetUniform1i("animated", false);
	}


	// this absolutely sucks. camera sucks. I don't want to be manually interpolating, but here we are. ffs.
	//glm::vec3 interpolatedTransform = gameObject.transform->translation + playerCamGameObject->transform->translation;
	//interpolatedTransform = ((interpolatedTransform)* interpolation) + ((player->prevPos + playerCamGameObject->transform->translation) * (1.0f - interpolation));
	// this sucks. should have a "get hierarchial position" as well as "local position"
	//camera->SetPos(interpolatedTransform);
	//camera->SetLookatPt(interpolatedTransform + gameObject.sphericalTransform->GetForward());
	//camera->SetUp(gameObject.sphericalTransform->GetUp());



	// not efficient. works for now.
	RDE::Transform interpolatedTransform = *gameObject.transform;
	interpolatedTransform.translation = ((interpolatedTransform.translation) * interpolation) + (gameObject.prevPos * (1.0f - interpolation));

	gameObject.defaultShaderReference->SetUniformMatrix4fv("transformMatrix", false, (parentTransform * interpolatedTransform.GetMatrix()));
	RDE::RENDERER::instance->DrawModelIndexed(modelName, textureName);
}

std::vector<RDE::bounding_box_struct> PlayerRenderComponent::GetBbox()
{
	return RDE::RENDERER::instance->GetBoundingBoxList(modelName);
}
