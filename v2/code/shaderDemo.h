#ifndef SHADERDEMO_H
#define SHADERDEMO_H

#include <memory>
#include <vector>
#include <glm\glm.hpp>
#include "GUI.h"
#include <CEGUI\EventArgs.h>

namespace RDE
{
	class GameObject;
	class Camera;
	class Shader;
}

class ShaderDemo
{
	enum SHADING_MODE
	{
		cel,
		normal
	};

public:
	ShaderDemo(float aspectRatio, RDE::GUI * guiRef);
	~ShaderDemo();

	void FixedUpdate(float dt);
	bool Update(float dt);
	void Render(float interpolation);

	void Activate();

private:
	void DeActivate();
	void OnResetColourClicked(const CEGUI::EventArgs & e);
	void OnSwitchModeClicked(const CEGUI::EventArgs & e);
	void SwitchLightingState();
private:
	std::shared_ptr<const RDE::Shader> lightShader;

	RDE::GameObject * root;
	RDE::GameObject * spinningRoot;
	RDE::Camera * camera;
	RDE::GameObject * zombie;
	RDE::GameObject * plane;
	//RDE::GameObject * gazebo;
	RDE::GameObject * light;
	SHADING_MODE lightState;
	std::vector<RDE::GameObject *> zombieList;

	glm::vec3 lightColour;
	glm::vec4 lightSpecularIntensity;
	glm::vec4 ambientLightColour;
	
	RDE::GUI * gui;
	CEGUI::Slider * lightColourSliderR;
	CEGUI::Slider * lightColourSliderG;
	CEGUI::Slider * lightColourSliderB;

	CEGUI::PushButton * resetButton;
	CEGUI::PushButton * switchModeButton;
};

#endif // !HIERARCHYDEMO_H
