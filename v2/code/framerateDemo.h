#ifndef FRAMERATEDEMO_H
#define FRAMERATEDEMO_H



#include <CEGUI\EventArgs.h>

#include <vector>
#include <memory>
#include <glm\glm.hpp>

namespace RDE
{
	class Shader;
	class Camera;
	class GameObject;
	class GUI;
}

class FramerateDemo
{
public:
	FramerateDemo(float aspectRatio, RDE::GUI * mainGui);
	~FramerateDemo();

	void FixedUpdate(float dt);
	bool Update(float dt);
	void Render(float interpolation);

private:
	void OnFpsButtonClicked(const CEGUI::EventArgs & e);

private:
	std::shared_ptr<const RDE::Shader> simpleShader;
	RDE::Camera *camera;
	bool continueRunning = true;
	float elapsedTime = 0.0f;
	bool instanced = true;
	int size = 300;
	std::vector<glm::mat4> roomFloorList;

	CEGUI::PushButton * fpsButton;

	RDE::GUI * gui;
};


#endif // !FRAMERATEDEMO_H
