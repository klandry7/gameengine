#include "model.h"
#include "debugIO.h"
#include <glm/gtc/type_ptr.hpp>

// Credit to http://ogldev.atspace.co.uk/www/tutorial38/tutorial38.html for animation tutorial

RDE::Model::Model()
{
	animArraySize = 0;
	sceneRootNode = new aiNode_struct;
}

// need to actually use the destructor on this one
// on delete clean up all the gl buffers and such 
RDE::Model::~Model()
{
}

bool RDE::Model::LoadData(const char * filename, bool flipUVs)
{
	bool result = LoadModelData(filename, flipUVs);
	return result;
}

void RDE::Model::BindData()
{
	CreateBuffers();
	BindDataToBuffers();
}

bool RDE::Model::AddBuffer(const char * name, int shaderLayoutLocation)
{
	/*bool result = false;
	glBindVertexArray(indexedVAO);
	GLuint bufferObject = GL_INVALID_VALUE;
	glGenBuffers(1, &bufferObject);
	if (bufferObject != GL_INVALID_VALUE)
	{
	buffers.insert(std::make_pair(name, bufferObject));
	glEnableVertexAttribArray(shaderLayoutLocation);
	result = true;
	}
	else
	{
	printf("generating buffer [%s] failed", name);
	}
	glBindVertexArray(0);
	return result;*/
	return false;
}

void RDE::Model::DrawIndexed()
{
	glBindVertexArray(indexedVAO);
	glDrawElements(GL_TRIANGLES, vertexCount, GL_UNSIGNED_SHORT, (void *)0);
	glBindVertexArray(0);
}

void RDE::Model::DrawInstanced(unsigned int modelCount, std::vector<glm::mat4> transformationMatrixList)
{
	glBindVertexArray(indexedVAO);

	glBindBuffer(GL_ARRAY_BUFFER, instancedMatricesBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::mat4) * modelCount, &transformationMatrixList[0], GL_DYNAMIC_DRAW);
	
	// for some reason, desktop only works if these are called here.
	// possible answer: https://forums.khronos.org/showthread.php/7694-When-to-call-glEnableVertexAttribArray()
	glEnableVertexAttribArray(6);
	glEnableVertexAttribArray(7);
	glEnableVertexAttribArray(8);
	glEnableVertexAttribArray(9);

	glDrawElementsInstancedBaseVertex(GL_TRIANGLES, vertexCount, GL_UNSIGNED_SHORT, (void*)0, modelCount, 0);

	glBindVertexArray(0);
}

std::vector<RDE::bounding_box_struct> RDE::Model::GetBoundingBoxList()
{
	return transformedBoundingBoxList;
}

void RDE::Model::BindDataToBuffers()
{
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, total_vertices.size() * sizeof(glm::vec3), &total_vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, texCoordBufferObject);
	glBufferData(GL_ARRAY_BUFFER, total_texCoords.size() * sizeof(glm::vec2), &total_texCoords[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, vertexNormalBufferObject);
	glBufferData(GL_ARRAY_BUFFER, total_normals.size() * sizeof(glm::vec3), &total_normals[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, total_indices.size() * sizeof(GLushort), &total_indices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, BONE_ID_LOCATION);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Bones[0]) * Bones.size(), &Bones[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, BONE_WEIGHT_LOCATION);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Bones[0]) * Bones.size(), &Bones[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	
	glBindVertexArray(indexedVAO);

	
	// 1st attribute buffer : vertices
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glVertexAttribPointer(
		0,  // The attribute we want to configure
		3,                            // size
		GL_FLOAT,                     // type
		GL_FALSE,                     // normalized?
		0,                            // stride
		(void*)0                      // array buffer offset
	);

	// 2nd attribute buffer : UVs
	glBindBuffer(GL_ARRAY_BUFFER, texCoordBufferObject);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size : U+V => 2
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	// 3rd attribute buffer : vertex normals
	glBindBuffer(GL_ARRAY_BUFFER, vertexNormalBufferObject);
	glVertexAttribPointer(
		2,  // The attribute we want to configure
		3,                            // size
		GL_FLOAT,                     // type
		GL_FALSE,                     // normalized?
		0,                            // stride
		(void*)0                      // array buffer offset
	);

	// 4th attribute buffer: index data 
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
	glVertexAttribPointer(
		3,  // The attribute we want to configure
		3,                            // size
		GL_UNSIGNED_SHORT,			  // type
		GL_FALSE,                     // normalized?
		0,                            // stride
		(void*)0                      // array buffer offset
	);

	// notice the I. this is because it's int, not float
	// 5th attribute. bone id's
	glBindBuffer(GL_ARRAY_BUFFER, BONE_ID_LOCATION);
	glVertexAttribIPointer(4, 4, GL_UNSIGNED_INT, sizeof(vertex_bone_data_struct), (const GLvoid*)0);

	// 6th attribute. bone weights
	glBindBuffer(GL_ARRAY_BUFFER, BONE_WEIGHT_LOCATION);
	glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(vertex_bone_data_struct), (const GLvoid*)(NUM_BONES_PER_VEREX * 4));



	glBindBuffer(GL_ARRAY_BUFFER, instancedMatricesBufferObject);
	unsigned int INSTANCED_LOCATION = 6;
	for (unsigned int i = 0; i < 4; i++)
	{
		//glEnableVertexAttribArray(INSTANCED_LOCATION + i);
		glVertexAttribPointer(INSTANCED_LOCATION + i, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (const GLvoid*)(sizeof(glm::vec4) * i));
		glVertexAttribDivisor(INSTANCED_LOCATION + i, 1);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}

void RDE::Model::CreateBuffers()
{
	glGenVertexArrays(1, &indexedVAO);
	glBindVertexArray(indexedVAO);

	glGenBuffers(1, &vertexBufferObject);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &texCoordBufferObject);
	glEnableVertexAttribArray(1);

	glGenBuffers(1, &vertexNormalBufferObject);
	glEnableVertexAttribArray(2);

	glGenBuffers(1, &indexBufferObject);
	glEnableVertexAttribArray(3);

	glGenBuffers(1, &BONE_ID_LOCATION);
	glEnableVertexAttribArray(4);

	glGenBuffers(1, &BONE_WEIGHT_LOCATION);
	glEnableVertexAttribArray(5);

	glGenBuffers(1, &instancedMatricesBufferObject);
	/*glEnableVertexAttribArray(6);
	glEnableVertexAttribArray(7);
	glEnableVertexAttribArray(8);
	glEnableVertexAttribArray(9);*/


	glBindVertexArray(0);
}

// go back to v1 and try to load model data on a seperate thread.
// importer instance isn't thread safe. Each thread needs it's own importer instasnce.
bool RDE::Model::LoadModelData(const char * filename, bool flipUVs)
{
	bool result = true;
	const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);


	if (flipUVs)
	{
		pScene = Importer.ReadFile(filename,
			aiProcess_Triangulate |
			aiProcess_GenSmoothNormals |
			aiProcess_FlipUVs | 
			aiProcess_FixInfacingNormals | 
			aiProcess_ImproveCacheLocality | 
			aiProcess_OptimizeMeshes | 
			aiProcess_OptimizeGraph | 
			aiProcess_FindInvalidData);
	}
	else
	{
		pScene = Importer.ReadFile(filename,
			aiProcess_Triangulate |
			aiProcess_GenSmoothNormals | 
			aiProcess_FixInfacingNormals | 
			aiProcess_ImproveCacheLocality | 
			aiProcess_OptimizeMeshes | 
			aiProcess_OptimizeGraph | 
			aiProcess_FindInvalidData);
	}

	// if you successfully extracted a scene 
	printfD("Loading Model %s ... \n", filename);
	if (pScene)
	{
		// store the inverse transform of the scene 
		m_GlobalInverseTransform = pScene->mRootNode->mTransformation;
		m_GlobalInverseTransform.Inverse();
		m_GlobalInverseTransformGLM = AiMat4ToGlmMat4(m_GlobalInverseTransform);

		int sizeCount = 0;
		GLshort firstOffset = -1;
		std::vector<int> extraOffsets;

		glm::vec3 minPt = glm::vec3(0.0f, 0.0f, 0.0f);
		glm::vec3 maxPt = glm::vec3(0.0f, 0.0f, 0.0f);

		// for every mesh in the scene 
		for (unsigned int i = 0; i < pScene->mNumMeshes; i++)
		{
			const aiMesh* paiMesh = pScene->mMeshes[i];
			unsigned int materialIndex = paiMesh->mMaterialIndex;

			std::vector <glm::vec3> aiVertices;
			std::vector <glm::vec3> aiNormals;
			std::vector <glm::vec2> aiTexCoords;
			std::vector <GLushort> indices;

			// get vertex information
			for (unsigned int n = 0; n < paiMesh->mNumVertices; n++)
			{
				// get the data
				const aiVector3D* pPos = &(paiMesh->mVertices[n]);
				const aiVector3D* pNormal = &(paiMesh->mNormals[n]);// : &Zero3D;
				const aiVector3D* pTexCoord = paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][n]) : &Zero3D;
				// push it back
				aiVertices.push_back(glm::vec3((float)pPos->x, (float)pPos->y, (float)pPos->z));
				aiNormals.push_back(glm::vec3((float)pNormal->x, (float)pNormal->y, (float)pNormal->z));
				aiTexCoords.push_back(glm::vec2((float)pTexCoord->x, (float)pTexCoord->y));

				// get min/max points
				if (pPos->z > maxPt.z) {
					maxPt.z = pPos->z;
				}
				if (pPos->z < minPt.z) {
					minPt.z = pPos->z;
				}

				if (pPos->x > maxPt.x) {
					maxPt.x = pPos->x;
				}
				if (pPos->x < minPt.x) {
					minPt.x = pPos->x;
				}

				if (pPos->y > maxPt.y) {
					maxPt.y = pPos->y;
				}
				if (pPos->y < minPt.y) {
					minPt.y = pPos->y;
				}
			}

			// get index information
			for (unsigned int n = 0; n < paiMesh->mNumFaces; n++)
			{
				const aiFace& Face = paiMesh->mFaces[n];
				assert(Face.mNumIndices == 3);
				indices.push_back((GLushort)Face.mIndices[0]);
				indices.push_back((GLushort)Face.mIndices[1]);
				indices.push_back((GLushort)Face.mIndices[2]);
			}
			//printf("Face Indices Extracted\n");

			// add the model to the scene 
			total_vertices.insert(total_vertices.end(), aiVertices.begin(), aiVertices.end());
			total_texCoords.insert(total_texCoords.end(), aiTexCoords.begin(), aiTexCoords.end());
			total_normals.insert(total_normals.end(), aiNormals.begin(), aiNormals.end());

			GLushort offset = (GLushort)total_indices.size();

			if (firstOffset == -1)
			{
				firstOffset = offset;
			}

			for (size_t n = 0; n < indices.size(); n++)
			{
				total_indices.push_back(offset + indices[n]);
			}

			extraOffsets.push_back(sizeCount);
			sizeCount += aiVertices.size();
		}
		vertexCount = (GLshort)sizeCount;

		bounding_box_struct newBbox = {};
		maxPt *= 1.4f;
		minPt *= 1.4f;
		InitboundingBox(maxPt, minPt, &newBbox);
		originalBoundingBoxList.push_back(newBbox);
		//transformedBoundingBoxList.push_back(newBbox);


		if (pScene->HasAnimations())
		{
			animArraySize = pScene->mNumAnimations;
			animArray = new animation_struct[animArraySize];
			for (unsigned int i = 0; i < animArraySize; i++)
			{
				//The node animation channels.
				//aiNodeAnim ** mChannels;
				std::map<std::string, aiNodeAnim *> animMap;

				for (unsigned int n = 0; n < pScene->mAnimations[i]->mNumChannels; n++)
				{
					aiNodeAnim *temp = new aiNodeAnim();
					memcpy(&temp, &pScene->mAnimations[i]->mChannels[n], sizeof(pScene->mAnimations[i]->mChannels[n]));
					animArray[i].aiNodeAnimMap.insert(std::make_pair(std::string(pScene->mAnimations[i]->mChannels[n]->mNodeName.C_Str()), temp));
					//printf("names: %s\n\n", pScene->mAnimations[i]->mChannels[n]->mNodeName.C_Str());
				}

				//Duration of the animation in ticks.
				animArray[i].mDuration = pScene->mAnimations[i]->mDuration;
				//The mesh animation channels. unused.
				//aiMeshAnim ** mMeshChannels;
				//The name of the animation.
				animArray[i].mName = pScene->mAnimations[i]->mName;
				//The number of bone animation channels.
				animArray[i].mNumChannels = pScene->mAnimations[i]->mNumChannels;
				//The number of mesh animation channels.
				animArray[i].mNumMeshChannels = pScene->mAnimations[i]->mNumMeshChannels;
				//Ticks per second.
				animArray[i].mTicksPerSecond = (pScene->mAnimations[i]->mTicksPerSecond);
			}
		}

		//rootNode = new aiNode();// pScene->mRootNode;
		//memcpy(&rootNode, &pScene->mRootNode, sizeof(aiNode));
		//rootNode = new aiNode(pScene->mRootNode);


		Bones.resize(sizeCount);
		for (unsigned int i = 0; i < Bones.size(); i++)
		{
			Bones.at(i).Reset();
		}

		for (unsigned int i = 0; i < pScene->mNumMeshes; i++)
		{
			if (pScene->mMeshes[0]->HasBones())
			{
				LoadBones(pScene->mMeshes[i], extraOffsets.at(i));
			}
		}

		if (pScene->HasAnimations())
		{
			sceneRootNode->mMeshes = new unsigned int[pScene->mRootNode->mNumMeshes];
			memcpy(sceneRootNode->mMeshes, pScene->mRootNode->mMeshes, sizeof(pScene->mRootNode->mMeshes[0]) * pScene->mRootNode->mNumMeshes);
			
			sceneRootNode->mName = std::string(pScene->mRootNode->mName.data);

			sceneRootNode->mNumChildren = pScene->mRootNode->mNumChildren;
			sceneRootNode->mNumMeshes = pScene->mRootNode->mNumMeshes;
			// ignoring parent for now because it isn't even used
			//aiNode_struct * mParent;
			sceneRootNode->mTransformation = pScene->mRootNode->mTransformation;

			CopyNodeHierarchy(pScene->mRootNode, sceneRootNode);
		}	

		transformedBoundingBoxList.resize(originalBoundingBoxList.size());
		for (unsigned int i = 0; i < transformedBoundingBoxList.size(); i++)
		{
			transformedBoundingBoxList.at(i) = originalBoundingBoxList.at(i);
		}


		printfD("Loading Model %s succeeded\n", filename);
	}
	else
	{
		printfD("Loading Model %s FAILURE. No scene could be extracted.\n", filename);
		result = false;
	}

	return result;
}

void RDE::Model::InitboundingBox(glm::vec3 maxPt, glm::vec3 minPt, bounding_box_struct * source)
{
	// initialize bounding box front face points
	source->frontTopLeft = { minPt.x, maxPt.y, maxPt.z };
	source->frontTopRight = { maxPt.x, maxPt.y, maxPt.z };
	source->frontBottomLeft = { minPt.x, minPt.y, maxPt.z };
	source->frontBottomRight = { maxPt.x, minPt.y, maxPt.z };
	// initialize bounding box back face 
	source->backTopLeft = { minPt.x, maxPt.y, minPt.z };
	source->backTopRight = { maxPt.x, maxPt.y, minPt.z };
	source->backBottomLeft = { minPt.x, minPt.y, minPt.z };
	source->backBottomRight = { maxPt.x, minPt.y, minPt.z };

	source->boxAxisX = {
		source->frontBottomRight.x - source->frontBottomLeft.x,
		source->frontBottomRight.y - source->frontBottomLeft.y,
		source->frontBottomRight.z - source->frontBottomLeft.z
	};
	glm::normalize(source->boxAxisX);

	source->boxAxisY = {
		source->frontTopLeft.x - source->frontBottomLeft.x,
		source->frontTopLeft.y - source->frontBottomLeft.y,
		source->frontTopLeft.z - source->frontBottomLeft.z
	};
	glm::normalize(source->boxAxisY);

	source->boxAxisZ = {
		source->frontBottomLeft.x - source->backBottomLeft.x,
		source->frontBottomLeft.y - source->backBottomLeft.y,
		source->frontBottomLeft.z - source->backBottomLeft.z
	};
	glm::normalize(source->boxAxisZ);
}

glm::mat4 RDE::Model::AiMat4ToGlmMat4(aiMatrix4x4 assimpMatrix)
{
	float aaa[16] = {
		assimpMatrix.a1, assimpMatrix.a2, assimpMatrix.a3, assimpMatrix.a4,
		assimpMatrix.b1, assimpMatrix.b2, assimpMatrix.b3, assimpMatrix.b4,
		assimpMatrix.c1, assimpMatrix.c2, assimpMatrix.c3, assimpMatrix.c4,
		assimpMatrix.d1, assimpMatrix.d2, assimpMatrix.d3, assimpMatrix.d4
	};
	glm::mat4 bbb = glm::make_mat4(aaa);

	/*glm::mat4 bbb(1.0);

	bbb[0][0] = assimpMatrix.a1;
	bbb[0][1] = assimpMatrix.b1;
	bbb[0][2] = assimpMatrix.c1;
	bbb[0][3] = assimpMatrix.d1;

	bbb[1][0] = assimpMatrix.a2;
	bbb[1][1] = assimpMatrix.b2;
	bbb[1][2] = assimpMatrix.c2;
	bbb[1][3] = assimpMatrix.d2;

	bbb[2][0] = assimpMatrix.a3;
	bbb[2][1] = assimpMatrix.b3;
	bbb[2][2] = assimpMatrix.c3;
	bbb[2][3] = assimpMatrix.d3;

	bbb[3][0] = assimpMatrix.a4;
	bbb[3][1] = assimpMatrix.b4;
	bbb[3][2] = assimpMatrix.c4;
	bbb[3][3] = assimpMatrix.d4;

	bbb = glm::transpose(bbb);*/

	return bbb;
}

unsigned int RDE::Model::GetAnimCount()
{
	return animArraySize;
}

float RDE::Model::GetAnimDuration(unsigned int index)
{
	return (float)(animArray[index].mDuration / animArray[index].mTicksPerSecond);
}

// load bone data while also creating a bounding box for each bone (allowing for bounding boxes that are more accurate)
void RDE::Model::LoadBones(const aiMesh* pMesh, int extraOffset)
{
	// for every bone 
	for (unsigned int i = 0; i < pMesh->mNumBones; i++)
	{
		unsigned int BoneIndex = 0;
		std::string BoneName(pMesh->mBones[i]->mName.data);
		//printf("%s\n", pMesh->mBones[i]->mName.data);

		if (m_BoneMapping.find(BoneName) == m_BoneMapping.end())
		{
			BoneIndex = m_NumBones;
			m_NumBones++;
			bone_info_struct bi;
			m_BoneInfo.push_back(bi);
			m_BoneMapping[BoneName] = BoneIndex;
			m_BoneInfo[BoneIndex].BoneOffset = pMesh->mBones[i]->mOffsetMatrix;
		}
		else
		{
			BoneIndex = m_BoneMapping[BoneName];
		}

		glm::vec3 maxPt = glm::vec3(0.0f, 0.0f, 0.0f);
		glm::vec3 minPt = glm::vec3(0.0f, 0.0f, 0.0f);

		//for every weight
		for (unsigned int j = 0; j < pMesh->mBones[i]->mNumWeights; j++)
		{
			unsigned int VertexID = extraOffset + pMesh->mBones[i]->mWeights[j].mVertexId;
			float Weight = pMesh->mBones[i]->mWeights[j].mWeight;
			Bones[VertexID].AddBoneData(BoneIndex, Weight);

			glm::vec3 tempVert = total_vertices.at(VertexID);
			// get min/max points
			if (tempVert.z > maxPt.z) {
				maxPt.z = tempVert.z;
			}
			if (tempVert.z < minPt.z) {
				minPt.z = tempVert.z;
			}

			if (tempVert.x > maxPt.x) {
				maxPt.x = tempVert.x;
			}
			if (tempVert.x < minPt.x) {
				minPt.x = tempVert.x;
			}

			if (tempVert.y > maxPt.y) {
				maxPt.y = tempVert.y;
			}
			if (tempVert.y < minPt.y) {
				minPt.y = tempVert.y;
			}
		}
		bounding_box_struct newBbox = {};
		minPt *= 1.2f;
		maxPt *= 1.2f;
		InitboundingBox(maxPt, minPt, &newBbox);
		originalBoundingBoxList.push_back(newBbox);
	}
	//assert(1 == 0);
}

// calculate bone transformations that go to the shader every frame 
// should be passing in an animation index instead of always 0.
// check using unsigned int aiScene::mNumAnimations and bool aiScene::HasAnimations()
glm::mat4 RDE::Model::BoneTransform(float timeInSeconds, std::vector<glm::mat4>& transforms, unsigned int animIndex)
{
	aiMatrix4x4 identity;

	//float TicksPerSecond = (float)(pScene->mAnimations[0]->mTicksPerSecond != 0 ? pScene->mAnimations[0]->mTicksPerSecond : 25.0f);
	float TicksPerSecond = (float)(animArray[animIndex].mTicksPerSecond != 0 ? animArray[animIndex].mTicksPerSecond : 25.0f);
	float TimeInTicks = timeInSeconds * TicksPerSecond;
	//float AnimationTime = (float)(fmod(TimeInTicks, pScene->mAnimations[0]->mDuration));
	float AnimationTime = (float)(fmod(TimeInTicks, animArray[animIndex].mDuration));

	ReadNodeHeirarchy(AnimationTime, sceneRootNode, identity, animIndex);

	transforms.resize(m_NumBones);

	for (unsigned int i = 0; i < m_NumBones; i++)
	{
		transforms[i] = AiMat4ToGlmMat4(m_BoneInfo[i].FinalTransformation);
		for (unsigned int n = 0; n < 8; n++)
		{
			glm::mat4 transformGLM = AiMat4ToGlmMat4(m_BoneInfo[n].FinalTransformation);
			transformGLM = glm::transpose(transformGLM);
			//transformGLM = glm::inverse(transformGLM);
			transformedBoundingBoxList.at(i + 1).vertices[n] = glm::vec3(transformGLM * glm::vec4(originalBoundingBoxList.at(i + 1).vertices[n], 1.0f));
		}
	}

	//glm::mat4
	return AiMat4ToGlmMat4(identity);
}

glm::mat4 RDE::Model::GetInverseTransform()
{
	return m_GlobalInverseTransformGLM;
}

void RDE::Model::ReadNodeHeirarchy(float AnimationTime, const aiNode_struct* pNode, const aiMatrix4x4 & ParentTransform, unsigned int animIndex)
{
	//std::string NodeName(pNode->mName);
	//std::string NodeName(.data);

	aiMatrix4x4 NodeTransformation(pNode->mTransformation);

	// because animArray[0].aiNodeAnimMap[NodeName] is a pointer, it will return NULL if you try and index it even though
	// normal map behaviour is to create new index element
	const aiNodeAnim *pNodeAnim = animArray[animIndex].aiNodeAnimMap[pNode->mName];

	if (pNodeAnim)
	{
		// Interpolate scaling and generate scaling transformation matrix
		aiVector3D Scaling;
		CalcInterpolatedScaling(Scaling, AnimationTime, pNodeAnim);
		aiMatrix4x4 ScalingM;
		//ScalingM.InitScaleTransform(Scaling.x, Scaling.y, Scaling.z);
		ScalingM.a1 = Scaling.x;
		ScalingM.b2 = Scaling.y;
		ScalingM.c3 = Scaling.z;
		ScalingM.d4 = 1.0f;


		// Interpolate rotation and generate rotation transformation matrix
		aiQuaternion RotationQ;
		CalcInterpolatedRotation(RotationQ, AnimationTime, pNodeAnim);
		aiMatrix4x4 RotationM = aiMatrix4x4(RotationQ.GetMatrix());

		// Interpolate translation and generate translation transformation matrix
		aiVector3D Translation;
		CalcInterpolatedPosition(Translation, AnimationTime, pNodeAnim);
		aiMatrix4x4 TranslationM;
		//TranslationM.InitTranslationTransform(Translation.x, Translation.y, Translation.z);
		TranslationM.a4 = Translation.x;
		TranslationM.b4 = Translation.y;
		TranslationM.c4 = Translation.z;

		// Combine the above transformations
		NodeTransformation = TranslationM * RotationM * ScalingM;
	}

	aiMatrix4x4 GlobalTransformation = ParentTransform * NodeTransformation;
	//glm::mat4 GlobalTransformationGLM = AiMat4ToGlmMat4(GlobalTransformation);

	if (m_BoneMapping.find(pNode->mName) != m_BoneMapping.end())
	{
		unsigned int BoneIndex = m_BoneMapping[pNode->mName];
		m_BoneInfo[BoneIndex].FinalTransformation = m_GlobalInverseTransform * GlobalTransformation * m_BoneInfo[BoneIndex].BoneOffset;


	}

	for (unsigned int i = 0; i < pNode->mNumChildren; i++)
	{
		ReadNodeHeirarchy(AnimationTime, pNode->mChildren[i], GlobalTransformation, animIndex);
	}
}

void RDE::Model::CopyNodeHierarchy(const aiNode * originalNode, aiNode_struct * copyNode)
{
	copyNode->mChildren = new aiNode_struct* [originalNode->mNumChildren];

	for (unsigned int i = 0; i < originalNode->mNumChildren; i++)
	{
		copyNode->mChildren[i] = new aiNode_struct;

		copyNode->mChildren[i]->mMeshes = new unsigned int[originalNode->mChildren[i]->mNumMeshes];
		memcpy(copyNode->mChildren[i]->mMeshes,	originalNode->mChildren[i]->mMeshes, sizeof(originalNode->mChildren[i]->mMeshes[0]) * originalNode->mChildren[i]->mNumMeshes);
		
		copyNode->mChildren[i]->mName =			std::string(originalNode->mChildren[i]->mName.data);
		copyNode->mChildren[i]->mNumChildren	= originalNode->mChildren[i]->mNumChildren;
		copyNode->mChildren[i]->mNumMeshes		= originalNode->mChildren[i]->mNumMeshes;
		copyNode->mChildren[i]->mTransformation = originalNode->mChildren[i]->mTransformation;
		// ignoring parent for now because it isn't even used
		//aiNode_struct * mParent;
		
		CopyNodeHierarchy(originalNode->mChildren[i], copyNode->mChildren[i]);
	}
}

void RDE::Model::CalcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumScalingKeys == 1)
	{
		Out = pNodeAnim->mScalingKeys[0].mValue;
		return;
	}

	unsigned int ScalingIndex = FindScaling(AnimationTime, pNodeAnim);
	unsigned int NextScalingIndex = (ScalingIndex + 1);
	assert(NextScalingIndex < pNodeAnim->mNumScalingKeys);
	float DeltaTime = (float)(pNodeAnim->mScalingKeys[NextScalingIndex].mTime - pNodeAnim->mScalingKeys[ScalingIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mScalingKeys[ScalingIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& Start = pNodeAnim->mScalingKeys[ScalingIndex].mValue;
	const aiVector3D& End = pNodeAnim->mScalingKeys[NextScalingIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}

void RDE::Model::CalcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumPositionKeys == 1)
	{
		Out = pNodeAnim->mPositionKeys[0].mValue;
		return;
	}

	unsigned int PositionIndex = FindPosition(AnimationTime, pNodeAnim);
	unsigned int NextPositionIndex = (PositionIndex + 1);
	assert(NextPositionIndex < pNodeAnim->mNumPositionKeys);
	float DeltaTime = (float)(pNodeAnim->mPositionKeys[NextPositionIndex].mTime - pNodeAnim->mPositionKeys[PositionIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mPositionKeys[PositionIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& Start = pNodeAnim->mPositionKeys[PositionIndex].mValue;
	const aiVector3D& End = pNodeAnim->mPositionKeys[NextPositionIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}

void RDE::Model::CalcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	// we need at least two values to interpolate...
	if (pNodeAnim->mNumRotationKeys == 1)
	{
		Out = pNodeAnim->mRotationKeys[0].mValue;
		return;
	}

	unsigned int RotationIndex = FindRotation(AnimationTime, pNodeAnim);
	unsigned int NextRotationIndex = (RotationIndex + 1);
	assert(NextRotationIndex < pNodeAnim->mNumRotationKeys);
	float DeltaTime = (float)(pNodeAnim->mRotationKeys[NextRotationIndex].mTime - pNodeAnim->mRotationKeys[RotationIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mRotationKeys[RotationIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiQuaternion& StartRotationQ = pNodeAnim->mRotationKeys[RotationIndex].mValue;
	const aiQuaternion& EndRotationQ = pNodeAnim->mRotationKeys[NextRotationIndex].mValue;
	aiQuaternion::Interpolate(Out, StartRotationQ, EndRotationQ, Factor);
	Out = Out.Normalize();
}

unsigned int RDE::Model::FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumRotationKeys > 0);

	for (unsigned int i = 0; i < pNodeAnim->mNumRotationKeys - 1; i++)
	{
		if (AnimationTime < (float)pNodeAnim->mRotationKeys[i + 1].mTime)
		{
			return i;
		}
	}

	assert(0);

	return 0;
}

unsigned int RDE::Model::FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	for (unsigned int i = 0; i < pNodeAnim->mNumPositionKeys - 1; i++)
	{
		if (AnimationTime < (float)pNodeAnim->mPositionKeys[i + 1].mTime)
		{
			return i;
		}
	}

	assert(0);

	return 0;
}

unsigned int RDE::Model::FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumScalingKeys > 0);

	for (unsigned int i = 0; i < pNodeAnim->mNumScalingKeys - 1; i++)
	{
		if (AnimationTime < (float)pNodeAnim->mScalingKeys[i + 1].mTime)
		{
			return i;
		}
	}

	assert(0);

	return 0;
}