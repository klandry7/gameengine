#ifndef GAME_H
#define GAME_H

// need a game state stack / enum

#include <CEGUI\EventArgs.h>

#include "walkingDemo.h"
#include "hierarchyDemo.h"
#include "shaderDemo.h"
#include "mapgenDemo.h"
#include "framerateDemo.h"
//#include "pathfindingDemo.h"
#include <stack>

namespace RDE
{
	class GUI;
}

class Game
{
private:
	enum GAME_STATE
	{
		mainMenuActive,
		walkingDemoActive,
		hierarchyDemoActive,
		shaderDemoActive,
		frameRateDemoActive,
		mapgenDemoActive,
		//pathfindingDemoActive
	};

public:
	Game(float _aspectRatio, const size_t windowRef);
	//void Input();
	void FixedUpdate(float dt);
	bool Update(float dt);
	void Render(float interpolation);

private:
	bool OnExitClicked(const CEGUI::EventArgs & e);
	void OnWalkingDemoClicked(const CEGUI::EventArgs & e);
	
	void OnHierarchyDemoClicked(const CEGUI::EventArgs & e);
	void OnShaderDemoClicked(const CEGUI::EventArgs & e);
	void OnFrameRateDemoClicked(const CEGUI::EventArgs & e);
	void OnMapgenDemoClicked(const CEGUI::EventArgs & e);
	//void OnPathfindingDemoClicked(const CEGUI::EventArgs & e);

	// AI demo??

	void HideButtons();
	void ShowButtons();

private:
	RDE::GUI *mainMenuGui;
	bool continueRunning = true;
	bool modelLoadComplete = false;
	WalkingDemo * walkingDemo;
	HierarchyDemo * hierarchyDemo;
	ShaderDemo * shaderDemo;
	MapgenDemo * mapgenDemo;
	FramerateDemo * framerateDemo;
	//PathfindingDemo * pathfindingDemo;
	float aspectRatio;
	std::stack<GAME_STATE> gameStateStack;
	const size_t wndwRef;

	CEGUI::PushButton * hierarchyDemoButton;
	CEGUI::PushButton * walkingDemoButton;
	CEGUI::PushButton * shaderDemoButton;
	CEGUI::PushButton * mapgenDemoButton;
	CEGUI::PushButton * framerateDemoButton;
	//CEGUI::PushButton * pathfindingDemoButton;
	CEGUI::PushButton * exitButton;


};

#endif // !GAME_H
