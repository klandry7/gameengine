#ifndef DEFAULTPHYSICSCOMPONENT_H
#define DEFAULTPHYSICSCOMPONENT_H

#include "physicsComponent.h"
#include "boundingBox.h"

namespace RDE
{
	class GameObject;
	struct bounding_box_struct;

	namespace MAPGEN
	{
		struct Map;
		struct Rect;
	}
}

class DefaultPhysicsComponent : public RDE::PhysicsComponent
{
public:
	DefaultPhysicsComponent(float width, float height, float tileSizeMod);
	~DefaultPhysicsComponent();
	virtual void Init(RDE::GameObject & self);
	virtual void Update(RDE::GameObject & self, float dt, const RDE::MAPGEN::Map *map);

	
private:
	bool rectRectCollision(float x, float y, float w, float h, float tileX, float tileY, float tileW, float tileH);

private:
	//RDE::bounding_box_struct collisionBoundingBox;
	float bboxWidth;
	float bboxHeight;
	float bboxHalfWidth;
	float bboxHalfHeight;
	float tileSizeModifier;
};

#endif // !DEFAULTPHYSICSCOMPONENT_H

