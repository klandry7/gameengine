#ifndef UPDATECOMPONENT_H
#define UPDATECOMPONENT_H

// forward declaration
namespace RDE
{
	class GameObject;

	class UpdateComponent
	{
	public:
		// virtual init
		virtual void Init(RDE::GameObject & gameObject) = 0;
		// virtual update
		virtual void Update(RDE::GameObject & gameObject, float dt) = 0;
		// virtual dtor
		virtual ~UpdateComponent() {};
	};
}

#endif // !UPDATECOMPONENT_H
