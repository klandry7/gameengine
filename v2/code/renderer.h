#ifndef RENDERER_H
#define RENDERER_H

#include "GLExtension.h"
#include "boundingBox.h"

#include <glm/glm.hpp>

#include <thread>
#include <mutex>
#include <queue>
#include <map>
#include <vector>



namespace RDE
{
	class Model;

	class Renderer
	{
	private:
		// I should have a list of these 
		// the list position corresponds to layout location 
		// you if you ask for list[0] that's implicitly layout location 0 
		// this again gets messy once you have vars spanning multiple layout spaces 
		// I think there needs to be a buffer manager that tracks all this stuff 
		// should also be able to define how many spaces is spans and the manager will track
		// all of this for you 
		// ask manager for layout location 0 structure and it gives it to you 
		// model has a vao construction function that builds based on vao and 
		// buffer layout

		/*struct buffer_layout_struct
		{
			unsigned int dataSize; // sizeof(data type)
								   // glVertexAttribPointer vars
			unsigned int dataLength; // # of attributes ie vec3 has 3, vec2 has 2
			GLenum dataType; // GL_DATATYPE
			GLboolean isNormalized;
			unsigned int stride;
			unsigned int offset;
		};*/

	public:
		// ctor
		Renderer();
		// have an add default buffers function that loads a default setup
		//void AddBufferLayout(unsigned int dataSize, unsigned int attributePosiion, unsigned int dataLength, GLenum dataType, GLboolean isNormalized, unsigned int stride, unsigned int offset);
		// unload model
		void UnloadModel(const char * name);
		// get model anim count
		unsigned int ModelAnimCount(const char * name);
		// get model anim duration
		float GetAnimDuration(const char * name, unsigned int index);
		// get model bone transform list
		std::vector<glm::mat4> GetBoneTransforms(const char * modelName, float animationTime, unsigned int animIndex);
		// draw model indexed
		void DrawModelIndexed(const char * modelName, const char * textureName);
		// draw model instanced
		void DrawModelInstanced(const char * modelName, const char * textureName, unsigned int modelCount, std::vector<glm::mat4>  transformList);


		// load texture into opengl
		void LoadTexture(const char * filepath, const char * name);
		// unload texture
		void UnloadTexture(const char * name);
		// set GL clear colour
		void SetClearColour(float r, float g, float b);
		// get model bounding box list 
		std::vector<bounding_box_struct> GetBoundingBoxList(const char * model);
		// get model inverse transform
		glm::mat4 GetInverseTransform(const char * name);

		//void AddResourceTask(std::function<void()> function, const char * filepath, const char * name);
		//void StartThreadedLoad();
		// load model on a seperate thrtead
		void ThreadedModelLoad(const char * filepath, const char * name, bool flipUVs);
		// is the loading of all models complete?
		bool IsThreadedLoadComplete();
		// bind a model into mem
		void BindModel(const char * name);

	private:
		///<summary>Load a 3D model into memory. Must be loaded before it can be drawn. A wrapper around Assimp. Read here for supported formats: http://assimp.sourceforge.net/lib_html/index.html </summary>
		void LoadModel(const char * filepath, const char * name, bool flipUVs);

	private:
		// the name in the map should be the name in the shader
		// can I force this? You can gather uniforms but i dunno about layout vars

		// a map of the data
		//std::map<unsigned int, buffer_layout_struct> bufferLayout;
		// a vector to track the positions in use
		std::vector<unsigned int> bufferLayoutLocations;
		// map of loaded models
		std::map<std::string, RDE::Model*> loadedModels;
		// map of loaded textures 
		std::map<std::string, GLuint> loadedTextures;
		// default texture 
		GLint defaultTexture = -1;

		//std::mutex resourceQueueMutex;
		//std::queue<resource_task> resourceQueue;
		//int resourceIdCounter;
		// active threads vector
		std::vector<std::thread> activeThreads;
		// model loading mutex
		std::mutex modelVectorMutex;
		// list of threads to delete
		std::vector<std::thread::id> threadsToDelete;
		// threads to delete mutex
		std::mutex threadsToDeleteMutex;
	};

	namespace RENDERER
	{
		extern Renderer * instance;
	};
}

#endif // !RENDERER_H
