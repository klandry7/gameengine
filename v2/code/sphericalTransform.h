#ifndef SPHERICALTRANSFORM_H
#define SPHERICALTRANSFORM_H

#include <glm/glm.hpp>


// need to switch values to mod(0,max)
// need to be able to set in degrees
namespace RDE
{
#define RAD_90 1.571f

	class SphericalTransform
	{
	public:
		/// <summary> Spherical transform constructor. A spherical transform is used for direction/rotation about a sphere
		/// _lockVertAngleTo180 will lock the vertical angle from [-90, 90] (NOTE: all angles are internally radians)
		/// Spherical transform contains no movement or scale parts
		/// Best used for first person camera</summary>
		SphericalTransform(float initialHorizontalAngle, float initialVerticalAngle, bool _lockVertAngleTo180)
		{
			horizontalAngle = initialHorizontalAngle;
			verticalAngle = initialVerticalAngle;
			lockVertAngle = _lockVertAngleTo180;
			vertAngleLockValue = RAD_90;
			UpdateRotation(initialHorizontalAngle, initialVerticalAngle);
		}

		///<summary>Values in radians</summary>
		void UpdateRotation(float horizontalIncrement, float verticalIncrement)
		{
			// compute new orientation
			horizontalAngle += horizontalIncrement;
			verticalAngle += verticalIncrement;

			if (lockVertAngle)
			{
				LockAngle(&verticalAngle, vertAngleLockValue);
			}

			//direction: spherical coordinates to cartesian coordinates conversion
			forward = glm::vec3(
				cos(verticalAngle) * sin(horizontalAngle),
				sin(verticalAngle),
				cos(verticalAngle) * cos(horizontalAngle)
			);

			// right vector
			right = glm::vec3(
				sin(horizontalAngle - 3.14f / 2.0f),
				0,
				cos(horizontalAngle - 3.14f / 2.0f)
			);

			// up vector. the perpendicular to direction and right (cross product between right and direction)
			up = glm::cross(right, forward);

			glm::normalize(forward);
			glm::normalize(up);
			glm::normalize(right);
		}

		///<summary>Values in radians</summary>
		void SetRotation(float newHorizontalAngle, float newVerticalAngle)
		{
			// compute new orientation
			horizontalAngle = newHorizontalAngle;
			verticalAngle = newVerticalAngle;

			//direction: spherical coordinates to cartesian coordinates conversion
			forward = glm::vec3(
				cos(verticalAngle) * sin(horizontalAngle),
				sin(verticalAngle),
				cos(verticalAngle) * cos(horizontalAngle)
			);

			// right vector
			right = glm::vec3(
				sin(horizontalAngle - 3.14f / 2.0f),
				0,
				cos(horizontalAngle - 3.14f / 2.0f)
			);

			// up vector. the perpendicular to direction and right (cross product between right and direction)
			up = glm::cross(right, forward);

			glm::normalize(forward);
			glm::normalize(up);
			glm::normalize(right);
		}

		// for locks, allow different min/max? not for now.

		// unused
		void SetCustomVerticalLockValue(float newValue)
		{

		}

		// unused
		void SetCustomHorizontalLockValue(float newValue)
		{

		}

		// return up vec3
		glm::vec3 GetUp()
		{
			return up;
		}
		// return right vec3
		glm::vec3 GetRight()
		{
			return right;
		}
		// return forward vec3
		glm::vec3 GetForward()
		{
			return forward;
		}

	private:
		// lock the angle to not extend past max value
		void LockAngle(float * angle, float maxValue)
		{
			if (*angle > maxValue)
			{
				*angle = maxValue;
			}
			else if (*angle < -maxValue)
			{
				*angle = -maxValue;
			}
		}

	private:
		// lock vertical?
		bool lockVertAngle;
		// horizontal angle
		float horizontalAngle;
		// vertical angle
		float verticalAngle;
		// forward vec3
		glm::vec3 forward;
		// right vec3
		glm::vec3 right;
		// up vec3
		glm::vec3 up;
		// vertical lock value
		float vertAngleLockValue;
	};
}

#endif // !SPHERICALTRANSFORM_H