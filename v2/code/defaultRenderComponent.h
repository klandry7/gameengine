#ifndef DEFAULTRENDERCOMPONENT_H
#define DEFAULTRENDERCOMPONENT_H

#include "renderComponent.h"
class GameObject;
//class Shader;

#include <vector>


class DefaultRenderComponent : public RDE::RenderComponent
{
public:
	DefaultRenderComponent(const char * modelName, const char * textureName);
	~DefaultRenderComponent();

	virtual void Init(RDE::GameObject & gameObject);
	virtual void Update(RDE::GameObject & self, glm::mat4 parentTransform, float interpolation);
	virtual std::vector<RDE::bounding_box_struct> GetBbox();

public:
	const char * modelName;
	const char * textureName;
private:
	bool animated;
};

#endif // !DEFAULTRENDERCOMPONENT_H