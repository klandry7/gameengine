#ifndef PATHFINDINGDEMO_H
#define PATHFINDINGDEMO_H

#include <memory>
#include <glm\glm.hpp>
#include "mapGenerator.h"

namespace RDE
{
	class Shader;
	class Camera;
	class GameObject;
	class GUI;
}

class PathfindingDemo
{
public:
	PathfindingDemo(float aspectRatio);
	~PathfindingDemo();

	void FixedUpdate(float dt);
	bool Update(float dt);
	void Render(float interpolation);

	void RollNewMap(RDE::MAPGEN::LAYOUT_TYPES layoutType);
	void DeleteCurrentMap();
	void MapTileSelect(int x, int y);

private:
	std::shared_ptr<const RDE::Shader> simpleShader;
	RDE::Camera *camera;

	bool continueRunning = true;
	float mapTileSize;

	bool firstPass = true;

	glm::vec3 startPos;
	glm::vec3 destPos;
	std::vector <glm::mat4> path;


	RDE::MAPGEN::Map currentMap;
	std::vector<glm::mat4> roomFloorList;
	std::vector<glm::mat4> tiles_noEdge;
	std::vector<glm::mat4> tiles_1edge;
	std::vector<glm::mat4> tiles_2edgeAdjacent;
	std::vector<glm::mat4> tiles_2edgeOpposite;
	std::vector<glm::mat4> tiles_3edge;
	std::vector<glm::mat4> tiles_4edge;
};


#endif // !PATHFINDINGDEMO_H
