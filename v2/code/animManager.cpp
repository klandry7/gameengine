#include "animManager.h"
#include "renderer.h"

RDE::AnimManager::AnimManager()
{
	animCount = 0;
	currentActiveAnim = 0;
	modelName = " ";
	animationTime = 0.0f;
}

RDE::AnimManager::~AnimManager()
{
	// can't delete const char * 
	//delete modelName;
	delete anims;
}

void RDE::AnimManager::Init(const char * name, unsigned int _animCount)
{
	animCount = _animCount;
	modelName = name;
	
	anims = new anim_struct[animCount];

	for (unsigned int i = 0; i < animCount; i++)
	{
		anims[i].duration = RDE::RENDERER::instance->GetAnimDuration(modelName, i);
		anims[i].repeating = false;
	}
}

void RDE::AnimManager::Update(float dt)
{	
	if (animCount <= 0)
		return;

	animationTime += dt;

	if (ActiveAnimComplete() && !anims[currentActiveAnim].repeating)
	{
		animationTime -= dt;
	}
}

void RDE::AnimManager::SetActiveAnimation(unsigned int newActive, bool loopingState)
{
	if (newActive < animCount || animCount >= 0)
	{
		currentActiveAnim = newActive;
		SetLooping(loopingState);
	}
}

bool RDE::AnimManager::ActiveAnimComplete()
{
	if (animCount <= 0)
	{
		return false;
	}
	else
	{
		return animationTime >= anims[currentActiveAnim].duration ? true : false;
	}
}

void RDE::AnimManager::SetLooping(bool state)
{
	if (animCount <= 0)
	{
		return;
	}
	else
	{
		anims[currentActiveAnim].repeating = state;
	}
}

unsigned int RDE::AnimManager::CurrentActiveAnim()
{
	return currentActiveAnim;
}

float RDE::AnimManager::CurrentActiveAnimTime()
{
	return animationTime;
}

int RDE::AnimManager::GetAnimCount()
{
	return animCount;
}
