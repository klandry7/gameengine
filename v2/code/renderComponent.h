#ifndef RENDERCOMPONENT_H
#define RENDERCOMPONENT_H

#include <glm\glm.hpp>
//#include "boundingBox.h"
#include <vector>

namespace RDE
{
	class GameObject;
	struct bounding_box_struct;
	
	class RenderComponent
	{
	public:
		// virtual init
		virtual void Init(RDE::GameObject & gameObject) = 0;
		// virtual update
		virtual void Update(RDE::GameObject & self, glm::mat4 parentTransform, float interpolation) = 0;
		// virtual bbox get
		virtual std::vector<RDE::bounding_box_struct> GetBbox() = 0;
		// virtual dtor
		virtual ~RenderComponent() {};
	};
}

#endif // !RENDERCOMPONENT_H
