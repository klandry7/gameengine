#include "windowApp.h"

//  debug only
#include <iostream>

//#define GLFW_OPENGL_PROFILE 1

#define FULLSCREEN_ON 0

void RDE::WindowApp::PostWindowCloseMessage(bool state)
{
	glfwSetWindowShouldClose(window, state);
}

bool32 RDE::WindowApp::WindowShouldClose()
{
	return glfwWindowShouldClose(window);
}

void RDE::WindowApp::SetTime(real32 time)
{
	glfwSetTime((real64)time);
}

real32 RDE::WindowApp::GetTime()
{
	return (real32)glfwGetTime();
}

void RDE::WindowApp::Terminate()
{
	glfwTerminate();
}

void RDE::WindowApp::SetMouseState(bool32 show, bool32 restrictToWindow)
{
	if (show)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
	else if (!restrictToWindow)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	}
	else if (restrictToWindow)
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	}
}

// check if the provided window handle is valid and create its OpenGL context
int RDE::WindowApp::InitGLEW()
{
#if GLEW_ACTIVE
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (err != GLEW_OK) {
		std::cout << "Glew Error: -> " << glewGetErrorString(err) << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}
#endif 
	return 0;
}

RDE::WindowApp::WindowApp(int _windowWidth, int _windowHeight, const char * windowName)
{
	if (InitGLFW(3, 3) != 0)
	{
		fprintf(stderr, "Failed to init GLFW.\n");
		getchar();
		exit(0);
	}

	windowWidth = _windowWidth;
	windowHeight = _windowHeight;

	//monitors = glfwGetMonitors(&monitorCount);

	// start the window as invisible.
	// can perform loading and then window can be shown whenever
	glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

	// for fullscreen put this in first null -> glfwGetPrimaryMonitor()
	if (FULLSCREEN_ON)
	{
		window = glfwCreateWindow(windowWidth, windowHeight, windowName, glfwGetPrimaryMonitor(), NULL);
	}
	else
	{
		window = glfwCreateWindow(windowWidth, windowHeight, windowName, NULL, NULL);
	}


	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window.\n");
		getchar();
		glfwTerminate();
		exit(1);
	}
	

	glfwMakeContextCurrent(window);

#if GLEW_ACTIVE
	// setup glew
	if (InitGLEW() == -1)
	{
		exit(1);
	}
#elif defined(NANOGUI_GLAD)
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		throw std::runtime_error("Could not initialize GLAD!");
	glGetError(); // pull and ignore unhandled errors like GL_INVALID_ENUM
#endif
	
	// Ensure we can capture the input
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);


	//printf("OpenGL version is (%s)\n", glGetString(GL_VERSION)); 
	printf("GPU is (%s)\n", glGetString(GL_RENDERER)); 
	// printf("max shader layout location vars %i\n", GL_MAX_VARYING_COMPONENTS/4);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LEQUAL);
	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);
	// cull back faces 
	glCullFace(GL_BACK);
	// front faces are clockwise
	glFrontFace(GL_CCW);
	// default clear colour to dark blue 
	glClearColor(0.0f, 0.0f, 0.4f, 1.0f);

	glEnable(GL_BLEND);

	

	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	
}

void RDE::WindowApp::GetScreenCentre(float *halfX, float *halfY)
{
	*halfX = windowWidth * 0.5f;
	*halfY = windowHeight * 0.5f;
}

void RDE::WindowApp::SetDoubleBuffering(bool state)
{
	glfwWindowHint(GLFW_DOUBLEBUFFER, state);
	doubleBuffered = state;

	if (doubleBuffered == false)
	{
		glfwSwapInterval(0);
		glDrawBuffer(GL_FRONT);
		glReadBuffer(GL_FRONT);
	}
	else
	{
		glfwSwapInterval(1);
		glDrawBuffer(GL_BACK);
		glReadBuffer(GL_BACK);
	}
}

void RDE::WindowApp::SetAASampleRate(int samples)
{
	// can't anti alias without double buffering on.
	glfwWindowHint(GLFW_SAMPLES, samples);
	aaSampleRate = samples;
}


int RDE::WindowApp::InitGLFW(int OGLversionMajor, int OGLversionMinor)
{
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, OGLversionMajor);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, OGLversionMinor);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	glfwWindowHint(GLFW_DEPTH_BITS, 16);
	glfwWindowHint(GLFW_RED_BITS, 8);
	glfwWindowHint(GLFW_GREEN_BITS, 8);
	glfwWindowHint(GLFW_BLUE_BITS, 8);
	glfwWindowHint(GLFW_ALPHA_BITS, 8);

	SetDoubleBuffering(true);
	SetAASampleRate(4);

	
	glfwSetTime(0.0);

	return 0;
}


// this signifies the end of a frame. 
void RDE::WindowApp::SwapBuffers()
{

	// not nessecary
	glBindTexture(GL_TEXTURE_2D, 0);
	// not nessecary
	glBindVertexArray(0);
	glUseProgram(0);

	if (doubleBuffered)
	{
		glfwSwapBuffers(window);
	}
	else
	{
		glFlush();
		glFinish();
	}
}

void RDE::WindowApp::PollEvents()
{
	glfwPollEvents();
}

void RDE::WindowApp::ClearBuffers()
{
	// clear the buffers  
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);// | GL_STENCIL_BUFFER_BIT);
}

void RDE::WindowApp::ShowWindow()
{
	glfwShowWindow(window);
}
