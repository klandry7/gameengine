#include "hierarchyTraversal.h"

#include "gameObject.h"
#include "camera.h"
#include "debugIO.h"

#include <stack>
#include <glm\glm.hpp>
#include "transform.h"

#define MODEL_COUNTER_ACTIVE 0

void RDE::HIERARCHY::RenderHierarchyTraversal(RDE::GameObject * rootNode, RDE::Camera * camera, float interpolation)
{
	// the traversal stack
	std::stack<RDE::GameObject *> renderTraversalStack;
	// the path of nodes from the root 
	std::stack<RDE::GameObject *> pathStack;
	// the transform stack
	std::stack<glm::mat4> transformStack;
	transformStack.push(glm::mat4(1.0f));
	renderTraversalStack.push(rootNode);
	//unsigned int depth = 0;

#if MODEL_COUNTER_ACTIVE
	unsigned int modelCounter = 0;
#endif // MODEL_COUNTER_ACTIVE

	// depth first traversal of the scene
	while (!renderTraversalStack.empty())
	{
		// get the node currently on the top of the stack
		RDE::GameObject *current = renderTraversalStack.top();

		// if the node currently on the top of the traversal stack is equal to the node on the top of the path stack 
		// then that means everything below it has been visited 
		if (!pathStack.empty() && current == pathStack.top())
		{
			pathStack.pop();
			renderTraversalStack.pop();
			// since everything below this node has been visited, we can now pop off it's transformation matrix 
			transformStack.pop();
		}
		// else, we're visiting a new node
		else
		{
			// check if we actually want to render this node. still apply it's transformations regardless of visibility

			if (!current->IsRoot() && current->RenderEnabled())
			{
				std::vector<RDE::bounding_box_struct> bl = current->GetBoundingBox();
				//simpleShader->SetUniform1i("animated", false);

				//glm::mat4 inverseTransform = renderContext::renderer->GetInverseTransform("henrietta");
				glm::mat4 translation = transformStack.top() * current->transform->GetMatrix();// *glm::inverse(inverseTransform);//current->transform->GetMatrix() * transformStack.top();
				bool rend = false;

				for (unsigned int i = 0; i < bl.size(); i++)
				{
					RDE::bounding_box_struct b = bl.at(i);

					/*for (unsigned int n = 0; n < 8; n++)
					{
					simpleShader->SetUniformMatrix4fv("transformMatrix", false, translation * glm::translate(glm::mat4(1.0f), b.vertices[n]));
					renderContext::renderer->DrawModel("sphere", "uvMap");
					}*/

					if (camera->BoundingBoxIsInView(b, translation))
					{
						rend = true;
						break;
					}
				}
				if (rend)//camera->BoundingBoxIsInView(current->GetBoundingBox().at(0), (current->transform->GetMatrix() * transformStack.top())))
				{
					current->Render(transformStack.top(), interpolation);
#if MODEL_COUNTER_ACTIVE
					modelCounter++;
#endif
				}
			}
			/*glm::mat4 inverseTransform = renderContext::renderer->GetInverseTransform("henrietta");
			glm::mat4 translation = transformStack.top() * current->transform->GetMatrix() * inverseTransform;
			glm::vec3 temp = glm::vec3(translation * glm::vec4(0,0,0, 1.0f));

			if (camera->IsInView(temp))
			{
			current->Render(transformStack.top(), interpolation);
			}*/


			/*if (!current->IsRoot() && current->RenderEnabled() && camera->BoundingBoxIsInView(current->GetBoundingBox().at(0), (current->transform->GetMatrix() * transformStack.top()) ))
			{
			current->Render(transformStack.top(), interpolation);
			}*/

			pathStack.push(current);
			// do I want to push the whole transformation matrix (which includes scale) or just translation and rotation???
			// set an "inherit scale" flag?
			transformStack.push(current->transform->GetMatrix() * transformStack.top());

			for (unsigned int i = 0; i < current->GetChildrenCount(); i++)
			{
				renderTraversalStack.push(current->GetChild(i));
			}
		}
	}
#if MODEL_COUNTER_ACTIVE
	printfD("model count: %i\n", modelCounter);
#endif
}

void RDE::HIERARCHY::UpdateHierarchyTraversal(RDE::GameObject * rootNode, float dt)
{
	// the traversal stack
	std::stack<RDE::GameObject *> renderTraversalStack;
	// the path of nodes from the root 
	std::stack<RDE::GameObject *> pathStack;
	renderTraversalStack.push(rootNode);

	// depth first traversal of the scene
	while (!renderTraversalStack.empty())
	{
		// get the node currently on the top of the stack
		RDE::GameObject *current = renderTraversalStack.top();

		// if the node currently on the top of the traversal stack is equal to the node on the top of the path stack 
		// then that means everything below it has been visited 
		if (!pathStack.empty() && current == pathStack.top())
		{
			pathStack.pop();
			renderTraversalStack.pop();
		}
		// else, we're visiting a new node
		else
		{
			// check if we actually want to render this node. still apply it's transformations regardless of visibility
			if (!current->IsRoot() && current->UpdateEnabled())
			{
				current->Update(dt);
			}

			pathStack.push(current);

			for (unsigned int i = 0; i < current->GetChildrenCount(); i++)
			{
				renderTraversalStack.push(current->GetChild(i));
			}
		}
	}
}