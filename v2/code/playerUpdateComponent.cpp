#include "playerUpdateComponent.h"
#include "input.h"
#include <glm\glm.hpp>
#include "gameObject.h"
#include "camera.h"
#include "sphericalTransform.h"
#include "transform.h"
#include "animManager.h"

#define DEBUG_MOVE 0

PlayerUpdateComponent::PlayerUpdateComponent(float aspectRatio)
{
	//gameObject.sphericalTransform = new RDE::SphericalTransform(0, 0, true);
	//gameObject.sphericalTransform->SetRotation(0.64f, 0.0f);
}

PlayerUpdateComponent::~PlayerUpdateComponent()
{

}

void PlayerUpdateComponent::Update(RDE::GameObject & gameObject, float dt)
{
	float speed = 100.0f;
	if (RDE::INPUT::instance->IsJoystickPresent())
	{
		float horizontalAxis = -RDE::INPUT::instance->GetAxisState(RIGHT_HORIZONTAL_AXIS, 0) * camRotVel * dt;
		float verticalAxis = -RDE::INPUT::instance->GetAxisState(RIGHT_VERTICAL_AXIS, 0) * camRotVel * dt;
		gameObject.sphericalTransform->UpdateRotation(horizontalAxis, verticalAxis);

		float multiplier = 1;
		if (DEBUG_MOVE)
		{
			if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::action3, RDE::INPUT::INPUT_TYPES::joypad1, RDE::INPUT::BUTTON_STATE::heldDown))
			{
				multiplier = 3;
			}
			if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::action2, RDE::INPUT::INPUT_TYPES::joypad1, RDE::INPUT::BUTTON_STATE::heldDown))
			{
				gameObject.velocity.y -= 0.03f * multiplier;
			}
			else if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::action1, RDE::INPUT::INPUT_TYPES::joypad1, RDE::INPUT::BUTTON_STATE::heldDown))
			{
				gameObject.velocity.y += 0.03f * multiplier;
			}
		}

		glm::vec3 dir = gameObject.sphericalTransform->GetForward();
		glm::normalize(dir);
		// left/right
		if (RDE::INPUT::instance->GetAxisState(LEFT_VERTICAL_AXIS, 0) < 0)
		{
			gameObject.velocity += glm::vec3(dir.x, 0, dir.z) *  dt * speed * multiplier;
		}
		else if (RDE::INPUT::instance->GetAxisState(LEFT_VERTICAL_AXIS, 0) > 0)
		{
			gameObject.velocity += glm::vec3(dir.x, 0, dir.z) * -dt * speed * multiplier;
		}
		// forward/backward
		if (RDE::INPUT::instance->GetAxisState(LEFT_HORIZONTAL_AXIS, 0) < 0)
		{
			gameObject.velocity += gameObject.sphericalTransform->GetRight() * -dt * speed * multiplier;
		}
		else if (RDE::INPUT::instance->GetAxisState(LEFT_HORIZONTAL_AXIS, 0) > 0)
		{
			gameObject.velocity += gameObject.sphericalTransform->GetRight() * dt * speed * multiplier;
		}
	}
	else
	{
		glm::vec3 dir = gameObject.sphericalTransform->GetForward();
		glm::normalize(dir);

		// forward /backward
		if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::up, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::heldDown))
		{
			gameObject.velocity += glm::vec3(dir.x, 0, dir.z) *  dt * speed;
		}
		else if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::down, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::heldDown))
		{
			gameObject.velocity += glm::vec3(dir.x, 0, dir.z) * -dt * speed;
		}
		// L/R
		if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::left, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::heldDown))
		{
			gameObject.velocity += gameObject.sphericalTransform->GetRight() * -dt * speed;
		}
		else if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::right, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::heldDown))
		{
			gameObject.velocity += gameObject.sphericalTransform->GetRight() * dt * speed;
		}
	}
	if (fabsf(glm::length(gameObject.velocity)) > 0.5f)
	{
		gameObject.velocity = gameObject.velocity - (0.05f * gameObject.velocity * dt);
	}

	if(glm::length(gameObject.velocity) > 0.5f || glm::length(gameObject.velocity) < -0.5f)
		gameObject.animManager->Update(dt);

	float rad = atan2(gameObject.velocity.z, -gameObject.velocity.x);
	gameObject.transform->rotation.y = rad;
}

void PlayerUpdateComponent::Init(RDE::GameObject & gameObject)
{
	gameObject.animManager->SetActiveAnimation(0, true);
}
