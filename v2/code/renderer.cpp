#include "renderer.h"
#include "debugIO.h"
#include "model.h"

#include <iostream>

RDE::Renderer* RDE::RENDERER::instance = new RDE::Renderer();

// for testing 
// http://www.opengl-tutorial.org/beginners-tutorials/tutorial-5-a-textured-cube/
GLuint loadBMP_custom(const char * imagepath)
{
	printfD("Reading image %s ... ", imagepath);

	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file;
	fopen_s(&file, imagepath, "rb");
	if (!file) { printfD("FAILURE. File could not be opened. Are you in the right directory ? \n"); return 0; }

	// Read the header, i.e. the 54 first bytes

	// If less than 54 bytes are read, problem
	if (fread(header, 1, 54, file) != 54) {
		printfD("FAILURE. Not a correct BMP file\n");
		return 0;
	}
	// A BMP files always begins with "BM"
	if (header[0] != 'B' || header[1] != 'M') {
		printfD("FAILURE. Not a correct BMP file\n");
		return 0;
	}
	// Make sure this is a 24bpp file
	if (*(int*)&(header[0x1E]) != 0) { printfD("FAILURE. Not a correct BMP file\n");    return 0; }
	if (*(int*)&(header[0x1C]) != 24) { printfD("FAILURE. Not a correct BMP file\n");    return 0; }

	// Read the information about the image
	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);

	// Some BMP files are misformatted, guess missing information
	if (imageSize == 0)    imageSize = width*height * 3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos == 0)      dataPos = 54; // The BMP header is done that way

										 // Create a buffer
	data = new unsigned char[imageSize];

	// Read the actual data from the file into the buffer
	fread(data, 1, imageSize, file);

	// Everything is in memory now, the file wan be closed
	fclose(file);

	// Create one OpenGL texture
	GLuint textureID;
	glGenTextures(1, &textureID);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	// OpenGL has now copied the data. Free our own version
	delete[] data;

	// nice trilinear filtering.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);

	printfD("successful\n");

	// Return the ID of the texture we just created
	return textureID;
}

RDE::Renderer::Renderer()
{
	//resourceIdCounter = 0;
}

/*void RDE::Renderer::AddBufferLayout(
	unsigned int layoutLocation,
	unsigned int dataSize,
	unsigned int dataLength,
	GLenum dataType,
	GLboolean isNormalized,
	unsigned int stride,
	unsigned int offset)
{
	buffer_layout_struct newBufferLayout = {};
	newBufferLayout.dataSize = dataSize;
	newBufferLayout.dataLength = dataLength;
	newBufferLayout.dataType = dataType;
	newBufferLayout.isNormalized = isNormalized;
	newBufferLayout.stride = stride;
	newBufferLayout.offset = offset;
	// a map of the data
	bufferLayout.insert(std::make_pair(layoutLocation, newBufferLayout));
	// a vector to track the positions in use
	bufferLayoutLocations.push_back(layoutLocation);
}*/

/*

{
//GLuint bufferObject;
unsigned int dataSize; // sizeof(data type)
// glVertexAttribPointer vars
unsigned int attributePosiion;
unsigned int dataLength; // # of attributes ie vec3 has 3, vec2 has 2
GLenum dataType;
GLboolean isNormalized;
unsigned int stride;
unsigned int offset;
}buffer_layout_struct;
*/


// remove model function 
// look at UpdateTranslationMatrixBuffer for a glBufferSubData example for removing models or updating
// need to make sure if you try to remove you can't render it anymore. render a primitive shape 
// should have a set of primitives that cannot be removed and are added on startup
// cube, quad, capsule, sphere


void RDE::Renderer::LoadModel(const char * filepath, const char * name, bool flipUVs)
{
	//printf("path: %s, name: %s\n", filepath, name);

	// if it isn't loaded
	if (loadedModels.find(std::string(name)) == loadedModels.end())
	{
		modelVectorMutex.lock();
			loadedModels.insert(std::make_pair(std::string(name), new RDE::Model()));
		modelVectorMutex.unlock();
		loadedModels.at(std::string(name))->LoadData(filepath, flipUVs);
	}

	//threadsToDelete.insert(std::make_pair(threadId, true));
	threadsToDeleteMutex.lock();
		threadsToDelete.push_back(std::this_thread::get_id());
	threadsToDeleteMutex.unlock();
}

unsigned int RDE::Renderer::ModelAnimCount(const char * name)
{
	if (loadedModels.find(name) == loadedModels.end())
	{
		return 0;
	}
	else 
	{
		return loadedModels.at(name)->GetAnimCount();
	}
}

float RDE::Renderer::GetAnimDuration(const char * name, unsigned int index)
{
	return loadedModels.at(std::string(name))->GetAnimDuration(index);
}

void RDE::Renderer::UnloadModel(const char * name)
{

}

std::vector<glm::mat4> RDE::Renderer::GetBoneTransforms(const char * modelName, float animationTime, unsigned int animIndex)
{
	//animationTime += 0.016f;
	// a vector in which to store the transforms
	std::vector<glm::mat4> result;

	if (loadedModels.find(modelName) == loadedModels.end())
		return result;

	if (animIndex > loadedModels.at(std::string(modelName))->GetAnimCount()-1)
	{
		return result;
	}

	// get the transforms 
	loadedModels.at(std::string(modelName))->BoneTransform(animationTime, result, animIndex);
	return result;
}

void RDE::Renderer::DrawModelIndexed(const char * modelName, const char * textureName)
{
	// don't draw a model that doesn't exists.
	// to be replaced with drawing a default object
	if (loadedModels.find(modelName) == loadedModels.end())
		return;

	if (loadedTextures.find(textureName) == loadedTextures.end()){
		if (defaultTexture == -1)
		{
			defaultTexture = loadBMP_custom("../models/pink-pixel.bmp");
			if (defaultTexture == 0)
			{
				// should create new 1x1 pink pixel
				// should also be able to mount directories. static ../models/* sucks
				return;
			} 
		}
		glBindTexture(GL_TEXTURE_2D, defaultTexture);
	}
	else{
		glBindTexture(GL_TEXTURE_2D, loadedTextures.at(std::string(textureName)));
	}
	
	loadedModels.at(std::string(modelName))->DrawIndexed();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void RDE::Renderer::DrawModelInstanced(const char * modelName, const char * textureName, unsigned int modelCount, std::vector<glm::mat4> transformList)
{
	if (loadedModels.find(modelName) == loadedModels.end())
		return;

	if (loadedModels.find(modelName) == loadedModels.end())
		return;

	if (loadedTextures.find(textureName) == loadedTextures.end()) {
		if (defaultTexture == -1)
		{
			defaultTexture = loadBMP_custom("../models/pink-pixel.bmp");
			if (defaultTexture == 0)
			{
				// should create new 1x1 pink pixel
				// should also be able to mount directories. static ../models/* sucks
				return;
			}
		}
		glBindTexture(GL_TEXTURE_2D, defaultTexture);
	}
	else {
		glBindTexture(GL_TEXTURE_2D, loadedTextures.at(std::string(textureName)));
	}

	loadedModels.at(std::string(modelName))->DrawInstanced(modelCount, transformList);
	glBindTexture(GL_TEXTURE_2D, 0);
}



void RDE::Renderer::LoadTexture(const char * filepath, const char * name)
{
	loadedTextures.insert(std::make_pair(std::string(name), loadBMP_custom(filepath)));
}

void RDE::Renderer::UnloadTexture(const char * name)
{
	//glDeleteTextures(1, &tex_2d);
}

void RDE::Renderer::SetClearColour(float r, float g, float b)
{
	glClearColor(r, g, b, 1.0f);
}

std::vector<RDE::bounding_box_struct> RDE::Renderer::GetBoundingBoxList(const char * model)
{
	std::vector<RDE::bounding_box_struct> result;
	if (!(loadedModels.find(model) == loadedModels.end()))
	{
		result = loadedModels.at(std::string(model))->GetBoundingBoxList();
	}
	return result;
}

glm::mat4 RDE::Renderer::GetInverseTransform(const char * name)
{
	return loadedModels.at(std::string(name))->GetInverseTransform();
}

/*void RDE::Renderer::AddResourceTask(std::function<void()> function, const char * filepath, const char * name)
{
	resource_task newRT;
	newRT.f = function;
	newRT.id = resourceIdCounter++;
	newRT.name = std::string(name);
	newRT.path = std::string(filepath);
	resourceQueueMutex.lock();
		resourceQueue.push(newRT);
	resourceQueueMutex.unlock();
}*/

/*void RDE::Renderer::StartThreadedLoad()
{

}*/

void RDE::Renderer::ThreadedModelLoad(const char * filepath, const char * name, bool flipUVs)
{
	//thread_struct newTS;
	//newTS.t = std::thread(&RDE::Renderer::LoadModel, this, filepath, name, flipUV);
	//newTS.id = newTS.t.get_id();
	//activeThreads.push_back(newTS);
	
	activeThreads.push_back(std::thread(&RDE::Renderer::LoadModel, this, filepath, name, flipUVs));
}

// call this to join threads and check if anything is still loading
bool RDE::Renderer::IsThreadedLoadComplete()
{
	for (int i = activeThreads.size()-1; i >= 0; i--)
	{
		if (activeThreads.at(i).joinable())
		{
			bool canJoin = false;
			threadsToDeleteMutex.lock();
			for (int n = threadsToDelete.size() - 1; n >= 0; n--)
			{
				//std::cout << "compare... " << threadsToDelete.at(n) << " and " << activeThreads.at(i).get_id() << std::endl;
				if (threadsToDelete.at(n) == activeThreads.at(i).get_id())
				{
					canJoin = true;
					threadsToDelete.erase(threadsToDelete.begin() + n);
					break;
				}
			}
			threadsToDeleteMutex.unlock();

			if (canJoin)
			{
				activeThreads.at(i).join();
				activeThreads.erase(activeThreads.begin() + i);
			}
		}
	}

	if (activeThreads.size() > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void RDE::Renderer::BindModel(const char * name)
{
	if (loadedModels.find(std::string(name)) == loadedModels.end())
		return;

	loadedModels.at(std::string(name))->BindData();
}
