#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace RDE
{
	class Transform
	{
	public:
		/// <summary>
		/// Transform constructor. Initialize translation to zero, rotation to zero, scale to one
		/// Currently, rotations are in radians
		/// </summary>
		Transform()
		{
			translation = glm::vec3(0.0f, 0.0f, 0.0f);
			rotation = glm::vec3(0.0f, 0.0f, 0.0f);
			scale = glm::vec3(1.0f, 1.0f, 1.0f);
		}
		/// <summary>
		/// Create and return the Transformation (Translate Rotate Scale) Mat4
		/// </summary>
		glm::mat4 GetMatrix()
		{
			// translation
			glm::mat4 translationMatrix = glm::translate(glm::mat4(1.0f), translation);
			// rotation
			glm::mat4 rotationMatrix = glm::rotate(glm::mat4(1.0f), rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
			rotationMatrix = glm::rotate(rotationMatrix, rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
			rotationMatrix = glm::rotate(rotationMatrix, rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
			// scale
			glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0f), scale);
			// return result
			return translationMatrix * rotationMatrix * scaleMatrix;
		}

		/// <summary>
		/// Create and return the translation/rotation Mat4
		/// </summary>
		/*glm::mat4 GetTranslationRotationMatrix()
		{
		// translation
		glm::mat4 translationMatrix = glm::translate(glm::mat4(1.0f), translation);
		// rotation
		glm::mat4 rotationMatrix = glm::rotate(translationMatrix, rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
		rotationMatrix = glm::rotate(rotationMatrix, rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
		rotationMatrix = glm::rotate(rotationMatrix, rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
		// return result
		return rotationMatrix;
		}*/

	public:
		// translation vec3
		glm::vec3 translation;
		// rotation vec3
		glm::vec3 rotation;
		// scale vec3
		glm::vec3 scale;
	};
}

#endif // !TRANSFORM_H