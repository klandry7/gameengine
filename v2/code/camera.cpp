#include "camera.h"
#include <glm/gtc/matrix_transform.hpp>

#include "debugIO.h"

RDE::Camera::Camera(float _displayRatio)
{
	updateProjection = true;
	updateView = true;
	pos = glm::vec3(0, 0, -1);
	lookatPt = glm::vec3(0, 0, 1);
	up = glm::vec3(0, 1, 0);
	fov = 45.0f;
	zNear = 0.01f;
	zFar = 100.0f;
	displayRatio = _displayRatio;
	frustum = new CFrustum(GetProjViewMatrix());
}

RDE::Camera::~Camera()
{
	delete frustum;
}

glm::mat4 RDE::Camera::GetViewMatrix()
{
	if (updateView)
	{
		updateView = false;
		viewMatrix = glm::lookAt(pos, lookatPt, up);
	}
	return viewMatrix;
}
glm::mat4 RDE::Camera::GetProjectionMatrix()
{
	if (updateProjection)
	{
		updateProjection = false;
		projectionMatrix = glm::perspective(fov, displayRatio, zNear, zFar);
	}
	return projectionMatrix;
}

glm::mat4 RDE::Camera::GetProjViewMatrix()
{
	// could save a projView matrix to possibly save one a matrix multiplication
	glm::mat4 result = GetProjectionMatrix() * GetViewMatrix();
	return (result);
}

void RDE::Camera::SetPos(glm::vec3 v)
{
	pos = v;
	updateView = true;
}
void RDE::Camera::SetLookatPt(glm::vec3 v)
{
	lookatPt = v;
	updateView = true;
}
void RDE::Camera::SetUp(glm::vec3 v)
{
	up = v;
	updateView = true;
}

void RDE::Camera::SetFov(float f)
{
	fov = f;
	updateProjection = true;
}
void RDE::Camera::SetZNear(float f)
{
	zNear = f;
	updateProjection = true;
}
void RDE::Camera::SetZFar(float f)
{
	zFar = f;
	updateProjection = true;
}
void RDE::Camera::SetDisplayRatio(float f)
{
	displayRatio = f;
	updateProjection = true;
}

bool RDE::Camera::BoundingBoxIsInView(const bounding_box_struct & bbox, glm::mat4 translation)
{
	// update view and projection matrices and set the frustum
	/*if (updateProjection || updateView)
	{	
	}*/
	frustum->Set(GetProjViewMatrix());
	
	// also need to transform bbox corners based on the object's transform and hierarchial transform
	// you'll have both because culling is only done in render hierarchy traversal

	// this is a massive waste. should just use vec4 for bbox. change it once it's confirmed working
	bounding_box_struct transformedBbox = {};
	transformedBbox.frontTopLeft		= glm::vec3((translation * glm::vec4(bbox.frontTopLeft, 1.0)));
	transformedBbox.frontTopRight		= glm::vec3((translation * glm::vec4(bbox.frontTopRight, 1.0)));
	transformedBbox.frontBottomLeft		= glm::vec3((translation * glm::vec4(bbox.frontBottomLeft, 1.0)));
	transformedBbox.frontBottomRight	= glm::vec3((translation * glm::vec4(bbox.frontBottomRight, 1.0)));
	transformedBbox.backTopLeft			= glm::vec3((translation * glm::vec4(bbox.backTopLeft, 1.0)));
	transformedBbox.backTopRight		= glm::vec3((translation * glm::vec4(bbox.backTopRight, 1.0)));
	transformedBbox.backBottomLeft		= glm::vec3((translation * glm::vec4(bbox.backBottomLeft, 1.0)));
	transformedBbox.backBottomRight		= glm::vec3((translation * glm::vec4(bbox.backBottomRight, 1.0)));

	transformedBbox.boxAxisX = bbox.boxAxisX;// glm::vec3((translation * glm::vec4(bbox.boxAxisX, 1.0)));
	transformedBbox.boxAxisY = bbox.boxAxisY;// glm::vec3((translation * glm::vec4(bbox.boxAxisY, 1.0)));
	transformedBbox.boxAxisZ = bbox.boxAxisZ;// glm::vec3((translation * glm::vec4(bbox.boxAxisZ, 1.0)));

	/*bool result = frustum->CubeIntersection(transformedBbox);
	printfD("%i", result);
	return result;*/
	return frustum->CubeIntersection(transformedBbox);
}

bool RDE::Camera::IsInView(const glm::vec3 & position)
{
	//glm::vec3 temp = glm::vec3(GetViewMatrix() * glm::vec4(position, 1.0f));
	/*glm::vec3 AB = position - pos;
	glm::vec3 tempLookat = glm::vec3(lookatPt.x, lookatPt.y, -lookatPt.z);
	float dot = glm::dot(AB, tempLookat);
	
	return dot > 0 ? true : false;*/

	return true;
}
