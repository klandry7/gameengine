#ifndef INPUT_H
#define INPUT_H

#include "common.h"

namespace RDE
{

// this can't stay like this. 
// the left seems to be the same for most controllers, but not the right
// could have a thing that's like "move the stick left. now up."
#define LEFT_HORIZONTAL_AXIS 0
#define LEFT_VERTICAL_AXIS 1
#define RIGHT_VERTICAL_AXIS 2
#define RIGHT_HORIZONTAL_AXIS 3

	namespace INPUT
	{	
		enum GAME_BUTTONS
		{
			start,
			select,

			up,
			down,
			left,
			right,
			// face buttons
			action1,
			action2,
			action3,
			action4
		};

		enum INPUT_TYPES
		{
			keyboard,
			joypad1,
			joypad2,
			joypad3,
			joypad4
		};

		enum BUTTON_STATE
		{
			pressed,
			released,
			heldDown
		};

		// this struct is the set of buttons needed in game 
		// it will be defined once for keyboard and once for joypad
		// keyboard one will have GLFW key codes 
		// joypad one will have the GLFW button codes 
		struct game_buttons
		{
			int start;
			int select;

			int up;
			int down;
			int left;
			int right;

			int action1; // top
			int action2; // right 
			int action3; // bottom
			int action4; // left

			// L/R triggers
		};
	};
	
	class Input
	{
		// all the structs needed for the input class
	private:
		// whether a button is pressed or not
		struct game_button_state
		{
			bool32 pressed;
		};

		// game controller state struct. if gamepad (analog) need to handle joystick
		struct game_controller
		{
			bool32 isAnalog;
			// union so I can easily loop through the buttons but also access them by name
			union
			{
				game_button_state Buttons[5];
				struct
				{
					game_button_state start;
					game_button_state select;
					game_button_state up;
					game_button_state down;
					game_button_state left;
					game_button_state right;
					game_button_state action1;
					game_button_state action2;
					game_button_state action3;
					game_button_state action4;
				};
			};

			// has to be in this order to match with defined values 
			union
			{
				real32 axes[4];
				struct
				{
					real32 leftHorizontalAxis;
					real32 leftVerticalAxis;
					real32 rightVerticalAxis;
					real32 rightHorizontalAxis;
				};
			};
		};

		// struct to manage all game input
		struct game_input
		{
			// it should be safe to just always assume there's a keyboard because even if there isn't 
			// it'll just return false for checking any presses so it basically handles itself
			game_controller keyboard;
			game_controller joypads[4];
		};

	public:
		// ctor
		Input();
		// update input
		void UpdateInput(size_t window);
		// swap previous and current input
		void SwapInput();
		// is joystick present?
		bool32 IsJoystickPresent();
		// get buttons state (pass in button, controller, state you want to check for)
		bool GetButtonState(INPUT::GAME_BUTTONS button, INPUT::INPUT_TYPES controller, INPUT::BUTTON_STATE state);
		// get joystick axis state
		float GetAxisState(int axis, int controller);
		// get current mouse position
		void GetMousePos(float * x, float * y);

	private:
		// get key state
		bool32 GetKeyState(int button, size_t window);
		// initialize keyboard
		void InitKeyboardGameKeys(INPUT::game_buttons * gameKeys);
		// initialize joypad
		void InitJoypadGameKeys(INPUT::game_buttons * gameKeys);
		// internally process / update input
		void ProcessInput(game_controller * controller, const INPUT::game_buttons * buttons, size_t window);
		// select button state
		bool ButtonStateSelector(game_button_state oldController, game_button_state newController, INPUT::BUTTON_STATE state);
		// check for button down
		bool ButtonHeldDown(game_button_state oldController, game_button_state newController);
		// check for button released
		bool ButtonReleased(game_button_state oldController, game_button_state newController);
		// check for button press (not down on previous frame, down on current frame)
		bool ButtonPressed(game_button_state oldController, game_button_state newController);
		/*public:
		game_buttons keyboardKeys;
		game_buttons joypadButtons[4];*/

	private:
		// keyboard keys layout
		INPUT::game_buttons keyboardKeys;
		// joypad button layouts
		INPUT::game_buttons joypadButtons[4];
		// current keyboard state
		game_controller currentKeyboardController;
		// previous keyboard state
		game_controller previousKeyboardController;

		// current joypad state
		game_controller currentJoypadControllers[4];
		// previous joypad state
		game_controller previousJoypadControllers[4];
		
		// mouse X
		double mousePosX;
		// mouse Y
		double mousePosY;
	};
	namespace INPUT
	{
		extern Input * instance;
	}
}

#endif