#include "shader.h"
#include <vector>

#include <glm/gtc/type_ptr.hpp>

// for file reading
#include <fstream>
#include <sstream>
#include <string>

#include "common.h"

#include "debugIO.h"

RDE::Shader::Shader(const char * nom)
{
	shaderName = nom;
	program = glCreateProgram();
	isCompiled = false;
	if (program == 0)
	{
		printfD("shader creation failed");
	}
}

RDE::Shader::Shader(const char * nom, const char * vertPath, const char * fragPath)
{
	shaderName = nom;
	program = glCreateProgram();
	isCompiled = false;
	if (program == 0)
	{
		printfD("shader creation failed");
	}
	AddVertexShader(vertPath);
	AddFragmentShader(fragPath);
	CompileShader();
}

RDE::Shader::~Shader()
{
}

void RDE::Shader::AddVertexShader(const char * text)
{
	AddProgram(text, GL_VERTEX_SHADER);
}

void RDE::Shader::AddFragmentShader(const char * text)
{
	AddProgram(text, GL_FRAGMENT_SHADER);
}

void RDE::Shader::AddGeometryShader(const char * text)
{
	AddProgram(text, GL_GEOMETRY_SHADER);
}

void RDE::Shader::CompileShader()
{
	if (isCompiled)
		return;

	glLinkProgram(program);

	GLint isLinked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		//The maxLength includes the NULL character
		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);
		printfD("%s\n", &infoLog[0]);
	}
	else
	{
		printfD("program for shader [%s] linked\n", shaderName);
	}

	// gether all the uniforms so you don't have to add them manually
	int total = -1;
	glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &total);
	for (int i = 0; i < total; i++)
	{
		int nameLen = -1, num = -1;
		GLenum type = GL_ZERO;
		char uniformNameChar[100];
		glGetActiveUniform(program, GLuint(i), sizeof(uniformNameChar) - 1, &nameLen, &num, &type, uniformNameChar);
		uniformNameChar[nameLen] = 0;
		GLuint location = glGetUniformLocation(program, uniformNameChar);
		std::string uniformNameStr(uniformNameChar);
		uniforms.insert(std::make_pair(uniformNameStr, location));
		printfD("uniform [%s] detected and added to list for shader [%s] \n", uniformNameChar, shaderName);

		std::size_t arrayNameLength = uniformNameStr.find("[0]");
		if (arrayNameLength != std::string::npos) {
			std::string temp = uniformNameStr.substr(0, arrayNameLength);
			const GLchar* uniformName = (GLchar*)temp.c_str();

			GLuint uniformIdx = 0;
			glGetUniformIndices(program, 1, &uniformName, &uniformIdx);

			const int nameLen2 = (strlen("LightPositions") + 1);
			GLchar name[100];
			GLint uniformSize = 0;
			GLenum uniformType = GL_NONE;
			glGetActiveUniform(program, uniformIdx, nameLen2, NULL, &uniformSize, &uniformType, name);

			// ok 
			for (int n = 1; n < uniformSize; n++)
			{
				char Name[128];
				memset(Name, 0, sizeof(Name));
				// this is c++ 11. write a string to a buffer
				// pointer to buffer, bytes to be used, the string, additional args(just like printf)
				snprintf(Name, sizeof(Name), "%s[%d]", uniformNameStr.substr(0, arrayNameLength).c_str(), n);
				GLuint locations = glGetUniformLocation(program, Name);
				std::string uniformNameStr2(Name);
				uniforms.insert(std::make_pair(uniformNameStr2, locations));				
			}
			printfD("|-> %s[1-%i] also detected and added \n", uniformName, uniformSize-1);
		}
	}
	isCompiled = true;
}

void RDE::Shader::Bind() const
{
	glUseProgram(program);
}

void RDE::Shader::Unbind() const
{
	glUseProgram(0);
}

// pass in a list of string / value pairs ? 
// update all by iterating over uniforms
/*
void RDE::Shader::UpdateUniforms()
{

}
*/

void RDE::Shader::SetUniform1i(const char * uniformName, int value) const
{
	//glUniform1i(uniforms[uniformName], value);
	glUniform1i(uniforms.at(std::string(uniformName)), value);
}

void RDE::Shader::SetUniform1f(const char * uniformName, float value) const
{
	//glUniform1f(uniforms[uniformName], value);
	glUniform1f(uniforms.at(std::string(uniformName)), value);
}

void RDE::Shader::SetUniform3fv(const char * uniformName, glm::vec3 value) const
{
	//glUniform3fv(uniforms[uniformName], 1, glm::value_ptr(value));
	glUniform3fv(uniforms.at(std::string(uniformName)), 1, glm::value_ptr(value));
}

void RDE::Shader::SetUniformMatrix4fv(const char * uniformName, bool transpose, glm::mat4 value) const
{
	//glUniformMatrix4fv(uniforms[uniformName], 1, transpose, &value[0][0]);
	glUniformMatrix4fv(uniforms.at(std::string(uniformName)), 1, transpose, &value[0][0]);
}

void RDE::Shader::SetUniform4fv(const char * uniformName, glm::vec4 value) const
{
	glUniform4fv(uniforms.at(std::string(uniformName)), 1, glm::value_ptr(value));
}

std::string ReadFileT(const char *filePath)
{
	std::string shaderCode;
	std::ifstream shaderStream(filePath, std::ios::in);
	if (shaderStream.is_open())
	{
		std::string line = "";
		while (getline(shaderStream, line))
		{
			shaderCode += "\n" + line;
		}
		shaderStream.close();
		return shaderCode;
	}
	else
	{
		printfD("failed to open file [%s]", filePath);
		getchar();
		return NULL;
	}
}

void RDE::Shader::AddProgram(const char * filePath, GLenum shaderType)
{
	// https://www.opengl.org/wiki/Shader_Compilation
	// create the shader
	GLuint shaderID = glCreateShader(shaderType);

	// read the shader code from the file 
	// should move this to it's own function 
	std::string shaderCode = ReadFileT(filePath);

	// compile the shader 
	printfD("compiling shader: %s\n", filePath);
	char const *sourcePointer = shaderCode.c_str();
	// shader handle, number of elements, source code, specifies and array of string lengths 
	glShaderSource(shaderID, 1, &sourcePointer, NULL);
	// compile the shader created above 
	glCompileShader(shaderID);

	// Check Shader status
	GLint result = GL_FALSE;
	int infoLogLength;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (result == GL_FALSE)
	{
		if (infoLogLength > 0)
		{

		}
		std::vector<char> ShaderErrorMessage(infoLogLength + 1);
		glGetShaderInfoLog(shaderID, infoLogLength, NULL, &ShaderErrorMessage[0]);
		//printf("%i\n", result);
		printfD("shader error: %s\n", &ShaderErrorMessage[0]);
	}
	else
	{
		printfD("success!\n");
	}

	glAttachShader(program, shaderID);
}