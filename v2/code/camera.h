#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include "frustum.h"

namespace RDE
{
	class Camera
	{
	public:
		// ctor
		Camera(float _displayRatio);
		// dtor
		~Camera();
		// get view mat4
		glm::mat4 GetViewMatrix();
		// get projection mat4
		glm::mat4 GetProjectionMatrix();
		// get projection view mat4
		glm::mat4 GetProjViewMatrix();
		// set position
		void SetPos(glm::vec3 v);
		// set lookat point
		void SetLookatPt(glm::vec3 v);
		// set up 
		void SetUp(glm::vec3 v);
		// set field of view 
		void SetFov(float f);
		// set z near 
		void SetZNear(float f);
		// set z far
		void SetZFar(float f);
		// set display ratio
		void SetDisplayRatio(float f);
		// check if a bounding Box is in view 
		bool BoundingBoxIsInView(const bounding_box_struct & bbox, glm::mat4 translation);
		// check if an object is in view
		bool IsInView(const glm::vec3 & position);
	private:
		// view matrix
		glm::mat4 viewMatrix;
		// position
		glm::vec3 pos;
		// lookat point
		glm::vec3 lookatPt;
		// up
		glm::vec3 up;
		// Projection matrix : 45? Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
		glm::mat4 projectionMatrix;
		// field of view
		float fov;
		// z near 
		float zNear;
		// z far
		float zFar;
		// display ratio
		float displayRatio;
		// frustum used for culling 
		CFrustum * frustum;
		// switched to false every time a variable changes, requiring a new proj/view matrix
		// cannot be modified from outside
		// does proj mat need to be updated
		bool updateProjection;
		// does view mat need to be updated
		bool updateView;
	};
}

#endif // !CAMERA_H