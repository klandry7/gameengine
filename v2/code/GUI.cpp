#include "GUI.h"
#include "GLExtension.h"
#include <CEGUI\RendererModules\OpenGL\GL3Renderer.h>
#include <GLFW\glfw3.h>

CEGUI::OpenGL3Renderer * ::RDE::GUI::m_renderer = nullptr;
CEGUI::GUIContext * RDE::GUI::m_context = nullptr;

CEGUI::Key::Scan GlfwToCeguiKey(int glfwKey)
{
	switch (glfwKey)
	{
	case GLFW_KEY_UNKNOWN: return CEGUI::Key::Unknown;
	case GLFW_KEY_ESCAPE: return CEGUI::Key::Escape;
	case GLFW_KEY_SPACE         :  return CEGUI::Key::Space;
	case GLFW_KEY_APOSTROPHE :  return CEGUI::Key::Apostrophe;
	case GLFW_KEY_COMMA :  return CEGUI::Key::Comma;
	case GLFW_KEY_MINUS :  return CEGUI::Key::Minus;
	case GLFW_KEY_PERIOD :  return CEGUI::Key::Period;
	case GLFW_KEY_SLASH :  return CEGUI::Key::Slash;
	case GLFW_KEY_0 :  return CEGUI::Key::Zero;
	case GLFW_KEY_1 :  return CEGUI::Key::One;
	case GLFW_KEY_2 :  return CEGUI::Key::Two;
	case GLFW_KEY_3 :  return CEGUI::Key::Three;
	case GLFW_KEY_4 :  return CEGUI::Key::Four;
	case GLFW_KEY_5 :  return CEGUI::Key::Five;
	case GLFW_KEY_6 :  return CEGUI::Key::Six;
	case GLFW_KEY_7 :  return CEGUI::Key::Seven;
	case GLFW_KEY_8 :  return CEGUI::Key::Eight;
	case GLFW_KEY_9 :  return CEGUI::Key::Nine;
	case GLFW_KEY_SEMICOLON :  return CEGUI::Key::Semicolon;
	case GLFW_KEY_EQUAL :  return CEGUI::Key::Equals;
	case GLFW_KEY_A :  return CEGUI::Key::A;
	case GLFW_KEY_B :  return CEGUI::Key::B;
	case GLFW_KEY_C :  return CEGUI::Key::C;
	case GLFW_KEY_D :  return CEGUI::Key::D;
	case GLFW_KEY_E :  return CEGUI::Key::E;
	case GLFW_KEY_F :  return CEGUI::Key::F;
	case GLFW_KEY_G :  return CEGUI::Key::G;
	case GLFW_KEY_H :  return CEGUI::Key::H;
	case GLFW_KEY_I :  return CEGUI::Key::I;
	case GLFW_KEY_J :  return CEGUI::Key::J;
	case GLFW_KEY_K :  return CEGUI::Key::K;
	case GLFW_KEY_L :  return CEGUI::Key::L;
	case GLFW_KEY_M :  return CEGUI::Key::M;
	case GLFW_KEY_N :  return CEGUI::Key::N;
	case GLFW_KEY_O :  return CEGUI::Key::O;
	case GLFW_KEY_P :  return CEGUI::Key::P;
	case GLFW_KEY_Q :  return CEGUI::Key::Q;
	case GLFW_KEY_R :  return CEGUI::Key::R;
	case GLFW_KEY_S :  return CEGUI::Key::S;
	case GLFW_KEY_T :  return CEGUI::Key::T;
	case GLFW_KEY_U :  return CEGUI::Key::U;
	case GLFW_KEY_V :  return CEGUI::Key::V;
	case GLFW_KEY_W :  return CEGUI::Key::W;
	case GLFW_KEY_X :  return CEGUI::Key::X;
	case GLFW_KEY_Y :  return CEGUI::Key::Y;
	case GLFW_KEY_Z :  return CEGUI::Key::Z;
	case GLFW_KEY_F1: return CEGUI::Key::F1;
	case GLFW_KEY_F2: return CEGUI::Key::F2;
	case GLFW_KEY_F3: return CEGUI::Key::F3;
	case GLFW_KEY_F4: return CEGUI::Key::F4;
	case GLFW_KEY_F5: return CEGUI::Key::F5;
	case GLFW_KEY_F6: return CEGUI::Key::F6;
	case GLFW_KEY_F7: return CEGUI::Key::F7;
	case GLFW_KEY_F8: return CEGUI::Key::F8;
	case GLFW_KEY_F9: return CEGUI::Key::F9;
	case GLFW_KEY_F10: return CEGUI::Key::F10;
	case GLFW_KEY_F11: return CEGUI::Key::F11;
	case GLFW_KEY_F12: return CEGUI::Key::F12;
	case GLFW_KEY_F13: return CEGUI::Key::F13;
	case GLFW_KEY_F14: return CEGUI::Key::F14;
	case GLFW_KEY_F15: return CEGUI::Key::F15;
	case GLFW_KEY_UP: return CEGUI::Key::ArrowUp;
	case GLFW_KEY_DOWN: return CEGUI::Key::ArrowDown;
	case GLFW_KEY_LEFT: return CEGUI::Key::ArrowLeft;
	case GLFW_KEY_RIGHT: return CEGUI::Key::ArrowRight;
	case GLFW_KEY_LEFT_SHIFT: return CEGUI::Key::LeftShift;
	case GLFW_KEY_RIGHT_SHIFT: return CEGUI::Key::RightShift;
	case GLFW_KEY_LEFT_CONTROL: return CEGUI::Key::LeftControl;
	case GLFW_KEY_RIGHT_CONTROL: return CEGUI::Key::RightControl;
	case GLFW_KEY_LEFT_ALT: return CEGUI::Key::LeftAlt;
	case GLFW_KEY_RIGHT_ALT: return CEGUI::Key::RightAlt;
	case GLFW_KEY_TAB: return CEGUI::Key::Tab;
	case GLFW_KEY_ENTER: return CEGUI::Key::Return;
	case GLFW_KEY_BACKSPACE: return CEGUI::Key::Backspace;
	case GLFW_KEY_INSERT: return CEGUI::Key::Insert;
	case GLFW_KEY_DELETE: return CEGUI::Key::Delete;
	case GLFW_KEY_PAGE_UP: return CEGUI::Key::PageUp;
	case GLFW_KEY_PAGE_DOWN: return CEGUI::Key::PageDown;
	case GLFW_KEY_HOME: return CEGUI::Key::Home;
	case GLFW_KEY_END: return CEGUI::Key::End;
	case GLFW_KEY_KP_ENTER: return CEGUI::Key::NumpadEnter;
	default: return CEGUI::Key::Unknown;
	}
}

CEGUI::MouseButton GlfwToCeguiButton(int glfwButton)
{
	switch (glfwButton)
	{
	case GLFW_MOUSE_BUTTON_LEFT: return CEGUI::LeftButton;
	case GLFW_MOUSE_BUTTON_RIGHT: return CEGUI::RightButton;
	case GLFW_MOUSE_BUTTON_MIDDLE: return CEGUI::MiddleButton;
	default: return CEGUI::NoButton;
	}
}


void RDE::GUI::GLFW_KeyCallback(GLFWwindow * window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS)
	{
		m_context->injectKeyDown(GlfwToCeguiKey(key));
	}
	if (action == GLFW_RELEASE)
	{
		m_context->injectKeyUp(GlfwToCeguiKey(key));
	}
}

void RDE::GUI::GLFW_CursorPositionCallback(GLFWwindow * window, double xpos, double ypos)
{
	m_context->injectMousePosition((float)xpos, (float)ypos);
}

void RDE::GUI::GLFW_MouseButtonCallback(GLFWwindow * window, int button, int action, int mods)
{
	if (action == GLFW_PRESS)
	{
		m_context->injectMouseButtonDown(GlfwToCeguiButton(button));
	}
	if (action == GLFW_RELEASE)
	{
		m_context->injectMouseButtonUp(GlfwToCeguiButton(button));
	}
}

void RDE::GUI::GLFW_CharacterModsCallback(GLFWwindow* window, unsigned int codepoint, int mods)
{
	//CEGUI::utf32 codePoint;
	m_context->injectChar(codepoint);
	//printf("%i", codepoint);
}

RDE::GUI::GUI(const size_t windowRef)
{
	glfwSetKeyCallback((GLFWwindow *)windowRef, RDE::GUI::GLFW_KeyCallback);
	glfwSetCursorPosCallback((GLFWwindow *)windowRef, RDE::GUI::GLFW_CursorPositionCallback);
	glfwSetMouseButtonCallback((GLFWwindow *)windowRef, RDE::GUI::GLFW_MouseButtonCallback);
	glfwSetCharModsCallback((GLFWwindow *)windowRef, RDE::GUI::GLFW_CharacterModsCallback);
}

RDE::GUI::~GUI()
{
	m_rootWindow = nullptr;
}



void RDE::GUI::Init(const std::string & resourceDirectory)
{
	// need to init if it hasn't been already
	if (RDE::GUI::m_renderer == nullptr)
	{
		m_renderer = &CEGUI::OpenGL3Renderer::bootstrapSystem();
		// static cast to be safe
		CEGUI::DefaultResourceProvider* rp = static_cast<CEGUI::DefaultResourceProvider*>(CEGUI::System::getSingleton().getResourceProvider());
		// set resource directories
		rp->setResourceGroupDirectory("imagesets", resourceDirectory + "/imagesets/");
		rp->setResourceGroupDirectory("schemes", resourceDirectory + "/schemes/");
		rp->setResourceGroupDirectory("fonts", resourceDirectory + "/fonts/");
		rp->setResourceGroupDirectory("layouts", resourceDirectory + "/layouts/");
		rp->setResourceGroupDirectory("looknfeels", resourceDirectory + "/looknfeel/");
		rp->setResourceGroupDirectory("lua_scripts", resourceDirectory + "/lua_scripts/");

		CEGUI::ImageManager::setImagesetDefaultResourceGroup("imagesets");
		CEGUI::Scheme::setDefaultResourceGroup("schemes");
		CEGUI::Font::setDefaultResourceGroup("fonts");
		CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
		CEGUI::WindowManager::setDefaultResourceGroup("layouts");
		CEGUI::ScriptModule::setDefaultResourceGroup("lua_scripts");
	}
	m_context = &CEGUI::System::getSingleton().createGUIContext(m_renderer->getDefaultRenderTarget());
	m_rootWindow = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "root");
	m_context->setRootWindow(m_rootWindow);
}

void RDE::GUI::Destroy()
{
	CEGUI::System::getSingleton().destroyGUIContext(*m_context);
	m_context = nullptr;
}

void RDE::GUI::Draw()
{
	m_renderer->beginRendering();
	m_context->draw();
	m_renderer->endRendering();
	// can cause flickering if not disabled after ui render
	glDisable(GL_SCISSOR_TEST);
}

void RDE::GUI::Update(float dt)
{
	m_context->injectTimePulse(dt);
}

void RDE::GUI::LoadScheme(const std::string & schemeFilepath)
{
	CEGUI::SchemeManager::getSingleton().createFromFile(schemeFilepath);
}

void RDE::GUI::SetFont(const std::string & fontFilepath)
{
	CEGUI::FontManager::getSingleton().createFromFile(fontFilepath + ".font");
	m_context->setDefaultFont(fontFilepath);
}

void RDE::GUI::SetMouseCursor(const std::string & imageFile)
{
	m_context->getMouseCursor().setDefaultImage(imageFile);
}

void RDE::GUI::ShowMouseCursor()
{
	m_context->getMouseCursor().show();
}

void RDE::GUI::HideMouseCursor()
{
	m_context->getMouseCursor().hide();
}

CEGUI::Window * RDE::GUI::CreateWidget(const std::string & type, const glm::vec4 & destRectPerc, const glm::vec4 & destRectPix, const std::string & name)
{
	CEGUI::Window * newWindow = CEGUI::WindowManager::getSingleton().createWindow(type, name);
	m_rootWindow->addChild(newWindow);
	SetWidgetDestRect(newWindow, destRectPerc, destRectPix);
	return newWindow;
}

void RDE::GUI::SetWidgetDestRect(CEGUI::Window * widget, const glm::vec4 & destRectPerc, const glm::vec4 & destRectPix)
{
	widget->setPosition(CEGUI::UVector2(
		CEGUI::UDim(destRectPerc.x, destRectPix.x),
		CEGUI::UDim(destRectPerc.y, destRectPix.y)));
	widget->setSize(CEGUI::USize(
		CEGUI::UDim(destRectPerc.z, destRectPix.z),
		CEGUI::UDim(destRectPerc.w, destRectPix.w)));
}
