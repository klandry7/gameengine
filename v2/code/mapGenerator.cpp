 #include "mapGenerator.h"
#include "debugIO.h"
#include <math.h>
#include <random>
//#include <map>
//#include <unordered_set>
#include <assert.h>
#include <queue>

typedef RDE::MAPGEN::TILE_TYPE tile_type;
typedef RDE::MAPGEN::Cell mapCell;

class Region
{
public:
	Region(int regionId)
	{
		id = regionId;
		regionSize = 0;
		isMainRegion = false;
		isAccessibleFromMainRegion = false;
	}

	~Region()
	{
		connectedRegions.clear();
		edgeTiles.clear();
		tiles.clear();
	}

	bool operator==(const Region &o) const {
		return (id == o.id && id == o.id);
	}

	Region& operator =(const Region &o) {
		tiles = o.tiles;
		edgeTiles = o.edgeTiles;
		connectedRegions = o.connectedRegions;
		regionSize = o.regionSize;
		isAccessibleFromMainRegion = o.isAccessibleFromMainRegion;
		isMainRegion = o.isMainRegion;
		id = o.id;
		return *this;
	}

	// for sorting regions by size
	bool operator< (const Region &o) const {
		return regionSize < o.regionSize;
	}

	void AddTile(mapCell tile, bool isEdgeTile)
	{
		tiles.push_back(tile);
		if (isEdgeTile)
			edgeTiles.push_back(tile);
		regionSize++;
	}

	bool IsConnected(Region otherRegion)
	{
		for (unsigned int i = 0; i < connectedRegions.size(); i++)
		{
			if (connectedRegions.at(i).id == otherRegion.id)
			{
				return true;
			}
		}
		return false;
	}

public:
	std::vector<mapCell> tiles;
	std::vector<mapCell> edgeTiles;
	std::vector<Region> connectedRegions;
	int regionSize;
	bool isAccessibleFromMainRegion;
	bool isMainRegion;
	int id;
};



RDE::MapGenerator::MapGenerator()
{
	wallCode = -1;
	cardinal = new mapCell[4];
	cardinal[0] = { 0,  1 };
	cardinal[1] = { 0, -1 };
	cardinal[2] = { 1,  0 };
	cardinal[3] = { -1, 0 };
}

RDE::MapGenerator::~MapGenerator()
{
	delete[] cardinal;
}

// this will only stay within max if max is an odd number
unsigned int oddrand(int min, int max)
{
	unsigned int result = (min + rand() % (max - min + 1));
	//unsigned int result = 2 * rand() + 1;
	if (result % 2 == 0)
		result++;
	return result;
}

int randomNumber(int min, int max)
{
	return min + (rand() % (int)(max - min + 1));
}

int sgn(int val)
{
	if (val < 0)
	{
		return -1;
	}
	else if (val > 0)
	{
		return 1;
	}
	return 0;
}

glm::ivec2 RDE::MAPGEN::GetTilePosFromWorldPos2D(glm::vec2 worldPos, float tileScaleX, float tileScaleY)
{
	glm::ivec2 result((int)floorf(worldPos.x / tileScaleX), (int)floorf(worldPos.y / tileScaleY));
	return result;
}

glm::ivec2 RDE::MAPGEN::GetTilePosFromWorldPos3D(glm::vec3 worldPos, float tileScaleX, float tileScaleY)
{
	glm::ivec2 result((int)floorf(worldPos.x / tileScaleX), (int)floorf(worldPos.z / tileScaleY));
	return result;
}

glm::vec2 RDE::MAPGEN::GetWorldPos2DFromTilePos2D(int x, int y, float tileScaleX, float tileScaleY)
{
	return glm::vec2((float(x) * tileScaleX), (float(y) * tileScaleY));
}

glm::vec3 RDE::MAPGEN::GetWorldPos3DFromTilePos2D(int x, int y, float tileScaleX, float tileScaleY)
{
	return glm::vec3((float(x) * tileScaleX), 0.0f, (float(y) * tileScaleY));
}

void RDE::MapGenerator::PrintMapToConsole(RDE::MAPGEN::Map * map)
{
	for (int y = 0; y < map->height; y++)
	{
		for (int x = 0; x < map->width; x++)
		{
			if (map->regions[y][x] == wallCode)
			{
				printf("`");
			}
			else
			{
				printf("%c", static_cast<char>('A' - 1 + map->regions[y][x]));
			}
		}
		printf("\n");
	}
	printf("\n");
}

void RDE::MapGenerator::PrintMapToConsoleReverse(RDE::MAPGEN::Map * map)
{
	for (int y = map->height - 1; y > 0; y--)
	{
		for (int x = map->width - 1; x > 0; x--)
		{
			if (map->regions[y][x] == wallCode)
			{
				printf("`");
			}
			else
			{
				printf("%c", static_cast<char>('A' - 1 + map->regions[y][x]));
			}
		}
		printf("\n");
	}
	printf("\n");
}

bool RDE::MapGenerator::RectOverlap(RDE::MAPGEN::Rect r1, RDE::MAPGEN::Rect r2)
{
	return r1.x - 1 <= r2.x + r2.width &&
		r2.x <= r1.x - 1 + r1.width + 2 &&
		r1.y - 1 <= r2.y + r2.height &&
		r2.y <= r1.y - 1 + r1.height + 2;
}

void RDE::MapGenerator::InitMapToAllWalls(RDE::MAPGEN::Map * map)
{
	// initialize the map to empty (all walls)
	map->dungeon = new RDE::MAPGEN::TILE_TYPE *[map->height];
	map->regions = new int *[map->height];
	for (int y = 0; y < map->height; y++)
	{
		map->dungeon[y] = new RDE::MAPGEN::TILE_TYPE[map->width];
		map->regions[y] = new int[map->width];
		for (int x = 0; x < map->width; x++)
		{
			map->regions[y][x] = map->currentRegion;
			map->dungeon[y][x] = tile_type::Wall;
		}
	}
}

RDE::MAPGEN::Map RDE::MapGenerator::NewMap(int width, int height, RDE::MAPGEN::LAYOUT_TYPES layout)
{
	RDE::MAPGEN::Map result;
	result.width = width;
	result.height = height;

	result.currentRegion = wallCode;
	result.numRoomTries = 250;
	result.extraConnectorChance = 5;
	result.roomExtraSize = 4;
	result.windingPercent = 20;
	result.tryRoomsFirst = false;
	result.streamline = true;
	
	// GOOD VALUES
	// < 35 - basically just open space
	// 35 - extremely open 
	// 40 - a good balance
	// 45 - very small regions, disconnected
	// > 45 - not enough space. Mostly useless. Too high and it could gen all walls.
	result.chanceToStartAlive = 40;
	result.birthLimit = 4; // don't modify
	result.deathLimit = 3; // don't modify

	switch (layout)
	{
	case RDE::MAPGEN::cave:
		GenerateCave(&result);
		break;
	case RDE::MAPGEN::basic:
		GenerateBasic(&result);
		break;
	default:
		InitMapToAllWalls(&result);
		break;
	}
	//PrintMapToConsole(&result);
	return result;
}

RDE::MAPGEN::Map RDE::MapGenerator::NewMapDebugConsole(int width, int height, RDE::MAPGEN::LAYOUT_TYPES layout)
{
	RDE::MAPGEN::Map result;
	result.width = width;
	result.height = height;

	result.currentRegion = wallCode;
	result.numRoomTries = 250;
	result.extraConnectorChance = 5;
	result.roomExtraSize = 4;
	result.windingPercent = 20;
	result.tryRoomsFirst = false;
	result.streamline = true;

	// GOOD VALUES
	// < 35 - basically just open space
	// 35 - extremely open 
	// 40 - a good balance
	// 45 - very small regions, disconnected
	// > 45 - not enough space. Mostly useless. Too high and it could gen all walls.
	result.chanceToStartAlive = 40;
	result.birthLimit = 4; // don't modify
	result.deathLimit = 3; // don't modify

	switch (layout)
	{
	case RDE::MAPGEN::cave:
		GenerateCaveDebug(&result);
		break;
	case RDE::MAPGEN::basic:
		GenerateBasicDebug(&result);
		break;
	default:
		InitMapToAllWalls(&result);
		break;
	}
	//PrintMapToConsole(&result);
	return result;
}

RDE::MAPGEN::Map RDE::MapGenerator::InitMapForDebugEngineBuild(int width, int height, RDE::MAPGEN::LAYOUT_TYPES layout)
{
	RDE::MAPGEN::Map result;
	result.width = width;
	result.height = height;

	result.currentRegion = wallCode;
	result.numRoomTries = 250;
	result.extraConnectorChance = 5;
	result.roomExtraSize = 4;
	result.windingPercent = 20;
	result.tryRoomsFirst = false;
	result.streamline = true;

	// GOOD VALUES
	// < 35 - basically just open space
	// 35 - extremely open 
	// 40 - a good balance
	// 45 - very small regions, disconnected
	// > 45 - not enough space. Mostly useless. Too high and it could gen all walls.
	result.chanceToStartAlive = 40;
	result.birthLimit = 4; // don't modify
	result.deathLimit = 3; // don't modify

	/*switch (layout)
	{
	case RDE::MAPGEN::cave:
		InitMapForCellularAutomata(&result);
		break;
	case RDE::MAPGEN::basic:
		InitMapToAllWalls(&result);
		break;
	default:
		InitMapToAllWalls(&result);
		break;
	}*/
	//PrintMapToConsole(&result);
	return result;
}

RDE::MAPGEN::Map RDE::MapGenerator::InEngineBasicStep(int step, RDE::MAPGEN::Map map)
{
	switch (step)
	{
	case 0:
		InitMapToAllWalls(&map);
		printf("dungeon init to all walls\n");
		PrintMapToConsoleReverse(&map);
		break;
	case 1:
		AddRooms(&map);
		printf("dungeon place rooms\n");
		PrintMapToConsoleReverse(&map);
		break;
	case 2:
		FillSpaceWithMaze(&map);
		printf("dungeon fill space with maze\n");
		PrintMapToConsoleReverse(&map);
		break;
	case 3:
		ConnectMazeRegions(&map);
		printf("dungeon connect rooms\n");
		PrintMapToConsoleReverse(&map);
		break;
	case 4:
		RemoveDeadEnds(&map);
		printf("dungeon remove dead ends\n");
		PrintMapToConsoleReverse(&map);
		printf("Basic Gen Complete\n\n");
		break;
	default:
		//AddRooms(&map);
		break;
	}
	return map;
}

RDE::MAPGEN::Map RDE::MapGenerator::InEngineCaveStep(int step, RDE::MAPGEN::Map map)
{
	if (step == 0)
	{
		InitMapForCellularAutomata(&map);
		printf("cellular automata init using game of life\n");
		PrintMapToConsoleReverse(&map);
	}
	else if (step >= 0 && step <= 12)
	{
		SimulationStep(&map);
		printf("cellular automata simulation step: %i\n", step);
		PrintMapToConsoleReverse(&map);
	}
	else if (step == 13)
	{
		std::vector<Region> regionList;
		DetectRegions(&map, &regionList);
		printf("cellular automata region detection\n");
		PrintMapToConsoleReverse(&map);






		RemoveSmallRegions(&map, &regionList, 12);
		printf("cellular automata remove small regions\n");
		PrintMapToConsoleReverse(&map);

		// sort region list from smallest to largets
		std::sort(regionList.begin(), regionList.end());
		// reverse so it's now largest to smallest
		std::reverse(regionList.begin(), regionList.end());
		//for (unsigned int i = 0; i < regionList.size(); i++) { printfD("region %i size: %i\n", i, regionList.at(i).regionSize); }
		// select the larget region as main region.
		// now connect all other regions to it
		regionList.at(0).isMainRegion = true;
		regionList.at(0).isAccessibleFromMainRegion = true;
		// increment the region counter. this will be the code for all pathways
		map.IncRegionCounter();
		ConnectClosestRegions(&map, &regionList);
		regionList.clear();


	}
	else if (step == 14)
	{
		printf("cellular automata ensure connectivity\n");
		PrintMapToConsoleReverse(&map);
	}
	else if (step == 15)
	{
		printf("cave gen complete\n\n");
	}

	return map;
}


#pragma region basic with maze room connection

void RDE::MapGenerator::GenerateBasic(RDE::MAPGEN::Map * map)
{
	InitMapToAllWalls(map);
	// square room placement
	AddRooms(map);
	// maze connection
	FillSpaceWithMaze(map);
	ConnectMazeRegions(map);
	RemoveDeadEnds(map);
}

void RDE::MapGenerator::GenerateBasicDebug(RDE::MAPGEN::Map * map)
{
	InitMapToAllWalls(map);
	printf("dungeon init to all walls\n");
	PrintMapToConsole(map);
	getchar();
	// square room placement
	AddRooms(map);
	printf("dungeon place rooms\n");
	PrintMapToConsole(map);
	getchar();
	// maze connection
	FillSpaceWithMaze(map);
	printf("dungeon fill space with maze\n");
	PrintMapToConsole(map);
	getchar();
	ConnectMazeRegions(map);
	printf("dungeon connect rooms\n");
	PrintMapToConsole(map);
	getchar();
	RemoveDeadEnds(map);
	printf("dungeon remove dead ends\n");
	PrintMapToConsole(map);
	getchar();
}

void RDE::MapGenerator::Carve(RDE::MAPGEN::Map * map, int x, int y, RDE::MAPGEN::TILE_TYPE t)
{
	map->dungeon[y][x] = t;
	map->regions[y][x] = map->currentRegion;
}

bool RDE::MapGenerator::CanCarve(RDE::MAPGEN::Map * map, RDE::MAPGEN::Cell pos, RDE::MAPGEN::Cell direction)
{
	bool result = true;
	// if it goes out of bounds, can't carve
	if (pos.x + (direction.x * 3) < 0 || pos.x + (direction.x * 3) >= map->width ||
		pos.y + (direction.y * 3) < 0 || pos.y + (direction.y * 3) >= map->height)
	{
		result = false;
	}
	// can't carve into a non-wall tile
	else if (map->dungeon[pos.y + (direction.y * 2)][pos.x + (direction.x * 2)] != tile_type::Wall)
	{
		result = false;
	}
	return result;
}

void RDE::MapGenerator::AddRooms(RDE::MAPGEN::Map * map)
{
	for (int i = 0; i < map->numRoomTries; i++)
	{
		int size = randomNumber(1, 3 + map->roomExtraSize) * 2 + 1;
		int rectangularity = randomNumber(0, 1 + size / 2) * 2;
		int width = size;
		int height = size;

		// 50% chance for more rectangular room
		if (randomNumber(0, 2) == 0)
		{
			width += rectangularity;
		}
		else
		{
			height += rectangularity;
		}

		int x = oddrand(1, map->width - width / 2);
		int y = oddrand(1, map->height - height / 2);

		RDE::MAPGEN::Rect newRoom;
		newRoom.x = x;
		newRoom.y = y;
		newRoom.width = width;
		newRoom.height = height;

		bool overlap = false;

		for each (RDE::MAPGEN::Rect other in map->rooms)
		{
			if (RectOverlap(newRoom, other))
			{
				overlap = true;
				break;
			}
		}

		// skip over overlapping rooms
		if (overlap)
			continue;

		// skip over rooms that go off the edge
		if (x + width > map->width || y + height > map->height)
			continue;

		// add the new room
		map->rooms.push_back(newRoom);
		// increment the region counter. change this to a function
		map->IncRegionCounter();

		for (int ix = x; ix < x + width; ix++)
		{
			for (int iy = y; iy < y + height; iy++)
			{
				Carve(map, ix, iy, tile_type::RoomFloor);
			}
		}
	}
}

void RDE::MapGenerator::FillSpaceWithMaze(RDE::MAPGEN::Map * map)
{
	for (int y = 1; y < map->height; y += 2)
	{
		for (int x = 1; x < map->width; x += 2)
		{
			//ignore already carved spaces
			if (map->dungeon[y][x] != tile_type::Wall)
				continue;
			// else grow the maze
			GrowMaze(map, x, y);
		}
	}
}

// "growing tree" implementation: http://www.astrolog.org/labyrnth/algrithm.htm
void RDE::MapGenerator::GrowMaze(RDE::MAPGEN::Map * map, int x, int y)
{
	std::vector<mapCell> cells;
	cells.push_back(mapCell{ x, y });
	map->IncRegionCounter();
	Carve(map, x, y, tile_type::Floor);
	mapCell lastDir{ 0,0 };

	while (cells.size() > 0)
	{
		// get last element in vector
		mapCell cell = cells.back();

		// see which adjacent cells are open
		std::vector<mapCell> unmadeCells;
		for (int i = 0; i < 4; i++)
		{
			if (CanCarve(map, cell, mapCell{ cardinal[i].x,cardinal[i].y }))
			{
				unmadeCells.push_back(mapCell{ cardinal[i].x,cardinal[i].y });
			}
		}

		if (unmadeCells.size() > 0)
		{
			mapCell dir;
			bool lastDirFound = false;
			for (std::vector<mapCell>::iterator it = unmadeCells.begin(); it != unmadeCells.end(); ++it)
			{
				if (it->x == lastDir.x && it->y == lastDir.y)
				{
					lastDirFound = true;
					break;
				}
			}

			// prefer going in a straight line instead of turning
			if (lastDirFound && randomNumber(0, 100) < map->windingPercent)
			{
				dir = lastDir;
			}
			else
			{
				dir = unmadeCells.at(randomNumber(0, unmadeCells.size() - 1));
			}

			// carve wall between valid cells
			Carve(map, cell.x + dir.x, cell.y + dir.y, tile_type::Floor);
			// carve out valid cell
			Carve(map, cell.x + (dir.x * 2), cell.y + (dir.y * 2), tile_type::Floor);

			cells.push_back(mapCell{ cell.x + (dir.x * 2), cell.y + (dir.y * 2) });
			lastDir = dir;
		}
		else
		{
			// no adjacent uncarved cells 
			cells.pop_back();
			// this path is now ended
			lastDir.x = 0;
			lastDir.y = 0;
		}
	}
}

void RDE::MapGenerator::ConnectMazeRegions(RDE::MAPGEN::Map * map)
{
	map->IncRegionCounter();
	const int mergeCode = map->currentRegion;
	//PrintMap(map);

	// gather up all connectors
	// a room tile and another tile with a wall between them 
	std::vector<std::vector<mapCell>> connectorList;
	for (unsigned int i = 0; i < map->rooms.size(); i++)
	{
		std::vector<mapCell> connectors;
		RDE::MAPGEN::Rect currentRect;
		currentRect.x = map->rooms.at(i).x;
		currentRect.y = map->rooms.at(i).y;
		currentRect.width = map->rooms.at(i).width;
		currentRect.height = map->rooms.at(i).height;

		for (int y = currentRect.y; y < currentRect.y + currentRect.height; y++)
		{
			for (int x = currentRect.x; x < currentRect.x + currentRect.width; x++)
			{
				int adjacencyCounter = 0;
				mapCell currentCell = { x, y };
				for (int n = 0; n < 4; n++)
				{
					mapCell oneOver = { currentCell.x + cardinal[n].x, currentCell.y + cardinal[n].y };
					mapCell twoOver = { currentCell.x + (cardinal[n].x * 2), currentCell.y + (cardinal[n].y * 2) };
					if (twoOver.x > 0 && twoOver.x < map->width - 1 && twoOver.y > 0 && twoOver.y < map->height - 1 &&
						map->dungeon[oneOver.y][oneOver.x] == tile_type::Wall && map->dungeon[twoOver.y][twoOver.x] != tile_type::Wall)
					{
						connectors.push_back(oneOver);
					}
				}
			}
		}
		connectorList.push_back(connectors);
	}

	//printf("connectors gathered\n");

	// for each room's list of connectors perform a flood fill from that connector
	// in theory, the first one should flood that room and all the passages
	// then the rest will just floodfill that room
	for (std::vector<std::vector<mapCell>>::iterator connectorsItr = connectorList.begin(); connectorsItr != connectorList.end(); ++connectorsItr)
	{
		std::queue<mapCell> mapFloodFillQueue;
		// choose a random connector 
		int startingCellIndex = randomNumber(0, connectorsItr->size() - 1);
		mapCell startingCell = connectorsItr->at(startingCellIndex);
		// remove it from the list of connectors
		connectorsItr->erase(connectorsItr->begin() + startingCellIndex);
		// push the starting position
		mapFloodFillQueue.push(mapCell{ startingCell.x,startingCell.y });
		// set the starting position to a door
		map->dungeon[startingCell.y][startingCell.x] = tile_type::Door;
		//printf("%i %i\n", startingCell.x, startingCell.y);

		std::vector<mapCell> visitedNodes;
		while (mapFloodFillQueue.size() > 0)
		{
			mapCell current = mapFloodFillQueue.front();
			mapFloodFillQueue.pop();

			map->regions[current.y][current.x] = mergeCode;

			for (int i = 0; i < 4; i++)
			{
				mapCell oneOver{ current.x + cardinal[i].x, current.y + cardinal[i].y };
				if (map->regions[oneOver.y][oneOver.x] != mergeCode && map->dungeon[oneOver.y][oneOver.x] != tile_type::Wall)
				{
					// if it isn't already visited
					if (std::find(visitedNodes.begin(), visitedNodes.end(), oneOver) == visitedNodes.end())
					{
						mapFloodFillQueue.push(oneOver);
						visitedNodes.push_back(oneOver);
					}
				}
			}
			//assert(((int)mapFloodFillQueue.size()) < (map->width * map->height));
		}
	}
	//printf("merge code == %c\n", static_cast<char>('A' - 1 + mergeCode));
	//PrintMap(map);
}

void RDE::MapGenerator::RemoveDeadEnds(RDE::MAPGEN::Map * map)
{
	bool done = false;
	while (!done)
	{
		done = true;
		for (int y = 1; y < map->height; y++)
		{
			for (int x = 1; x < map->width; x++)
			{
				if (map->dungeon[y][x] == tile_type::Wall) {
					continue;
				}

				int exits = 0;
				for (int i = 0; i < 4; i++)
				{
					if (map->dungeon[y + cardinal[i].y][x + cardinal[i].x] != tile_type::Wall) {
						exits++;
					}
				}

				if (exits != 1) {
					continue;
				}

				done = false;
				map->dungeon[y][x] = tile_type::Wall;
				map->regions[y][x] = wallCode;
			}
		}
	}
}
#pragma endregion

#pragma region Cellular Automata

void RDE::MapGenerator::GenerateCave(RDE::MAPGEN::Map * map)
{
	// init to random
	InitMapForCellularAutomata(map);
	// simulate game of life rules.
	// do not change # of steps
	int simulationSteps = 12;
	for (int i = 0; i < simulationSteps; i++)
	{
		SimulationStep(map);
	}
	std::vector<Region> regionList;
	DetectRegions(map, &regionList);
	RemoveSmallRegions(map, &regionList, 12);

	// sort region list from smallest to largets
	std::sort(regionList.begin(), regionList.end());
	// reverse so it's now largest to smallest
	std::reverse(regionList.begin(), regionList.end());
	//for (unsigned int i = 0; i < regionList.size(); i++) { printfD("region %i size: %i\n", i, regionList.at(i).regionSize); }
	// select the larget region as main region.
	// now connect all other regions to it
	regionList.at(0).isMainRegion = true;
	regionList.at(0).isAccessibleFromMainRegion = true;
	// increment the region counter. this will be the code for all pathways
	map->IncRegionCounter();
	ConnectClosestRegions(map, &regionList);
	regionList.clear();
}

void RDE::MapGenerator::GenerateCaveDebug(RDE::MAPGEN::Map * map)
{
	// init to random
	InitMapForCellularAutomata(map);
	printf("cellular automata init using game of life\n");
	PrintMapToConsole(map);
	getchar();
	// simulate game of life rules.
	// do not change # of steps
	int simulationSteps = 12;
	for (int i = 0; i < simulationSteps; i++)
	{
		SimulationStep(map);
		printf("cellular automata simulation step: %i\n", i);
		PrintMapToConsole(map);
		getchar();
	}
	std::vector<Region> regionList;
	DetectRegions(map, &regionList);
	printf("cellular automata region detection\n");
	PrintMapToConsole(map);
	getchar();

	RemoveSmallRegions(map, &regionList, 12);
	printf("cellular automata remove small regions\n");
	PrintMapToConsole(map);
	getchar();

	// sort region list from smallest to largets
	std::sort(regionList.begin(), regionList.end());
	// reverse so it's now largest to smallest
	std::reverse(regionList.begin(), regionList.end());
	//for (unsigned int i = 0; i < regionList.size(); i++) { printfD("region %i size: %i\n", i, regionList.at(i).regionSize); }
	// select the larget region as main region.
	// now connect all other regions to it
	regionList.at(0).isMainRegion = true;
	regionList.at(0).isAccessibleFromMainRegion = true;
	// increment the region counter. this will be the code for all pathways
	map->IncRegionCounter();
	ConnectClosestRegions(map, &regionList);
	regionList.clear();

	printf("cellular automata ensure connectivity\n");
	PrintMapToConsole(map);
	getchar();
}


void RDE::MapGenerator::InitMapForCellularAutomata(RDE::MAPGEN::Map * map)
{
	map->IncRegionCounter();
	map->dungeon = new RDE::MAPGEN::TILE_TYPE *[map->height];
	map->regions = new int *[map->height];
	for (int y = 0; y < map->height; y++)
	{
		map->dungeon[y] = new RDE::MAPGEN::TILE_TYPE[map->width];
		map->regions[y] = new int[map->width];
		for (int x = 0; x < map->width; x++)
		{
			if ((x == 0) || (x == (map->width - 1)) || (y == 0) || (y == (map->height - 1)))
			{
				map->regions[y][x] = wallCode;
				map->dungeon[y][x] = tile_type::Wall;
			}
			else
			{
				if (randomNumber(0, 100) < map->chanceToStartAlive)
				{
					map->regions[y][x] = wallCode;
					map->dungeon[y][x] = tile_type::Wall;
				}
				else
				{
					map->regions[y][x] = map->currentRegion;
					map->dungeon[y][x] = tile_type::Floor;
				}
			}
		}
	}
}

void RDE::MapGenerator::SimulationStep(RDE::MAPGEN::Map * map)
{
	// can go from 0 to width/height - 1 because InitMapForCellularAutomata forces edges to be walls. saves an edge check.
	for (int y = 1; y < map->height-1; y++)
	{
		for (int x = 1; x < map->width-1; x++)
		{
			int aliveNeighbourCount = CountAliveNeighbours(map, x, y);
			// if this cell is alive (wall) ...
			if (map->dungeon[y][x] == tile_type::Wall)
			{
				// and it doesn't have enough living neighbours ...
				if (aliveNeighbourCount < map->deathLimit)
				{
					// it dies (floor)
					map->dungeon[y][x] = tile_type::Floor;
					map->regions[y][x] = map->currentRegion;
				}
				else
				{
					// it lives (wall)
					map->dungeon[y][x] = tile_type::Wall;
					map->regions[y][x] = wallCode;
				}
			}
			// else cell is dead (floor) ...
			else
			{
				// and it has enough living neighbours ...
				if (aliveNeighbourCount > map->birthLimit)
				{
					// it lives (wall)
					map->dungeon[y][x] = tile_type::Wall;
					map->regions[y][x] = wallCode;
				}
				else
				{
					// it dies (floor)
					map->dungeon[y][x] = tile_type::Floor;
					map->regions[y][x] = map->currentRegion;
				}
			}
		}
	}
}

int RDE::MapGenerator::CountAliveNeighbours(RDE::MAPGEN::Map * map, int x, int y)
{
	// get the total count of alive (wall) neighbours
	int neighbourCount = 0;
	// n = neighbour, c = current cell
	// n n n
	// n c n
	// n n n
	for (int iy = -1; iy < 2; iy++)
	{
		for (int ix = -1; ix < 2; ix++)
		{
			// ignore middle point (the current point)
			if ((ix == 0) && (iy == 0))
			{

			}
			// this only works because SimulationStep goes from 1 to height-1
			// so it can never go over the edge. saves an edge check.
			else if (map->dungeon[y + iy][x + ix] == tile_type::Wall)
			{
				neighbourCount++;
			}	
		}
	}
	return neighbourCount;
}

void RDE::MapGenerator::DetectRegions(RDE::MAPGEN::Map * map, std::vector<Region> * regions)
{
	// at this point, every tile should be -1 (wall) or 0 (floor/map->currentRegion)
	// empty code will indicate a region is undetected
	const int undetectedRegionCode = map->currentRegion++;
	// can go from 0 to width/height - 1 because InitMapForCellularAutomata forces edges to be walls. saves an edge check.
	for (int y = 1; y < map->height - 1; y++)
	{
		for (int x = 1; x < map->width - 1; x++)
		{
			// if a region isn't marked as a new region
			if (map->regions[y][x] == undetectedRegionCode)
			{		
				std::queue<mapCell> cellsToCheck;
				cellsToCheck.push(mapCell{ x,y });
				Region newRegion(map->currentRegion);
				map->IncRegionCounter();

				while (cellsToCheck.size() > 0)
				{
					// get the first point 
					mapCell current = cellsToCheck.front();
					cellsToCheck.pop();
					// don't 
					if (map->regions[current.y][current.x] != newRegion.id)
					{
						// we're not carving or modifying the landscape, so only need to modify regions and not dungeon
						map->regions[current.y][current.x] = newRegion.id;

						// if it's an edge cell
						if (map->dungeon[current.y + 1][current.x] == tile_type::Wall ||
							map->dungeon[current.y - 1][current.x] == tile_type::Wall ||
							map->dungeon[current.y][current.x + 1] == tile_type::Wall ||
							map->dungeon[current.y][current.x - 1] == tile_type::Wall)
						{
							newRegion.AddTile(current, true);
						}
						else { newRegion.AddTile(current, false); }

						// enqueue adjacent unchecked cells
						for (int i = 0; i < 4; i++)
						{
							mapCell oneOver{ current.x + cardinal[i].x, current.y + cardinal[i].y };
							if (map->regions[oneOver.y][oneOver.x] == undetectedRegionCode)
							{
								cellsToCheck.push(oneOver);
							}
						}
					}
				}
				regions->push_back(newRegion);
			}
		}
	}
}

void RDE::MapGenerator::RemoveSmallRegions(RDE::MAPGEN::Map * map, std::vector<Region> * regionList, int sizeThreshold)
{
	for (std::vector<Region>::iterator itr = regionList->begin(); itr != regionList->end();)
	{
		//printfD("%i\n", itr->regionSize);// tiles.size());
		if (itr->regionSize <= sizeThreshold)
		{
			//printfD("remove\n");
			for (unsigned int i = 0; i < itr->tiles.size(); i++)
			{
				int y = itr->tiles.at(i).y;
				int x = itr->tiles.at(i).x;
				map->regions[y][x] = wallCode;
				map->dungeon[y][x] = tile_type::Wall;
			}
			itr = regionList->erase(itr);
		}
		else
		{
			++itr;
		}
	}
}

// forceAccessibleFromMainRegion is false by default. check header.
// find and connect regions closest to each other
// 1) in the first pass when not forcing accessibility to main region, all it will do is 
// connect to the closest region and that's it
// 2) in the next pass when forcing accessibility to the main region, look for whichever 
// connected room is  closest to the main region (but not connected) and connect that one to the main region
void RDE::MapGenerator::ConnectClosestRegions(RDE::MAPGEN::Map * map, std::vector<Region> * regions, bool forceAccessibleFromMainRegion)
{
	//std::vector<Region *> roomListA;
	//std::vector<Region *> roomListB;
	// instead of pointer to region, an index
	std::vector<int> roomIndicesListA;
	std::vector<int> roomIndicesListB;

	// initialize the room lists
	if (forceAccessibleFromMainRegion)
	{
		for (unsigned int i = 0; i < regions->size(); i++)
		{
			if (regions->at(i).isAccessibleFromMainRegion)
			{
				// list of regions accessible from main region
				roomIndicesListB.push_back(i);
			}
			else
			{
				// list of regions not accessible from main room
				roomIndicesListA.push_back(i);
			}
		}	
	}
	else
	{
		for (unsigned int i = 0; i < regions->size(); i++)
		{
			roomIndicesListA.push_back(i);
			roomIndicesListB.push_back(i);
		}
	}

	//printf("A: %i B: %i\n", roomListA.size(), roomListB.size());

	int bestDist = 0;
	mapCell bestTileA;
	mapCell bestTileB;
	//Region * bestRegionA;
	//Region * bestRegionB;
	int bestRegionIndexA;
	int bestRegionIndexB;

	bool possibleConnectionFound = false;

	//for each (Region * regionA in roomListA)
	for(unsigned int i = 0; i < roomIndicesListA.size(); i++)
	{
		// don't want to do this when forcing accessibility because we're not just looking for the first connection
		// we're looking for the closest connection to the main room based on adjacent rooms
		// we're always considering all adjacent rooms before making the connection
		if (!forceAccessibleFromMainRegion)
		{
			possibleConnectionFound = false;
			if (regions->at(roomIndicesListA.at(i)).connectedRegions.size() > 0)
				continue;
		}
		//for each (Region * regionB in roomListB)
		for(unsigned int n = 0; n < roomIndicesListB.size(); n++)
		{
			// if the regions are the same, don't bother comparing or if they're already connected
			// or if B is connected to A 
			if (roomIndicesListA.at(i) == roomIndicesListB.at(n)|| regions->at(roomIndicesListA.at(i)).IsConnected(regions->at(roomIndicesListB.at(n)))){
				//printf("ids: %i %i\n", regionA->id, regionB->id);
				continue;
			}

			for (unsigned int tileIndexA = 0; tileIndexA < regions->at(roomIndicesListA.at(i)).edgeTiles.size(); tileIndexA++)
			{
				for (unsigned int tileIndexB = 0; tileIndexB < regions->at(roomIndicesListB.at(n)).edgeTiles.size(); tileIndexB++)
				{
					mapCell tileA = regions->at(roomIndicesListA.at(i)).edgeTiles.at(tileIndexA);
					mapCell tileB = regions->at(roomIndicesListB.at(n)).edgeTiles.at(tileIndexB);
					int distBetweenRegions = (int)(powf((float)(tileA.x - tileB.x), 2.0f) + powf((float)(tileA.y - tileB.y), 2.0f));
					
					// if the distance between the 2 tiles is < the current best or this is the first iteration
					if (distBetweenRegions < bestDist || !possibleConnectionFound)
					{
						bestDist = distBetweenRegions;
						possibleConnectionFound = true;
						bestTileA = tileA;
						bestTileB = tileB;
						bestRegionIndexA = roomIndicesListA.at(i);
						bestRegionIndexB = roomIndicesListB.at(n);
						
						//printf("ids %i %i\n", bestRegionA->id, bestRegionB->id);
						//printf("a.x = %i, a.y = %i, b.x = %i, b.y = %i\n", bestTileA.x, bestTileA.y, bestTileB.x, bestTileB.y);
					}
				}
			}
		}
		if (possibleConnectionFound && !forceAccessibleFromMainRegion)
		{
			CreatePassage(bestRegionIndexA, bestRegionIndexB, bestTileA, bestTileB, map, regions);
		}
	}

	if (possibleConnectionFound && forceAccessibleFromMainRegion)
	{
		CreatePassage(bestRegionIndexA, bestRegionIndexB, bestTileA, bestTileB, map, regions);
		ConnectClosestRegions(map, regions, true);
	}
	if (!forceAccessibleFromMainRegion)
	{
		ConnectClosestRegions(map, regions, true);
	}

	/*bestRegionA = nullptr;
	delete bestRegionA;
	bestRegionB = nullptr;
	delete bestRegionB;
	for (std::vector<Region*>::iterator it = roomListA.begin(); it != roomListA.end(); ++it)
	{
		*it = nullptr;
		delete (*it);
	}
	roomListA.clear();
	for (std::vector<Region*>::iterator it = roomListB.begin(); it != roomListB.end(); ++it)
	{
		*it = nullptr;
		delete (*it);
	}
	roomListB.clear();*/
}

void RDE::MapGenerator::SetAccessibleFromMainRegion(int r, std::vector<Region> * regions)
{
	if (!regions->at(r).isAccessibleFromMainRegion)
	{
		regions->at(r).isAccessibleFromMainRegion = true;
		for (unsigned int i = 0; i < regions->size(); i++)
		{
			for (unsigned int n = 0; n < regions->at(r).connectedRegions.size(); n++)
			{
				if (regions->at(r).connectedRegions.at(n).id == regions->at(i).id)
				{
					SetAccessibleFromMainRegion(r, regions);
				}
			}
		}
	}
}

void RDE::MapGenerator::ConnectCaveRegions(int a, int b, std::vector<Region> * regions)
{
	if (regions->at(a).isAccessibleFromMainRegion)
	{
		SetAccessibleFromMainRegion(b, regions);
	}
	else if (regions->at(b).isAccessibleFromMainRegion)
	{
		SetAccessibleFromMainRegion(a, regions);
	}
	regions->at(a).connectedRegions.push_back(regions->at(b));
	regions->at(b).connectedRegions.push_back(regions->at(a));
}

void RDE::MapGenerator::CreatePassage(int regionA, int regionB, RDE::MAPGEN::Cell tileA, RDE::MAPGEN::Cell tileB, RDE::MAPGEN::Map * map, std::vector<Region> * regions)
{
	ConnectCaveRegions(regionA, regionB, regions);

	std::vector<mapCell> line;
	GetLine(tileA, tileB, &line);
	for each (mapCell c in line)
	{
		//printf("%i %i\n", c.x, c.y);
		DrawCircle(c, 1, map);
	}
}

void RDE::MapGenerator::GetLine(RDE::MAPGEN::Cell from, RDE::MAPGEN::Cell to, std::vector<RDE::MAPGEN::Cell> * result)
{
	//printf("path\n");
	int x = from.x;
	int y = from.y;

	int dx = to.x - from.x;
	int dy = to.y - from.y;

	bool inverted = false;

	// x step 
	int step = sgn(dx);
	// y step
	int gradientStep = sgn(dy);

	int longest = abs(dx);
	int shortest = abs(dy);

	if (longest < shortest)
	{
		inverted = true;
		longest = abs(dy);
		shortest = abs(dx);

		step = sgn(dy);
		gradientStep = sgn(dx);
	}

	int gradientAccumulation = longest / 2;
	for (int i = 0; i <= longest; i++)
	{
		//printf("%i %i\n", x, y);
		result->push_back(mapCell{ x, y });

		if (inverted)
		{
			y += step;
		}
		else
		{
			x += step;
		}

		gradientAccumulation += shortest;
		if (gradientAccumulation >= shortest)
		{
			if (inverted)
			{
				x += gradientStep;
			}
			else
			{
				y += gradientStep;
			}
			gradientAccumulation -= longest;
		}
	}
}

void RDE::MapGenerator::DrawCircle(RDE::MAPGEN::Cell c, int r, RDE::MAPGEN::Map * map)
{
	for (int x = -r; x <= r; x++)
	{
		for (int y = -r; y <= r; y++)
		{
			if (((x*x) + (y*y)) <= (r*r))
			{
				int drawX = c.x + x;
				int drawY = c.y + y;

				if ((x >= 0) && (x < map->width) && (y >= 0) && (y < map->height))
				{
					map->regions[drawY][drawX] = map->currentRegion;
					map->dungeon[drawY][drawX] = tile_type::Floor;
				}
			}
		}
	}
}

#pragma endregion