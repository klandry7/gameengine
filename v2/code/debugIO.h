#ifndef DEBUGIO_H
#define DEBUGIO_H

// could set as compiler flag in build script
#define DEBUG_PRINT_ENABLED 1

#if DEBUG_PRINT_ENABLED
	#include <stdio.h>
	#define printfD(f_, ...) (printf((f_), __VA_ARGS__))
#else
	#define printfD(f_, ...) ("debug print not enabled")
#endif // DEBUG_PRINT

#endif // !DEBUGIO_H
