#include "mapgenDemo.h"

#include <glm/gtc/matrix_transform.hpp>

#include "transform.h"
#include "input.h"
#include "renderer.h"
#include "debugIO.h"
#include "hierarchyTraversal.h"
#include "transform.h"
#include "shader.h"
#include "camera.h"
#include "sphericalTransform.h"
#include "gameObject.h"

#define RAD90 3.14f / 2.0f
#define RAD180 3.14f

#define BASIC_MAX 5 // if == max, stop 
#define CAVE_MAX 16 // if == max, stop

MapgenDemo::MapgenDemo(float aspectRatio)
{
	mapTileSize = 2.5f;

	camera = new RDE::Camera(aspectRatio);
	camera->SetZFar(1000.0f);

	simpleShader = std::make_shared<RDE::Shader>(RDE::Shader("passthrough", "../shaders/simpleVert.vert", "../shaders/simpleFrag.frag"));
	generationStepCounter = 0;


	mapgen = new RDE::MapGenerator();
}

MapgenDemo::~MapgenDemo()
{
	// delete everything bruh
}

void MapgenDemo::FixedUpdate(float dt)
{
}

bool MapgenDemo::Update(float dt)
{
	if (generationStepCounter == 0 || 
		(currentLayoutGen == RDE::MAPGEN::LAYOUT_TYPES::cave && generationStepCounter == CAVE_MAX) ||
		(currentLayoutGen == RDE::MAPGEN::LAYOUT_TYPES::basic && generationStepCounter == BASIC_MAX))
	{
		if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::action1, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::released))
		{
			if (firstPass)
				firstPass = false;
			else
				ClearTileLists();
			//RollNewMap(RDE::MAPGEN::LAYOUT_TYPES::basic);
			currentLayoutGen = RDE::MAPGEN::LAYOUT_TYPES::basic;
			generationStepCounter = 0;
			currentMap = mapgen->InitMapForDebugEngineBuild(51, 51, RDE::MAPGEN::LAYOUT_TYPES::basic);
			//currentMap = mapgen->InEngineBasicStep(generationStepCounter, currentMap);
			//generationStepCounter++;
		}
		else if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::action2, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::released))
		{
			if (firstPass)
				firstPass = false;
			else
				ClearTileLists();
			//RollNewMap(RDE::MAPGEN::LAYOUT_TYPES::cave);
			currentLayoutGen = RDE::MAPGEN::LAYOUT_TYPES::cave;
			generationStepCounter = 0;
			currentMap = mapgen->InitMapForDebugEngineBuild(51, 51, RDE::MAPGEN::LAYOUT_TYPES::cave);

			//generationStepCounter++;
		}
	}



	if (firstPass == false && ((currentLayoutGen == RDE::MAPGEN::LAYOUT_TYPES::basic && generationStepCounter < BASIC_MAX) ||
		(currentLayoutGen == RDE::MAPGEN::LAYOUT_TYPES::cave && generationStepCounter < CAVE_MAX)))
	{		
		if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::action1, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::released))
		{
			if (currentLayoutGen == RDE::MAPGEN::LAYOUT_TYPES::basic)
			{
				currentMap = mapgen->InEngineBasicStep(generationStepCounter, currentMap);
				generationStepCounter++;
				UpdateMapTiles();
			}
		}
		else if(RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::action2, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::released))
		{
			if (currentLayoutGen == RDE::MAPGEN::LAYOUT_TYPES::cave)
			{
				currentMap = mapgen->InEngineCaveStep(generationStepCounter, currentMap);
				generationStepCounter++;
				UpdateMapTiles();
			}
		}
	}

	// camera setup
	float camX = (float(currentMap.width) * 2.5f / 2.0f);
	float camY = (float(currentMap.height) * 2.5f / 2.0f);
	camera->SetPos(glm::vec3(camX, camX + camY, camY - 0.01f));
	camera->SetLookatPt(glm::vec3(camX, 1.0f, camY));

	// hit escape to quit
	if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::start, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::pressed))
	{
		return false;
	}
	else
	{
		return true;
	}
}

void MapgenDemo::Render(float interpolation)
{
	// remember to bind shader before you can set any uniforms
	simpleShader->Bind();
	simpleShader->SetUniformMatrix4fv("projectionViewMatrix", false, camera->GetProjViewMatrix());
	simpleShader->SetUniform1i("animated", false);
	simpleShader->SetUniform1i("instanced", true);

	RDE::RENDERER::instance->DrawModelInstanced("floor-tile", "floor", roomFloorList.size(), roomFloorList);
	// 0
	RDE::RENDERER::instance->DrawModelInstanced("fill-tile", "stoneWall", tiles_noEdge.size(), tiles_noEdge);
	// 1
	RDE::RENDERER::instance->DrawModelInstanced("top-wall", "stoneWall", tiles_1edge.size(), tiles_1edge);
	// 2
	RDE::RENDERER::instance->DrawModelInstanced("top-left-wall", "stoneWall", tiles_2edgeAdjacent.size(), tiles_2edgeAdjacent);
	RDE::RENDERER::instance->DrawModelInstanced("top-bottom-wall", "stoneWall", tiles_2edgeOpposite.size(), tiles_2edgeOpposite);
	// 3
	RDE::RENDERER::instance->DrawModelInstanced("top-left-right-wall", "stoneWall", tiles_3edge.size(), tiles_3edge);
	// 4
	RDE::RENDERER::instance->DrawModelInstanced("4-edge-wall", "stoneWall", tiles_4edge.size(), tiles_4edge);

	simpleShader->SetUniform1i("instanced", false);
	simpleShader->SetUniformMatrix4fv("transformMatrix", false, glm::mat4(1.0f));
	RDE::RENDERER::instance->DrawModelIndexed("sphere", "henrietta");
}

void MapgenDemo::UpdateMapTiles()
{
	ClearTileLists();
	
	RDE::MAPGEN::Cell * cardinal = new RDE::MAPGEN::Cell[4];
	cardinal[0] = { 0,  1 };
	cardinal[1] = { 0, -1 };
	cardinal[2] = { 1,  0 };
	cardinal[3] = { -1, 0 };

	for (int y = 0; y < currentMap.height; y++)
	{
		for (int x = 0; x < currentMap.width; x++)
		{
			if (currentMap.regions[y][x] == -1)
			{
				MapTileSelect(x, y);
			}
			else
			{
				roomFloorList.push_back(glm::translate(glm::mat4(1.0f), glm::vec3((float(x) * mapTileSize) + (mapTileSize / 2.0f), -1.5f, (float(y) * mapTileSize) + (mapTileSize / 2.0f))));
			}
		}
	}	
	delete cardinal;
}

void MapgenDemo::ClearTileLists()
{
	// clear out the old map
	// if regions isn't null, dungeon should also not be null.
	/*if (currentMap.regions != NULL)
	{
		for (int y = 0; y < currentMap.height; y++)
		{
			delete[] currentMap.regions[y];
			delete[] currentMap.dungeon[y];
		}
		delete[] currentMap.regions;
		delete[] currentMap.dungeon;
	}*/

	roomFloorList.clear();
	tiles_noEdge.clear();
	tiles_1edge.clear();
	tiles_2edgeAdjacent.clear();
	tiles_2edgeOpposite.clear();
	tiles_3edge.clear();
	tiles_4edge.clear();
}

// a first person map where inner space doesn't matter will be different
// from a top down map where all space matters
// a first person map can be build entirely using 1 wall rotated 
// a top down tile based map will need 
// 0 edge (1)
// 1 edge (1)
// 2 edge (2)
// 3 edge (1)
// 4 edge (1)
void MapgenDemo::MapTileSelect(int x, int y)
{
	RDE::MAPGEN::Cell * cardinal = new RDE::MAPGEN::Cell[4];
	cardinal[0] = { 0,  1 };
	cardinal[1] = { 0, -1 };
	cardinal[2] = { 1,  0 };
	cardinal[3] = { -1, 0 };

	glm::mat4 translate = glm::translate(glm::mat4(1.0f), glm::vec3((float(x) * mapTileSize) + (mapTileSize / 2.0f), -1.5f, (float(y) * mapTileSize) + (mapTileSize / 2.0f)));
	glm::vec3 rotationAxis = glm::vec3(0.0f, 1.0f, 0.0f);

	// changed to unsigned int (4 bits) and bitmask
	bool top = false, bottom = false, left = false, right = false;

	for (int i = 0; i < 4; i++)
	{
		int yInc = y + cardinal[i].y;
		int xInc = x + cardinal[i].x;
		if (yInc >= 0 && yInc < currentMap.height && xInc >= 0 && xInc < currentMap.width &&
			currentMap.dungeon[yInc][xInc] != RDE::MAPGEN::TILE_TYPE::Wall)
		{
			switch (i)
			{
			case 0:
				top = true;
				break;
			case 1:
				bottom = true;
				break;
			case 2:
				left = true;
				break;
			case 3:
				right = true;
				break;
			default:
				break;
			}
		}
	}

	// with a bitmask this could probably be a case since each is unique

	// 4
	if (top && bottom && left && right) {
		tiles_4edge.push_back(translate);
	}
	// 3
	else if (bottom && left && right) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), RAD180, rotationAxis);
		tiles_3edge.push_back((translate * rotate));
	}
	else if (top && left && right) {
		tiles_3edge.push_back(translate);
	}
	else if (top && bottom && left) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), RAD90, rotationAxis);
		tiles_3edge.push_back((translate * rotate));
	}
	else if (top && bottom && right) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), -RAD90, rotationAxis);
		tiles_3edge.push_back((translate * rotate));
	}
	// 2
	else if (top && left) {
		tiles_2edgeAdjacent.push_back(translate);
	}
	else if (top && right) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), -RAD90, rotationAxis);
		tiles_2edgeAdjacent.push_back((translate * rotate));
	}
	else if (bottom && left) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), RAD90, rotationAxis);
		tiles_2edgeAdjacent.push_back((translate * rotate));
	}
	else if (bottom && right) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), RAD180, rotationAxis);
		tiles_2edgeAdjacent.push_back((translate * rotate));
	}
	else if (top && bottom) {
		tiles_2edgeOpposite.push_back(translate);
	}
	else if (left && right) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), RAD90, rotationAxis);
		tiles_2edgeOpposite.push_back((translate * rotate));
	}
	// 1
	else if (top) {
		tiles_1edge.push_back(translate);
	}
	else if (bottom) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), RAD180, rotationAxis);
		tiles_1edge.push_back((translate * rotate));
	}
	else if (left) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), RAD90, rotationAxis);
		tiles_1edge.push_back((translate * rotate));
	}
	else if (right) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), -RAD90, rotationAxis);
		tiles_1edge.push_back((translate * rotate));
	}
	// 0
	else if (!top && !bottom && !left && !right) {
		tiles_noEdge.push_back(translate);
	}

	delete cardinal;
}