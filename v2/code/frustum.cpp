/*
Copyright (c) 2013, Lunar Workshop, Inc.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software must display the following acknowledgement:
   This product includes software developed by Lunar Workshop, Inc.
4. Neither the name of the Lunar Workshop nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY LUNAR WORKSHOP INC ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LUNAR WORKSHOP BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "frustum.h"
#include <glm/gtc/type_ptr.hpp>

CFrustum::CFrustum(const glm::mat4 & m)
{
	/*float dArray[16] = { 0.0 };
	glm::mat4 temp = glm::transpose(m);
	const float *pSource = (const float*)glm::value_ptr(temp);
	for (unsigned int i = 0; i < 16; ++i)
		dArray[i] = pSource[i];

	// I'll explain all this junk in a future video.
	//[width * y + x]
	p[FRUSTUM_RIGHT].x = dArray[4 * 3 + 0] - dArray[0];
	p[FRUSTUM_RIGHT].y = dArray[4 * 3 + 1] - dArray[4 * 0 + 1];
	p[FRUSTUM_RIGHT].z = dArray[4 * 3 + 2] - dArray[4 * 0 + 2];
	p[FRUSTUM_RIGHT].d = dArray[4 * 3 + 3] - dArray[4 * 0 + 3];

	p[FRUSTUM_LEFT].x = dArray[4 * 3 + 0] + dArray[0];
	p[FRUSTUM_LEFT].y = dArray[4 * 3 + 1] + dArray[4 * 0 + 1];
	p[FRUSTUM_LEFT].z = dArray[4 * 3 + 2] + dArray[4 * 0 + 2];
	p[FRUSTUM_LEFT].d = dArray[4 * 3 + 3] + dArray[4 * 0 + 3];

	p[FRUSTUM_DOWN].x = dArray[4 * 3 + 0] + dArray[4 * 1 + 0];
	p[FRUSTUM_DOWN].y = dArray[4 * 3 + 1] + dArray[4 * 1 + 1];
	p[FRUSTUM_DOWN].z = dArray[4 * 3 + 2] + dArray[4 * 1 + 2];
	p[FRUSTUM_DOWN].d = dArray[4 * 3 + 3] + dArray[4 * 1 + 3];

	p[FRUSTUM_UP].x = dArray[4 * 3 + 0] - dArray[4 * 1 + 0];
	p[FRUSTUM_UP].y = dArray[4 * 3 + 1] - dArray[4 * 1 + 1];
	p[FRUSTUM_UP].z = dArray[4 * 3 + 2] - dArray[4 * 1 + 2];
	p[FRUSTUM_UP].d = dArray[4 * 3 + 3] - dArray[4 * 1 + 3];

	p[FRUSTUM_FAR].x = dArray[4 * 3 + 0] - dArray[4 * 2 + 0];
	p[FRUSTUM_FAR].y = dArray[4 * 3 + 1] - dArray[4 * 2 + 1];
	p[FRUSTUM_FAR].z = dArray[4 * 3 + 2] - dArray[4 * 2 + 2];
	p[FRUSTUM_FAR].d = dArray[4 * 3 + 3] - dArray[4 * 2 + 3];

	p[FRUSTUM_NEAR].x = dArray[4 * 3 + 0] + dArray[4 * 2 + 0];
	p[FRUSTUM_NEAR].y = dArray[4 * 3 + 1] + dArray[4 * 2 + 1];
	p[FRUSTUM_NEAR].z = dArray[4 * 3 + 2] + dArray[4 * 2 + 2];
	p[FRUSTUM_NEAR].d = dArray[4 * 3 + 3] + dArray[4 * 2 + 3];*/

	// [y][x] format

	p[FRUSTUM_RIGHT].x = m[0][3] - m[0][0];
	p[FRUSTUM_RIGHT].y = m[1][3] - m[1][0];
	p[FRUSTUM_RIGHT].z = m[2][3] - m[2][0];
	p[FRUSTUM_RIGHT].d = m[3][3] - m[3][0];

	p[FRUSTUM_LEFT].x = m[0][3] + m[0][0];
	p[FRUSTUM_LEFT].y = m[1][3] + m[1][0];
	p[FRUSTUM_LEFT].z = m[2][3] + m[2][0];
	p[FRUSTUM_LEFT].d = m[3][3] + m[3][0];

	p[FRUSTUM_DOWN].x = m[0][3] + m[0][1];
	p[FRUSTUM_DOWN].y = m[1][3] + m[1][1];
	p[FRUSTUM_DOWN].z = m[2][3] + m[2][1];
	p[FRUSTUM_DOWN].d = m[3][3] + m[3][1];

	p[FRUSTUM_UP].x = m[0][3] - m[0][1];
	p[FRUSTUM_UP].y = m[1][3] - m[1][1];
	p[FRUSTUM_UP].z = m[2][3] - m[2][1];
	p[FRUSTUM_UP].d = m[3][3] - m[3][1];

	p[FRUSTUM_FAR].x = m[0][3] - m[0][2];
	p[FRUSTUM_FAR].y = m[1][3] - m[1][2];
	p[FRUSTUM_FAR].z = m[2][3] - m[2][2];
	p[FRUSTUM_FAR].d = m[3][3] - m[3][2];

	p[FRUSTUM_NEAR].x = m[0][3] + m[0][2];
	p[FRUSTUM_NEAR].y = m[1][3] + m[1][2];
	p[FRUSTUM_NEAR].z = m[2][3] + m[2][2];
	p[FRUSTUM_NEAR].d = m[3][3] + m[3][2];

	/*p[FRUSTUM_LEFT].x = m[3][0] + m[0][0];
	p[FRUSTUM_LEFT].y = m[3][1] + m[0][1];
	p[FRUSTUM_LEFT].z = m[3][2] + m[0][2];
	p[FRUSTUM_LEFT].d = m[3][3] + m[0][3];

	p[FRUSTUM_RIGHT].x = m[3][0] - m[0][0];
	p[FRUSTUM_RIGHT].y = m[3][1] - m[0][1];
	p[FRUSTUM_RIGHT].z = m[3][2] - m[0][2];
	p[FRUSTUM_RIGHT].d = m[3][3] - m[0][3];


	p[FRUSTUM_UP].x = m[3][0] - m[1][0];
	p[FRUSTUM_UP].y = m[3][1] - m[1][1];
	p[FRUSTUM_UP].z = m[3][2] - m[1][2];
	p[FRUSTUM_UP].d = m[3][3] - m[1][3];

	p[FRUSTUM_DOWN].x = m[3][0] + m[1][0];
	p[FRUSTUM_DOWN].y = m[3][1] + m[1][1];
	p[FRUSTUM_DOWN].z = m[3][2] + m[1][2];
	p[FRUSTUM_DOWN].d = m[3][3] + m[1][3];


	p[FRUSTUM_NEAR].x = m[3][0] + m[2][0];
	p[FRUSTUM_NEAR].y = m[3][1] + m[2][1];
	p[FRUSTUM_NEAR].z = m[3][2] + m[2][2];
	p[FRUSTUM_NEAR].d = m[3][3] + m[2][3];

	p[FRUSTUM_FAR].x = m[3][0] - m[2][0];
	p[FRUSTUM_FAR].y = m[3][1] - m[2][1];
	p[FRUSTUM_FAR].z = m[3][2] - m[2][2];
	p[FRUSTUM_FAR].d = m[3][3] - m[2][3];*/



	// Normalize all plane normals
	for(int i = 0; i < 6; i++)
		p[i].Normalize();
}

void CFrustum::Set(const glm::mat4 & m)
{
	/*float dArray[16] = { 0.0 };
	glm::mat4 temp = glm::transpose(m);
	const float *pSource = (const float*)glm::value_ptr(temp);
	for (unsigned int i = 0; i < 16; ++i)
		dArray[i] = pSource[i];

	p[FRUSTUM_RIGHT].x = dArray[4 * 3 + 0] - dArray[0];
	p[FRUSTUM_RIGHT].y = dArray[4 * 3 + 1] - dArray[4 * 0 + 1];
	p[FRUSTUM_RIGHT].z = dArray[4 * 3 + 2] - dArray[4 * 0 + 2];
	p[FRUSTUM_RIGHT].d = dArray[4 * 3 + 3] - dArray[4 * 0 + 3];

	p[FRUSTUM_LEFT].x = dArray[4 * 3 + 0] + dArray[0];
	p[FRUSTUM_LEFT].y = dArray[4 * 3 + 1] + dArray[4 * 0 + 1];
	p[FRUSTUM_LEFT].z = dArray[4 * 3 + 2] + dArray[4 * 0 + 2];
	p[FRUSTUM_LEFT].d = dArray[4 * 3 + 3] + dArray[4 * 0 + 3];

	p[FRUSTUM_DOWN].x = dArray[4 * 3 + 0] + dArray[4 * 1 + 0];
	p[FRUSTUM_DOWN].y = dArray[4 * 3 + 1] + dArray[4 * 1 + 1];
	p[FRUSTUM_DOWN].z = dArray[4 * 3 + 2] + dArray[4 * 1 + 2];
	p[FRUSTUM_DOWN].d = dArray[4 * 3 + 3] + dArray[4 * 1 + 3];

	p[FRUSTUM_UP].x = dArray[4 * 3 + 0] - dArray[4 * 1 + 0];
	p[FRUSTUM_UP].y = dArray[4 * 3 + 1] - dArray[4 * 1 + 1];
	p[FRUSTUM_UP].z = dArray[4 * 3 + 2] - dArray[4 * 1 + 2];
	p[FRUSTUM_UP].d = dArray[4 * 3 + 3] - dArray[4 * 1 + 3];

	p[FRUSTUM_FAR].x = dArray[4 * 3 + 0] - dArray[4 * 2 + 0];
	p[FRUSTUM_FAR].y = dArray[4 * 3 + 1] - dArray[4 * 2 + 1];
	p[FRUSTUM_FAR].z = dArray[4 * 3 + 2] - dArray[4 * 2 + 2];
	p[FRUSTUM_FAR].d = dArray[4 * 3 + 3] - dArray[4 * 2 + 3];

	p[FRUSTUM_NEAR].x = dArray[4 * 3 + 0] + dArray[4 * 2 + 0];
	p[FRUSTUM_NEAR].y = dArray[4 * 3 + 1] + dArray[4 * 2 + 1];
	p[FRUSTUM_NEAR].z = dArray[4 * 3 + 2] + dArray[4 * 2 + 2];
	p[FRUSTUM_NEAR].d = dArray[4 * 3 + 3] + dArray[4 * 2 + 3];*/

	p[FRUSTUM_RIGHT].x = m[0][3] - m[0][0];
	p[FRUSTUM_RIGHT].y = m[1][3] - m[1][0];
	p[FRUSTUM_RIGHT].z = m[2][3] - m[2][0];
	p[FRUSTUM_RIGHT].d = m[3][3] - m[3][0];

	p[FRUSTUM_LEFT].x = m[0][3] + m[0][0];
	p[FRUSTUM_LEFT].y = m[1][3] + m[1][0];
	p[FRUSTUM_LEFT].z = m[2][3] + m[2][0];
	p[FRUSTUM_LEFT].d = m[3][3] + m[3][0];

	p[FRUSTUM_DOWN].x = m[0][3] + m[0][1];
	p[FRUSTUM_DOWN].y = m[1][3] + m[1][1];
	p[FRUSTUM_DOWN].z = m[2][3] + m[2][1];
	p[FRUSTUM_DOWN].d = m[3][3] + m[3][1];

	p[FRUSTUM_UP].x = m[0][3] - m[0][1];
	p[FRUSTUM_UP].y = m[1][3] - m[1][1];
	p[FRUSTUM_UP].z = m[2][3] - m[2][1];
	p[FRUSTUM_UP].d = m[3][3] - m[3][1];

	p[FRUSTUM_FAR].x = m[0][3] - m[0][2];
	p[FRUSTUM_FAR].y = m[1][3] - m[1][2];
	p[FRUSTUM_FAR].z = m[2][3] - m[2][2];
	p[FRUSTUM_FAR].d = m[3][3] - m[3][2];

	p[FRUSTUM_NEAR].x = m[0][3] + m[0][2];
	p[FRUSTUM_NEAR].y = m[1][3] + m[1][2];
	p[FRUSTUM_NEAR].z = m[2][3] + m[2][2];
	p[FRUSTUM_NEAR].d = m[3][3] + m[3][2];


	/*p[FRUSTUM_LEFT].x = m[3][0] + m[0][0];
	p[FRUSTUM_LEFT].y = m[3][1] + m[0][1];
	p[FRUSTUM_LEFT].z = m[3][2] + m[0][2];
	p[FRUSTUM_LEFT].d = m[3][3] + m[0][3];

	p[FRUSTUM_RIGHT].x = m[3][0] - m[0][0];
	p[FRUSTUM_RIGHT].y = m[3][1] - m[0][1];
	p[FRUSTUM_RIGHT].z = m[3][2] - m[0][2];
	p[FRUSTUM_RIGHT].d = m[3][3] - m[0][3];


	p[FRUSTUM_UP].x = m[3][0] - m[1][0];
	p[FRUSTUM_UP].y = m[3][1] - m[1][1];
	p[FRUSTUM_UP].z = m[3][2] - m[1][2];
	p[FRUSTUM_UP].d = m[3][3] - m[1][3];

	p[FRUSTUM_DOWN].x = m[3][0] + m[1][0];
	p[FRUSTUM_DOWN].y = m[3][1] + m[1][1];
	p[FRUSTUM_DOWN].z = m[3][2] + m[1][2];
	p[FRUSTUM_DOWN].d = m[3][3] + m[1][3];


	p[FRUSTUM_NEAR].x = m[3][0] + m[2][0];
	p[FRUSTUM_NEAR].y = m[3][1] + m[2][1];
	p[FRUSTUM_NEAR].z = m[3][2] + m[2][2];
	p[FRUSTUM_NEAR].d = m[3][3] + m[2][3];

	p[FRUSTUM_FAR].x = m[3][0] - m[2][0];
	p[FRUSTUM_FAR].y = m[3][1] - m[2][1];
	p[FRUSTUM_FAR].z = m[3][2] - m[2][2];
	p[FRUSTUM_FAR].d = m[3][3] - m[2][3];*/

	// Normalize all plane normals
	for (int i = 0; i < 6; i++)
		p[i].Normalize();
}


// need to switch this with a plane - cube intersection text. text the bounding box of the object
bool CFrustum::SphereIntersection(const glm::vec3 & vecCenter, float flRadius)
{
	// Loop through each plane that comprises the frustum.
	for (int i = 0; i < 6; i++)
	{
		// Plane-sphere intersection test. If p*n + d + r < 0 then we're outside the plane.
		// http://youtu.be/4p-E_31XOPM
		if (((p[i].x * vecCenter.x) + (p[i].y * vecCenter.y) + (p[i].z * vecCenter.z) )
			+ p[i].d + flRadius <= 0)
			return false;
	}

	// If none of the planes had the entity lying on its "negative" side then it must be
	// on the "positive" side for all of them. Thus the entity is inside or touching the frustum.
	return true;
}



// http://zach.in.tu-clausthal.de/teaching/cg_literatur/lighthouse3d_view_frustum_culling/index.html
bool CFrustum::CubeIntersection(const RDE::bounding_box_struct & bbox)
{
	bool result = true;

	for (int i = 0; i < 6; i++)
	{
		glm::vec3 planeNormal = glm::vec3(p[i].x, p[i].y, p[i].z);

		// plane normal in box space
		glm::vec3 normal = glm::vec3(
			glm::dot(bbox.boxAxisX, planeNormal),
			glm::dot(bbox.boxAxisY, planeNormal),
			glm::dot(bbox.boxAxisZ, planeNormal)
		);

		// min
		glm::vec3 pp = bbox.backBottomLeft;
		// max
		glm::vec3 n = bbox.frontTopRight;

		if (normal.x >= 0)
			pp.x = bbox.frontTopRight.x;
		if (normal.y >= 0)
			pp.y = bbox.frontTopRight.y;
		if (normal.z >= 0)
			pp.z = bbox.frontTopRight.z;

		if(normal.x >= 0)
			n.x = bbox.backBottomLeft.x;
		if (normal.y >= 0)
			n.y = bbox.backBottomLeft.y;
		if (normal.z >= 0)
			n.z = bbox.backBottomLeft.z;


		if (
			(p[i].x * pp.x) +
			(p[i].y * pp.y) +
			(p[i].z * pp.z) + 
			 p[i].d <= 0
			)
		{
			return false;
		}
		else if(
			(p[i].x * n.x) +
			(p[i].y * n.y) +
			(p[i].z * n.z)
			+ p[i].d <= 0
		)
		{
			result = true;
		}
	}
	return result;
}