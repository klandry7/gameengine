#include "framerateDemo.h"

#include <glm/gtc/matrix_transform.hpp>

#include "transform.h"
#include "input.h"
#include "renderer.h"
#include "debugIO.h"
#include "transform.h"
#include "shader.h"
#include "camera.h"
#include "sphericalTransform.h"
#include "gameObject.h"
#include "GUI.h"

#define RAD90 3.14f / 2.0f
#define RAD180 3.14f

FramerateDemo::FramerateDemo(float aspectRatio, RDE::GUI * mainGui)
{
	camera = new RDE::Camera(aspectRatio);
	camera->SetZFar(1000.0f);

	simpleShader = std::make_shared<RDE::Shader>(RDE::Shader("passthrough", "../shaders/heightColour.vert", "../shaders/heightColour.frag"));
	for (int y = 0; y < size; y++)
	{
		for (int x = 0; x < size; x++)
		{
			roomFloorList.push_back(glm::translate(glm::mat4(1.0f), glm::vec3(float(x) * 3.0f, 0.0f, float(y) * 3.0f)));
		}
	}

	camera->SetPos(glm::vec3(-5.0f, 10.0f, -5.0f));
	camera->SetLookatPt(glm::vec3(size, 0.0f, size));

	fpsButton = static_cast<CEGUI::PushButton*>(mainGui->CreateWidget("TaharezLook/Button", glm::vec4(0.01f, 0.01f, 0.6f, 0.07f), glm::vec4(0.0f), "FpsButton"));
	fpsButton->setText("");
	fpsButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&FramerateDemo::OnFpsButtonClicked, this));

	gui = mainGui;
	fpsButton->hide();
}

FramerateDemo::~FramerateDemo()
{
	// delete everything bruh
}

void FramerateDemo::FixedUpdate(float dt)
{
}

bool FramerateDemo::Update(float dt)
{
	elapsedTime += dt;
	//printf("%f\n", 1.0f/dt);

	if (!fpsButton->isVisible())
	{
		fpsButton->show();
		fpsButton->enable();
	}
	
	if (instanced)
	{
		fpsButton->setText(std::string(std::to_string(size * size) + " cubes instanced rendered at FPS: " + std::to_string(1.0f / dt)));
	}
	else
	{
		fpsButton->setText(std::string(std::to_string(size * size) + " cubes indexed rendered FPS: " + std::to_string(1.0f / dt)));
	}
	
	if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::action1, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::pressed))
	{
		instanced = !instanced;
	}

	gui->Update(dt);


	// hit escape to quit
	if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::start, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::pressed))
	{
		fpsButton->hide();
		return false;
	}
	else
	{
		return true;
	}
}

void FramerateDemo::Render(float interpolation)
{
	// remember to bind shader before you can set any uniforms
	simpleShader->Bind();
	simpleShader->SetUniformMatrix4fv("projectionViewMatrix", false, camera->GetProjViewMatrix());
	simpleShader->SetUniform1f("elapsedTime", elapsedTime);

	simpleShader->SetUniform1i("instanced", instanced);
	if (instanced)
	{
		RDE::RENDERER::instance->DrawModelInstanced("skybox", "shader defined colour", roomFloorList.size(), roomFloorList);
	}
	else
	{
		for (int i = 0; i < size * size; i++)
		{
			simpleShader->SetUniformMatrix4fv("transformMatrix", false, roomFloorList.at(i));
			RDE::RENDERER::instance->DrawModelIndexed("skybox", "shader defined colour");
		}
	}
	gui->Draw();
}

void FramerateDemo::OnFpsButtonClicked(const CEGUI::EventArgs & e)
{
	instanced = !instanced;
}

