#ifndef	PLAYER_RENDERCOMPONENT_H
#define PLAYER_RENDERCOMPONENT_H

#include "renderComponent.h"
class GameObject;
//class Shader;

#include <vector>


class PlayerRenderComponent : public RDE::RenderComponent
{
public:
	PlayerRenderComponent(const char * modelName, const char * textureName);
	~PlayerRenderComponent();

	virtual void Init(RDE::GameObject & gameObject);
	virtual void Update(RDE::GameObject & self, glm::mat4 parentTransform, float interpolation);
	virtual std::vector<RDE::bounding_box_struct> GetBbox();

public:
	const char * modelName;
	const char * textureName;
private:
	bool animated;
};

#endif // !DEFAULTRENDERCOMPONENT_H