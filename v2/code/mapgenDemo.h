#ifndef MAPGENDEMO_H
#define MAPGENDEMO_H



#include <memory>
#include <glm\glm.hpp>
#include "mapGenerator.h"

namespace RDE
{
	class Shader;
	class Camera;
	class GameObject;
	class GUI;
}

class MapgenDemo
{
public:
	MapgenDemo(float aspectRatio);
	~MapgenDemo();

	void FixedUpdate(float dt);
	bool Update(float dt);
	void Render(float interpolation);

	void UpdateMapTiles();
	void ClearTileLists();
	void MapTileSelect(int x, int y);

private:
	std::shared_ptr<const RDE::Shader> simpleShader;
	RDE::Camera *camera;

	bool continueRunning = true;
	float mapTileSize;

	bool firstPass = true;

	RDE::MAPGEN::LAYOUT_TYPES currentLayoutGen = RDE::MAPGEN::LAYOUT_TYPES::basic;
	int generationStepCounter = 0;

	RDE::MapGenerator * mapgen;

	RDE::MAPGEN::Map currentMap;
	std::vector<glm::mat4> roomFloorList;
	std::vector<glm::mat4> tiles_noEdge;
	std::vector<glm::mat4> tiles_1edge;
	std::vector<glm::mat4> tiles_2edgeAdjacent;
	std::vector<glm::mat4> tiles_2edgeOpposite;
	std::vector<glm::mat4> tiles_3edge;
	std::vector<glm::mat4> tiles_4edge;
};

#endif // !MAPGENDEMO_H
