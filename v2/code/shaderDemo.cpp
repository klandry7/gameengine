#include "shaderDemo.h"
#include "gameObject.h"
#include "transform.h"

#include "defaultPhysicsComponent.h"
#include "defaultRenderComponent.h"
#include "defaultUpdateComponent.h"

#include "camera.h"
#include "Shader.h"

#include "hierarchyTraversal.h"

#include "input.h"

#include "renderer.h"

#include <math.h>

ShaderDemo::ShaderDemo(float aspectRatio, RDE::GUI * guiRef) : gui(guiRef)
{
	lightState = SHADING_MODE::normal;

	camera = new RDE::Camera(aspectRatio);
	camera->SetPos(glm::vec3(0.0f, 8.0f, 8.0f));
	camera->SetLookatPt(glm::vec3(0, 4, 0));
	camera->SetZFar(1000.0f);

	//simpleShader = std::make_shared<RDE::Shader>(RDE::Shader("light shader", "../shaders/lightVert.vert", "../shaders/lightFrag.frag"));

	lightShader = std::make_shared<RDE::Shader>(RDE::Shader("cel shader", "../shaders/celShadeVert.vert", "../shaders/celShadeFrag.frag"));


	root = new RDE::GameObject(nullptr, new DefaultRenderComponent("", ""), new DefaultUpdateComponent(), new DefaultPhysicsComponent(0, 0, 0), &lightShader);
	plane = new RDE::GameObject(root, new DefaultRenderComponent("largePlane", "uvMap"), new DefaultUpdateComponent(), new DefaultPhysicsComponent(0, 0, 0), &lightShader);

	//gazebo = new RDE::GameObject(root, new DefaultRenderComponent("chibi", "uvMap"), new DefaultUpdateComponent(), new DefaultPhysicsComponent(0, 0, 0), &simpleShader);

	zombie = new RDE::GameObject(root, new DefaultRenderComponent("zombie", "zombie"), new DefaultUpdateComponent(), new DefaultPhysicsComponent(0, 0, 0), &lightShader);
	zombie->transform->scale *= 1.25f;

	light = new RDE::GameObject(root, new DefaultRenderComponent("sphere", "yellow"), new DefaultUpdateComponent(), new DefaultPhysicsComponent(0, 0, 0), &lightShader);
	light->transform->translation = glm::vec3(0.0f, 5.0f, 4.5f);
	light->prevPos = light->transform->translation;

	lightColour = glm::vec3(0.4f, 0.5f, 0.7f);
	lightSpecularIntensity = glm::vec4(0.1f, 0.1f, 0.1f, 1.0f);
	ambientLightColour = glm::vec4(0.2f, 0.2f, 0.2f, 1.0f);

	lightColourSliderR = static_cast<CEGUI::Slider*>(gui->CreateWidget("TaharezLook/Slider", glm::vec4(0.1f, 0.1f, 0.03f, 0.15f), glm::vec4(0.0f), "LightColourSliderR"));
	lightColourSliderR->hide();
	lightColourSliderR->setMaxValue(1.0f);
	lightColourSliderR->setCurrentValue(0.4f);
	
	lightColourSliderG = static_cast<CEGUI::Slider*>(gui->CreateWidget("TaharezLook/Slider", glm::vec4(0.2f, 0.1f, 0.03f, 0.15f), glm::vec4(0.0f), "LightColourSliderG"));
	lightColourSliderG->hide();
	lightColourSliderG->setMaxValue(1.0f);
	lightColourSliderG->setCurrentValue(0.5f);

	lightColourSliderB = static_cast<CEGUI::Slider*>(gui->CreateWidget("TaharezLook/Slider", glm::vec4(0.3f, 0.1f, 0.03f, 0.15f), glm::vec4(0.0f), "LightColourSliderB"));
	lightColourSliderB->hide();
	lightColourSliderB->setMaxValue(1.0f);
	lightColourSliderB->setCurrentValue(0.7f);

	resetButton = static_cast<CEGUI::PushButton*>(gui->CreateWidget("TaharezLook/Button", glm::vec4(0.1f, 0.27f, 0.23f, 0.05f), glm::vec4(0.0f), "LightColourResetButton"));
	resetButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ShaderDemo::OnResetColourClicked, this));
	resetButton->setText("Reset Light Colour");
	resetButton->hide();

	switchModeButton = static_cast<CEGUI::PushButton*>(gui->CreateWidget("TaharezLook/Button", glm::vec4(0.1f, 0.34f, 0.23f, 0.05f), glm::vec4(0.0f), "SwitchModeButton"));
	switchModeButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ShaderDemo::OnSwitchModeClicked, this));
	switchModeButton->setText("Switch Lighting Mode");
	switchModeButton->hide();
}

ShaderDemo::~ShaderDemo()
{
}

void ShaderDemo::FixedUpdate(float dt)
{
	
}

bool ShaderDemo::Update(float dt)
{
	if (RDE::INPUT::instance->IsJoystickPresent())
	{
		float speed = 10.0f;

		if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::action3, RDE::INPUT::INPUT_TYPES::joypad1, RDE::INPUT::BUTTON_STATE::pressed))
		{
			SwitchLightingState();
		}
		if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::action2, RDE::INPUT::INPUT_TYPES::joypad1, RDE::INPUT::BUTTON_STATE::heldDown))
		{
			light->transform->translation += glm::vec3(0, -1.0f, 0) * dt * speed;
		}
		else if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::action1, RDE::INPUT::INPUT_TYPES::joypad1, RDE::INPUT::BUTTON_STATE::heldDown))
		{
			light->transform->translation += glm::vec3(0, 1.0f, 0) * dt * speed;
		}

		// left/right
		if (RDE::INPUT::instance->GetAxisState(LEFT_VERTICAL_AXIS, 0) < 0)
		{
			light->transform->translation += glm::vec3(0, 0, -1.0f) *  dt * speed;
		}
		else if (RDE::INPUT::instance->GetAxisState(LEFT_VERTICAL_AXIS, 0) > 0)
		{
			light->transform->translation += glm::vec3(0, 0, 1.0f) * dt * speed;
		}
		// forward/backward
		if (RDE::INPUT::instance->GetAxisState(LEFT_HORIZONTAL_AXIS, 0) < 0)
		{
			light->transform->translation += glm::vec3(-1.0f, 0, 0) * dt * speed;
		}
		else if (RDE::INPUT::instance->GetAxisState(LEFT_HORIZONTAL_AXIS, 0) > 0)
		{
			light->transform->translation += glm::vec3(1.0f, 0, 0) * dt * speed;
		}
	}
	light->prevPos = light->transform->translation;

	lightColour.r = lightColourSliderR->getCurrentValue();
	lightColour.g = lightColourSliderG->getCurrentValue();
	lightColour.b = lightColourSliderB->getCurrentValue();
	gui->Update(dt);


	if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::start, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::pressed))
	{
		DeActivate();
		return false;
	}
	else
	{
		RDE::HIERARCHY::UpdateHierarchyTraversal(root, dt);
		return true;
	}
}

void ShaderDemo::Render(float interpolation)
{
	lightShader->Bind();
	lightShader->SetUniformMatrix4fv("projectionViewMatrix", false, camera->GetProjViewMatrix());
	lightShader->SetUniformMatrix4fv("viewMatrix", false, camera->GetViewMatrix());
	
	lightShader->SetUniform3fv("lightPos", light->transform->translation);
	lightShader->SetUniform4fv("lightIntensity", lightSpecularIntensity);
	lightShader->SetUniform4fv("ambientLightIntensity", ambientLightColour);
	
	lightShader->SetUniform3fv("LightColor", lightColour);

	if (lightState == SHADING_MODE::cel)
	{
		lightShader->SetUniform1i("celShadingActive", 1);
	}
	else if (lightState == SHADING_MODE::normal)
	{
		lightShader->SetUniform1i("celShadingActive", 0);
	}

	RDE::HIERARCHY::RenderHierarchyTraversal(root, camera, interpolation);
	gui->Draw();
}

void ShaderDemo::Activate()
{
	lightColourSliderR->show();
	lightColourSliderR->enable();

	lightColourSliderG->show();
	lightColourSliderG->enable();

	lightColourSliderB->show();
	lightColourSliderB->enable();

	resetButton->show();
	resetButton->enable();

	switchModeButton->show();
	switchModeButton->enable();
}

void ShaderDemo::DeActivate()
{
	lightColourSliderR->hide();
	lightColourSliderG->hide();
	lightColourSliderB->hide();
	resetButton->hide();
	switchModeButton->hide();
}

void ShaderDemo::OnResetColourClicked(const CEGUI::EventArgs & e)
{
	lightColourSliderR->setCurrentValue(0.4f);
	lightColourSliderG->setCurrentValue(0.5f);
	lightColourSliderB->setCurrentValue(0.7f);
}

void ShaderDemo::OnSwitchModeClicked(const CEGUI::EventArgs & e)
{
	SwitchLightingState();
}

void ShaderDemo::SwitchLightingState()
{
	if (lightState == SHADING_MODE::cel)
	{
		lightState = SHADING_MODE::normal;
	}
	else if (lightState == SHADING_MODE::normal)
	{
		lightState = SHADING_MODE::cel;
	}
}
