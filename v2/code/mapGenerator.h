#ifndef MAPGENERATOR_H
#define MAPGENERATOR_H

#include <vector>
#include <glm\glm.hpp>
// IDEA
// to detect regions that're being stood on, 
// completely ignore bounds and whatever.
// each region will maintain it's unique ID on regions
// just lookup your x/y on map to get region ID
// there is then a map of region ids with region details
// this this removes culling from the equation and just 
// lets you render based on what region you're standing in
// could also keep list of adjacent regions and also render them

class Region;

namespace RDE
{
	namespace MAPGEN
	{

		glm::ivec2 GetTilePosFromWorldPos2D(glm::vec2 worldPos, float tileScaleX, float tileScaleY);
		glm::ivec2 GetTilePosFromWorldPos3D(glm::vec3 worldPos, float tileScaleX, float tileScaleY);
		glm::vec2 GetWorldPos2DFromTilePos2D(int x, int y, float tileScaleX, float tileScaleY);

		glm::vec3 GetWorldPos3DFromTilePos2D(int x, int y, float tileScaleX, float tileScaleY);


		enum TILE_TYPE
		{
			None,
			Hole,
			Floor,
			RoomFloor,
			Wall,
			Door,
			OpenDoor,
			ClosedDoor,
			DoorNorth,
			DoorEast,
			DoorSouth,
			DoorWest,
			Spawner
		};

		/*enum MAP_WALL_TILES
		{
			top,
			bottom,
			left,
			right,

			top_left,
			top_right,
			bottom_left,
			bottom_right,
			top_bottom,
			left_right,

			bottom_left_right,
			top_left_right,
			top_bottom_left,
			top_bottom_right,

			no_edge
		};*/

		struct Rect
		{
			int x;
			int y;
			int height;
			int width;
		};

		struct Map
		{
			int width;
			int height;

			// for basic rect rooms
			int numRoomTries;
			int extraConnectorChance;
			int roomExtraSize;
			int windingPercent;
			bool tryRoomsFirst;
			bool streamline;

			// for cellular automata
			int chanceToStartAlive; // value of 0-100
			int deathLimit;
			int birthLimit;

			int ** regions; // region map
			TILE_TYPE ** dungeon; // tile map

			std::vector<Rect> rooms;

			int currentRegion;

			void IncRegionCounter(){
				currentRegion++;
			}
		};


		struct Cell
		{
			int x;
			int y;

			Cell& operator =(const Cell& o){
				x = o.x;
				y = o.y;
				return *this;
			}
			bool operator==(const Cell &o) const {
				return (x == o.x && y == o.y);
			}
		};

		enum DIRECTION
		{
			up,
			down,
			left,
			right
		};
		
		enum LAYOUT_TYPES
		{
			cave,
			basic
		};
	}

	class MapGenerator
	{
	public:
		// ctor
		MapGenerator();
		// dtor
		~MapGenerator();
		// generate new map
		RDE::MAPGEN::Map NewMap(int width, int height, RDE::MAPGEN::LAYOUT_TYPES layout);
		// generate new map with debug console printing
		RDE::MAPGEN::Map NewMapDebugConsole(int width, int height, RDE::MAPGEN::LAYOUT_TYPES layout);
		// renerate new map with step by step in engine building
		RDE::MAPGEN::Map InitMapForDebugEngineBuild(int width, int height, RDE::MAPGEN::LAYOUT_TYPES layout);
		// in engine update step for basic gen
		RDE::MAPGEN::Map InEngineBasicStep(int step, RDE::MAPGEN::Map map);
		// in engine update step for cave gen
		RDE::MAPGEN::Map InEngineCaveStep(int step, RDE::MAPGEN::Map map);
	private:
		// calls functions to generate basic map 
		void GenerateBasic(RDE::MAPGEN::Map * map);
		// calls functions to generate basic map with debug console print
		void GenerateBasicDebug(RDE::MAPGEN::Map * map);
		// calls functions to generate cave 
		void GenerateCave(RDE::MAPGEN::Map * map);
		// calls functions to generate cave  with debug console print
		void GenerateCaveDebug(RDE::MAPGEN::Map * map);
		// check for rect overlap
		bool RectOverlap(RDE::MAPGEN::Rect r1, RDE::MAPGEN::Rect r2);
		// init map to all walls
		void InitMapToAllWalls(RDE::MAPGEN::Map * map);
		// init map for cave gen (noise)
		void InitMapForCellularAutomata(RDE::MAPGEN::Map * map);
		// cave gen smoothing step
		void SimulationStep(RDE::MAPGEN::Map * map);
		// count alive neighours (for cave gen)
		int  CountAliveNeighbours(RDE::MAPGEN::Map * map, int x, int y);
		// detect regions (for cave gen)
		void DetectRegions(RDE::MAPGEN::Map * map, std::vector<Region>* regions);
		// remove regions below size threshold (for cave gen)
		void RemoveSmallRegions(RDE::MAPGEN::Map * map, std::vector<Region>* regionList, int sizeThreshold);
		// connect closest regions (for cave gen)
		void ConnectClosestRegions(RDE::MAPGEN::Map * map, std::vector<Region>* regions, bool forceAccessibleFromMainRegion = false);
		// set a region as accessible from main region (for cave gen)
		void SetAccessibleFromMainRegion(int r, std::vector<Region>* regions);
		// connect cave regions (for cave gen)
		void ConnectCaveRegions(int a, int b, std::vector<Region> * regions);
		// create passage (for cave gen)
		void CreatePassage(int regionA, int regionB, RDE::MAPGEN::Cell tileA, RDE::MAPGEN::Cell tileB, RDE::MAPGEN::Map * map, std::vector<Region> * regions);
		// get a line (for cave gen)
		void GetLine(RDE::MAPGEN::Cell from, RDE::MAPGEN::Cell to, std::vector<RDE::MAPGEN::Cell>* result);
		// draw a circle (for cave gen)
		void DrawCircle(RDE::MAPGEN::Cell c, int r, RDE::MAPGEN::Map * map);
		// add rooms (for basic)
		void AddRooms(RDE::MAPGEN::Map * map);
		// carve a tile type into the map
		void Carve(RDE::MAPGEN::Map * map, int x, int y, RDE::MAPGEN::TILE_TYPE t);
		// fill empty space with maze (for basic)
		void FillSpaceWithMaze(RDE::MAPGEN::Map * map);
		// grow the maze (for basic)
		void GrowMaze(RDE::MAPGEN::Map * map, int x, int y);
		// check if tile can be carved  (for basic)
		bool CanCarve(RDE::MAPGEN::Map * map, RDE::MAPGEN::Cell pos, RDE::MAPGEN::Cell direction);
		// print map to console 
		void PrintMapToConsole(RDE::MAPGEN::Map * map);
		// print map to console upside down
		void PrintMapToConsoleReverse(RDE::MAPGEN::Map * map);
		// connect maze regions  (for basic)
		void ConnectMazeRegions(RDE::MAPGEN::Map * map);
		// remove dead ends from maze  (for basic)
		void RemoveDeadEnds(RDE::MAPGEN::Map * map);
	private:
		// wall code
		int wallCode;
		// array of cardinal directions
		RDE::MAPGEN::Cell * cardinal;
	};
}

/*
map rendering will have to happen differenly than it currently is
it won't be part of the hierarchy. this excludes it from updates/render hierarchy stuff
will need to use instanced rendering for best performance
for simplest map draw, pass in the model/texture you want for the floor and wall and maybe cieling (both 1x1. wall is cube in this case)
also pass in sizes (x/z and y height) so it draw properly. all sizes must be uniform.
I could manually scale automatically if I kept the model sizes, but that's extra. need it working first
it'll directly call the renderer and draw whatever area you need instanced

could later expand to draw up/down/left/right walls, variations on textures, pillars if there's no adjacency

so you'll just pass in a map to the renderer alongside the gameobject hierarchy

each region should have a list of it's tiles and transform matrices so that they only get computed once
and then use that for instanced render.
*/

#endif // !MAPGENERATOR_H

