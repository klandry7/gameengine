#ifndef DEMOGAMEONE_H
#define DEMOGAMEONE_H

#include <memory>
#include <glm\glm.hpp>
#include "mapGenerator.h"

namespace RDE
{
	class Shader;
	class Camera;
	class GameObject;
	class GUI;
}

class WalkingDemo
{
public:
	WalkingDemo(float aspectRatio);
	~WalkingDemo();

	void FixedUpdate(float dt);
	bool Update(float dt);
	void Render(float interpolation);

	void RollNewMap(RDE::MAPGEN::LAYOUT_TYPES layoutType);
	void DeleteCurrentMap();
	void MapTileSelect(int x, int y);

private:
	std::shared_ptr<const RDE::Shader> simpleShader;
	RDE::Camera *camera;

	RDE::GameObject *henrietta;
	RDE::GameObject *pleinair;
	RDE::GameObject *boblamp;
	RDE::GameObject *rootNode;
	RDE::GameObject *playerCamGameObject;
	RDE::GameObject *freeCam;
	RDE::GameObject *player;

	bool continueRunning = true;
	float mapTileSize;
	bool freeCamState = false;

	glm::vec3 startingPos;

	RDE::MAPGEN::Map currentMap;
	std::vector<glm::mat4> roomFloorList;
	std::vector<glm::mat4> tiles_noEdge;
	std::vector<glm::mat4> tiles_1edge;
	std::vector<glm::mat4> tiles_2edgeAdjacent;
	std::vector<glm::mat4> tiles_2edgeOpposite;
	std::vector<glm::mat4> tiles_3edge;
	std::vector<glm::mat4> tiles_4edge;
};

#endif // !DEMOGAMEONE_H
