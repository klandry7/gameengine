#ifndef MAINCOMPONENT_H
#define MAINCOMPONENT_H

#include "common.h"
class Game;

namespace RDE
{
	class WindowApp;
	class Main
	{
	public:
		// ctor
		Main();
		// start the engine
		void Start();
		// stop the engine
		void Stop();
	private:
		// called by start. Returns once stop is called. The core loop.
		void Run();
		// the base render function
		void Render(float interpolation);
		// cleanup and shutdown
		void Cleanup();
	private:
		// bool for whether the engine is running or not
		bool32 running;
		// window context
		RDE::WindowApp * windowApp;
		// the game instance
		Game *game;
	};
}

#endif // !MAIN_H