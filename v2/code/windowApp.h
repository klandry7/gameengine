#ifndef WINDOWAPP_H
#define WINDOWAPP_H

#include "common.h"

// needs to be included before GLFW
#include "GLExtension.h"

#include <GLFW/glfw3.h>

namespace RDE
{
	class WindowApp
	{
	public:
		// ctor
		WindowApp(int _windowWidth, int _windowHeight, const char * title);
		// post message to close windo
		void PostWindowCloseMessage(bool state);
		// close window?
		bool32 WindowShouldClose();
		// set time
		void SetTime(real32 time);
		// get time
		float GetTime();
		// glfw terminate
		void Terminate();
		// set mouse state
		void SetMouseState(bool32 show, bool32 restrictToWindow);
		// get screen centre
		void GetScreenCentre(float * halfX, float * halfY);
		// set double buffering state
		void SetDoubleBuffering(bool state);
		// set anti aliasing sample rate
		void SetAASampleRate(int samples);
		// swap render buffers
		void SwapBuffers();
		// poll events
		void PollEvents();
		// clear buffers
		void ClearBuffers();
		// show window (starts hidden)
		void ShowWindow();
		// get window handle 
		size_t GetWindow()
		{
			return (size_t)window;
		}

	private:
		// init glew
		int InitGLEW();
		// init glfw
		int InitGLFW(int OGLversionMajor, int OGLversionMinor);

	private:
		// window context
		GLFWwindow * window;
		// double buffering
		bool doubleBuffered;
		// anti aliasing rate
		int aaSampleRate;
		// window width
		int windowWidth;
		// window height
		int windowHeight;
	};
}

#endif // !WINDOWAPP_H