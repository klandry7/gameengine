#include "gameObject.h"
#include "renderComponent.h"
#include "updateComponent.h"
#include "physicsComponent.h"
#include "transform.h"
#include "animManager.h"
#include "sphericalTransform.h"

// move to data oriented design for game object?
// late game, yes. For now, no.

RDE::GameObject::GameObject(RDE::GameObject *_parent, RDE::RenderComponent * r, RDE::UpdateComponent *u, RDE::PhysicsComponent * p, const std::shared_ptr<const RDE::Shader> * s) : renderComponent(r), updateComponent(u), physicsComponent(p), defaultShaderReference(*s)
{
	transform = new RDE::Transform();
	animManager = new RDE::AnimManager();
	velocity = glm::vec3(0.0f, 0.0f, 0.0f);
	prevPos = glm::vec3(0.0f, 0.0f, 0.0f);
	sphericalTransform = new RDE::SphericalTransform(0, 0, true);
	// call an init(*this) so you can to startup stuff
	renderComponent->Init(*this);
	updateComponent->Init(*this);
	
	// hierarchy initialization
	if (_parent != nullptr)
	{
		EnableRender(true);
		EnableUpdate(true);
		SetParent(_parent);
		_parent->AddChild(this);
	}
	else
	{
		SetParent(_parent);
		EnableRender(false);
		EnableUpdate(false);
	}
}

RDE::GameObject::~GameObject()
{
	// for all the children
	for (unsigned int i = 0; i < children.size(); i++)
	{
		if (parent == nullptr)
		{
			children[i]->SetParent(nullptr);
		}
		else
		{
			// set their parent to this gameobject's parent
			parent->AddChild(GetChild(i));
		}
	}

	// cannot delete the scene root
	// currently this is a problem. need to make it so root is stored elsewhere
	//if (parent == nullptr)
	//	return;

	if (parent != nullptr)
	{
		// for all of the parents children
		for (int i = parent->GetChildrenCount() - 1; i >= 0; i--)
		{
			// when you find this object
			if (parent->children[i] == this)
			{
				// remove it from the parent's list of children
				parent->children.erase(parent->children.begin() + i);
				break;
			}
		}
	}
	
	delete transform;
	delete animManager;
	delete renderComponent;
	delete updateComponent;
	delete physicsComponent;
}

void RDE::GameObject::Render(glm::mat4 parentTransform, float interpolation)
{
	renderComponent->Update(*this, parentTransform, interpolation);
}

void RDE::GameObject::Update(float dt)
{
	updateComponent->Update(*this, dt);
}

void RDE::GameObject::PhysicsUpdate(float dt, const RDE::MAPGEN::Map *map)
{
	physicsComponent->Update(*this, dt, map);
}

void RDE::GameObject::EnableUpdate(bool state)
{
	updateState = state;
}

bool RDE::GameObject::UpdateEnabled()
{
	return updateState;
}

void RDE::GameObject::EnableRender(bool state)
{
	renderState = state;
}

bool RDE::GameObject::RenderEnabled()
{
	return renderState;
}

RDE::GameObject * RDE::GameObject::GetChild(int index)
{
	return children.at(index);
}

std::vector<RDE::bounding_box_struct> RDE::GameObject::GetBoundingBox()
{
	return renderComponent->GetBbox();
}

glm::vec3 RDE::GameObject::GetHierarchialTranslation()
{
	glm::vec3 result;

	if (parent != nullptr)
		result = transform->translation + parent->GetHierarchialTranslation();
	else
		result = transform->translation;
	return result;

}

glm::vec3 RDE::GameObject::GetHierarchialPrevPos()
{
	glm::vec3 result;

	if (parent != nullptr)
		result = prevPos + parent->GetHierarchialTranslation();
	else
		result = prevPos;
	return result;
}

void RDE::GameObject::AddChild(GameObject * c)
{
	children.push_back(c);
	c->SetParent(this);
}

void RDE::GameObject::SetParent(GameObject * c)
{
	this->parent = c;
}

bool RDE::GameObject::IsRoot()
{
	return (parent == nullptr ? true : false);
}

unsigned int RDE::GameObject::GetChildrenCount()
{
	return children.size();
}
