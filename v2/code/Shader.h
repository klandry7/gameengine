#ifndef SHADER_H
#define SHADER_H

#include "GLExtension.h"

//#include <cstdlib>
//#include <cctype>
//#include <list>
#include <map> 

#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>

#include <vector>
namespace RDE
{
	class Shader
	{
	public:
		// ctor 1
		Shader(const char *nom);
		// ctor 2
		Shader(const char * nom, const char * vertPath, const char * fragPath);
		// dtor
		~Shader();

		// add vert shader
		void AddVertexShader(const char * text);
		// add frag shader
		void AddFragmentShader(const char * text);
		// add geometry shader
		void AddGeometryShader(const char * text);
		// compile shader
		void CompileShader();

		/// <summary>Bind the shader. Wrapper around glUseProgram(program). 
		///MUST be called before setting any uniforms or using the shader in any way
		///If another shader is currently bound, it will no longer be.</summary>
		void Bind() const;
		///<summary>Unbind the shader. Wrapper around glUseProgram(0)
		///Remember: once Unbind is called, shader is inactive until Bind is called again.
		///Never really needs to be called. At the end of a frame the current shader is unbound and Binding a new shader means the current is no longer bound.</summary>
		void Unbind() const;

		//void UpdateUniforms();

		///<summary>Set uniform int/bool value</summary>
		void SetUniform1i(const char * uniformName, int value) const;
		///<summary>Set uniform float value</summary>
		void SetUniform1f(const char * uniformName, float value) const;
		///<summary>Set uniform vec3 value</summary>
		void SetUniform3fv(const char * uniformName, glm::vec3 value) const;
		///<summary>Set uniform mat4 value</summary>
		void SetUniformMatrix4fv(const char * uniformName, bool transpose, glm::mat4 value) const;
		///<summary>Set uniform vec4 value</summary>
		void SetUniform4fv(const char * uniformName, glm::vec4 value) const;

	private:
		// add GL program
		void AddProgram(const char * fp, GLenum type);

	private:
		// GL program
		GLuint program;
		// shader name
		const char *shaderName;
		// map of uniforms 
		std::map<std::string, GLuint> uniforms;
		// is it compiled??
		bool isCompiled;
	};
}

#endif // !SHADER_H
