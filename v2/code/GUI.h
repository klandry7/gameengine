#ifndef	ENGINE_GUI_H
#define ENGINE_GUI_H

#include <CEGUI\CEGUI.h>

#include <glm\glm.hpp>
//#include <GLFW\glfw3.h>

struct GLFWwindow;

namespace CEGUI
{
	class OpenGL3Renderer;
}

namespace RDE
{
	class GUI
	{
	public:
		// ctor
		GUI(const size_t windowRef);
		// dtor
		~GUI();
		/// <summary>
		/// MUST be called after windowapp is initialized (aka after opengl is fully initialized)
		/// </summary>
		void Init(const std::string & resourceDirectory);
		/// <summary>Destroy the GUI context.</summary>
		void Destroy();

		/// <summary>Draw the ui. Should be called last to render on top.</summary>
		void Draw();
		/// <summary>Update. For any animations that need a time value.</summary>
		void Update(float dt);

		/// <summary>Load a UI scheme. Multiple schemes can be loaded at once.</summary>
		void LoadScheme(const std::string& schemeFilepath);
		/// <summary>Load and set a font. Path must NOT include .font extension (will be added in automatically)</summary>
		void SetFont(const std::string& fontFilepath);

		//set mouse cursor image
		void SetMouseCursor(const std::string & imageFile);
		// show mouse cursor
		void ShowMouseCursor();
		// hide mouse cursor
		void HideMouseCursor();

		/// <summary>Get CEUI OpenGL3Renderer</summary>
		static CEGUI::OpenGL3Renderer * GetRenderer() { return m_renderer; }
		/// <summary>Get CEUI GUI context</summary>
		const CEGUI::GUIContext * GetContext() { return m_context; }

		/// <summary>CEGUI widget type, % of parent vec4(x,y, width, height), pixel position vec4(x,y, width, height), widget name</summary>
		CEGUI::Window * CreateWidget(const std::string & type, const glm::vec4 & destRectPerc, const glm::vec4 & destRectPix, const std::string& name = "");
		/// <summary>CEGUI widget pointer, % of parent vec4(x,y, width, height), pixel position vec4(x,y, width, height)</summary>
		static void SetWidgetDestRect(CEGUI::Window * widget, const glm::vec4 & destRectPerc, const glm::vec4 & destRectPix);

	private:
		// Get Key State. must be static to work with GLFW c style callback.
		static void GLFW_KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
		// get cursor state. must be static to work with GLFW c style callback.
		static void GLFW_CursorPositionCallback(GLFWwindow* window, double xpos, double ypos);
		// get mouse state. must be static to work with GLFW c style callback.
		static void GLFW_MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
		// get char mods state. must be static to work with GLFW c style callback.
		static void GLFW_CharacterModsCallback(GLFWwindow * window, unsigned int codepoint, int mods);
	private:
		// CEGUI render context
		static CEGUI::OpenGL3Renderer * m_renderer;
		// CEGUI gui context
		static CEGUI::GUIContext * m_context;
		// CEGUI window context
		CEGUI::Window * m_rootWindow = nullptr;
	};
}

#endif