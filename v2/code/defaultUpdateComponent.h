#ifndef DEFAULTUPDATECOMPONENT_H
#define DEFAULTUPDATECOMPONENT_H

#include "updateComponent.h"
class GameObject;

class DefaultUpdateComponent : public RDE::UpdateComponent
{
public:
	DefaultUpdateComponent();
	~DefaultUpdateComponent();

	virtual void Update(RDE::GameObject & gameObject, float dt);
	virtual void Init(RDE::GameObject & gameObject);
private:

};

#endif 
