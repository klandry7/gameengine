#include "windowApp.h"
#include "main.h"
#include "input.h"
#include "game.h"

#include <vector>
#include <glm/glm.hpp>
#include "renderer.h"

#include <random>

#define TARGET_SECONDS_PER_FRAME 1.0f / 60.0f
#define SECONDARY_SECONDS_PER_FRAME	1.0f / 30.0f
#define FOUR_BY_THREE 4.0f / 3.0f
#define SIXTEEN_BY_NINE 16.0f / 9.0f
#define FRAMETIME_CAP 0.2f

/// <summary><para>
/// MainComponent Constructor 
/// </para></summary>
RDE::Main::Main()
{
	running = false;
	// create a new window
	windowApp = new RDE::WindowApp(800, 600, "RDE");
	// set the mouse state
	windowApp->SetMouseState(false, false);
	RDE::RENDERER::instance->SetClearColour(0, 0, 0);
	// keep double buffering enabled for stable physics sim
	//window->SetDoubleBuffering(true);
	// 1050.0f / 1680.0f
	game = new Game(SIXTEEN_BY_NINE, windowApp->GetWindow());
	// the window starts invisible to prevent showing an all white window (that looks like it froze)
	// need to unhide it before starting obviously
	windowApp->ShowWindow();
}

/// <summary><para>
/// Called once to run the engine.
/// </para></summary>
void RDE::Main::Start()
{
	if (running){
		return;
	}	
	Run();
}

/// <summary><para>
/// Called once to stop the engine.
/// </para></summary>
void RDE::Main::Stop()
{
	if (!running){
		return;
	}
	running = false;
}

/// <summary><para>
/// Called once by MainComponent::Start() and runs until MainComponent::Stop() is called
/// </para></summary>
void RDE::Main::Run()
{
	if (windowApp)
		running = true;

	windowApp->SetTime(0);

	float totalPhysicsStepTime = 0.0f;
	const float dtPhysicsStep = 0.005f;

	float previousTime = windowApp->GetTime();
	float accumulator = 0.00f;

	while (running)
	{
		float currentTime = windowApp->GetTime();
		float frameTime = currentTime - previousTime;
		if (frameTime > FRAMETIME_CAP)
			frameTime = FRAMETIME_CAP;
		
		previousTime = currentTime;

		accumulator += frameTime;

		//int loops = 0;
		bool render = false;
		
		while (accumulator >= dtPhysicsStep)
		{
			render = true;
			//loops++;
			game->FixedUpdate(dtPhysicsStep);
			totalPhysicsStepTime += dtPhysicsStep;
			accumulator -= dtPhysicsStep;
		}

		//printf("%i\n", loops);
		const float alpha = accumulator / dtPhysicsStep;
		
		windowApp->PollEvents();
		RDE::INPUT::instance->UpdateInput(windowApp->GetWindow());

		
		if (game->Update(frameTime) == false)
		{
			windowApp->PostWindowCloseMessage(true);
		}

		RDE::INPUT::instance->SwapInput();

		if (windowApp->WindowShouldClose())
		{
			running = false;
		}

		//float interpolation = (t + TARGET_SECONDS_PER_FRAME - frameTime) / TARGET_SECONDS_PER_FRAME;
		if (frameTime > 0.0185f)
		{
			//printf("frame time: %f accumulator: %f alpha: %f\n", frameTime, accumulator, alpha);
			//render = false;
		}
			

		// incredibly bad. performs terribly. from 0.0008 to 0.002
		/*float t1 = window->GetTime();
		std::vector<glm::mat4> boneTransform = renderContext::renderer->GetBoneTransforms("henrietta", 0, 0);
		float t2 = window->GetTime() - t1;
		printf("time: %f\n", t2);*/

		//printf("dt: %f\n", frameTime);

		//float t1 = window->GetTime();
		if (render)
		{
			Render(alpha);
		}
		else
		{
			//printf("missed render \n");
		}	
		//float t2 = window->GetTime() - t1;
		//printf("time: %f\n", t2);
	}
	Cleanup();
}

/// <summary><para>
/// Render to the window
/// </para></summary>
void RDE::Main::Render(float interpolation)
{
	// clear screen
	windowApp->ClearBuffers();
	// render
	//float t1 = window->GetTime();
	game->Render(interpolation);
	//printf("render dt: %f\n", window->GetTime() - t1);
	// unbind stuff then swap front/back buffers
	windowApp->SwapBuffers();
}

/// <summary><para>
/// called after MainComponent::Stop() is called and MainComponent::Run() has exited it's loop
/// </para></summary>
void RDE::Main::Cleanup()
{
	windowApp->Terminate();
}

/// <summary><para> Program entry point </para></summary>
int main(int argc, char *argv[])
{
	srand((unsigned int)time(0));
	//srand(1);
	RDE::Main * mc = new RDE::Main();
	mc->Start();
	return 0;
}