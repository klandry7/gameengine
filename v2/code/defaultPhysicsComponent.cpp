#include "defaultPhysicsComponent.h"
#include "transform.h"
#include "gameObject.h"
#include "mapGenerator.h"

#include <algorithm>
#include <math.h>

#define APPROX_ZERO 0.000001f

bool DefaultPhysicsComponent::rectRectCollision(float x, float y, float w, float h, float tileX, float tileY, float tileW, float tileH)
{
	bool result =  
		x < tileX + tileW &&
		tileX < x + w &&
		y < tileY + tileH &&
		tileY < y + h;
	//if (result) { printf("%f %f %f %f %f %f %f %f \n", x, y, w, h, tileX, tileY, tileW, tileH); }
	return result;
}

bool ApproxEqual(float a, float b, float diff)
{	
	return (fabsf(a - b) < diff);
}

bool AABBvsAABBcollision(glm::vec2 bottomRightA, glm::vec2 sizeA, glm::vec2 bottomRightB, glm::vec2 sizeB)
{
	// minpt
	glm::vec2 aabbBottomRight(
		(bottomRightA.x - (bottomRightB.x + sizeB.x)),
		(bottomRightA.y - (bottomRightB.y + sizeB.y)));

	// maxpt
	glm::vec2 aabbTopLeft(
		(aabbBottomRight.x + sizeA.x + sizeB.x),
		(aabbBottomRight.y + sizeA.y + sizeB.y));

	return (aabbBottomRight.x <= 0 && aabbBottomRight.y <= 0 &&
		aabbTopLeft.x >= 0 && aabbTopLeft.y >= 0);
}

float GetRayIntersectionFractionOfFirstRay(glm::vec2 originA, glm::vec2 endA, glm::vec2 originB, glm::vec2 endB)
{
	glm::vec2 r = endA - originA;
	glm::vec2 s = endB - originB;

	float numerator = glm::dot((originB - originA), r);
	float denominator = glm::dot(r, s);

	if (ApproxEqual(numerator, 0.0f, APPROX_ZERO), ApproxEqual(denominator, 0.0f, APPROX_ZERO))
	{
		// lines are colinear
		// should check if they overlap (calc intersection point)
		return std::numeric_limits<float>::infinity();
	}
	if (ApproxEqual(denominator, 0.0f, APPROX_ZERO))
	{
		// lines are parallel
		return std::numeric_limits<float>::infinity();
	}

	float u = numerator / denominator;
	float t = glm::dot((originB - originA), s) / denominator;

	if ((t >= 0) && (t <= 1) && (u >= 0) && (u <= 1))
	{
		return t;
	}

	return std::numeric_limits<float>::infinity();
}

float GetRayItersectionFraction(glm::vec2 origin, glm::vec2 direction, glm::vec2 min, glm::vec2 max)
{
	glm::vec2 end = origin + direction;

	// for each of AABB 4 edges calculate the min fraction of direction
	// in order to find where ray first intersect, the AABB
	float minT = GetRayIntersectionFractionOfFirstRay(origin, end, min, glm::vec2(min.x, max.y));
	float x;
	x = GetRayIntersectionFractionOfFirstRay(origin, end, glm::vec2(min.x, max.y), max);
	if (x < minT)
		minT = x;
	x = GetRayIntersectionFractionOfFirstRay(origin, end, max, glm::vec2(max.x, min.y));
	if (x < minT)
		minT = x;
	x = GetRayIntersectionFractionOfFirstRay(origin, end, glm::vec2(max.x, min.y), min);
	if (x < minT)
		minT = x;

	// 
	return minT;
}



DefaultPhysicsComponent::DefaultPhysicsComponent(float width, float height, float tileSizeMod)
{
	bboxHeight = height;
	bboxWidth = width;

	bboxHalfWidth = bboxWidth / 2.0f;
	bboxHalfHeight = bboxHeight / 2.0f;

	tileSizeModifier = tileSizeMod;
}

DefaultPhysicsComponent::~DefaultPhysicsComponent()
{
}

// init with a bounding box?
void DefaultPhysicsComponent::Init(RDE::GameObject & self)
{
	
}

// currently have hierarchial translation
// need hierarchial scale

//REMEMBER: left/up is positive. Opengl coords, not screen coords. Use bottom right for corner.
void DefaultPhysicsComponent::Update(RDE::GameObject & self, float dt, const RDE::MAPGEN::Map *map)
{
	if (!self.UpdateEnabled())
		return;

	self.velocity = self.velocity - (0.04f * self.velocity);

	//// !! update position !! ////
	self.prevPos = self.transform->translation;
	self.transform->translation = self.transform->translation + (self.velocity * dt);

	//// !! detect collision !! ////
	
	// broadphase 
	// get the possible bounding area based on the previous position 
	// and the current position. only check collision in that square region
	
	// get current and previous position
	glm::vec3 prevPos = self.GetHierarchialTranslation();
	glm::vec3 pos = self.GetHierarchialTranslation() + (self.velocity * dt);
	
	// broadphase check area
	glm::vec2 minPt(fminf(pos.x, prevPos.x), fminf(pos.z, prevPos.z));
	glm::vec2 maxPt(fmaxf(pos.x, prevPos.x), fmaxf(pos.z, prevPos.z));

	glm::vec2 tileSize(tileSizeModifier, tileSizeModifier);

	// get the bottom right corner 
	float bottomRightX = minPt.x - bboxHalfWidth;
	float bottomRightY = minPt.y - bboxHalfHeight;
	// get the current tile index based on bottomRight corner location
	glm::ivec2 minTilePos = RDE::MAPGEN::GetTilePosFromWorldPos2D(glm::vec2(bottomRightX, bottomRightY), tileSize.x, tileSize.y);

	// get the top left corner 
	float topLeftX = maxPt.x + bboxHalfWidth;
	float topLeftY = maxPt.y + bboxHalfHeight;
	// get the current tile index based on top left corner location
	glm::ivec2 maxTilePos = RDE::MAPGEN::GetTilePosFromWorldPos2D(glm::vec2(topLeftX, topLeftY), tileSize.x, tileSize.y);
	
	//printf("<%i, %i>,  <%i, %i>\n", minTileX, minTileY, maxTileX, maxTileY);

	bool collisionOccurred = false;

	// scan the surrounding tiles for collision
	for (int y = minTilePos.y; y <= maxTilePos.y; y++)
	{
		for (int x = minTilePos.x; x <= maxTilePos.x; x++)
		{
			// if out of bounds, skip
			if (x < 0 || x >= map->width || y < 0 || y >= map->height)
				continue;

			// if it's a wall, check for collision
			// https://hamaluik.com/posts/simple-aabb-collision-using-minkowski-difference/
			// https://hamaluik.com/posts/swept-aabb-collision-using-minkowski-difference/
			if (map->dungeon[y][x] == RDE::MAPGEN::TILE_TYPE::Wall)
			{
				if (collisionOccurred)
				{				
				}
				prevPos = self.GetHierarchialTranslation();

				// the tile x/y and w/h
				glm::vec2 tileBottomRight = RDE::MAPGEN::GetWorldPos2DFromTilePos2D(x, y, tileSize.x, tileSize.y);

				// the bbox x/y and w/h
				float bboxBottomRightX = prevPos.x - bboxHalfWidth;
				float bboxBottomRightY = prevPos.z - bboxHalfHeight;

				// bbox minpt
				glm::vec2 aabbBottomRight(
					(tileBottomRight.x - (bboxBottomRightX + bboxWidth)),
					(tileBottomRight.y - (bboxBottomRightY + bboxHeight)));

				// bbox maxpt
				glm::vec2 aabbTopLeft(
					(aabbBottomRight.x + tileSize.x + bboxWidth),
					(aabbBottomRight.y + tileSize.y + bboxHeight));

				if (AABBvsAABBcollision(
					RDE::MAPGEN::GetWorldPos2DFromTilePos2D(x, y, tileSize.x, tileSize.y),
					glm::vec2(tileSize.x, tileSize.y),
					glm::vec2(prevPos.x - bboxHalfWidth, prevPos.z - bboxHalfHeight),
					glm::vec2(bboxWidth, bboxHeight) ))
				{
					//printf("<%i, %i>,  <%i, %i>\n", minTileX, minTileY, maxTileX, maxTileY);
					//printf("<%f, %f>,  <%f, %f>\n", aabbBottomRight.x, aabbBottomRight.y, aabbTopLeft.x, aabbTopLeft.y);
					
					glm::vec2 point(0.0, 0.0f);

					float minDist = fabsf(point.x - aabbBottomRight.x);
					glm::vec2 boundPoint(aabbBottomRight.x, point.y);
					if (fabs(aabbTopLeft.x - point.x) < minDist)
					{
						minDist = fabs(aabbTopLeft.x - point.x);
						boundPoint = glm::vec2(aabbTopLeft.x, point.y);
					}
					if (fabs(aabbTopLeft.y - point.y) < minDist)
					{
						minDist = fabs(aabbTopLeft.y - point.y);
						boundPoint = glm::vec2(point.x, aabbTopLeft.y);
					}
					if (fabsf(aabbBottomRight.y - point.y) < minDist)
					{
						minDist = fabsf(aabbBottomRight.y - point.y);
						boundPoint = glm::vec2(point.x, aabbBottomRight.y);
					}

					self.transform->translation.x += boundPoint.x;
					self.transform->translation.z += boundPoint.y;
					//self.velocity *= 0.50f;
					//self.transform->translation = self.transform->translation + (self.velocity * dt);
					collisionOccurred = true;
					//break;
				}
				/*else
				{
					// get relative motion, but tiles never move so...
					glm::vec3 relativeMotion = self.velocity * dt;
					// raycast the relative motion vector against the minkowski aabb
					float h = GetRayItersectionFraction(glm::vec2(0.0f), glm::vec2(relativeMotion.x, relativeMotion.z), aabbBottomRight, aabbTopLeft);
					// check to see if a collision will happen this frame 
					// returns infinity if no intersection
					if (h < std::numeric_limits<float>::infinity())
					{
						// true, there will be an intersection this frame 
						if (!collisionOccurred)
						{
							self.transform->translation = self.prevPos;

						}
						self.transform->translation += (self.velocity * dt * h);// (1.0f - h));
						glm::vec3 tangent = glm::normalize(relativeMotion);
						float temp = -tangent.x;
						tangent.x = tangent.z;
						tangent.z = temp;
						self.velocity = glm::dot(self.velocity, tangent) * tangent;
						//self.transform->translation = self.transform->translation + (self.velocity * dt) * 0.5f;
						collisionOccurred = true;
						//break;
					}
					else
					{
						// no intersection, keep on moving 
						//self.transform->translation = self.transform->translation + (self.velocity * dt);		
					}
				}*/
			}
		}
	}
	if (!collisionOccurred)
	{
	}

}
