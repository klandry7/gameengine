#include "game.h"

#include "GUI.h"
#include "renderer.h"
#include "debugIO.h"

// !! IMPORTANT !!
// abstract components even higher to just a component
// then allow to add an arbitrary # of components 
// then gameobject will just run through it's update if it has one 
// will have to have a unity like thing to check if a component is attatched
// this may be a bit much in 1 month considering all else that has to be done
// https://www.raywenderlich.com/24878/introduction-to-component-based-architecture-in-games


// typedef long namespace chains so they're not so ugly

// should change bounding box to be the largest points and form bbox based on that and make it an AABB
// so then there's no situation where itd be out of the AABB (as long as bone origin matches model origin)
// AABB scales and translates but doesn't rotate.

Game::Game(float _aspectRatio, const size_t windowRef) : wndwRef(windowRef)
{
	gameStateStack.push(GAME_STATE::mainMenuActive);
	aspectRatio = _aspectRatio;

	RDE::RENDERER::instance->LoadTexture("../models/henrietta.bmp", "henrietta");
	RDE::RENDERER::instance->LoadTexture("../models/labyrinth/wall2_1.bmp", "pillar");
	RDE::RENDERER::instance->LoadTexture("../models/xskybox.bmp", "skybox");
	RDE::RENDERER::instance->LoadTexture("../models/labyrinth/floor.bmp", "floor"); 
	RDE::RENDERER::instance->LoadTexture("../models/colourMap.bmp", "colourMap");
	RDE::RENDERER::instance->LoadTexture("../models/uvMap.bmp", "uvMap");
	RDE::RENDERER::instance->LoadTexture("../models/labyrinth/cargo_wooden_decor.bmp", "woodBox");
	RDE::RENDERER::instance->LoadTexture("../models/zombieLow/zombie_tex.bmp", "zombie");
	RDE::RENDERER::instance->LoadTexture("../models/yellow.bmp", "yellow");


	// test textures for walls 
	RDE::RENDERER::instance->LoadTexture("../models/openDungeonTileset/textures/Claimedwall2C.bmp", "stoneWall");
	// 1 edge
	RDE::RENDERER::instance->ThreadedModelLoad("../models/openDungeonTileset/obj/top.obj", "top-wall", false);
	// 2 edge
	RDE::RENDERER::instance->ThreadedModelLoad("../models/openDungeonTileset/obj/top_left.obj", "top-left-wall", false);
	RDE::RENDERER::instance->ThreadedModelLoad("../models/openDungeonTileset/obj/top_bottom.obj", "top-bottom-wall", false);
	// 3 edge
	RDE::RENDERER::instance->ThreadedModelLoad("../models/openDungeonTileset/obj/top_left_right.obj", "top-left-right-wall", false);
	// 4 edge
	RDE::RENDERER::instance->ThreadedModelLoad("../models/openDungeonTileset/obj/4_edge.obj", "4-edge-wall", false);
	// no edge
	RDE::RENDERER::instance->ThreadedModelLoad("../models/openDungeonTileset/obj/fill_tile.obj", "fill-tile", false);
	// floor
	RDE::RENDERER::instance->ThreadedModelLoad("../models/openDungeonTileset/obj/floor_tile.obj", "floor-tile", false);
	
	RDE::RENDERER::instance->ThreadedModelLoad("../models/largePlane.obj", "largePlane", false);

	// models
	RDE::RENDERER::instance->ThreadedModelLoad("../models/HenriettaMatchingOrigin-fixedMesh.dae", "henrietta", false);
	RDE::RENDERER::instance->ThreadedModelLoad("../models/pleinair_textured.obj", "pleinair", false);
	RDE::RENDERER::instance->ThreadedModelLoad("../models/labyrinth/pillar.obj", "pillar", false);
	RDE::RENDERER::instance->ThreadedModelLoad("../models/boblampclean.md5mesh", "boblamp", true);
	//RDE::RENDERER::instance->ThreadedModelLoad("../models/labyrinth/floorQuad.obj", "floorQuad", false);
	//RDE::RENDERER::instance->ThreadedModelLoad("../models/labyrinth/skinnyPillar.obj", "skinnyPillar", false);
	RDE::RENDERER::instance->ThreadedModelLoad("../models/skybox.obj", "skybox", true);

	RDE::RENDERER::instance->ThreadedModelLoad("../models/sphere.obj", "sphere", false);

	RDE::RENDERER::instance->ThreadedModelLoad("../models/zombieLow/Zombie_low.dae", "zombie", false);
	

	

	// init input to (theoretically) custom layout
	// actually, should have a different (change input layout) function. setting these to private until that happens
	//inputNamespace::input->InitKeyboardGameKeys(&inputNamespace::input->keyboardKeys);
	//inputNamespace::input->InitJoypadGameKeys(&inputNamespace::input->joypadButtons[0]);

	mainMenuGui = new RDE::GUI(windowRef);
	mainMenuGui->Init("../GUI/cegui-datafiles");
	mainMenuGui->LoadScheme("TaharezLook.scheme");
	mainMenuGui->LoadScheme("AlfiskoSkin.scheme");
	mainMenuGui->SetFont("DejaVuSans-10");
	mainMenuGui->SetMouseCursor("TaharezLook/MouseArrow");
	mainMenuGui->ShowMouseCursor();



	hierarchyDemoButton = static_cast<CEGUI::PushButton*>(mainMenuGui->CreateWidget("TaharezLook/Button", glm::vec4(0.2f, 0.15f, 0.6f, 0.075f), glm::vec4(0.0f), "HierarchyDemoButton"));
	hierarchyDemoButton->setText("Hierarchy Demo");
	hierarchyDemoButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::OnHierarchyDemoClicked, this));

	walkingDemoButton = static_cast<CEGUI::PushButton*>(mainMenuGui->CreateWidget("TaharezLook/Button", glm::vec4(0.2f, 0.25f, 0.6f, 0.075f), glm::vec4(0.0f), "WalkingDemoButton"));
	walkingDemoButton->setText("Walking Demo");
	walkingDemoButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::OnWalkingDemoClicked, this));

	shaderDemoButton = static_cast<CEGUI::PushButton*>(mainMenuGui->CreateWidget("TaharezLook/Button", glm::vec4(0.2f, 0.35f, 0.6f, 0.075f), glm::vec4(0.0f), "ShaderDemoButton"));
	shaderDemoButton->setText("Shader Demo");
	shaderDemoButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::OnShaderDemoClicked, this));

	mapgenDemoButton = static_cast<CEGUI::PushButton*>(mainMenuGui->CreateWidget("TaharezLook/Button", glm::vec4(0.2f, 0.45f, 0.6f, 0.075f), glm::vec4(0.0f), "MapgenDemoButton"));
	mapgenDemoButton->setText("Mapgen Demo");
	mapgenDemoButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::OnMapgenDemoClicked, this));

	framerateDemoButton = static_cast<CEGUI::PushButton*>(mainMenuGui->CreateWidget("TaharezLook/Button", glm::vec4(0.2f, 0.55f, 0.6f, 0.075f), glm::vec4(0.0f), "FramerateDemoButton"));
	framerateDemoButton->setText("Framerate Demo");
	framerateDemoButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::OnFrameRateDemoClicked, this));

	/*pathfindingDemoButton = static_cast<CEGUI::PushButton*>(mainMenuGui->CreateWidget("TaharezLook/Button", glm::vec4(0.2f, 0.65f, 0.6f, 0.075f), glm::vec4(0.0f), "PathfindingDemoButton"));
	pathfindingDemoButton->setText("Pathfinding Demo");
	pathfindingDemoButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::OnPathfindingDemoClicked, this));*/

	exitButton = static_cast<CEGUI::PushButton*>(mainMenuGui->CreateWidget("TaharezLook/Button", glm::vec4(0.2f, 0.65f, 0.6f, 0.075f), glm::vec4(0.0f), "ExitButton"));
	exitButton->setText("Exit Game");
	exitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::OnExitClicked, this));
}

// physics update 
void Game::FixedUpdate(float dt)
{
	if (modelLoadComplete)
	{
		if (gameStateStack.top() == GAME_STATE::walkingDemoActive)
		{
			walkingDemo->FixedUpdate(dt);
		}	
		else if (gameStateStack.top() == GAME_STATE::hierarchyDemoActive)
		{
			hierarchyDemo->FixedUpdate(dt);
		}
		else if (gameStateStack.top() == GAME_STATE::shaderDemoActive)
		{
			shaderDemo->FixedUpdate(dt);
		}
		else if (gameStateStack.top() == GAME_STATE::mapgenDemoActive)
		{
			mapgenDemo->FixedUpdate(dt);
		}
		else if (gameStateStack.top() == GAME_STATE::frameRateDemoActive)
		{
			framerateDemo->FixedUpdate(dt);
		}
		/*else if (gameStateStack.top() == GAME_STATE::pathfindingDemoActive)
		{
			pathfindingDemo->FixedUpdate(dt);
		}*/
	}
}

bool Game::Update(float dt)
{
	if (modelLoadComplete)
	{
		if (gameStateStack.top() == GAME_STATE::walkingDemoActive)
		{
			if (!walkingDemo->Update(dt))
			{
				//delete demoGameOne;
				gameStateStack.pop();
				ShowButtons();
			}
		}
		else if (gameStateStack.top() == GAME_STATE::hierarchyDemoActive)
		{
			if (!hierarchyDemo->Update(dt))
			{
				gameStateStack.pop();
				ShowButtons();
			}
		}
		else if (gameStateStack.top() == GAME_STATE::shaderDemoActive)
		{
			if (!shaderDemo->Update(dt))
			{
				gameStateStack.pop();
				ShowButtons();
			}
		}
		else if (gameStateStack.top() == GAME_STATE::mapgenDemoActive)
		{
			if (!mapgenDemo->Update(dt))
			{
				gameStateStack.pop();
				ShowButtons();
			}
		}
		else if (gameStateStack.top() == GAME_STATE::frameRateDemoActive)
		{
			if (!framerateDemo->Update(dt))
			{
				gameStateStack.pop();
				ShowButtons();
			}
		}
		/*else if (gameStateStack.top() == GAME_STATE::pathfindingDemoActive)
		{
			if (!pathfindingDemo->Update(dt))
			{
				gameStateStack.pop();
				ShowButtons();
			}
		}*/
		else if (gameStateStack.top() == GAME_STATE::mainMenuActive)
		{
			mainMenuGui->Update(dt);
		}
	}
	return continueRunning;
}

void Game::Render(float interpolation)
{
	if (!RDE::RENDERER::instance->IsThreadedLoadComplete())
	{
		
	}
	else if (!modelLoadComplete)
	{
		// should have a bind all thing, doing this is really annoying
		RDE::RENDERER::instance->BindModel("henrietta");
		RDE::RENDERER::instance->BindModel("boblamp");
		//RDE::RENDERER::instance->BindModel("floorQuad");
		//RDE::RENDERER::instance->BindModel("skinnyPillar");
		//RDE::RENDERER::instance->BindModel("pillar");

		// really make a bind all, thisis getting obnoxious
		RDE::RENDERER::instance->BindModel("top-left-wall");
		RDE::RENDERER::instance->BindModel("fill-tile");
		RDE::RENDERER::instance->BindModel("top-wall");
		RDE::RENDERER::instance->BindModel("floor-tile");
		RDE::RENDERER::instance->BindModel("top-bottom-wall");
		RDE::RENDERER::instance->BindModel("top-left-right-wall");
		RDE::RENDERER::instance->BindModel("4-edge-wall");
		RDE::RENDERER::instance->BindModel("skybox");
		RDE::RENDERER::instance->BindModel("sphere");
		RDE::RENDERER::instance->BindModel("largePlane");
		RDE::RENDERER::instance->BindModel("zombie");

		modelLoadComplete = true;

		// can't do this until all models are loaded
		walkingDemo = new WalkingDemo(aspectRatio);
		hierarchyDemo = new HierarchyDemo(aspectRatio);
		shaderDemo = new ShaderDemo(aspectRatio, mainMenuGui);
		mapgenDemo = new MapgenDemo(aspectRatio);
		framerateDemo = new FramerateDemo(aspectRatio, mainMenuGui);
		//pathfindingDemo = new PathfindingDemo(aspectRatio);
	}


	if (modelLoadComplete)
	{
		if (gameStateStack.top() == GAME_STATE::walkingDemoActive)
		{
			walkingDemo->Render(interpolation);
		}
		else if (gameStateStack.top() == GAME_STATE::hierarchyDemoActive)
		{
			hierarchyDemo->Render(interpolation);
		}
		else if (gameStateStack.top() == GAME_STATE::shaderDemoActive)
		{
			shaderDemo->Render(interpolation);
		}
		else if (gameStateStack.top() == GAME_STATE::mapgenDemoActive)
		{
			mapgenDemo->Render(interpolation);
		}
		else if (gameStateStack.top() == GAME_STATE::frameRateDemoActive)
		{
			framerateDemo->Render(interpolation);
		}
		/*else if (gameStateStack.top() == GAME_STATE::pathfindingDemoActive)
		{
			pathfindingDemo->Render(interpolation);
		}*/
	}
	if (gameStateStack.top() == GAME_STATE::mainMenuActive)
	{
		mainMenuGui->Draw();
	}
}

bool Game::OnExitClicked(const CEGUI::EventArgs & e)
{
	continueRunning = false;
	return true;
}

void Game::OnWalkingDemoClicked(const CEGUI::EventArgs & e)
{
	//demoGameOne = new DemoGameOne(aspectRatio);
	gameStateStack.push(GAME_STATE::walkingDemoActive);
	HideButtons();
}

void Game::OnHierarchyDemoClicked(const CEGUI::EventArgs & e)
{
	gameStateStack.push(GAME_STATE::hierarchyDemoActive);
	HideButtons();
}

void Game::OnShaderDemoClicked(const CEGUI::EventArgs & e)
{
	gameStateStack.push(GAME_STATE::shaderDemoActive);
	HideButtons();
	shaderDemo->Activate();
}

void Game::OnFrameRateDemoClicked(const CEGUI::EventArgs & e)
{
	gameStateStack.push(GAME_STATE::frameRateDemoActive);
	HideButtons();
}

void Game::OnMapgenDemoClicked(const CEGUI::EventArgs & e)
{
	gameStateStack.push(GAME_STATE::mapgenDemoActive);
	HideButtons();
}

/*void Game::OnPathfindingDemoClicked(const CEGUI::EventArgs & e)
{
	gameStateStack.push(GAME_STATE::pathfindingDemoActive);
	HideButtons();
}*/

void Game::HideButtons()
{
	hierarchyDemoButton->hide();
	walkingDemoButton->hide();
	shaderDemoButton->hide();
	mapgenDemoButton->hide();
	framerateDemoButton->hide();
	//pathfindingDemoButton->hide();
	exitButton->hide();
}

void Game::ShowButtons()
{
	hierarchyDemoButton->show();
	walkingDemoButton->show();
	shaderDemoButton->show();
	mapgenDemoButton->show();
	framerateDemoButton->show();
	//pathfindingDemoButton->show();
	exitButton->show();

	hierarchyDemoButton->enable();
	walkingDemoButton->enable();
	shaderDemoButton->enable();
	mapgenDemoButton->enable();
	framerateDemoButton->enable();
	//pathfindingDemoButton->enable();
	exitButton->enable();
}