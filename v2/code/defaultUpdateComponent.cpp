#include "defaultUpdateComponent.h"
#include "transform.h"
#include "debugIO.h"
#include "gameObject.h"
#include "animManager.h"
#include "input.h"

DefaultUpdateComponent::DefaultUpdateComponent()
{
	
}

DefaultUpdateComponent::~DefaultUpdateComponent()
{
}

void DefaultUpdateComponent::Init(RDE::GameObject & gameObject)
{
	gameObject.animManager->SetActiveAnimation(0, true);
}

void DefaultUpdateComponent::Update(RDE::GameObject & gameObject, float dt)
{
	if (gameObject.IsRoot())
		return;

	gameObject.animManager->Update(dt);
}


