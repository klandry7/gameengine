#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <vector>
#include <glm\glm.hpp>
#include "boundingBox.h"

#include <memory>
namespace RDE
{
	class Shader;
	class RenderComponent;
	class PhysicsComponent;
	class UpdateComponent;
	class Transform;
	class AnimManager;
	class SphericalTransform;

	namespace MAPGEN
	{
		struct Map;
	}

}

// go back and implement dirty flags for improved render hierarchy performance (see design book)
// animation is very tricky

namespace RDE
{
	class GameObject
	{
	public:
		// ctor
		GameObject(RDE::GameObject *_parent, RDE::RenderComponent *r, RDE::UpdateComponent *u, RDE::PhysicsComponent * p, const std::shared_ptr<const RDE::Shader> * s);
		// dtor
		~GameObject();
		// render function
		void Render(glm::mat4 parentTransform, float interpolation);
		// update function
		void Update(float dt);
		// physics update function
		void PhysicsUpdate(float dt, const RDE::MAPGEN::Map *map);
		// enable/disable update
		void EnableUpdate(bool state);
		// is update enabled?
		bool UpdateEnabled();
		// enable/disable object rendering
		void EnableRender(bool state);
		// is rendering enabled?
		bool RenderEnabled();

		// parent/child hierarchy functions
		// is object root? (is parent null)
		bool IsRoot();
		// get children count
		unsigned int GetChildrenCount();
		// get child at index
		RDE::GameObject * GetChild(int index);
		// get model bounding box
		std::vector<bounding_box_struct> GetBoundingBox();
		// get hierarchial world position
		glm::vec3 GetHierarchialTranslation();
		// get hierarchial previous world position
		glm::vec3 GetHierarchialPrevPos();

	private:
		// parent/child hierarchy functions
		// add child gameobject
		void AddChild(RDE::GameObject *c);
		// set parent gameobject 
		void SetParent(RDE::GameObject *c);

	public:
		// object's transform
		RDE::Transform * transform;
		// object's spherical transform
		RDE::SphericalTransform *sphericalTransform;
		// object's velocity
		glm::vec3 velocity;
		// object's previous position
		glm::vec3 prevPos;
		// object's animation manager
		RDE::AnimManager *animManager;
		// object's shader ref
		const std::shared_ptr<const RDE::Shader> defaultShaderReference;


	private:
		// render component
		RDE::RenderComponent *renderComponent;
		// update component
		RDE::UpdateComponent *updateComponent;
		// physics component
		RDE::PhysicsComponent * physicsComponent;
		// parent object
		RDE::GameObject *parent;
		// children objects
		std::vector<RDE::GameObject *> children;
		// render?
		bool renderState;
		// update?
		bool updateState;
		//unsigned int id; // ? maybe have a global class with just "get id" that returns a unique id for every gameobject.
	};
}

#endif // !GAMEOBJECT_H