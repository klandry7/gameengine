#ifndef PLAYER_UPDATECOMPONENT_H
#define PLAYER_UPDATECOMPONENT_H

#include "updateComponent.h"
namespace RDE
{
	class GameObject;
	//class SphericalTransform;
}

class PlayerUpdateComponent : public RDE::UpdateComponent
{
public:
	PlayerUpdateComponent(float aspectRatio);
	~PlayerUpdateComponent();

	virtual void Update(RDE::GameObject & gameObject, float dt);
	virtual void Init(RDE::GameObject & gameObject);
private:
	//RDE::Camera *camera;
	//RDE::SphericalTransform *camRotation;
	float camRotVel = 4.0f;
};

#endif 
