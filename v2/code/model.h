#ifndef MODEL_H
#define MODEL_H

#include "common.h"

#include <GL/glew.h>
#include <vector>

// Include GLM
#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>

// these are all assimp files
#include <assimp/Importer.hpp>  // C++ importer interface
#include <assimp/scene.h>       // Output data structure
#include <assimp/postprocess.h> // Post processing flags

#include <map>

#include "boundingBox.h"

#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))
//#define ZERO_MEM(a) memset(a, 0, sizeof(a))
#define NUM_BONES_PER_VEREX 8

namespace RDE
{
	struct aiNode_struct
	{
		//The child nodes of this node.
		aiNode_struct ** mChildren;
		//The meshes of this node.
		unsigned int * mMeshes;
		//The name of the node.
		std::string mName;
		//The number of child nodes of this node.
		unsigned int mNumChildren;
		//The number of meshes of this node.
		unsigned int mNumMeshes;
		//Parent node.
		aiNode_struct * mParent;
		//The transformation relative to the node's parent. 
		aiMatrix4x4 mTransformation;
	};


	class Model
	{
		struct bone_info_struct
		{
			// bone offset ma4
			aiMatrix4x4 BoneOffset;
			// bone final transformation mat4
			aiMatrix4x4 FinalTransformation;
		};

		struct vertex_bone_data_struct
		{
			// bone IDs (names given in modelling program)
			unsigned int IDs[NUM_BONES_PER_VEREX];
			// bone weights
			float Weights[NUM_BONES_PER_VEREX];

			// struct ctor
			vertex_bone_data_struct()
			{
				Reset();
			};

			// reset struct to default. reset all IDs and weights.
			void Reset()
			{
				//ZERO_MEM(IDs);
				//ZERO_MEM(Weights);
				for (unsigned int i = 0; i < ARRAY_COUNT(IDs); i++)
				{
					IDs[i] = 0;
					assert(IDs[i] == 0);
				}
				for (unsigned int i = 0; i < ARRAY_COUNT(Weights); i++)
				{
					Weights[i] = 0.0;
					assert(Weights[i] == 0.0);
				}
			}

			// add bone weight/ID to lists
			void AddBoneData(unsigned int BoneID, float Weight)
			{
				for (unsigned int i = 0; i < ARRAY_SIZE_IN_ELEMENTS(IDs); i++)
				{
					if (Weights[i] == 0.0)
					{
						IDs[i] = BoneID;
						Weights[i] = Weight;
						return;
					}
				}
				// should never get here - more bones than we have space for
				assert(0);
			}
		};

		struct animation_struct
		{
			//The node animation channels.
			std::map<std::string, aiNodeAnim *> aiNodeAnimMap;
			//Duration of the animation in ticks.
			double mDuration;
			//The mesh animation channels.
			aiMeshAnim ** mMeshChannels;
			//The name of the animation.
			aiString mName;
			//The number of bone animation channels.
			unsigned int mNumChannels;
			//The number of mesh animation channels.
			unsigned int mNumMeshChannels;
			//Ticks per second.
			double mTicksPerSecond;
		};

	public:
		// ctor
		Model();
		// dtor
		~Model();

		// get model data into memory. No OpenGL calls.
		bool LoadData(const char * filename, bool flipUVs);
		// bind model data. OpenGL calls.
		void BindData();
		// unused.
		bool AddBuffer(const char * name, int shaderLayoutLocation);
		// draw model using indexed render 
		void DrawIndexed();
		// draw model using instanced render
		void DrawInstanced(unsigned int modelCount, std::vector<glm::mat4> transformationMatrixList);
		// get bounding box for model
		std::vector<bounding_box_struct> GetBoundingBoxList();

	private:
		// bind GL buffers
		void BindDataToBuffers();
		// create GL buffers
		void CreateBuffers();
		// Load model data
		bool LoadModelData(const char * filename, bool flipUVs);
		// initialize model BBox
		void InitboundingBox(glm::vec3 maxPt, glm::vec3 minPt, bounding_box_struct * source);

	private:
		// internal vertex list
		std::vector<glm::vec3> total_vertices;
		// internal texCoord list
		std::vector<glm::vec2> total_texCoords;
		// internal normal list
		std::vector<glm::vec3> total_normals;
		// internal indices list
		std::vector<GLushort> total_indices;
		// vertex array object 
		GLuint indexedVAO;
		// vertex data 
		GLuint vertexBufferObject;
		// texture coordinates
		GLuint texCoordBufferObject;
		// vertex normal
		GLuint vertexNormalBufferObject;
		// index data 
		GLuint indexBufferObject;
		// instanced matrices 
		GLuint instancedMatricesBufferObject;
		// model vert count
		GLshort vertexCount;

		// assimp variables
		// need to convert / get rid of 

		// the global inverse transform of the scene
		aiMatrix4x4 m_GlobalInverseTransform;
		// GLM global inverse transform of scene
		glm::mat4 m_GlobalInverseTransformGLM;

		// bones vector
		std::vector<vertex_bone_data_struct> Bones;
		// bone ID GL layout location
		GLuint BONE_ID_LOCATION;
		// bone weight GL layout location
		GLuint BONE_WEIGHT_LOCATION;
		// maps a bone name to its index based on name 
		std::map<std::string, uint32> m_BoneMapping; 
		// current number of bones in model
		uint32 m_NumBones = 0;
		// bone info vector (offset/final transform)
		std::vector<bone_info_struct> m_BoneInfo;
		// max number of bones allowed in model
		static const unsigned int MAX_BONES = 100;
		// animation array
		animation_struct *animArray;
		// animation array size
		unsigned int animArraySize;
		// scene root node
		aiNode_struct * sceneRootNode;
		// model scene
		const aiScene* pScene;
		// assimp importer instance
		Assimp::Importer Importer;

		//bounding_box_struct boundingBox;
		// there should be at least 1 here. if not, the model loading failed
		// first model is the entire size of the model in default pose. 
		// all others are bone bboxes
		// original bbox list
		std::vector<RDE::bounding_box_struct> originalBoundingBoxList;
		// modified bbox list
		std::vector<RDE::bounding_box_struct> transformedBoundingBoxList;

	public:
		// returns anim count
		unsigned int GetAnimCount();
		// returns anim duration
		float GetAnimDuration(unsigned int index);
		//returns bone transform at point in time
		glm::mat4 BoneTransform(float timeInSeconds, std::vector<glm::mat4>& transforms, unsigned int animIndex);
		// returns inverse transform
		glm::mat4 GetInverseTransform();
	private:
		// load and prepare bone data
		void LoadBones(const aiMesh * pMesh, int extraOffset);
		// read node hierarchy to get anim information
		void ReadNodeHeirarchy(float AnimationTime, const aiNode_struct * pNode, const aiMatrix4x4 & ParentTransform, unsigned int animIndex);
		// convert to assimp format to RDE wrapper format
		void CopyNodeHierarchy(const aiNode * originalNode, aiNode_struct * copyNode);

		// calc interpolated scaling
		void CalcInterpolatedScaling(aiVector3D & Out, float AnimationTime, const aiNodeAnim * pNodeAnim);
		// calc interpolated position
		void CalcInterpolatedPosition(aiVector3D & Out, float AnimationTime, const aiNodeAnim * pNodeAnim);
		// calc interpolated rotation
		void CalcInterpolatedRotation(aiQuaternion & Out, float AnimationTime, const aiNodeAnim * pNodeAnim);

		// find rotation at pt in time
		unsigned int FindRotation(float AnimationTime, const aiNodeAnim * pNodeAnim);
		// find position at pt in time
		unsigned int FindPosition(float AnimationTime, const aiNodeAnim * pNodeAnim);
		// find scaling at pt in time
		unsigned int FindScaling(float AnimationTime, const aiNodeAnim * pNodeAnim);
		// convert assimp mat4 to glm mat4 
		glm::mat4 AiMat4ToGlmMat4(aiMatrix4x4 assimpMatrix);
	};
}

#endif // !MODEL_H
