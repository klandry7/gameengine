#ifndef ANIMMANAGER_H
#define ANIMMANAGER_H

// transform
// model index
// texture index 
// list of animations (animation indexes, really)
// play animation 
// is animation playing 
// animation time

namespace RDE
{
	class AnimManager
	{
		typedef struct
		{
			// animation duration
			float duration;
			// repeat the animation?
			bool repeating;
		}anim_struct;

	public:
		// ctor
		AnimManager();
		// dtor
		~AnimManager();
		// perform initialization. set name and anim count.
		void Init(const char * name, unsigned int _animCount);
		// update the animation
		void Update(float dt);
		// set the current active animation
		void SetActiveAnimation(unsigned int newActive, bool loopingState);
		// is the animation complete (if it's single fire)
		bool ActiveAnimComplete();
		// set anim to loop or not
		void SetLooping(bool state);
		// get current anim
		unsigned int CurrentActiveAnim();
		// get current anim time
		float CurrentActiveAnimTime();
		// get anim count
		int GetAnimCount();
		
	private:
		// model name
		const char * modelName;
		// anim list
		anim_struct * anims;
		// anim count
		unsigned int animCount;
		// current active anim
		int currentActiveAnim;
		// current anim time
		float animationTime;
	};
}

#endif