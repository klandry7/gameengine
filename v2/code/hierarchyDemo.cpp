#include "hierarchyDemo.h"
#include "gameObject.h"
#include "transform.h"
#include <glm\glm.hpp>

#include "defaultPhysicsComponent.h"
#include "defaultRenderComponent.h"
#include "defaultUpdateComponent.h"

#include "camera.h"
#include "Shader.h"

#include "hierarchyTraversal.h"

#include "input.h"

#include "renderer.h"

#include <math.h>

HierarchyDemo::HierarchyDemo(float aspectRatio)
{
	camera = new RDE::Camera(aspectRatio);
	camera->SetPos(glm::vec3(0.0f, 7.0f, 14.0f));
	camera->SetLookatPt(glm::vec3(0, 5, 0));
	camera->SetZFar(1000.0f);

	simpleShader = std::make_shared<RDE::Shader>(RDE::Shader("hierarchy demo passthrough", "../shaders/simpleVert.vert", "../shaders/simpleFrag.frag"));

	root = new RDE::GameObject(nullptr, new DefaultRenderComponent("", ""), new DefaultUpdateComponent(), new DefaultPhysicsComponent(0, 0, 0), &simpleShader);
	spinningRoot = new RDE::GameObject(root, new DefaultRenderComponent("", ""), new DefaultUpdateComponent(), new DefaultPhysicsComponent(0, 0, 0), &simpleShader);

	//plane = new RDE::GameObject(root, new DefaultRenderComponent("largePlane", "uvMap"), new DefaultUpdateComponent(), new DefaultPhysicsComponent(0, 0, 0), &simpleShader);

	boblampRoot = new RDE::GameObject(spinningRoot, new DefaultRenderComponent("boblamp", "colourMap"), new DefaultUpdateComponent(), new DefaultPhysicsComponent(0, 0, 0), &simpleShader);
	boblampRoot->transform->scale *= 0.1;
	//boblampRoot->transform->trvanslation = glm::vec3(5.0f, 5.0f, 0.0f);
	//boblampRoot->prevPos = boblampRoot->transform->translation;

	for (int i = 0; i < 4; i++)
	{
		sphereList.push_back(new RDE::GameObject(boblampRoot, new DefaultRenderComponent("sphere", "colourMap"), new DefaultUpdateComponent(), new DefaultPhysicsComponent(0, 0, 0), &simpleShader));
		sphereList.at(i)->transform->scale *= 15.0f;
		glm::vec3 mod(0.0f, 30.0f, 0.0f);
		float amount = 40.0f;
		originalY = 30;
		if (i == 0)
		{
			mod.x = amount;
			mod.z = amount;
		}
		else if (i == 1)
		{
			mod.x = -amount;
			mod.z = amount;
		}
		else if (i == 2)
		{
			mod.x = amount;
			mod.z = -amount;
		}
		else if (i == 3)
		{
			mod.x = -amount;
			mod.z = -amount;
		}
		sphereList.at(i)->transform->translation = mod;
		sphereList.at(i)->prevPos = mod;
	}
	

}

HierarchyDemo::~HierarchyDemo()
{
}

void HierarchyDemo::FixedUpdate(float dt)
{
	
	sineCounter += dt * 10.0f;
	for (int i = 0; i < 4; i++)
	{
		sphereList.at(i)->transform->translation.y = originalY + (sinf(sineCounter + (float(i) ) ));
		sphereList.at(i)->prevPos = sphereList.at(i)->transform->translation;
	}

	spinningRoot->transform->rotation.y += dt * 0.5f;
}

bool HierarchyDemo::Update(float dt)
{
	

	if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::start, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::pressed))
	{
		return false;
	}
	else
	{
		RDE::HIERARCHY::UpdateHierarchyTraversal(spinningRoot, dt);
		return true;
	}
}

void HierarchyDemo::Render(float interpolation)
{
	//printf("root pos: <%f %f %f>\n", root->transform->translation.x, root->transform->translation.y, root->transform->translation.z);

	simpleShader->Bind();
	simpleShader->SetUniformMatrix4fv("projectionViewMatrix", false, camera->GetProjViewMatrix());

	RDE::HIERARCHY::RenderHierarchyTraversal(root, camera, interpolation);

}
