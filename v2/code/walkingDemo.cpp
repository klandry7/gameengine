#include "walkingDemo.h"

#include <glm/gtc/matrix_transform.hpp>

#include "transform.h"
#include "input.h"
#include "renderer.h"
#include "defaultRenderComponent.h"
#include "defaultUpdateComponent.h"
#include "playerUpdateComponent.h"
#include "playerRenderComponent.h"
#include "defaultPhysicsComponent.h"
#include "debugIO.h"
#include "hierarchyTraversal.h"
#include "transform.h"
#include "shader.h"
#include "camera.h"
#include "sphericalTransform.h"
#include "gameObject.h"


#include <queue>

#define RAD90 3.14f / 2.0f
#define RAD180 3.14f

WalkingDemo::WalkingDemo(float aspectRatio)
{
	mapTileSize = 2.5f;

	camera = new RDE::Camera(aspectRatio);
	camera->SetLookatPt(glm::vec3(0, 0, -1));
	camera->SetZFar(1000.0f);

	simpleShader = std::make_shared<RDE::Shader>(RDE::Shader("passthrough", "../shaders/simpleVert.vert", "../shaders/simpleFrag.frag"));

	rootNode = new RDE::GameObject(nullptr, new DefaultRenderComponent("", ""), new DefaultUpdateComponent(), new DefaultPhysicsComponent(0,0, mapTileSize), &simpleShader);

	//boblamp = new RDE::GameObject(rootNode, new DefaultRenderComponent("boblamp", "boblamp"), new DefaultUpdateComponent(), new DefaultPhysicsComponent(0, 0, mapTileSize), &simpleShader);
	//boblamp->transform->translation += glm::vec3(-15, 0, 0);
	//boblamp->transform->scale *= 0.2f;
	//boblamp->prevPos = boblamp->transform->translation;

	// just keeping this in to make sure delete doesn't break.
	henrietta = new RDE::GameObject(rootNode	, new DefaultRenderComponent("henrietta", "henrietta"), new DefaultUpdateComponent(), new DefaultPhysicsComponent(0,0, mapTileSize), &simpleShader);
	delete henrietta;


	player = new RDE::GameObject(rootNode, new DefaultRenderComponent("henrietta", "henrietta"), new PlayerUpdateComponent(aspectRatio), new DefaultPhysicsComponent(2.0f, 2.0f, mapTileSize), &simpleShader);
	player->transform->scale *= 0.4f;


	RollNewMap(RDE::MAPGEN::LAYOUT_TYPES::cave);

	bool breakout = false;
	for (int y = 0; y < currentMap.height; y++)
	{
		for (int x = 0; x < currentMap.width; x++)
		{
			if (currentMap.dungeon[y][x] == RDE::MAPGEN::TILE_TYPE::Floor)
			{
				glm::vec3 translation = glm::vec3(
					(float(x) * mapTileSize) + (mapTileSize / 2.0f) + 0.2f, 
					0.0f, 
					(float(y) * mapTileSize) + (mapTileSize / 2.0f) + 0.2f);

				player->transform->translation = translation;
				player->prevPos = translation;

				break;
			}
		}
		if (breakout)
			break;
	}

	startingPos = player->transform->translation;

	playerCamGameObject = new RDE::GameObject(player, new DefaultRenderComponent("", ""), new DefaultUpdateComponent(), new DefaultPhysicsComponent(0,0,mapTileSize), &simpleShader);
	playerCamGameObject->transform->translation += glm::vec3(0.0f, 12.0f, -1.0f);
	playerCamGameObject->prevPos = playerCamGameObject->transform->translation;

	//freeCam = new RDE::GameObject(rootNode, new DefaultRenderComponent(&simpleShader, "", ""), new DefaultUpdateComponent(), new DefaultPhysicsComponent());
}

WalkingDemo::~WalkingDemo()
{
	// delete everything bruh
}

void WalkingDemo::FixedUpdate(float dt)
{
	player->PhysicsUpdate(dt, &currentMap);
}


struct a_star_tile
{
	int x;
	int y;
	int h;
	int g;
};

bool WalkingDemo::Update(float dt)
{
	if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::action4, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::released))
	{
		freeCamState = !freeCamState;
		if (freeCamState)
		{
			player->transform->translation.y = -12.0f;
			//playerCamGameObject->transform->translation.y = 0.0f;
			player->sphericalTransform->SetRotation(0.0f, 0.0f);
		}
		else
		{
			player->transform->translation.y = 0.0f;
			playerCamGameObject->transform->translation.y = 12.0f;
			player->sphericalTransform->SetRotation(0.0f, 0.0f);

		}
		//printf("%i\n", freeCamState);
	}



	if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::action1, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::released))
	{
		DeleteCurrentMap();
		RollNewMap(RDE::MAPGEN::LAYOUT_TYPES::basic);
	}
	else if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::action2, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::released))
	{
		DeleteCurrentMap();
		RollNewMap(RDE::MAPGEN::LAYOUT_TYPES::cave);
	}

	RDE::HIERARCHY::UpdateHierarchyTraversal(rootNode, dt);

	// do a star here ok
	glm::ivec2 startTile = RDE::MAPGEN::GetTilePosFromWorldPos2D(glm::vec2(startingPos.x, startingPos.y), mapTileSize, mapTileSize);

	//std::priority_queue<a_star_tile> frontier;
	//std::map<a_star_tile, a_star_tile> cameFrom;
	//std::map<a_star_tile, int> costSoFar;
	//frontier.push(a_star_tile{ startTile.x, startTile.y });
	// hit escape to quit
	if (RDE::INPUT::instance->GetButtonState(RDE::INPUT::GAME_BUTTONS::start, RDE::INPUT::INPUT_TYPES::keyboard, RDE::INPUT::BUTTON_STATE::pressed))
	{
		return false;
	}
	else
	{
		return true;
	}
}

void WalkingDemo::Render(float interpolation)
{
	// this absolutely sucks. camera sucks. I don't want to be manually interpolating, but here we are. ffs.
	glm::vec3 interpolatedTransform = player->transform->translation + playerCamGameObject->transform->translation;
	interpolatedTransform = ((interpolatedTransform) * interpolation) + ((player->prevPos + playerCamGameObject->transform->translation) * (1.0f - interpolation));
	// this sucks. should have a "get hierarchial position" as well as "local position"
	camera->SetPos(interpolatedTransform);
	if(freeCamState)
		camera->SetLookatPt(interpolatedTransform + player->sphericalTransform->GetForward());
	else 
		camera->SetLookatPt(interpolatedTransform + glm::vec3(0.0f, -4.0f, 0.01f));

	//camera->SetLookatPt(player->transform->translation);
	camera->SetUp(player->sphericalTransform->GetUp());


	// remember to bind shader before you can set any uniforms
	simpleShader->Bind();
	simpleShader->SetUniformMatrix4fv("projectionViewMatrix", false, camera->GetProjViewMatrix());
	simpleShader->SetUniform1i("instanced", false);
	simpleShader->SetUniform1i("animated", false);
	RDE::HIERARCHY::RenderHierarchyTraversal(rootNode, camera, interpolation);

	simpleShader->SetUniform1i("animated", false);

	// for the skybox, need to switch to cw to render inside 
	// this should absolutely not be done here. need a thing in windowapp to do this
	// renderer could even have it's own shader and it handles skybox entirely?
	glFrontFace(GL_CW);
	glm::mat4 transfrm = glm::translate(glm::mat4(1.0f), glm::vec3(currentMap.width / 2.0f, 0, currentMap.height / 2.0f) * mapTileSize) * glm::scale(glm::mat4(1.0f), glm::vec3((float)(currentMap.width)) * mapTileSize);
	simpleShader->SetUniformMatrix4fv("transformMatrix", false, transfrm);
	RDE::RENDERER::instance->DrawModelIndexed("skybox", "skybox");
	simpleShader->SetUniformMatrix4fv("transformMatrix", false, glm::mat4(1.0f));
	glFrontFace(GL_CCW);

	simpleShader->SetUniform1i("instanced", true);

	RDE::RENDERER::instance->DrawModelInstanced("floor-tile", "floor", roomFloorList.size(), roomFloorList);
	// 0
	RDE::RENDERER::instance->DrawModelInstanced("fill-tile", "stoneWall", tiles_noEdge.size(), tiles_noEdge);
	// 1
	RDE::RENDERER::instance->DrawModelInstanced("top-wall", "stoneWall", tiles_1edge.size(), tiles_1edge);
	// 2
	RDE::RENDERER::instance->DrawModelInstanced("top-left-wall", "stoneWall", tiles_2edgeAdjacent.size(), tiles_2edgeAdjacent);
	RDE::RENDERER::instance->DrawModelInstanced("top-bottom-wall", "stoneWall", tiles_2edgeOpposite.size(), tiles_2edgeOpposite);
	// 3
	RDE::RENDERER::instance->DrawModelInstanced("top-left-right-wall", "stoneWall", tiles_3edge.size(), tiles_3edge);
	// 4
	RDE::RENDERER::instance->DrawModelInstanced("4-edge-wall", "stoneWall", tiles_4edge.size(), tiles_4edge);


	simpleShader->SetUniform1i("instanced", false);
	simpleShader->SetUniformMatrix4fv("transformMatrix", false, glm::mat4(1.0f));
	RDE::RENDERER::instance->DrawModelIndexed("sphere", "henrietta");

	//simpleShader->SetUniformMatrix4fv("transformMatrix", false, glm::translate(glm::mat4(1.0f), player->transform->translation + glm::vec3(1.0f, 0.0f, 1.0f)));
	//RDE::RENDERER::instance->DrawModelIndexed("skybox", "henrietta");
}


void WalkingDemo::RollNewMap(RDE::MAPGEN::LAYOUT_TYPES layoutType)
{
	RDE::MAPGEN::Cell * cardinal = new RDE::MAPGEN::Cell[4];
	cardinal[0] = { 0,  1 };
	cardinal[1] = { 0, -1 };
	cardinal[2] = { 1,  0 };
	cardinal[3] = { -1, 0 };

	RDE::MapGenerator * mapgen = new RDE::MapGenerator();
	currentMap = mapgen->NewMap(61, 61, layoutType);

	for (int y = 0; y < currentMap.height; y++)
	{
		for (int x = 0; x < currentMap.width; x++)
		{
			if (currentMap.regions[y][x] == -1)
			{
				MapTileSelect(x, y);
			}
			else
			{
				roomFloorList.push_back(glm::translate(glm::mat4(1.0f), glm::vec3((float(x) * mapTileSize) + (mapTileSize/2.0f), -1.5f, (float(y) * mapTileSize) + (mapTileSize / 2.0f))));
			}
		}
	}

	delete mapgen;
	delete cardinal;

	bool breakout = false;
	for (int y = 1; y < currentMap.height - 1; y++)
	{
		for (int x = 1; x < currentMap.width - 1; x++)
		{
			if (currentMap.dungeon[y][x] == RDE::MAPGEN::TILE_TYPE::Floor)
			{
				glm::vec3 translation = glm::vec3(
					(float(x) * mapTileSize) + (mapTileSize / 2.0f) + 0.2f,
					0.0f,
					(float(y) * mapTileSize) + (mapTileSize / 2.0f) + 0.2f);

				player->transform->translation.x = translation.x;
				player->transform->translation.z = translation.z;
				player->prevPos = translation;

				break;
			}
		}
		if (breakout)
			break;
	}
}

void WalkingDemo::DeleteCurrentMap()
{
	// clear out the old map
	// if regions isn't null, dungeon should also not be null.
	if (currentMap.regions != NULL)
	{
		for (int y = 0; y < currentMap.height; y++)
		{
			delete[] currentMap.regions[y];
			delete[] currentMap.dungeon[y];
		}
		delete[] currentMap.regions;
		delete[] currentMap.dungeon;
	}

	roomFloorList.clear();
	tiles_noEdge.clear();
	tiles_1edge.clear();
	tiles_2edgeAdjacent.clear();
	tiles_2edgeOpposite.clear();
	tiles_3edge.clear();
	tiles_4edge.clear();
}

// a first person map where inner space doesn't matter will be different
// from a top down map where all space matters
// a first person map can be build entirely using 1 wall rotated 
// a top down tile based map will need 
// 0 edge (1)
// 1 edge (1)
// 2 edge (2)
// 3 edge (1)
// 4 edge (1)
void WalkingDemo::MapTileSelect(int x, int y)
{
	RDE::MAPGEN::Cell * cardinal = new RDE::MAPGEN::Cell[4];
	cardinal[0] = { 0,  1 };
	cardinal[1] = { 0, -1 };
	cardinal[2] = { 1,  0 };
	cardinal[3] = { -1, 0 };

	glm::mat4 translate = glm::translate(glm::mat4(1.0f), glm::vec3((float(x) * mapTileSize) + (mapTileSize / 2.0f), -1.5f, (float(y) * mapTileSize) + (mapTileSize / 2.0f)));
	glm::vec3 rotationAxis = glm::vec3(0.0f, 1.0f, 0.0f);

	// changed to unsigned int (4 bits) and bitmask
	bool top = false, bottom = false, left = false, right = false;

	for (int i = 0; i < 4; i++)
	{
		int yInc = y + cardinal[i].y;
		int xInc = x + cardinal[i].x;
		if (yInc >= 0 && yInc < currentMap.height && xInc >= 0 && xInc < currentMap.width &&
			currentMap.dungeon[yInc][xInc] != RDE::MAPGEN::TILE_TYPE::Wall)
		{
			switch (i)
			{
			case 0:
				top = true;
				break;
			case 1:
				bottom = true;
				break;
			case 2:
				left = true;
				break;
			case 3:
				right = true;
				break;
			default:
				break;
			}
		}
	}

	// with a bitmask this could probably be a case since each is unique

	// 4
	if (top && bottom && left && right) {
		tiles_4edge.push_back(translate);
	}
	// 3
	else if (bottom && left && right) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), RAD180, rotationAxis);
		tiles_3edge.push_back((translate * rotate));
	}
	else if (top && left && right) {
		tiles_3edge.push_back(translate);
	}
	else if (top && bottom && left) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), RAD90, rotationAxis);
		tiles_3edge.push_back((translate * rotate));
	}
	else if (top && bottom && right) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), -RAD90, rotationAxis);
		tiles_3edge.push_back((translate * rotate));
	}
	// 2
	else if (top && left) {
		tiles_2edgeAdjacent.push_back(translate);
	}
	else if (top && right) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), -RAD90, rotationAxis);
		tiles_2edgeAdjacent.push_back((translate * rotate));
	}
	else if (bottom && left) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), RAD90, rotationAxis);
		tiles_2edgeAdjacent.push_back((translate * rotate));
	}
	else if (bottom && right) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), RAD180, rotationAxis);
		tiles_2edgeAdjacent.push_back((translate * rotate));
	}
	else if (top && bottom) {
		tiles_2edgeOpposite.push_back(translate);
	}
	else if (left && right) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), RAD90, rotationAxis);
		tiles_2edgeOpposite.push_back((translate * rotate));
	}
	// 1
	else if (top) {
		tiles_1edge.push_back(translate);
	}
	else if (bottom) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), RAD180, rotationAxis);
		tiles_1edge.push_back((translate * rotate));
	}
	else if (left) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), RAD90, rotationAxis);
		tiles_1edge.push_back((translate * rotate));
	}
	else if (right) {
		glm::mat4 rotate = glm::rotate(glm::mat4(1.0f), -RAD90, rotationAxis);
		tiles_1edge.push_back((translate * rotate));
	}
	// 0
	else if (!top && !bottom && !left && !right) {
		tiles_noEdge.push_back(translate);
	}

	delete cardinal;
}