#ifndef BOUNDINGBOX_H
#define BOUNDINGBOX_H

#include <glm\glm.hpp>
namespace RDE
{
	struct bounding_box_struct
	{
		union
		{
			glm::vec3 vertices[8];
			struct
			{
				// front face
				glm::vec3 frontTopLeft;
				glm::vec3 frontTopRight;
				glm::vec3 frontBottomLeft;
				glm::vec3 frontBottomRight;
				// back face 
				glm::vec3 backTopLeft;
				glm::vec3 backTopRight;
				glm::vec3 backBottomLeft;
				glm::vec3 backBottomRight;
				// axes 
				glm::vec3 boxAxisX;
				glm::vec3 boxAxisY;
				glm::vec3 boxAxisZ;
			};
		};
		inline bounding_box_struct() {}
	};
}

#endif // !BOUNDINGBOX_H
