#ifndef HIERARCHYDEMO_H
#define HIERARCHYDEMO_H

#include <memory>
#include <vector>

namespace RDE
{
	class GameObject;
	class Camera;
	class Shader;
}

class HierarchyDemo
{
public:
	HierarchyDemo(float aspectRatio);
	~HierarchyDemo();

	void FixedUpdate(float dt);
	bool Update(float dt);
	void Render(float interpolation);

private:
	std::shared_ptr<const RDE::Shader> simpleShader;
	RDE::GameObject * root;
	RDE::GameObject * spinningRoot;
	RDE::Camera * camera;
	RDE::GameObject * boblampRoot;
	RDE::GameObject * plane;
	float sineCounter;
	float originalY;
	std::vector<RDE::GameObject *> sphereList;

};

#endif // !HIERARCHYDEMO_H
